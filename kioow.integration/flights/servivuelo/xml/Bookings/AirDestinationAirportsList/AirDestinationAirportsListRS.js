module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirDestinationAirportsListResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirDestinationAirportsListResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirDestinationAirport": [
									{
										"AirportLocationCode": [
											"AAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Al Arish"
										]
									},
									{
										"AirportLocationCode": [
											"AAE"
										],
										"CityLocationCode": [
											"AAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Rabah Bitat, Annaba"
										]
									},
									{
										"AirportLocationCode": [
											"AAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Aalborg"
										]
									},
									{
										"AirportLocationCode": [
											"AAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Al Ain"
										]
									},
									{
										"AirportLocationCode": [
											"AAQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Anapa"
										]
									},
									{
										"AirportLocationCode": [
											"AAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Aarhus Tirstrup"
										]
									},
									{
										"AirportLocationCode": [
											"ABC"
										],
										"CityLocationCode": [
											"ABC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Albacete"
										]
									},
									{
										"AirportLocationCode": [
											"ABE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Allentown/Bethlehem/Easton"
										]
									},
									{
										"AirportLocationCode": [
											"ABH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Alpha"
										]
									},
									{
										"AirportLocationCode": [
											"ABI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Abilene"
										]
									},
									{
										"AirportLocationCode": [
											"ABJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CI"
										],
										"Description": [
											"Abidjan F Houphouet Boigny"
										]
									},
									{
										"AirportLocationCode": [
											"ABQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Albuquerque"
										]
									},
									{
										"AirportLocationCode": [
											"ABR"
										],
										"CityLocationCode": [
											"APG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Aberdeen Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"ABU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Atambua"
										]
									},
									{
										"AirportLocationCode": [
											"ABV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Abuja"
										]
									},
									{
										"AirportLocationCode": [
											"ABX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Albury"
										]
									},
									{
										"AirportLocationCode": [
											"ABY"
										],
										"CityLocationCode": [
											"ALB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Albany Dougherty County"
										]
									},
									{
										"AirportLocationCode": [
											"ABZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Aberdeen Dyce"
										]
									},
									{
										"AirportLocationCode": [
											"ACA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Acapulco Alvarez"
										]
									},
									{
										"AirportLocationCode": [
											"ACC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GH"
										],
										"Description": [
											"Accra Kotoka"
										]
									},
									{
										"AirportLocationCode": [
											"ACE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Lanzarote"
										]
									},
									{
										"AirportLocationCode": [
											"ACH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Altenrhein"
										]
									},
									{
										"AirportLocationCode": [
											"ACI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Alderney The Blaye"
										]
									},
									{
										"AirportLocationCode": [
											"ACK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Nantucket Memorial"
										]
									},
									{
										"AirportLocationCode": [
											"ACT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Waco Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"ACV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Arcata/Eureka"
										]
									},
									{
										"AirportLocationCode": [
											"ACY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Atlantic City International"
										]
									},
									{
										"AirportLocationCode": [
											"ADA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Adana"
										]
									},
									{
										"AirportLocationCode": [
											"ADB"
										],
										"CityLocationCode": [
											"IZM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Izmir Adnan Menderes"
										]
									},
									{
										"AirportLocationCode": [
											"ADD"
										],
										"CityLocationCode": [
											"ADD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ET"
										],
										"Description": [
											"Addis Ababa Bole Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"ADI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Arandis"
										]
									},
									{
										"AirportLocationCode": [
											"ADL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Adelaide"
										]
									},
									{
										"AirportLocationCode": [
											"ADQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kodiak"
										]
									},
									{
										"AirportLocationCode": [
											"ADZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"San Andres Island"
										]
									},
									{
										"AirportLocationCode": [
											"AEI"
										],
										"CityLocationCode": [
											"AEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Algeciras"
										]
									},
									{
										"AirportLocationCode": [
											"AEP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Buenos Aires J Newbery"
										]
									},
									{
										"AirportLocationCode": [
											"AER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Adler/Sochi"
										]
									},
									{
										"AirportLocationCode": [
											"AES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Aalesund Vigra"
										]
									},
									{
										"AirportLocationCode": [
											"AEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alexandria"
										]
									},
									{
										"AirportLocationCode": [
											"AFA"
										],
										"CityLocationCode": [
											"AFA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"San Rafael, Mendoza"
										]
									},
									{
										"AirportLocationCode": [
											"AFO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Afton"
										]
									},
									{
										"AirportLocationCode": [
											"AGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Agadir Almassira"
										]
									},
									{
										"AirportLocationCode": [
											"AGB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Munich Augsburg Muehlhausen"
										]
									},
									{
										"AirportLocationCode": [
											"AGE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Wangerooge Flugplatz"
										]
									},
									{
										"AirportLocationCode": [
											"AGF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Agen La Garenne"
										]
									},
									{
										"AirportLocationCode": [
											"AGH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Angelholm/Helsingborg"
										]
									},
									{
										"AirportLocationCode": [
											"AGP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Málaga"
										]
									},
									{
										"AirportLocationCode": [
											"AGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Agra Kheria"
										]
									},
									{
										"AirportLocationCode": [
											"AGS"
										],
										"CityLocationCode": [
											"AUG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Augusta Bush Field"
										]
									},
									{
										"AirportLocationCode": [
											"AGT"
										],
										"CityLocationCode": [
											"AGT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PY"
										],
										"Description": [
											"Ciudad del Este - Alejo García"
										]
									},
									{
										"AirportLocationCode": [
											"AGU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Aguascalientes"
										]
									},
									{
										"AirportLocationCode": [
											"AGX"
										],
										"CityLocationCode": [
											"AGX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Agatti Island"
										]
									},
									{
										"AirportLocationCode": [
											"AHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Ahe"
										]
									},
									{
										"AirportLocationCode": [
											"AHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Athens"
										]
									},
									{
										"AirportLocationCode": [
											"AHO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Alghero Fertilia"
										]
									},
									{
										"AirportLocationCode": [
											"AHU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Charif Al Idrissi"
										]
									},
									{
										"AirportLocationCode": [
											"AIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wainwright"
										]
									},
									{
										"AirportLocationCode": [
											"AIY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Atlantic City Bader Field"
										]
									},
									{
										"AirportLocationCode": [
											"AJA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Ajaccio Campo dell Oro"
										]
									},
									{
										"AirportLocationCode": [
											"AJR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Arvidsjaur"
										]
									},
									{
										"AirportLocationCode": [
											"AJU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Aracaju"
										]
									},
									{
										"AirportLocationCode": [
											"AKL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Auckland"
										]
									},
									{
										"AirportLocationCode": [
											"AKX"
										],
										"CityLocationCode": [
											"AKX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KZ"
										],
										"Description": [
											"Aktyubinsk"
										]
									},
									{
										"AirportLocationCode": [
											"ALA"
										],
										"CityLocationCode": [
											"ALA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KZ"
										],
										"Description": [
											"Almaty"
										]
									},
									{
										"AirportLocationCode": [
											"ALB"
										],
										"CityLocationCode": [
											"ALB"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Albany"
										]
									},
									{
										"AirportLocationCode": [
											"ALC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Alicante"
										]
									},
									{
										"AirportLocationCode": [
											"ALD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Alerta"
										]
									},
									{
										"AirportLocationCode": [
											"ALE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alpine"
										]
									},
									{
										"AirportLocationCode": [
											"ALF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Alta"
										]
									},
									{
										"AirportLocationCode": [
											"ALG"
										],
										"CityLocationCode": [
											"ALG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Algeria - Houari Boumediene"
										]
									},
									{
										"AirportLocationCode": [
											"ALI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alice"
										]
									},
									{
										"AirportLocationCode": [
											"ALL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Albenga"
										]
									},
									{
										"AirportLocationCode": [
											"ALM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alamogordo Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"ALO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Waterloo"
										]
									},
									{
										"AirportLocationCode": [
											"ALP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SY"
										],
										"Description": [
											"Aleppo Nejrab"
										]
									},
									{
										"AirportLocationCode": [
											"ALS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alamosa"
										]
									},
									{
										"AirportLocationCode": [
											"ALT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Alenquer"
										]
									},
									{
										"AirportLocationCode": [
											"ALY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Alexandria El Nozha"
										]
									},
									{
										"AirportLocationCode": [
											"AMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Amarillo International"
										]
									},
									{
										"AirportLocationCode": [
											"AMD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Ahmedabad"
										]
									},
									{
										"AirportLocationCode": [
											"AME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Alto Molocue"
										]
									},
									{
										"AirportLocationCode": [
											"AMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Mataram Selaparang"
										]
									},
									{
										"AirportLocationCode": [
											"AMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JO"
										],
										"Description": [
											"Amman Queen Alia"
										]
									},
									{
										"AirportLocationCode": [
											"AMR"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Almeria, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"AMS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Amsterdam Schipol"
										]
									},
									{
										"AirportLocationCode": [
											"ANC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Anchorage"
										]
									},
									{
										"AirportLocationCode": [
											"AND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Anderson"
										]
									},
									{
										"AirportLocationCode": [
											"ANE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Angers Marce"
										]
									},
									{
										"AirportLocationCode": [
											"ANF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Antofagasta Cerro Moreno"
										]
									},
									{
										"AirportLocationCode": [
											"ANK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Ankara Etimesgut"
										]
									},
									{
										"AirportLocationCode": [
											"ANN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Annette Island"
										]
									},
									{
										"AirportLocationCode": [
											"ANT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"St Anton"
										]
									},
									{
										"AirportLocationCode": [
											"ANU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AG"
										],
										"Description": [
											"Antigua"
										]
									},
									{
										"AirportLocationCode": [
											"ANY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Anthony"
										]
									},
									{
										"AirportLocationCode": [
											"AOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Ancona Falconara"
										]
									},
									{
										"AirportLocationCode": [
											"AOK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Karpathos"
										]
									},
									{
										"AirportLocationCode": [
											"AOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Alor Setar"
										]
									},
									{
										"AirportLocationCode": [
											"AOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Aosta Corrado Gex"
										]
									},
									{
										"AirportLocationCode": [
											"APG"
										],
										"CityLocationCode": [
											"APG"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Aberdeen"
										]
									},
									{
										"AirportLocationCode": [
											"APL"
										],
										"CityLocationCode": [
											"APL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Nampula"
										]
									},
									{
										"AirportLocationCode": [
											"APN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alpena County"
										]
									},
									{
										"AirportLocationCode": [
											"APS"
										],
										"CityLocationCode": [
											"APS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Anápolis"
										]
									},
									{
										"AirportLocationCode": [
											"APW"
										],
										"CityLocationCode": [
											"APW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"WS"
										],
										"Description": [
											"Apia"
										]
									},
									{
										"AirportLocationCode": [
											"AQA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Araraquara"
										]
									},
									{
										"AirportLocationCode": [
											"AQJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JO"
										],
										"Description": [
											"Aqaba"
										]
									},
									{
										"AirportLocationCode": [
											"AQP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Arequipa Rodriguez Ballon"
										]
									},
									{
										"AirportLocationCode": [
											"ARA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New Iberia"
										]
									},
									{
										"AirportLocationCode": [
											"ARD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Alor Island"
										]
									},
									{
										"AirportLocationCode": [
											"ARE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PR"
										],
										"Description": [
											"Arecibo"
										]
									},
									{
										"AirportLocationCode": [
											"ARG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Walnut Ridge"
										]
									},
									{
										"AirportLocationCode": [
											"ARH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Arkhangelsk"
										]
									},
									{
										"AirportLocationCode": [
											"ARI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Arica Chacalluta"
										]
									},
									{
										"AirportLocationCode": [
											"ARK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Arusha"
										]
									},
									{
										"AirportLocationCode": [
											"ARM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Armidale"
										]
									},
									{
										"AirportLocationCode": [
											"ARN"
										],
										"CityLocationCode": [
											"STO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Estocolmo Arlanda"
										]
									},
									{
										"AirportLocationCode": [
											"ARR"
										],
										"CityLocationCode": [
											"BUE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Alto Rio Senguerr"
										]
									},
									{
										"AirportLocationCode": [
											"ARU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Aracatuba"
										]
									},
									{
										"AirportLocationCode": [
											"ARW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Arad"
										]
									},
									{
										"AirportLocationCode": [
											"ASB"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TM"
										],
										"Description": [
											"ASHGABAD"
										]
									},
									{
										"AirportLocationCode": [
											"ASE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Aspen"
										]
									},
									{
										"AirportLocationCode": [
											"ASF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Astrakhan"
										]
									},
									{
										"AirportLocationCode": [
											"ASH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Nashua"
										]
									},
									{
										"AirportLocationCode": [
											"ASP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Alice Springs"
										]
									},
									{
										"AirportLocationCode": [
											"ASR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Kayseri"
										]
									},
									{
										"AirportLocationCode": [
											"AST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Astoria"
										]
									},
									{
										"AirportLocationCode": [
											"ASU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PY"
										],
										"Description": [
											"Asuncion Silvio Pettirossi"
										]
									},
									{
										"AirportLocationCode": [
											"ASW"
										],
										"CityLocationCode": [
											"ASW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Aswan"
										]
									},
									{
										"AirportLocationCode": [
											"ATA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Anta"
										]
									},
									{
										"AirportLocationCode": [
											"ATH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Atenas"
										]
									},
									{
										"AirportLocationCode": [
											"ATL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Atlanta West B Hartsfield"
										]
									},
									{
										"AirportLocationCode": [
											"ATM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Altamira"
										]
									},
									{
										"AirportLocationCode": [
											"ATQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Amritsar Raja Sansi"
										]
									},
									{
										"AirportLocationCode": [
											"ATW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Appleton Outagamie City"
										]
									},
									{
										"AirportLocationCode": [
											"ATY"
										],
										"CityLocationCode": [
											"ATY"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Watertown"
										]
									},
									{
										"AirportLocationCode": [
											"AUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AW"
										],
										"Description": [
											"Aruba Reina Beatrix"
										]
									},
									{
										"AirportLocationCode": [
											"AUG"
										],
										"CityLocationCode": [
											"AUG"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Augusta"
										]
									},
									{
										"AirportLocationCode": [
											"AUH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Abu Dhabi"
										]
									},
									{
										"AirportLocationCode": [
											"AUK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Alakanuk"
										]
									},
									{
										"AirportLocationCode": [
											"AUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Aurillac"
										]
									},
									{
										"AirportLocationCode": [
											"AUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Austin Bergstrom"
										]
									},
									{
										"AirportLocationCode": [
											"AUW"
										],
										"CityLocationCode": [
											"AUW"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wausau"
										]
									},
									{
										"AirportLocationCode": [
											"AUX"
										],
										"CityLocationCode": [
											"AUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Araguaina"
										]
									},
									{
										"AirportLocationCode": [
											"AVI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Ciego de Avila Maximo Gomez"
										]
									},
									{
										"AirportLocationCode": [
											"AVL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Asheville"
										]
									},
									{
										"AirportLocationCode": [
											"AVN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Avignon Caum"
										]
									},
									{
										"AirportLocationCode": [
											"AVP"
										],
										"CityLocationCode": [
											"AVP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Scranton Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"AVS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ávila, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"AVV"
										],
										"CityLocationCode": [
											"MEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"MELBOURNE AVALON AIRPORT"
										]
									},
									{
										"AirportLocationCode": [
											"AXD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Alexandroupolis Demokritos"
										]
									},
									{
										"AirportLocationCode": [
											"AXM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Armenia El Eden"
										]
									},
									{
										"AirportLocationCode": [
											"AXT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Akita"
										]
									},
									{
										"AirportLocationCode": [
											"AXU"
										],
										"CityLocationCode": [
											"AXU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ET"
										],
										"Description": [
											"Axum"
										]
									},
									{
										"AirportLocationCode": [
											"AYQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Ayers Rock Connellan"
										]
									},
									{
										"AirportLocationCode": [
											"AYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Ayr"
										]
									},
									{
										"AirportLocationCode": [
											"AYT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Antalya"
										]
									},
									{
										"AirportLocationCode": [
											"AZD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Yazd"
										]
									},
									{
										"AirportLocationCode": [
											"AZO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kalamazoo Battle Creek"
										]
									},
									{
										"AirportLocationCode": [
											"AZS"
										],
										"CityLocationCode": [
											"AZS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Samaná"
										]
									},
									{
										"AirportLocationCode": [
											"BAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Barranca De Upia"
										]
									},
									{
										"AirportLocationCode": [
											"BAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Barcelonnette"
										]
									},
									{
										"AirportLocationCode": [
											"BAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Baguio Loakan"
										]
									},
									{
										"AirportLocationCode": [
											"BAH"
										],
										"CityLocationCode": [
											"BAH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BH"
										],
										"Description": [
											"Bahrain"
										]
									},
									{
										"AirportLocationCode": [
											"BAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Buenos Aires"
										]
									},
									{
										"AirportLocationCode": [
											"BAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Batman"
										]
									},
									{
										"AirportLocationCode": [
											"BAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Battle Mountain"
										]
									},
									{
										"AirportLocationCode": [
											"BAQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Barranquilla East Cortissoz"
										]
									},
									{
										"AirportLocationCode": [
											"BAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Baker Island"
										]
									},
									{
										"AirportLocationCode": [
											"BAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Barretos"
										]
									},
									{
										"AirportLocationCode": [
											"BAU"
										],
										"CityLocationCode": [
											"BHZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Bauru"
										]
									},
									{
										"AirportLocationCode": [
											"BAX"
										],
										"CityLocationCode": [
											"BAX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Barnaul"
										]
									},
									{
										"AirportLocationCode": [
											"BAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Baia Mare"
										]
									},
									{
										"AirportLocationCode": [
											"BBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Balmaceda"
										]
									},
									{
										"AirportLocationCode": [
											"BBI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bhubaneswar"
										]
									},
									{
										"AirportLocationCode": [
											"BBK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BW"
										],
										"Description": [
											"Kasane"
										]
									},
									{
										"AirportLocationCode": [
											"BBU"
										],
										"CityLocationCode": [
											"BBU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Baneasa Aurel Vlaicu"
										]
									},
									{
										"AirportLocationCode": [
											"BCA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Baracoa"
										]
									},
									{
										"AirportLocationCode": [
											"BCD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Bacolod"
										]
									},
									{
										"AirportLocationCode": [
											"BCM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Bacau"
										]
									},
									{
										"AirportLocationCode": [
											"BCN"
										],
										"CityLocationCode": [
											"BCN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Barcelona"
										]
									},
									{
										"AirportLocationCode": [
											"BDA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BM"
										],
										"Description": [
											"Bermuda"
										]
									},
									{
										"AirportLocationCode": [
											"BDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Bundaberg"
										]
									},
									{
										"AirportLocationCode": [
											"BDH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Bandar Lengeh"
										]
									},
									{
										"AirportLocationCode": [
											"BDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hartford Bradley"
										]
									},
									{
										"AirportLocationCode": [
											"BDQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Vadodara"
										]
									},
									{
										"AirportLocationCode": [
											"BDS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Brindisi Papola Casale"
										]
									},
									{
										"AirportLocationCode": [
											"BEB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Benbecula"
										]
									},
									{
										"AirportLocationCode": [
											"BEE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Beagle Bay"
										]
									},
									{
										"AirportLocationCode": [
											"BEG"
										],
										"CityLocationCode": [
											"BEG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RS"
										],
										"Description": [
											"Nikola Tesla - Belgrade"
										]
									},
									{
										"AirportLocationCode": [
											"BEJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Berau"
										]
									},
									{
										"AirportLocationCode": [
											"BEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Belem Val de Cans"
										]
									},
									{
										"AirportLocationCode": [
											"BEN"
										],
										"CityLocationCode": [
											"BEN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"LY"
										],
										"Description": [
											"Benghazi"
										]
									},
									{
										"AirportLocationCode": [
											"BEO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Newcastle Belmont"
										]
									},
									{
										"AirportLocationCode": [
											"BER"
										],
										"CityLocationCode": [
											"BER"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Berlín"
										]
									},
									{
										"AirportLocationCode": [
											"BES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Brest Guipavas"
										]
									},
									{
										"AirportLocationCode": [
											"BEW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Beira"
										]
									},
									{
										"AirportLocationCode": [
											"BEY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LB"
										],
										"Description": [
											"Beirut"
										]
									},
									{
										"AirportLocationCode": [
											"BFD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bradford"
										]
									},
									{
										"AirportLocationCode": [
											"BFI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Seattle Boeing Field"
										]
									},
									{
										"AirportLocationCode": [
											"BFL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bakersfield Meadows"
										]
									},
									{
										"AirportLocationCode": [
											"BFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Bloemfontein"
										]
									},
									{
										"AirportLocationCode": [
											"BFS"
										],
										"CityLocationCode": [
											"BFS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Belfast"
										]
									},
									{
										"AirportLocationCode": [
											"BGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Bucaramanga Palo Negro"
										]
									},
									{
										"AirportLocationCode": [
											"BGF"
										],
										"CityLocationCode": [
											"BGF"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CF"
										],
										"Description": [
											"Bangui"
										]
									},
									{
										"AirportLocationCode": [
											"BGI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BB"
										],
										"Description": [
											"Barbados Grantley Adams"
										]
									},
									{
										"AirportLocationCode": [
											"BGM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Binghamton"
										]
									},
									{
										"AirportLocationCode": [
											"BGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Bergen Flesland"
										]
									},
									{
										"AirportLocationCode": [
											"BGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bangor"
										]
									},
									{
										"AirportLocationCode": [
											"BGW"
										],
										"CityLocationCode": [
											"BGW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IQ"
										],
										"Description": [
											"Baghdad Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"BGY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Bergamo Orio al Serio"
										]
									},
									{
										"AirportLocationCode": [
											"BHB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bar Harbor"
										]
									},
									{
										"AirportLocationCode": [
											"BHD"
										],
										"CityLocationCode": [
											"BFS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Belfast City"
										]
									},
									{
										"AirportLocationCode": [
											"BHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Blenheim"
										]
									},
									{
										"AirportLocationCode": [
											"BHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Bahia Blanca Comandante"
										]
									},
									{
										"AirportLocationCode": [
											"BHJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bhuj Rudra Mata"
										]
									},
									{
										"AirportLocationCode": [
											"BHM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Birmingham"
										]
									},
									{
										"AirportLocationCode": [
											"BHO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bhopal"
										]
									},
									{
										"AirportLocationCode": [
											"BHU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bhavnagar"
										]
									},
									{
										"AirportLocationCode": [
											"BHX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Birmingham"
										]
									},
									{
										"AirportLocationCode": [
											"BHY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Beihai"
										]
									},
									{
										"AirportLocationCode": [
											"BHZ"
										],
										"CityLocationCode": [
											"BHZ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Belo Horizonte"
										]
									},
									{
										"AirportLocationCode": [
											"BIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Bastia Poretta"
										]
									},
									{
										"AirportLocationCode": [
											"BIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Billings"
										]
									},
									{
										"AirportLocationCode": [
											"BIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Bimini"
										]
									},
									{
										"AirportLocationCode": [
											"BIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Bilbao"
										]
									},
									{
										"AirportLocationCode": [
											"BIQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Biarritz Parme"
										]
									},
									{
										"AirportLocationCode": [
											"BIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NP"
										],
										"Description": [
											"Biratnagar"
										]
									},
									{
										"AirportLocationCode": [
											"BIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bismarck"
										]
									},
									{
										"AirportLocationCode": [
											"BJA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Bejaia, SOUMMAM ABANE RAMDANE"
										]
									},
									{
										"AirportLocationCode": [
											"BJI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bemidji"
										]
									},
									{
										"AirportLocationCode": [
											"BJL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GM"
										],
										"Description": [
											"Banjul Yundum"
										]
									},
									{
										"AirportLocationCode": [
											"BJM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BI"
										],
										"Description": [
											"Bujumbura Intl"
										]
									},
									{
										"AirportLocationCode": [
											"BJS"
										],
										"CityLocationCode": [
											"BJS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Beijing"
										]
									},
									{
										"AirportLocationCode": [
											"BJV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Bodrum Milas"
										]
									},
									{
										"AirportLocationCode": [
											"BJX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Leon Guanajuato"
										]
									},
									{
										"AirportLocationCode": [
											"BJZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Badajoz Talaveral La Real"
										]
									},
									{
										"AirportLocationCode": [
											"BKA"
										],
										"CityLocationCode": [
											"MOW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Moscú - Bykovo"
										]
									},
									{
										"AirportLocationCode": [
											"BKI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kota Kinabalu"
										]
									},
									{
										"AirportLocationCode": [
											"BKK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Bangkok"
										]
									},
									{
										"AirportLocationCode": [
											"BKO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ML"
										],
										"Description": [
											"Bamako"
										]
									},
									{
										"AirportLocationCode": [
											"BKW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Beckley"
										]
									},
									{
										"AirportLocationCode": [
											"BLA"
										],
										"CityLocationCode": [
											"BLA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Barcelona General Ja Anzoategui"
										]
									},
									{
										"AirportLocationCode": [
											"BLE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Borlange/Falun Dala"
										]
									},
									{
										"AirportLocationCode": [
											"BLF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bluefield Mercer County"
										]
									},
									{
										"AirportLocationCode": [
											"BLI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bellingham"
										]
									},
									{
										"AirportLocationCode": [
											"BLK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Blackpool"
										]
									},
									{
										"AirportLocationCode": [
											"BLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Billund"
										]
									},
									{
										"AirportLocationCode": [
											"BLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Blonduos"
										]
									},
									{
										"AirportLocationCode": [
											"BLQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Bolonia Guglielmo Marconi"
										]
									},
									{
										"AirportLocationCode": [
											"BLR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bangalore Hindustan"
										]
									},
									{
										"AirportLocationCode": [
											"BLU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Blue Canyon"
										]
									},
									{
										"AirportLocationCode": [
											"BLW"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"WAIMANALO, BELLOWS FIELD"
										]
									},
									{
										"AirportLocationCode": [
											"BLZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MW"
										],
										"Description": [
											"Blantyre Chileka"
										]
									},
									{
										"AirportLocationCode": [
											"BMA"
										],
										"CityLocationCode": [
											"STO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Estocolmo Bromma"
										]
									},
									{
										"AirportLocationCode": [
											"BME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Broome"
										]
									},
									{
										"AirportLocationCode": [
											"BNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Nashville"
										]
									},
									{
										"AirportLocationCode": [
											"BND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Bandar Abbas"
										]
									},
									{
										"AirportLocationCode": [
											"BNE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Brisbane"
										]
									},
									{
										"AirportLocationCode": [
											"BNJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Bonn"
										]
									},
									{
										"AirportLocationCode": [
											"BNK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Ballina"
										]
									},
									{
										"AirportLocationCode": [
											"BNX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BA"
										],
										"Description": [
											"Banja Luka"
										]
									},
									{
										"AirportLocationCode": [
											"BOB"
										],
										"CityLocationCode": [
											"BOB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Bora Bora Motu Mute"
										]
									},
									{
										"AirportLocationCode": [
											"BOC"
										],
										"CityLocationCode": [
											"BOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PA"
										],
										"Description": [
											"Boca del Toro"
										]
									},
									{
										"AirportLocationCode": [
											"BOD"
										],
										"CityLocationCode": [
											"BOD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Merignac"
										]
									},
									{
										"AirportLocationCode": [
											"BOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Bogota Eldorado"
										]
									},
									{
										"AirportLocationCode": [
											"BOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Bournemouth"
										]
									},
									{
										"AirportLocationCode": [
											"BOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Boise Air Term Gowen Field"
										]
									},
									{
										"AirportLocationCode": [
											"BOJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Bourgas"
										]
									},
									{
										"AirportLocationCode": [
											"BOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Bally Kelly"
										]
									},
									{
										"AirportLocationCode": [
											"BOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Mumbai Chatrapati Shivaji"
										]
									},
									{
										"AirportLocationCode": [
											"BON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AN"
										],
										"Description": [
											"Bonaire Flamingo"
										]
									},
									{
										"AirportLocationCode": [
											"BOO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Bodo"
										]
									},
									{
										"AirportLocationCode": [
											"BOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Belfort"
										]
									},
									{
										"AirportLocationCode": [
											"BOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Boston Logan International"
										]
									},
									{
										"AirportLocationCode": [
											"BOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Bourges Airport"
										]
									},
									{
										"AirportLocationCode": [
											"BPN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Balikpapan Sepingan"
										]
									},
									{
										"AirportLocationCode": [
											"BPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Porto Seguro"
										]
									},
									{
										"AirportLocationCode": [
											"BPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Beaumont Jefferson County"
										]
									},
									{
										"AirportLocationCode": [
											"BQK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Brunswick Glynco Jetport"
										]
									},
									{
										"AirportLocationCode": [
											"BQN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PR"
										],
										"Description": [
											"Aguadilla Borinquen"
										]
									},
									{
										"AirportLocationCode": [
											"BQS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Blagoveschensk"
										]
									},
									{
										"AirportLocationCode": [
											"BQZ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Badajoz, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"BRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Barreiras"
										]
									},
									{
										"AirportLocationCode": [
											"BRC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"San Carlos de Bariloche"
										]
									},
									{
										"AirportLocationCode": [
											"BRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Brainerd Crow Wing"
										]
									},
									{
										"AirportLocationCode": [
											"BRE"
										],
										"CityLocationCode": [
											"123"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Bremen"
										]
									},
									{
										"AirportLocationCode": [
											"BRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Bari Palese"
										]
									},
									{
										"AirportLocationCode": [
											"BRM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Barquisimeto"
										]
									},
									{
										"AirportLocationCode": [
											"BRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Berna Belp"
										]
									},
									{
										"AirportLocationCode": [
											"BRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Brownsville South Parade"
										]
									},
									{
										"AirportLocationCode": [
											"BRQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CZ"
										],
										"Description": [
											"Brno Turany"
										]
									},
									{
										"AirportLocationCode": [
											"BRR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Barra North Bay"
										]
									},
									{
										"AirportLocationCode": [
											"BRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Bristol"
										]
									},
									{
										"AirportLocationCode": [
											"BRU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BE"
										],
										"Description": [
											"Brussels National"
										]
									},
									{
										"AirportLocationCode": [
											"BRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bardstown"
										]
									},
									{
										"AirportLocationCode": [
											"BSB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Brasilia"
										]
									},
									{
										"AirportLocationCode": [
											"BSG"
										],
										"CityLocationCode": [
											"BSG"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"GQ"
										],
										"Description": [
											"Bata"
										]
									},
									{
										"AirportLocationCode": [
											"BSH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Brighton"
										]
									},
									{
										"AirportLocationCode": [
											"BSL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Euroairport Basel"
										]
									},
									{
										"AirportLocationCode": [
											"BSR"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IQ"
										],
										"Description": [
											"BASRAH"
										]
									},
									{
										"AirportLocationCode": [
											"BTE"
										],
										"CityLocationCode": [
											"BTE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SL"
										],
										"Description": [
											"Bonthe"
										]
									},
									{
										"AirportLocationCode": [
											"BTK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Bratsk"
										]
									},
									{
										"AirportLocationCode": [
											"BTM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Butte"
										]
									},
									{
										"AirportLocationCode": [
											"BTR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Baton Rouge Ryan"
										]
									},
									{
										"AirportLocationCode": [
											"BTS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SK"
										],
										"Description": [
											"Bratislava Ivanka"
										]
									},
									{
										"AirportLocationCode": [
											"BTU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Bintulu"
										]
									},
									{
										"AirportLocationCode": [
											"BTV"
										],
										"CityLocationCode": [
											"BTV"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Burlington"
										]
									},
									{
										"AirportLocationCode": [
											"BUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Burwell"
										]
									},
									{
										"AirportLocationCode": [
											"BUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Burketown"
										]
									},
									{
										"AirportLocationCode": [
											"BUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HU"
										],
										"Description": [
											"Budapest Ferihegy"
										]
									},
									{
										"AirportLocationCode": [
											"BUE"
										],
										"CityLocationCode": [
											"BUE"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Buenos Aires"
										]
									},
									{
										"AirportLocationCode": [
											"BUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Buffalo Niagara"
										]
									},
									{
										"AirportLocationCode": [
											"BUH"
										],
										"CityLocationCode": [
											"BUH"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Bucarest"
										]
									},
									{
										"AirportLocationCode": [
											"BUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Bulolo"
										]
									},
									{
										"AirportLocationCode": [
											"BUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Buenaventura"
										]
									},
									{
										"AirportLocationCode": [
											"BUP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bhatinda"
										]
									},
									{
										"AirportLocationCode": [
											"BUQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZW"
										],
										"Description": [
											"Bulawayo"
										]
									},
									{
										"AirportLocationCode": [
											"BUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Burbank"
										]
									},
									{
										"AirportLocationCode": [
											"BUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GE"
										],
										"Description": [
											"Batumi"
										]
									},
									{
										"AirportLocationCode": [
											"BUT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Burtonwood"
										]
									},
									{
										"AirportLocationCode": [
											"BUY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Bunbury"
										]
									},
									{
										"AirportLocationCode": [
											"BVA"
										],
										"CityLocationCode": [
											"PAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Beauvais Tille"
										]
									},
									{
										"AirportLocationCode": [
											"BVB"
										],
										"CityLocationCode": [
											"BVB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Boa Vista"
										]
									},
									{
										"AirportLocationCode": [
											"BVC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Boa Vista"
										]
									},
									{
										"AirportLocationCode": [
											"BVH"
										],
										"CityLocationCode": [
											"BVH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Vilhena"
										]
									},
									{
										"AirportLocationCode": [
											"BWI"
										],
										"CityLocationCode": [
											"WAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Washington Baltimore"
										]
									},
									{
										"AirportLocationCode": [
											"BWN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BN"
										],
										"Description": [
											"Bandar Seri Begawan Brunei"
										]
									},
									{
										"AirportLocationCode": [
											"BWT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Burnie Wyn"
										]
									},
									{
										"AirportLocationCode": [
											"BXL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Blue Lagoon"
										]
									},
									{
										"AirportLocationCode": [
											"BXN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Imsik Airport"
										]
									},
									{
										"AirportLocationCode": [
											"BXU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Butuan"
										]
									},
									{
										"AirportLocationCode": [
											"BZE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BZ"
										],
										"Description": [
											"Belize City P.S.W. Goldston"
										]
									},
									{
										"AirportLocationCode": [
											"BZG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Bydgoszcz"
										]
									},
									{
										"AirportLocationCode": [
											"BZH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZW"
										],
										"Description": [
											"Bumi Hills"
										]
									},
									{
										"AirportLocationCode": [
											"BZN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bozeman Gallatin Field"
										]
									},
									{
										"AirportLocationCode": [
											"BZO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Bolzano"
										]
									},
									{
										"AirportLocationCode": [
											"BZR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Beziers Vias"
										]
									},
									{
										"AirportLocationCode": [
											"BZV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CG"
										],
										"Description": [
											"Brazzaville"
										]
									},
									{
										"AirportLocationCode": [
											"CAC"
										],
										"CityLocationCode": [
											"CAC"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Cascavel"
										]
									},
									{
										"AirportLocationCode": [
											"CAE"
										],
										"CityLocationCode": [
											"COU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Columbia Metropolitan"
										]
									},
									{
										"AirportLocationCode": [
											"CAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Cagliari Elmas"
										]
									},
									{
										"AirportLocationCode": [
											"CAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Cairo"
										]
									},
									{
										"AirportLocationCode": [
											"CAJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Canaima"
										]
									},
									{
										"AirportLocationCode": [
											"CAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Canton Akron"
										]
									},
									{
										"AirportLocationCode": [
											"CAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Campbeltown Machrihanish"
										]
									},
									{
										"AirportLocationCode": [
											"CAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Camiri"
										]
									},
									{
										"AirportLocationCode": [
											"CAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Guangzhou Baiyun"
										]
									},
									{
										"AirportLocationCode": [
											"CAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Clayton"
										]
									},
									{
										"AirportLocationCode": [
											"CAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HT"
										],
										"Description": [
											"Cap Haitien"
										]
									},
									{
										"AirportLocationCode": [
											"CAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Caribou"
										]
									},
									{
										"AirportLocationCode": [
											"CAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Casablanca Anfa"
										]
									},
									{
										"AirportLocationCode": [
											"CAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Cat Island"
										]
									},
									{
										"AirportLocationCode": [
											"CAX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Carlisle"
										]
									},
									{
										"AirportLocationCode": [
											"CAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GF"
										],
										"Description": [
											"Cayenne Rochambeau"
										]
									},
									{
										"AirportLocationCode": [
											"CBB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Cochabamba J Wilsterman"
										]
									},
									{
										"AirportLocationCode": [
											"CBG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Cambridge Airport"
										]
									},
									{
										"AirportLocationCode": [
											"CBR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Canberra"
										]
									},
									{
										"AirportLocationCode": [
											"CCC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Cayo Coco"
										]
									},
									{
										"AirportLocationCode": [
											"CCF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Carcassonne Salvaza"
										]
									},
									{
										"AirportLocationCode": [
											"CCI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Concordia"
										]
									},
									{
										"AirportLocationCode": [
											"CCJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Kozhikode"
										]
									},
									{
										"AirportLocationCode": [
											"CCP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Concepcion Carriel Sur"
										]
									},
									{
										"AirportLocationCode": [
											"CCS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Caracas Simon Bolivar"
										]
									},
									{
										"AirportLocationCode": [
											"CCU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Kolkata Netaji Subhas"
										]
									},
									{
										"AirportLocationCode": [
											"CDC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cedar City"
										]
									},
									{
										"AirportLocationCode": [
											"CDG"
										],
										"CityLocationCode": [
											"PAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Paris Charles de Gaulle"
										]
									},
									{
										"AirportLocationCode": [
											"CDH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Camden"
										]
									},
									{
										"AirportLocationCode": [
											"CDV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cordova Mudhole Smith"
										]
									},
									{
										"AirportLocationCode": [
											"CDZ"
										],
										"CityLocationCode": [
											"CDZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Cádiz RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"CEB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Cebu Mactan"
										]
									},
									{
										"AirportLocationCode": [
											"CEC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Crescent City Mc Namara"
										]
									},
									{
										"AirportLocationCode": [
											"CED"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Ceduna"
										]
									},
									{
										"AirportLocationCode": [
											"CEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Chiang Rai"
										]
									},
									{
										"AirportLocationCode": [
											"CEJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Cuenca, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"CEK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Chelyabinsk"
										]
									},
									{
										"AirportLocationCode": [
											"CEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Ciudad Obregon"
										]
									},
									{
										"AirportLocationCode": [
											"CEQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Cannes Mandelieu"
										]
									},
									{
										"AirportLocationCode": [
											"CER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Maupertus Airport"
										]
									},
									{
										"AirportLocationCode": [
											"CFD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Bryan"
										]
									},
									{
										"AirportLocationCode": [
											"CFE"
										],
										"CityLocationCode": [
											"CFE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Auvergne Clermont Ferrand"
										]
									},
									{
										"AirportLocationCode": [
											"CFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Donegal"
										]
									},
									{
										"AirportLocationCode": [
											"CFR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Caen Carpiquet"
										]
									},
									{
										"AirportLocationCode": [
											"CFS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Coffs Harbour"
										]
									},
									{
										"AirportLocationCode": [
											"CFU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kerkyra I Kapodistrais (Korfu)"
										]
									},
									{
										"AirportLocationCode": [
											"CGB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Cuiaba"
										]
									},
									{
										"AirportLocationCode": [
											"CGH"
										],
										"CityLocationCode": [
											"SAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Paulo Congonhas"
										]
									},
									{
										"AirportLocationCode": [
											"CGI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cape Girardeau"
										]
									},
									{
										"AirportLocationCode": [
											"CGK"
										],
										"CityLocationCode": [
											"JKT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Soekarno Hatta"
										]
									},
									{
										"AirportLocationCode": [
											"CGN"
										],
										"CityLocationCode": [
											"CGN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Colonia"
										]
									},
									{
										"AirportLocationCode": [
											"cgo"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"ZHENGZHOU"
										]
									},
									{
										"AirportLocationCode": [
											"CGQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Changchun"
										]
									},
									{
										"AirportLocationCode": [
											"CGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Campo Grande"
										]
									},
									{
										"AirportLocationCode": [
											"CGY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Cagayan de Oro Lumbia"
										]
									},
									{
										"AirportLocationCode": [
											"CHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chattanooga Lovell Field"
										]
									},
									{
										"AirportLocationCode": [
											"CHC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Christchurch"
										]
									},
									{
										"AirportLocationCode": [
											"CHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Caherciveen"
										]
									},
									{
										"AirportLocationCode": [
											"CHI"
										],
										"CityLocationCode": [
											"CHI"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chicago"
										]
									},
									{
										"AirportLocationCode": [
											"CHO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Charlottesville Albermarl"
										]
									},
									{
										"AirportLocationCode": [
											"CHQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Chania Souda"
										]
									},
									{
										"AirportLocationCode": [
											"CHR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Chateauroux"
										]
									},
									{
										"AirportLocationCode": [
											"CHS"
										],
										"CityLocationCode": [
											"CHS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Charleston"
										]
									},
									{
										"AirportLocationCode": [
											"CIA"
										],
										"CityLocationCode": [
											"ROM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Roma Ciampino"
										]
									},
									{
										"AirportLocationCode": [
											"CIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chico"
										]
									},
									{
										"AirportLocationCode": [
											"CID"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cedar Rapids"
										]
									},
									{
										"AirportLocationCode": [
											"CIH"
										],
										"CityLocationCode": [
											"CIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Changzhi"
										]
									},
									{
										"AirportLocationCode": [
											"CIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Carroll"
										]
									},
									{
										"AirportLocationCode": [
											"CIW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VC"
										],
										"Description": [
											"Canouan Island"
										]
									},
									{
										"AirportLocationCode": [
											"CIX"
										],
										"CityLocationCode": [
											"CIX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Chiclayo"
										]
									},
									{
										"AirportLocationCode": [
											"CJA"
										],
										"CityLocationCode": [
											"CJA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Cajamarca"
										]
									},
									{
										"AirportLocationCode": [
											"CJB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Coimbatore Peelamedu"
										]
									},
									{
										"AirportLocationCode": [
											"CJC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Calama"
										]
									},
									{
										"AirportLocationCode": [
											"CJS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Ciudad Juarez"
										]
									},
									{
										"AirportLocationCode": [
											"CJU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Jeju"
										]
									},
									{
										"AirportLocationCode": [
											"CKB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Clarksburg Benedum"
										]
									},
									{
										"AirportLocationCode": [
											"CKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Chongqing"
										]
									},
									{
										"AirportLocationCode": [
											"CKS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Carajas"
										]
									},
									{
										"AirportLocationCode": [
											"CKY"
										],
										"CityLocationCode": [
											"CKY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GF"
										],
										"Description": [
											"Conakry"
										]
									},
									{
										"AirportLocationCode": [
											"CKZ"
										],
										"CityLocationCode": [
											"CKZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Canakkale"
										]
									},
									{
										"AirportLocationCode": [
											"CLD"
										],
										"CityLocationCode": [
											"SAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"MCCLELLAN PALOMAR AIRPORT"
										]
									},
									{
										"AirportLocationCode": [
											"CLE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cleveland Hopkins"
										]
									},
									{
										"AirportLocationCode": [
											"CLJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Cluj Napoca"
										]
									},
									{
										"AirportLocationCode": [
											"CLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"College Station Easter"
										]
									},
									{
										"AirportLocationCode": [
											"CLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Port Angeles Fairchild"
										]
									},
									{
										"AirportLocationCode": [
											"CLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Carolina"
										]
									},
									{
										"AirportLocationCode": [
											"CLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Cali Alfonso B Aragon"
										]
									},
									{
										"AirportLocationCode": [
											"CLQ"
										],
										"CityLocationCode": [
											"CLQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Colima"
										]
									},
									{
										"AirportLocationCode": [
											"CLT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Charlotte Douglas"
										]
									},
									{
										"AirportLocationCode": [
											"CLY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Calvi Sainte Catherine"
										]
									},
									{
										"AirportLocationCode": [
											"CMB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LK"
										],
										"Description": [
											"Colombo Bandaranaike"
										]
									},
									{
										"AirportLocationCode": [
											"CME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Ciudad Del Carmen"
										]
									},
									{
										"AirportLocationCode": [
											"CMF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Chambery"
										]
									},
									{
										"AirportLocationCode": [
											"CMH"
										],
										"CityLocationCode": [
											"CUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"OH"
										],
										"Description": [
											"Columbus Port Columbus"
										]
									},
									{
										"AirportLocationCode": [
											"CMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Champaign Willard Unive"
										]
									},
									{
										"AirportLocationCode": [
											"CMN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Casablanca Mohammed V"
										]
									},
									{
										"AirportLocationCode": [
											"CMW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Camaguey Ign Agramonte"
										]
									},
									{
										"AirportLocationCode": [
											"CMX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hancock Houghton County"
										]
									},
									{
										"AirportLocationCode": [
											"CND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Constanta Kogalniceanu"
										]
									},
									{
										"AirportLocationCode": [
											"CNF"
										],
										"CityLocationCode": [
											"BHZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Belo Horizonte Tancredo"
										]
									},
									{
										"AirportLocationCode": [
											"CNM"
										],
										"CityLocationCode": [
											"CNM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Carlsbad"
										]
									},
									{
										"AirportLocationCode": [
											"CNQ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Corrientes"
										]
									},
									{
										"AirportLocationCode": [
											"CNS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Cairns"
										]
									},
									{
										"AirportLocationCode": [
											"CNX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Chiang Mai"
										]
									},
									{
										"AirportLocationCode": [
											"COC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Concordia"
										]
									},
									{
										"AirportLocationCode": [
											"COD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cody Yellowstone"
										]
									},
									{
										"AirportLocationCode": [
											"COI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cocoa Merritt Island"
										]
									},
									{
										"AirportLocationCode": [
											"COK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Kochi"
										]
									},
									{
										"AirportLocationCode": [
											"COL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Coll Island"
										]
									},
									{
										"AirportLocationCode": [
											"COM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Coleman"
										]
									},
									{
										"AirportLocationCode": [
											"CON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Concord"
										]
									},
									{
										"AirportLocationCode": [
											"COO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BJ"
										],
										"Description": [
											"Cotonou"
										]
									},
									{
										"AirportLocationCode": [
											"COP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cooperstown"
										]
									},
									{
										"AirportLocationCode": [
											"COR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Cordoba Paja Blancas"
										]
									},
									{
										"AirportLocationCode": [
											"COS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Colorado Springs"
										]
									},
									{
										"AirportLocationCode": [
											"COT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cotulla"
										]
									},
									{
										"AirportLocationCode": [
											"COU"
										],
										"CityLocationCode": [
											"COU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"MO"
										],
										"Description": [
											"Columbia"
										]
									},
									{
										"AirportLocationCode": [
											"CPC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"San Martin de Los Andes"
										]
									},
									{
										"AirportLocationCode": [
											"CPE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Campeche"
										]
									},
									{
										"AirportLocationCode": [
											"CPH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Copenhague"
										]
									},
									{
										"AirportLocationCode": [
											"CPJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Castellón,RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"CPO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Copiapo"
										]
									},
									{
										"AirportLocationCode": [
											"CPQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Campinas"
										]
									},
									{
										"AirportLocationCode": [
											"CPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Casper"
										]
									},
									{
										"AirportLocationCode": [
											"CPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Cape Town"
										]
									},
									{
										"AirportLocationCode": [
											"CPV"
										],
										"CityLocationCode": [
											"CPV"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"CAMPINA GRANDE"
										]
									},
									{
										"AirportLocationCode": [
											"CQF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Calais"
										]
									},
									{
										"AirportLocationCode": [
											"CQM"
										],
										"CityLocationCode": [
											"CQM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ciudad Real"
										]
									},
									{
										"AirportLocationCode": [
											"CRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Craiova"
										]
									},
									{
										"AirportLocationCode": [
											"CRD"
										],
										"CityLocationCode": [
											"CRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Comodoro Rivadavi, Chubut"
										]
									},
									{
										"AirportLocationCode": [
											"CRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Crooked Island"
										]
									},
									{
										"AirportLocationCode": [
											"CRK"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Angeles Mabalacat"
										]
									},
									{
										"AirportLocationCode": [
											"CRL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BE"
										],
										"Description": [
											"Charleroi Brussels"
										]
									},
									{
										"AirportLocationCode": [
											"CRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Corcoran"
										]
									},
									{
										"AirportLocationCode": [
											"CRP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Corpus Christi"
										]
									},
									{
										"AirportLocationCode": [
											"CRU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GD"
										],
										"Description": [
											"Carriacou Island"
										]
									},
									{
										"AirportLocationCode": [
											"CRV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Crotone"
										]
									},
									{
										"AirportLocationCode": [
											"CRW"
										],
										"CityLocationCode": [
											"CHS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Charleston Yeager"
										]
									},
									{
										"AirportLocationCode": [
											"CSL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Luis Obispo O Sullivan AAF"
										]
									},
									{
										"AirportLocationCode": [
											"CSX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Changsha"
										]
									},
									{
										"AirportLocationCode": [
											"CTA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Catania Fontanarossa"
										]
									},
									{
										"AirportLocationCode": [
											"CTC"
										],
										"CityLocationCode": [
											"CTC"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Catamarca"
										]
									},
									{
										"AirportLocationCode": [
											"CTG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Cartagena Rafael Nunez"
										]
									},
									{
										"AirportLocationCode": [
											"CTM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Chetumal"
										]
									},
									{
										"AirportLocationCode": [
											"CTS"
										],
										"CityLocationCode": [
											"SPK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Sapporo Chitose"
										]
									},
									{
										"AirportLocationCode": [
											"CTU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Chengdu"
										]
									},
									{
										"AirportLocationCode": [
											"CUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Cucuta Camilo Dazo"
										]
									},
									{
										"AirportLocationCode": [
											"CUE"
										],
										"CityLocationCode": [
											"CUE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Cuenca"
										]
									},
									{
										"AirportLocationCode": [
											"CUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Cuneo Levaldigi"
										]
									},
									{
										"AirportLocationCode": [
											"CUI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Currillo"
										]
									},
									{
										"AirportLocationCode": [
											"CUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Culiacan Fedl de Bachiguala"
										]
									},
									{
										"AirportLocationCode": [
											"CUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Cancun"
										]
									},
									{
										"AirportLocationCode": [
											"CUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AN"
										],
										"Description": [
											"Curacao Hato"
										]
									},
									{
										"AirportLocationCode": [
											"CUS"
										],
										"CityLocationCode": [
											"CUS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"NM"
										],
										"Description": [
											"Columbus"
										]
									},
									{
										"AirportLocationCode": [
											"CUU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Chihuahua General Fierro Villa"
										]
									},
									{
										"AirportLocationCode": [
											"CUZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Cuzco Velazco Astete"
										]
									},
									{
										"AirportLocationCode": [
											"CVG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cincinnati Northern Kentucky"
										]
									},
									{
										"AirportLocationCode": [
											"CVJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Cuernavaca"
										]
									},
									{
										"AirportLocationCode": [
											"CVM"
										],
										"CityLocationCode": [
											"CVM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"CIUDAD VICTORIA PEDRO J. MENDEZ INTL"
										]
									},
									{
										"AirportLocationCode": [
											"CVQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Carnarvon"
										]
									},
									{
										"AirportLocationCode": [
											"CVT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Coventry"
										]
									},
									{
										"AirportLocationCode": [
											"CWA"
										],
										"CityLocationCode": [
											"AUW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wausau Central Wisconsin"
										]
									},
									{
										"AirportLocationCode": [
											"CWB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Curitiba Afonso Pena"
										]
									},
									{
										"AirportLocationCode": [
											"CWC"
										],
										"CityLocationCode": [
											"CWC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Chernivitsi Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"CWL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Cardiff"
										]
									},
									{
										"AirportLocationCode": [
											"CYO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Cayo Largo"
										]
									},
									{
										"AirportLocationCode": [
											"CYS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Cheyenne"
										]
									},
									{
										"AirportLocationCode": [
											"CZL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Constantine"
										]
									},
									{
										"AirportLocationCode": [
											"CZM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Cozumel"
										]
									},
									{
										"AirportLocationCode": [
											"CZU"
										],
										"CityLocationCode": [
											"CZU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Corozal"
										]
									},
									{
										"AirportLocationCode": [
											"DAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Daytona Beach Regional"
										]
									},
									{
										"AirportLocationCode": [
											"DAC"
										],
										"CityLocationCode": [
											"DAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BD"
										],
										"Description": [
											"Hazrat Shahjalal Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"DAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Da Nang"
										]
									},
									{
										"AirportLocationCode": [
											"DAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Daparizo"
										]
									},
									{
										"AirportLocationCode": [
											"DAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Dakhla"
										]
									},
									{
										"AirportLocationCode": [
											"DAL"
										],
										"CityLocationCode": [
											"DFW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dallas/Fort Worth Love Field"
										]
									},
									{
										"AirportLocationCode": [
											"DAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SY"
										],
										"Description": [
											"Damascus"
										]
									},
									{
										"AirportLocationCode": [
											"DAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Danville"
										]
									},
									{
										"AirportLocationCode": [
											"DAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Dar Es Salaam"
										]
									},
									{
										"AirportLocationCode": [
											"DAT"
										],
										"CityLocationCode": [
											"DAT"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Datong"
										]
									},
									{
										"AirportLocationCode": [
											"DAV"
										],
										"CityLocationCode": [
											"DAV"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PA"
										],
										"Description": [
											"David"
										]
									},
									{
										"AirportLocationCode": [
											"DAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dayton James Cox Dayton"
										]
									},
									{
										"AirportLocationCode": [
											"DBN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dublin"
										]
									},
									{
										"AirportLocationCode": [
											"DBO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Dubbo"
										]
									},
									{
										"AirportLocationCode": [
											"DBV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Dubrovnik"
										]
									},
									{
										"AirportLocationCode": [
											"DCA"
										],
										"CityLocationCode": [
											"WAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Washington Ronald Reagan National"
										]
									},
									{
										"AirportLocationCode": [
											"DCF"
										],
										"CityLocationCode": [
											"DOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DM"
										],
										"Description": [
											"Dominica Cane Field"
										]
									},
									{
										"AirportLocationCode": [
											"DCM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Castres Mazamet"
										]
									},
									{
										"AirportLocationCode": [
											"DEB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HU"
										],
										"Description": [
											"Debrecen"
										]
									},
									{
										"AirportLocationCode": [
											"DEC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Decatur"
										]
									},
									{
										"AirportLocationCode": [
											"DED"
										],
										"CityLocationCode": [
											"ded"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"DEHRA DUN"
										]
									},
									{
										"AirportLocationCode": [
											"DEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Denis Island"
										]
									},
									{
										"AirportLocationCode": [
											"DEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Delhi Indira Gandhi"
										]
									},
									{
										"AirportLocationCode": [
											"DEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Denver"
										]
									},
									{
										"AirportLocationCode": [
											"DEP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Deparizo"
										]
									},
									{
										"AirportLocationCode": [
											"DER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Derim"
										]
									},
									{
										"AirportLocationCode": [
											"DES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Desroches"
										]
									},
									{
										"AirportLocationCode": [
											"DFW"
										],
										"CityLocationCode": [
											"DFW"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dallas/Fort Worth"
										]
									},
									{
										"AirportLocationCode": [
											"DGE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Mudgee"
										]
									},
									{
										"AirportLocationCode": [
											"DGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Durango Guadalupe Victoria"
										]
									},
									{
										"AirportLocationCode": [
											"DHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dothan"
										]
									},
									{
										"AirportLocationCode": [
											"DIE"
										],
										"CityLocationCode": [
											"DIE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Antsiranana"
										]
									},
									{
										"AirportLocationCode": [
											"DIG"
										],
										"CityLocationCode": [
											"DIG"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Deqen"
										]
									},
									{
										"AirportLocationCode": [
											"DIJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Dijon"
										]
									},
									{
										"AirportLocationCode": [
											"DIK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dickinson"
										]
									},
									{
										"AirportLocationCode": [
											"DIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Dili Comoro"
										]
									},
									{
										"AirportLocationCode": [
											"DIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CI"
										],
										"Description": [
											"Dimbokro"
										]
									},
									{
										"AirportLocationCode": [
											"DIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Dien Bien Phu Gialam"
										]
									},
									{
										"AirportLocationCode": [
											"DIU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Diu"
										]
									},
									{
										"AirportLocationCode": [
											"DIY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Diyarbakir"
										]
									},
									{
										"AirportLocationCode": [
											"DJE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Djerba Melita"
										]
									},
									{
										"AirportLocationCode": [
											"DKR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SN"
										],
										"Description": [
											"Dakar Yoff"
										]
									},
									{
										"AirportLocationCode": [
											"DLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CM"
										],
										"Description": [
											"Douala"
										]
									},
									{
										"AirportLocationCode": [
											"DLC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Dalian"
										]
									},
									{
										"AirportLocationCode": [
											"DLH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Duluth"
										]
									},
									{
										"AirportLocationCode": [
											"DLI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Dalat Lienkhang"
										]
									},
									{
										"AirportLocationCode": [
											"DLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Dalaman"
										]
									},
									{
										"AirportLocationCode": [
											"DLU"
										],
										"CityLocationCode": [
											"DLU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Dali"
										]
									},
									{
										"AirportLocationCode": [
											"DME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Moscow Domodedovo"
										]
									},
									{
										"AirportLocationCode": [
											"DMK"
										],
										"CityLocationCode": [
											"BKK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"DON MUANG"
										]
									},
									{
										"AirportLocationCode": [
											"DMM"
										],
										"CityLocationCode": [
											"DMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SA"
										],
										"Description": [
											"King Fahd Internacional, Damman"
										]
									},
									{
										"AirportLocationCode": [
											"DMU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Dimapur"
										]
									},
									{
										"AirportLocationCode": [
											"DND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Dundee"
										]
									},
									{
										"AirportLocationCode": [
											"DNK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Dnepropetrovsk"
										]
									},
									{
										"AirportLocationCode": [
											"DNR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Dinard Pleurtuit"
										]
									},
									{
										"AirportLocationCode": [
											"DNZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Denizli Cardak"
										]
									},
									{
										"AirportLocationCode": [
											"DOH"
										],
										"CityLocationCode": [
											"DOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"QA"
										],
										"Description": [
											"Doha Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"DOK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Donetsk"
										]
									},
									{
										"AirportLocationCode": [
											"DOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Saint Gatien Airport"
										]
									},
									{
										"AirportLocationCode": [
											"DOM"
										],
										"CityLocationCode": [
											"DOM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"DM"
										],
										"Description": [
											"Dominica"
										]
									},
									{
										"AirportLocationCode": [
											"DON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GT"
										],
										"Description": [
											"Dos Lagunas"
										]
									},
									{
										"AirportLocationCode": [
											"DOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Dourados"
										]
									},
									{
										"AirportLocationCode": [
											"DPO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Devonport"
										]
									},
									{
										"AirportLocationCode": [
											"DPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Denpasar Bali Ngurah Rai"
										]
									},
									{
										"AirportLocationCode": [
											"DRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Drummond Island"
										]
									},
									{
										"AirportLocationCode": [
											"DRK"
										],
										"CityLocationCode": [
											"DRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Drake Bay"
										]
									},
									{
										"AirportLocationCode": [
											"DRM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Drama"
										]
									},
									{
										"AirportLocationCode": [
											"DRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Durango La Plata"
										]
									},
									{
										"AirportLocationCode": [
											"DRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Dresden"
										]
									},
									{
										"AirportLocationCode": [
											"DRW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Darwin"
										]
									},
									{
										"AirportLocationCode": [
											"DSM"
										],
										"CityLocationCode": [
											"DSM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Des Moines, Iowa"
										]
									},
									{
										"AirportLocationCode": [
											"DTM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Dortmund"
										]
									},
									{
										"AirportLocationCode": [
											"DTT"
										],
										"CityLocationCode": [
											"DTT"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Detroit"
										]
									},
									{
										"AirportLocationCode": [
											"DTW"
										],
										"CityLocationCode": [
											"DTT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Detroit Wayne County"
										]
									},
									{
										"AirportLocationCode": [
											"DUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Dublin"
										]
									},
									{
										"AirportLocationCode": [
											"DUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Dunedin"
										]
									},
									{
										"AirportLocationCode": [
											"DUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Duck"
										]
									},
									{
										"AirportLocationCode": [
											"DUG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Douglas Bisbee"
										]
									},
									{
										"AirportLocationCode": [
											"DUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Durban"
										]
									},
									{
										"AirportLocationCode": [
											"DUS"
										],
										"CityLocationCode": [
											"DUS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Dusseldorf"
										]
									},
									{
										"AirportLocationCode": [
											"DUT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Dutch Harbor Emergency"
										]
									},
									{
										"AirportLocationCode": [
											"DVL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Devils Lake"
										]
									},
									{
										"AirportLocationCode": [
											"DVO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Davao"
										]
									},
									{
										"AirportLocationCode": [
											"DWC"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Dubai World Central - Al Maktoum"
										]
									},
									{
										"AirportLocationCode": [
											"DXB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Dubai"
										]
									},
									{
										"AirportLocationCode": [
											"DYG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Dayong"
										]
									},
									{
										"AirportLocationCode": [
											"DZA"
										],
										"CityLocationCode": [
											"DZA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"YT"
										],
										"Description": [
											"Dzaoudzi"
										]
									},
									{
										"AirportLocationCode": [
											"EAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Euroairport"
										]
									},
									{
										"AirportLocationCode": [
											"EAS"
										],
										"CityLocationCode": [
											"EAS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"San Sebastián"
										]
									},
									{
										"AirportLocationCode": [
											"EAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wenatchee Pangborn Field"
										]
									},
									{
										"AirportLocationCode": [
											"EAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Eau Claire"
										]
									},
									{
										"AirportLocationCode": [
											"EBB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UG"
										],
										"Description": [
											"Entebbe/Kampala"
										]
									},
									{
										"AirportLocationCode": [
											"EBJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Esbjerg"
										]
									},
									{
										"AirportLocationCode": [
											"EBL"
										],
										"CityLocationCode": [
											"EBL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IQ"
										],
										"Description": [
											"Erbil"
										]
									},
									{
										"AirportLocationCode": [
											"EBU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"St Etienne Boutheon"
										]
									},
									{
										"AirportLocationCode": [
											"ECN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CY"
										],
										"Description": [
											"Ercan"
										]
									},
									{
										"AirportLocationCode": [
											"EDI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Edinburgh Airport"
										]
									},
									{
										"AirportLocationCode": [
											"EDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Eldoret"
										]
									},
									{
										"AirportLocationCode": [
											"EDM"
										],
										"CityLocationCode": [
											"EDM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"La Roche"
										]
									},
									{
										"AirportLocationCode": [
											"EEM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Albacete, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"EEP"
										],
										"CityLocationCode": [
											"PNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Pamplona RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"EEU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"León, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"EFD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Houston Ellington Field"
										]
									},
									{
										"AirportLocationCode": [
											"EFL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kefallinia Argostolion"
										]
									},
									{
										"AirportLocationCode": [
											"EGC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Bergerac Roumanieres"
										]
									},
									{
										"AirportLocationCode": [
											"EGE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Vail/Eagle County"
										]
									},
									{
										"AirportLocationCode": [
											"EGO"
										],
										"CityLocationCode": [
											"EGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Belgorod"
										]
									},
									{
										"AirportLocationCode": [
											"EIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Eindhoven"
										]
									},
									{
										"AirportLocationCode": [
											"EKO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Elko"
										]
									},
									{
										"AirportLocationCode": [
											"ELA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Eagle Lake"
										]
									},
									{
										"AirportLocationCode": [
											"ELH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"North Eleuthera"
										]
									},
									{
										"AirportLocationCode": [
											"ELK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Elk City"
										]
									},
									{
										"AirportLocationCode": [
											"ELL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Ellisras"
										]
									},
									{
										"AirportLocationCode": [
											"ELM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Elmira"
										]
									},
									{
										"AirportLocationCode": [
											"ELP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"El Paso"
										]
									},
									{
										"AirportLocationCode": [
											"ELS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"East London"
										]
									},
									{
										"AirportLocationCode": [
											"EMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"East Midlands"
										]
									},
									{
										"AirportLocationCode": [
											"EME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Emden"
										]
									},
									{
										"AirportLocationCode": [
											"EMN"
										],
										"CityLocationCode": [
											"EMN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MR"
										],
										"Description": [
											"Nema"
										]
									},
									{
										"AirportLocationCode": [
											"ENA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kenai"
										]
									},
									{
										"AirportLocationCode": [
											"ENI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"El Nido"
										]
									},
									{
										"AirportLocationCode": [
											"ENS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Enschede"
										]
									},
									{
										"AirportLocationCode": [
											"ENU"
										],
										"CityLocationCode": [
											"ENU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"ENUGU - AKANU IBIAM INTL"
										]
									},
									{
										"AirportLocationCode": [
											"EPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Esperance"
										]
									},
									{
										"AirportLocationCode": [
											"eqs"
										],
										"CityLocationCode": [
											"ESQ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"ESQUEL"
										]
									},
									{
										"AirportLocationCode": [
											"ERC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Erzincan"
										]
									},
									{
										"AirportLocationCode": [
											"ERD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Berdyansk"
										]
									},
									{
										"AirportLocationCode": [
											"ERF"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Erfurt"
										]
									},
									{
										"AirportLocationCode": [
											"ERH"
										],
										"CityLocationCode": [
											"ERH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Errachidia Moulay Ali Chelif"
										]
									},
									{
										"AirportLocationCode": [
											"ERI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Erie"
										]
									},
									{
										"AirportLocationCode": [
											"ERS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Windhoek Eros"
										]
									},
									{
										"AirportLocationCode": [
											"ERZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Erzurum"
										]
									},
									{
										"AirportLocationCode": [
											"ESB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Ankara Esenboga"
										]
									},
									{
										"AirportLocationCode": [
											"ESC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Escanaba Delta County"
										]
									},
									{
										"AirportLocationCode": [
											"ESE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Ensenada"
										]
									},
									{
										"AirportLocationCode": [
											"ESM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Esmeraldas"
										]
									},
									{
										"AirportLocationCode": [
											"ESR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"El Salvador Salvador Bajo"
										]
									},
									{
										"AirportLocationCode": [
											"ESS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Essen"
										]
									},
									{
										"AirportLocationCode": [
											"ESU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Essaouira"
										]
									},
									{
										"AirportLocationCode": [
											"ETH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IL"
										],
										"Description": [
											"Elat"
										]
									},
									{
										"AirportLocationCode": [
											"ETZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Metz Nancy"
										]
									},
									{
										"AirportLocationCode": [
											"EUG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Eugene"
										]
									},
									{
										"AirportLocationCode": [
											"EUN"
										],
										"CityLocationCode": [
											"EUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Hassan I"
										]
									},
									{
										"AirportLocationCode": [
											"EUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AN"
										],
										"Description": [
											"St Eustatius F D Roosevelt"
										]
									},
									{
										"AirportLocationCode": [
											"EVE"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"HARSTAD-NARVIK"
										]
									},
									{
										"AirportLocationCode": [
											"EVN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Zvartnots International Airport"
										]
									},
									{
										"AirportLocationCode": [
											"EVV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Evansville Dress Regional"
										]
									},
									{
										"AirportLocationCode": [
											"EWN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New Bern Simmons Nott"
										]
									},
									{
										"AirportLocationCode": [
											"EWR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Newark"
										]
									},
									{
										"AirportLocationCode": [
											"EXT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Exeter"
										]
									},
									{
										"AirportLocationCode": [
											"EYW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Key West"
										]
									},
									{
										"AirportLocationCode": [
											"EZE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Buenos Aires Pistarini"
										]
									},
									{
										"AirportLocationCode": [
											"FAE"
										],
										"CityLocationCode": [
											"FAE"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"FO"
										],
										"Description": [
											"Islas Faroe"
										]
									},
									{
										"AirportLocationCode": [
											"FAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fairbanks"
										]
									},
									{
										"AirportLocationCode": [
											"FAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Roma"
										]
									},
									{
										"AirportLocationCode": [
											"FAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Farsund"
										]
									},
									{
										"AirportLocationCode": [
											"FAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Faro"
										]
									},
									{
										"AirportLocationCode": [
											"FAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fargo Hector Field"
										]
									},
									{
										"AirportLocationCode": [
											"FAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fresno Air Terminal"
										]
									},
									{
										"AirportLocationCode": [
											"FCA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kalispell Glacier Nato"
										]
									},
									{
										"AirportLocationCode": [
											"FCO"
										],
										"CityLocationCode": [
											"ROM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Roma Fiumicino"
										]
									},
									{
										"AirportLocationCode": [
											"FDF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MQ"
										],
										"Description": [
											"Fort de France Lamentin"
										]
									},
									{
										"AirportLocationCode": [
											"FDH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Friedrichshafen"
										]
									},
									{
										"AirportLocationCode": [
											"FEG"
										],
										"CityLocationCode": [
											"FEG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UZ"
										],
										"Description": [
											"Fergana"
										]
									},
									{
										"AirportLocationCode": [
											"FEN"
										],
										"CityLocationCode": [
											"FEN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Fernando de Noronha"
										]
									},
									{
										"AirportLocationCode": [
											"FER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Fergusons Gulf"
										]
									},
									{
										"AirportLocationCode": [
											"FEZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Fez Sais"
										]
									},
									{
										"AirportLocationCode": [
											"FIH"
										],
										"CityLocationCode": [
											"FIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CD"
										],
										"Description": [
											"N´Djili"
										]
									},
									{
										"AirportLocationCode": [
											"FKB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Karlsruhe/Baden Baden"
										]
									},
									{
										"AirportLocationCode": [
											"FKS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Fukushima"
										]
									},
									{
										"AirportLocationCode": [
											"FLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Florencia Capitolio"
										]
									},
									{
										"AirportLocationCode": [
											"FLG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Grand Canyon Pulliam Field"
										]
									},
									{
										"AirportLocationCode": [
											"FLI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Flateyri"
										]
									},
									{
										"AirportLocationCode": [
											"FLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Lauderdale"
										]
									},
									{
										"AirportLocationCode": [
											"FLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Florianopolis Hercilio L"
										]
									},
									{
										"AirportLocationCode": [
											"FLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Florence"
										]
									},
									{
										"AirportLocationCode": [
											"FLR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Florencia Peretola"
										]
									},
									{
										"AirportLocationCode": [
											"FLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Flores Island Santa Cruz"
										]
									},
									{
										"AirportLocationCode": [
											"FLY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Finley"
										]
									},
									{
										"AirportLocationCode": [
											"FMA"
										],
										"CityLocationCode": [
											"FMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"El Pucu, Formosa"
										]
									},
									{
										"AirportLocationCode": [
											"FMM"
										],
										"CityLocationCode": [
											"FMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Allgaeu, Memmingen"
										]
									},
									{
										"AirportLocationCode": [
											"FMO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Muenster"
										]
									},
									{
										"AirportLocationCode": [
											"FMY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Myers Page Field"
										]
									},
									{
										"AirportLocationCode": [
											"FNA"
										],
										"CityLocationCode": [
											"FNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SL"
										],
										"Description": [
											"Freetown"
										]
									},
									{
										"AirportLocationCode": [
											"FNC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Funchal"
										]
									},
									{
										"AirportLocationCode": [
											"FNI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Nimes Garons"
										]
									},
									{
										"AirportLocationCode": [
											"FNT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Flint Bishop"
										]
									},
									{
										"AirportLocationCode": [
											"FOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Fuzhou"
										]
									},
									{
										"AirportLocationCode": [
											"FOD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Dodge"
										]
									},
									{
										"AirportLocationCode": [
											"FON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Fortuna"
										]
									},
									{
										"AirportLocationCode": [
											"FOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Fortaleza Pinto Martins"
										]
									},
									{
										"AirportLocationCode": [
											"FPO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Freeport Grand Bahama"
										]
									},
									{
										"AirportLocationCode": [
											"FRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Frankfurt am Main"
										]
									},
									{
										"AirportLocationCode": [
											"FRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Friday Harbor"
										]
									},
									{
										"AirportLocationCode": [
											"FRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Riley"
										]
									},
									{
										"AirportLocationCode": [
											"FRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Fregate Island"
										]
									},
									{
										"AirportLocationCode": [
											"FRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Richardson"
										]
									},
									{
										"AirportLocationCode": [
											"FRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Floro Flora"
										]
									},
									{
										"AirportLocationCode": [
											"FRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GT"
										],
										"Description": [
											"Flores Santa Elena"
										]
									},
									{
										"AirportLocationCode": [
											"FRU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KG"
										],
										"Description": [
											"Bishkek"
										]
									},
									{
										"AirportLocationCode": [
											"FSC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Figari Sud Corse"
										]
									},
									{
										"AirportLocationCode": [
											"FSD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sioux Falls Regional"
										]
									},
									{
										"AirportLocationCode": [
											"FSM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Smith"
										]
									},
									{
										"AirportLocationCode": [
											"FTE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"El Calafate"
										]
									},
									{
										"AirportLocationCode": [
											"FTL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fortuna Ledge"
										]
									},
									{
										"AirportLocationCode": [
											"FUE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Fuerteventura"
										]
									},
									{
										"AirportLocationCode": [
											"FUK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Fukuoka"
										]
									},
									{
										"AirportLocationCode": [
											"FWA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Wayne Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"FYV"
										],
										"CityLocationCode": [
											"FYV"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fayetteville"
										]
									},
									{
										"AirportLocationCode": [
											"GAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gabbs"
										]
									},
									{
										"AirportLocationCode": [
											"GAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gaithersburg"
										]
									},
									{
										"AirportLocationCode": [
											"GAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Galena"
										]
									},
									{
										"AirportLocationCode": [
											"GAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gambell"
										]
									},
									{
										"AirportLocationCode": [
											"GAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MV"
										],
										"Description": [
											"Gan Island Seenu"
										]
									},
									{
										"AirportLocationCode": [
											"GAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Garaina"
										]
									},
									{
										"AirportLocationCode": [
											"GAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Gap"
										]
									},
									{
										"AirportLocationCode": [
											"GAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Guwahati Borjhar"
										]
									},
									{
										"AirportLocationCode": [
											"GBE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BW"
										],
										"Description": [
											"Gaborone Sir Seretse"
										]
									},
									{
										"AirportLocationCode": [
											"GCC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gillette Campbell County"
										]
									},
									{
										"AirportLocationCode": [
											"GCI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Guernsey"
										]
									},
									{
										"AirportLocationCode": [
											"GCK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Garden City"
										]
									},
									{
										"AirportLocationCode": [
											"GCM"
										],
										"CityLocationCode": [
											"GCM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KY"
										],
										"Description": [
											"OWEN ROBERT"
										]
									},
									{
										"AirportLocationCode": [
											"GCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Grand Canyon National"
										]
									},
									{
										"AirportLocationCode": [
											"GDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Guadalajara Miguel Hida"
										]
									},
									{
										"AirportLocationCode": [
											"GDN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Gdansk Rebiechowo"
										]
									},
									{
										"AirportLocationCode": [
											"GDT"
										],
										"CityLocationCode": [
											"GDT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TC"
										],
										"Description": [
											"Grand Turk is"
										]
									},
									{
										"AirportLocationCode": [
											"GDU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Guadalajara, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"GEC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CY"
										],
										"Description": [
											"Gecitkale"
										]
									},
									{
										"AirportLocationCode": [
											"GEE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"George Town"
										]
									},
									{
										"AirportLocationCode": [
											"GEG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Spokane International"
										]
									},
									{
										"AirportLocationCode": [
											"GEO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GY"
										],
										"Description": [
											"Georgetown Cheddi Jagan"
										]
									},
									{
										"AirportLocationCode": [
											"GER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Nueva Gerona Rafael Cabre"
										]
									},
									{
										"AirportLocationCode": [
											"GES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"General Santos Buayan"
										]
									},
									{
										"AirportLocationCode": [
											"GEV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Gallivare"
										]
									},
									{
										"AirportLocationCode": [
											"GFK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Grand Forks"
										]
									},
									{
										"AirportLocationCode": [
											"GGG"
										],
										"CityLocationCode": [
											"LOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Longview Gregg County"
										]
									},
									{
										"AirportLocationCode": [
											"GGT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"George Town Exuma"
										]
									},
									{
										"AirportLocationCode": [
											"GHA"
										],
										"CityLocationCode": [
											"GHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Noumerat ,Ghardaia"
										]
									},
									{
										"AirportLocationCode": [
											"GHB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Governors Harbour"
										]
									},
									{
										"AirportLocationCode": [
											"GHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Guanghan"
										]
									},
									{
										"AirportLocationCode": [
											"GIA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Girona, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"GIB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Gibraltar North Front"
										]
									},
									{
										"AirportLocationCode": [
											"GIG"
										],
										"CityLocationCode": [
											"RIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Rio de Janeiro - Intl"
										]
									},
									{
										"AirportLocationCode": [
											"GIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Gisborne"
										]
									},
									{
										"AirportLocationCode": [
											"GJT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Grand Junction Walker"
										]
									},
									{
										"AirportLocationCode": [
											"GLA"
										],
										"CityLocationCode": [
											"GLA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Glasgow"
										]
									},
									{
										"AirportLocationCode": [
											"GLE"
										],
										"CityLocationCode": [
											"GLE"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gainesville"
										]
									},
									{
										"AirportLocationCode": [
											"GLT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Gladstone"
										]
									},
									{
										"AirportLocationCode": [
											"GLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Glasgow"
										]
									},
									{
										"AirportLocationCode": [
											"GMP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Seoul Gimpo"
										]
									},
									{
										"AirportLocationCode": [
											"GMZ"
										],
										"CityLocationCode": [
											"EAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"San Sebastián de la Gomera"
										]
									},
									{
										"AirportLocationCode": [
											"GNB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lyon Grenoble Saint Geoirs"
										]
									},
									{
										"AirportLocationCode": [
											"GND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GD"
										],
										"Description": [
											"Grenada Point Saline"
										]
									},
									{
										"AirportLocationCode": [
											"GNV"
										],
										"CityLocationCode": [
											"GLE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gainesville J R Alison"
										]
									},
									{
										"AirportLocationCode": [
											"GNY"
										],
										"CityLocationCode": [
											"SFQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Guney Anadolu"
										]
									},
									{
										"AirportLocationCode": [
											"GOA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Génova Cristoforo Colombo"
										]
									},
									{
										"AirportLocationCode": [
											"GOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Gobabis"
										]
									},
									{
										"AirportLocationCode": [
											"GOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Goa Dabolim"
										]
									},
									{
										"AirportLocationCode": [
											"GOJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Nizhniy Novgorod"
										]
									},
									{
										"AirportLocationCode": [
											"GOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gold Beach"
										]
									},
									{
										"AirportLocationCode": [
											"GON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New London/Groton"
										]
									},
									{
										"AirportLocationCode": [
											"GOO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Goondiwindi"
										]
									},
									{
										"AirportLocationCode": [
											"GOP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Gorakhpur"
										]
									},
									{
										"AirportLocationCode": [
											"GOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Gothenburg Landvetter"
										]
									},
									{
										"AirportLocationCode": [
											"GOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CM"
										],
										"Description": [
											"Garoua"
										]
									},
									{
										"AirportLocationCode": [
											"GOV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Gove Nhulunbuy"
										]
									},
									{
										"AirportLocationCode": [
											"GOZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Gorna Orechovitsa"
										]
									},
									{
										"AirportLocationCode": [
											"GPA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Araxos Patrai"
										]
									},
									{
										"AirportLocationCode": [
											"GPS"
										],
										"CityLocationCode": [
											"GPS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"BALTRA ISLAND"
										]
									},
									{
										"AirportLocationCode": [
											"GPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gulfport/Biloxi Regional"
										]
									},
									{
										"AirportLocationCode": [
											"GPZ"
										],
										"CityLocationCode": [
											"GPZ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"MI"
										],
										"Description": [
											"Grand Rapids"
										]
									},
									{
										"AirportLocationCode": [
											"GRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Gamarra"
										]
									},
									{
										"AirportLocationCode": [
											"GRB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Green Bay Austin Field"
										]
									},
									{
										"AirportLocationCode": [
											"GRE"
										],
										"CityLocationCode": [
											"GRE"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IL"
										],
										"Description": [
											"Greenville"
										]
									},
									{
										"AirportLocationCode": [
											"GRJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"George"
										]
									},
									{
										"AirportLocationCode": [
											"GRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Gerona Costa Brava"
										]
									},
									{
										"AirportLocationCode": [
											"GRQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Groningen Eelde"
										]
									},
									{
										"AirportLocationCode": [
											"GRR"
										],
										"CityLocationCode": [
											"GPZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MN"
										],
										"Description": [
											"Grand Rapids Kent City"
										]
									},
									{
										"AirportLocationCode": [
											"GRU"
										],
										"CityLocationCode": [
											"SAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Paulo Guarulhos"
										]
									},
									{
										"AirportLocationCode": [
											"GRV"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Grozny"
										]
									},
									{
										"AirportLocationCode": [
											"GRX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Granada"
										]
									},
									{
										"AirportLocationCode": [
											"GRZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Graz Thalerhof"
										]
									},
									{
										"AirportLocationCode": [
											"GSO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Greensboro/High Point"
										]
									},
									{
										"AirportLocationCode": [
											"GSP"
										],
										"CityLocationCode": [
											"GRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Greenville/Spartanburg"
										]
									},
									{
										"AirportLocationCode": [
											"GSY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Grimsby"
										]
									},
									{
										"AirportLocationCode": [
											"GTF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Great Falls"
										]
									},
									{
										"AirportLocationCode": [
											"GTR"
										],
										"CityLocationCode": [
											"CUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MS"
										],
										"Description": [
											"Columbus Golden Triangle"
										]
									},
									{
										"AirportLocationCode": [
											"GTW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CZ"
										],
										"Description": [
											"Zlin"
										]
									},
									{
										"AirportLocationCode": [
											"GUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GT"
										],
										"Description": [
											"Guatemala City La Aurora"
										]
									},
									{
										"AirportLocationCode": [
											"GUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gunnison"
										]
									},
									{
										"AirportLocationCode": [
											"GUI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Guiria"
										]
									},
									{
										"AirportLocationCode": [
											"GUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Goulburn"
										]
									},
									{
										"AirportLocationCode": [
											"GUM"
										],
										"CityLocationCode": [
											"GUM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Guam"
										]
									},
									{
										"AirportLocationCode": [
											"GUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Alotau Gurney"
										]
									},
									{
										"AirportLocationCode": [
											"GVA"
										],
										"CityLocationCode": [
											"-"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Ginebra"
										]
									},
									{
										"AirportLocationCode": [
											"GVE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Gordonsville"
										]
									},
									{
										"AirportLocationCode": [
											"GWL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Gwalior"
										]
									},
									{
										"AirportLocationCode": [
											"GWY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Galway Carnmore"
										]
									},
									{
										"AirportLocationCode": [
											"GYD"
										],
										"CityLocationCode": [
											"BAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AZ"
										],
										"Description": [
											"Heydar Aliyev Internacional Baku"
										]
									},
									{
										"AirportLocationCode": [
											"GYE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Guayaquil Simon Bolivar"
										]
									},
									{
										"AirportLocationCode": [
											"GYN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Goiania Santa Genoveva"
										]
									},
									{
										"AirportLocationCode": [
											"GZM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MT"
										],
										"Description": [
											"Gozo"
										]
									},
									{
										"AirportLocationCode": [
											"GZT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Gaziantep"
										]
									},
									{
										"AirportLocationCode": [
											"HAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hamilton"
										]
									},
									{
										"AirportLocationCode": [
											"HAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Three Rivers"
										]
									},
									{
										"AirportLocationCode": [
											"HAJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Hanover"
										]
									},
									{
										"AirportLocationCode": [
											"HAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Haikou"
										]
									},
									{
										"AirportLocationCode": [
											"HAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Halali"
										]
									},
									{
										"AirportLocationCode": [
											"HAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Hamburgo Fuhlsbuettel"
										]
									},
									{
										"AirportLocationCode": [
											"HAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Hanoi Noibai"
										]
									},
									{
										"AirportLocationCode": [
											"HAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Harrisburg Skyport"
										]
									},
									{
										"AirportLocationCode": [
											"HAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Heathlands"
										]
									},
									{
										"AirportLocationCode": [
											"HAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Haugesund"
										]
									},
									{
										"AirportLocationCode": [
											"HAV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Havana Jose Marti"
										]
									},
									{
										"AirportLocationCode": [
											"HAW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Haverfordwest"
										]
									},
									{
										"AirportLocationCode": [
											"HAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Haycock"
										]
									},
									{
										"AirportLocationCode": [
											"HBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Hobart"
										]
									},
									{
										"AirportLocationCode": [
											"HBE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Alexandria Borg el Arab"
										]
									},
									{
										"AirportLocationCode": [
											"HBX"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Hubli"
										]
									},
									{
										"AirportLocationCode": [
											"HDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Heidelberg Airport"
										]
									},
									{
										"AirportLocationCode": [
											"HDF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Heringsdorf"
										]
									},
									{
										"AirportLocationCode": [
											"HDN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hayden Yampa Valley"
										]
									},
									{
										"AirportLocationCode": [
											"HDS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Hoedspruit"
										]
									},
									{
										"AirportLocationCode": [
											"HDY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Hat Yai"
										]
									},
									{
										"AirportLocationCode": [
											"HEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Helsinki"
										]
									},
									{
										"AirportLocationCode": [
											"HEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Hendon"
										]
									},
									{
										"AirportLocationCode": [
											"HER"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Creta - Heraklion North Kazantzakis"
										]
									},
									{
										"AirportLocationCode": [
											"HES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hermiston"
										]
									},
									{
										"AirportLocationCode": [
											"HET"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Hohhot"
										]
									},
									{
										"AirportLocationCode": [
											"HEV"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Huelva, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"HEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Santo Domingo Herrera"
										]
									},
									{
										"AirportLocationCode": [
											"HFD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hartford Brainard"
										]
									},
									{
										"AirportLocationCode": [
											"HFE"
										],
										"CityLocationCode": [
											"HFE"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Hefei"
										]
									},
									{
										"AirportLocationCode": [
											"HFT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Hammerfest"
										]
									},
									{
										"AirportLocationCode": [
											"HGH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Hangzhou"
										]
									},
									{
										"AirportLocationCode": [
											"HGN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Mae Hong Son"
										]
									},
									{
										"AirportLocationCode": [
											"HGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hagerstown Wash County"
										]
									},
									{
										"AirportLocationCode": [
											"HHH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hilton Head Island"
										]
									},
									{
										"AirportLocationCode": [
											"HHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Frankfurt Hahn"
										]
									},
									{
										"AirportLocationCode": [
											"HHQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Hua Hin"
										]
									},
									{
										"AirportLocationCode": [
											"HII"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lake Havasu City Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"HIJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Hiroshima"
										]
									},
									{
										"AirportLocationCode": [
											"HIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Jinju Sacheon"
										]
									},
									{
										"AirportLocationCode": [
											"HIR"
										],
										"CityLocationCode": [
											"HIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SB"
										],
										"Description": [
											"Henderson Internacional, Honiara"
										]
									},
									{
										"AirportLocationCode": [
											"HIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Hivaro"
										]
									},
									{
										"AirportLocationCode": [
											"HJR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Khajuraho"
										]
									},
									{
										"AirportLocationCode": [
											"HKD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Hakodate"
										]
									},
									{
										"AirportLocationCode": [
											"HKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Hong Kong"
										]
									},
									{
										"AirportLocationCode": [
											"HKK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Hokitika"
										]
									},
									{
										"AirportLocationCode": [
											"HKT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Phuket"
										]
									},
									{
										"AirportLocationCode": [
											"HLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Lanseria"
										]
									},
									{
										"AirportLocationCode": [
											"HLF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Hultsfred"
										]
									},
									{
										"AirportLocationCode": [
											"HLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Helena"
										]
									},
									{
										"AirportLocationCode": [
											"HLP"
										],
										"CityLocationCode": [
											"JKT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Halim Perdana"
										]
									},
									{
										"AirportLocationCode": [
											"HLZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Hamilton"
										]
									},
									{
										"AirportLocationCode": [
											"HMO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Hermosillo General Pesqueir"
										]
									},
									{
										"AirportLocationCode": [
											"HND"
										],
										"CityLocationCode": [
											"TYO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tokyo Haneda"
										]
									},
									{
										"AirportLocationCode": [
											"HNL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Honolulu"
										]
									},
									{
										"AirportLocationCode": [
											"HNM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hana"
										]
									},
									{
										"AirportLocationCode": [
											"HOB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hobbs Lea County"
										]
									},
									{
										"AirportLocationCode": [
											"HOE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LA"
										],
										"Description": [
											"Houeisay"
										]
									},
									{
										"AirportLocationCode": [
											"HOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Holguin Frank Pais"
										]
									},
									{
										"AirportLocationCode": [
											"HOK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Hooker Creek"
										]
									},
									{
										"AirportLocationCode": [
											"HOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Holikachu"
										]
									},
									{
										"AirportLocationCode": [
											"HOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Homer"
										]
									},
									{
										"AirportLocationCode": [
											"HON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Huron Howes"
										]
									},
									{
										"AirportLocationCode": [
											"HOP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hopkinsville"
										]
									},
									{
										"AirportLocationCode": [
											"HOQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Hof"
										]
									},
									{
										"AirportLocationCode": [
											"HOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Horta"
										]
									},
									{
										"AirportLocationCode": [
											"HOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hot Springs Memorial Field"
										]
									},
									{
										"AirportLocationCode": [
											"HOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Houston Hobby"
										]
									},
									{
										"AirportLocationCode": [
											"HPN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Westchester County"
										]
									},
									{
										"AirportLocationCode": [
											"HRB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Harbin"
										]
									},
									{
										"AirportLocationCode": [
											"HRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZW"
										],
										"Description": [
											"Harare"
										]
									},
									{
										"AirportLocationCode": [
											"HRG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Hurghada"
										]
									},
									{
										"AirportLocationCode": [
											"HRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Kharkov"
										]
									},
									{
										"AirportLocationCode": [
											"HRL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Harlingen Valley"
										]
									},
									{
										"AirportLocationCode": [
											"HSV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Huntsville"
										]
									},
									{
										"AirportLocationCode": [
											"HTI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Hamilton Island"
										]
									},
									{
										"AirportLocationCode": [
											"HTR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Hateruma"
										]
									},
									{
										"AirportLocationCode": [
											"HTS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Huntington Tri State"
										]
									},
									{
										"AirportLocationCode": [
											"HTY"
										],
										"CityLocationCode": [
											"HTY"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Hatay"
										]
									},
									{
										"AirportLocationCode": [
											"HUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Humboldt"
										]
									},
									{
										"AirportLocationCode": [
											"HUI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Hue Phu Bai"
										]
									},
									{
										"AirportLocationCode": [
											"HUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Houlton"
										]
									},
									{
										"AirportLocationCode": [
											"HUM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Houma"
										]
									},
									{
										"AirportLocationCode": [
											"HUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TW"
										],
										"Description": [
											"Hualien"
										]
									},
									{
										"AirportLocationCode": [
											"HUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Huatulco"
										]
									},
									{
										"AirportLocationCode": [
											"HUY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Humberside"
										]
									},
									{
										"AirportLocationCode": [
											"HVB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Hervey Bay"
										]
									},
									{
										"AirportLocationCode": [
											"HVN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New Haven"
										]
									},
									{
										"AirportLocationCode": [
											"HYA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hyannis Barnstable"
										]
									},
									{
										"AirportLocationCode": [
											"HYD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Hyderabad Begumpet"
										]
									},
									{
										"AirportLocationCode": [
											"HYS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hays"
										]
									},
									{
										"AirportLocationCode": [
											"IAD"
										],
										"CityLocationCode": [
											"WAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Washington Dulles"
										]
									},
									{
										"AirportLocationCode": [
											"IAG"
										],
										"CityLocationCode": [
											"IAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Niagara Falls Intl"
										]
									},
									{
										"AirportLocationCode": [
											"IAH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Houston George Bush Intercontinental"
										]
									},
									{
										"AirportLocationCode": [
											"IAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Iasi"
										]
									},
									{
										"AirportLocationCode": [
											"IBE"
										],
										"CityLocationCode": [
											"IBE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Ibague"
										]
									},
									{
										"AirportLocationCode": [
											"IBI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Iboki"
										]
									},
									{
										"AirportLocationCode": [
											"IBZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ibiza"
										]
									},
									{
										"AirportLocationCode": [
											"ICN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Seoul Incheon"
										]
									},
									{
										"AirportLocationCode": [
											"ICO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Sicogon Island"
										]
									},
									{
										"AirportLocationCode": [
											"ICT"
										],
										"CityLocationCode": [
											"ICT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Mid Continent Wichita"
										]
									},
									{
										"AirportLocationCode": [
											"IDA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Idaho Falls Fanning Field"
										]
									},
									{
										"AirportLocationCode": [
											"IDR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Indore"
										]
									},
									{
										"AirportLocationCode": [
											"IEG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Zielona Gora Babimost"
										]
									},
									{
										"AirportLocationCode": [
											"IEV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Kiev Zhulhany"
										]
									},
									{
										"AirportLocationCode": [
											"IFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Isfahan"
										]
									},
									{
										"AirportLocationCode": [
											"IFO"
										],
										"CityLocationCode": [
											"IFO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Ivano Frankivsk Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"IGM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kingman"
										]
									},
									{
										"AirportLocationCode": [
											"IGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Iguazu Cataratas"
										]
									},
									{
										"AirportLocationCode": [
											"IGU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Iguassu Falls Cataratas"
										]
									},
									{
										"AirportLocationCode": [
											"IJK"
										],
										"CityLocationCode": [
											"IJK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Izhevsk"
										]
									},
									{
										"AirportLocationCode": [
											"IKA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Teheran - Iman Khomeini Intl."
										]
									},
									{
										"AirportLocationCode": [
											"IKT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Irkutsk"
										]
									},
									{
										"AirportLocationCode": [
											"ILD"
										],
										"CityLocationCode": [
											"ILD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Lleida - Alguaire"
										]
									},
									{
										"AirportLocationCode": [
											"ILE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Killeen Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"ILG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wilmington Great Wilming"
										]
									},
									{
										"AirportLocationCode": [
											"ILH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Illisheim"
										]
									},
									{
										"AirportLocationCode": [
											"ILM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wilmington New Hanover"
										]
									},
									{
										"AirportLocationCode": [
											"ILO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Iloilo Mandurriao"
										]
									},
									{
										"AirportLocationCode": [
											"ILY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Islay Glenegedale"
										]
									},
									{
										"AirportLocationCode": [
											"IMP"
										],
										"CityLocationCode": [
											"IMP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Imperatriz"
										]
									},
									{
										"AirportLocationCode": [
											"IMT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Iron Mountain Ford"
										]
									},
									{
										"AirportLocationCode": [
											"INC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yinchuan"
										]
									},
									{
										"AirportLocationCode": [
											"IND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Indianapolis"
										]
									},
									{
										"AirportLocationCode": [
											"INL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"International Falls"
										]
									},
									{
										"AirportLocationCode": [
											"INN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Innsbruck Kranebitten"
										]
									},
									{
										"AirportLocationCode": [
											"INT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Winston Salem"
										]
									},
									{
										"AirportLocationCode": [
											"INV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Inverness"
										]
									},
									{
										"AirportLocationCode": [
											"IOA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Ioannina"
										]
									},
									{
										"AirportLocationCode": [
											"IOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Isle Of Man Ronaldsway"
										]
									},
									{
										"AirportLocationCode": [
											"IOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Ilheus"
										]
									},
									{
										"AirportLocationCode": [
											"IPC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Easter Island Mataveri"
										]
									},
									{
										"AirportLocationCode": [
											"IPH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Ipoh"
										]
									},
									{
										"AirportLocationCode": [
											"IPN"
										],
										"CityLocationCode": [
											"IPN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Usiminas, Ipatinga"
										]
									},
									{
										"AirportLocationCode": [
											"IPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Williamsport Lycoming"
										]
									},
									{
										"AirportLocationCode": [
											"IQQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Iquique Diego Aracena"
										]
									},
									{
										"AirportLocationCode": [
											"IQT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Iquitos C F Secada"
										]
									},
									{
										"AirportLocationCode": [
											"IRJ"
										],
										"CityLocationCode": [
											"IRJ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"La Rioja"
										]
									},
									{
										"AirportLocationCode": [
											"IRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kirksville"
										]
									},
									{
										"AirportLocationCode": [
											"ISB"
										],
										"CityLocationCode": [
											"ISB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PK"
										],
										"Description": [
											"BENAZIR BHUTTO INTL"
										]
									},
									{
										"AirportLocationCode": [
											"ISG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Ishigaki"
										]
									},
									{
										"AirportLocationCode": [
											"ISH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Ischia"
										]
									},
									{
										"AirportLocationCode": [
											"ISL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Isabel Pass"
										]
									},
									{
										"AirportLocationCode": [
											"ISO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kinston"
										]
									},
									{
										"AirportLocationCode": [
											"ISP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Islip Long Island Macarthur"
										]
									},
									{
										"AirportLocationCode": [
											"IST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Estambul Ataturk"
										]
									},
									{
										"AirportLocationCode": [
											"ITA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Itacoatiara"
										]
									},
									{
										"AirportLocationCode": [
											"ITH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Ithaca Tompkins County"
										]
									},
									{
										"AirportLocationCode": [
											"ITM"
										],
										"CityLocationCode": [
											"OSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Osaka Itami"
										]
									},
									{
										"AirportLocationCode": [
											"ITO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hilo"
										]
									},
									{
										"AirportLocationCode": [
											"IVA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Ambanja"
										]
									},
									{
										"AirportLocationCode": [
											"IVC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Invercargill"
										]
									},
									{
										"AirportLocationCode": [
											"IVL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Ivalo"
										]
									},
									{
										"AirportLocationCode": [
											"IXA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Agartala Singerbhil"
										]
									},
									{
										"AirportLocationCode": [
											"IXB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Bagdogra"
										]
									},
									{
										"AirportLocationCode": [
											"IXC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Chandigarh"
										]
									},
									{
										"AirportLocationCode": [
											"IXE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Mangalore Bajpe"
										]
									},
									{
										"AirportLocationCode": [
											"IXL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Leh"
										]
									},
									{
										"AirportLocationCode": [
											"IXM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Madurai"
										]
									},
									{
										"AirportLocationCode": [
											"IXT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Pasighat"
										]
									},
									{
										"AirportLocationCode": [
											"IXU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Aurangabad Chikkalthana"
										]
									},
									{
										"AirportLocationCode": [
											"IXZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Port Blair"
										]
									},
									{
										"AirportLocationCode": [
											"IZM"
										],
										"CityLocationCode": [
											"IZM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Izmir"
										]
									},
									{
										"AirportLocationCode": [
											"JAC"
										],
										"CityLocationCode": [
											"JAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jackson Hole"
										]
									},
									{
										"AirportLocationCode": [
											"JAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Jaipur Sanganeer"
										]
									},
									{
										"AirportLocationCode": [
											"JAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Jalapa"
										]
									},
									{
										"AirportLocationCode": [
											"JAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Jambol"
										]
									},
									{
										"AirportLocationCode": [
											"JAN"
										],
										"CityLocationCode": [
											"JAN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jackson"
										]
									},
									{
										"AirportLocationCode": [
											"JAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jasper"
										]
									},
									{
										"AirportLocationCode": [
											"JAX"
										],
										"CityLocationCode": [
											"JAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jacksonville"
										]
									},
									{
										"AirportLocationCode": [
											"JBR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jonesboro"
										]
									},
									{
										"AirportLocationCode": [
											"JCU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ceuta"
										]
									},
									{
										"AirportLocationCode": [
											"JDH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Jodhpur"
										]
									},
									{
										"AirportLocationCode": [
											"JEA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Jaén, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"JED"
										],
										"CityLocationCode": [
											"JED"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SA"
										],
										"Description": [
											"King Abdulaziz Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"JER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Jersey States"
										]
									},
									{
										"AirportLocationCode": [
											"JFK"
										],
										"CityLocationCode": [
											"NYC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New York J F Kennedy"
										]
									},
									{
										"AirportLocationCode": [
											"JHB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Johor Bahru Sultan Ismail"
										]
									},
									{
										"AirportLocationCode": [
											"JHM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kapalua"
										]
									},
									{
										"AirportLocationCode": [
											"JIJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ET"
										],
										"Description": [
											"Jijiga"
										]
									},
									{
										"AirportLocationCode": [
											"JIK"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Ikaria"
										]
									},
									{
										"AirportLocationCode": [
											"JJN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Jinjiang"
										]
									},
									{
										"AirportLocationCode": [
											"JKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Jonkoping Axamo"
										]
									},
									{
										"AirportLocationCode": [
											"JKH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Chios"
										]
									},
									{
										"AirportLocationCode": [
											"JKL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kalymnos"
										]
									},
									{
										"AirportLocationCode": [
											"JKT"
										],
										"CityLocationCode": [
											"JKT"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Jakarta"
										]
									},
									{
										"AirportLocationCode": [
											"JLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Joplin"
										]
									},
									{
										"AirportLocationCode": [
											"JMK"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Mykonos"
										]
									},
									{
										"AirportLocationCode": [
											"JNB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Johannesburg"
										]
									},
									{
										"AirportLocationCode": [
											"JNU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Juneau"
										]
									},
									{
										"AirportLocationCode": [
											"JNX"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Naxos"
										]
									},
									{
										"AirportLocationCode": [
											"JOE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Joensuu"
										]
									},
									{
										"AirportLocationCode": [
											"JOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Yogyakarta Adisutjipto"
										]
									},
									{
										"AirportLocationCode": [
											"JOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Port Saint Johns"
										]
									},
									{
										"AirportLocationCode": [
											"JOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Joinville Cubatao"
										]
									},
									{
										"AirportLocationCode": [
											"JOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Njombe"
										]
									},
									{
										"AirportLocationCode": [
											"JOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Jos"
										]
									},
									{
										"AirportLocationCode": [
											"JPA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Joao Pessoa Castro Pinto"
										]
									},
									{
										"AirportLocationCode": [
											"JPR"
										],
										"CityLocationCode": [
											"JPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"PARANA"
										]
									},
									{
										"AirportLocationCode": [
											"JRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Kilimanjaro"
										]
									},
									{
										"AirportLocationCode": [
											"JSH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Sitia"
										]
									},
									{
										"AirportLocationCode": [
											"JSI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Skiathos Airport"
										]
									},
									{
										"AirportLocationCode": [
											"JST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Johnstown Cambria County"
										]
									},
									{
										"AirportLocationCode": [
											"JSY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Syros Island"
										]
									},
									{
										"AirportLocationCode": [
											"JTR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Thira Santorini"
										]
									},
									{
										"AirportLocationCode": [
											"JTY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Astypalaia Island"
										]
									},
									{
										"AirportLocationCode": [
											"JUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Juara"
										]
									},
									{
										"AirportLocationCode": [
											"JUB"
										],
										"CityLocationCode": [
											"JUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SD"
										],
										"Description": [
											"Juba"
										]
									},
									{
										"AirportLocationCode": [
											"JUI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Juist"
										]
									},
									{
										"AirportLocationCode": [
											"JUJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"JUJUY"
										]
									},
									{
										"AirportLocationCode": [
											"JUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Juliaca"
										]
									},
									{
										"AirportLocationCode": [
											"JYV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Jyvaskyla"
										]
									},
									{
										"AirportLocationCode": [
											"KAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZW"
										],
										"Description": [
											"Kariba"
										]
									},
									{
										"AirportLocationCode": [
											"KAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Gangneung"
										]
									},
									{
										"AirportLocationCode": [
											"KAJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kajaani"
										]
									},
									{
										"AirportLocationCode": [
											"KAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kaltag"
										]
									},
									{
										"AirportLocationCode": [
											"KAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Aminu Kano"
										]
									},
									{
										"AirportLocationCode": [
											"KAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kuusamo"
										]
									},
									{
										"AirportLocationCode": [
											"KAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GY"
										],
										"Description": [
											"Kamarang"
										]
									},
									{
										"AirportLocationCode": [
											"KAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Karasburg"
										]
									},
									{
										"AirportLocationCode": [
											"KAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Kaitaia"
										]
									},
									{
										"AirportLocationCode": [
											"KAV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Kavanayen"
										]
									},
									{
										"AirportLocationCode": [
											"KAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Wakaya Island"
										]
									},
									{
										"AirportLocationCode": [
											"KAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Kau"
										]
									},
									{
										"AirportLocationCode": [
											"KBP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Kiev Borispol"
										]
									},
									{
										"AirportLocationCode": [
											"KBR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kota Bharu Pengkalan Chepa"
										]
									},
									{
										"AirportLocationCode": [
											"KBV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Krabi"
										]
									},
									{
										"AirportLocationCode": [
											"KCH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kuching"
										]
									},
									{
										"AirportLocationCode": [
											"KCZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Kochi"
										]
									},
									{
										"AirportLocationCode": [
											"KEA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Keisah"
										]
									},
									{
										"AirportLocationCode": [
											"KEF"
										],
										"CityLocationCode": [
											"REK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Reykjavik Keflavik"
										]
									},
									{
										"AirportLocationCode": [
											"KEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Kepi"
										]
									},
									{
										"AirportLocationCode": [
											"KEJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Kemerovo"
										]
									},
									{
										"AirportLocationCode": [
											"KEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Kiel"
										]
									},
									{
										"AirportLocationCode": [
											"KEM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kemi Tornio"
										]
									},
									{
										"AirportLocationCode": [
											"KEO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CI"
										],
										"Description": [
											"Odienne"
										]
									},
									{
										"AirportLocationCode": [
											"KER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Kerman"
										]
									},
									{
										"AirportLocationCode": [
											"KET"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MM"
										],
										"Description": [
											"Kengtung"
										]
									},
									{
										"AirportLocationCode": [
											"KEY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Kericho"
										]
									},
									{
										"AirportLocationCode": [
											"KGD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Kaliningrad"
										]
									},
									{
										"AirportLocationCode": [
											"KGI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Kalgoorlie"
										]
									},
									{
										"AirportLocationCode": [
											"KGL"
										],
										"CityLocationCode": [
											"KGL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RW"
										],
										"Description": [
											"Kigali Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"KGS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kos"
										]
									},
									{
										"AirportLocationCode": [
											"KHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Khaneh"
										]
									},
									{
										"AirportLocationCode": [
											"KHH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TW"
										],
										"Description": [
											"Kaohsiung"
										]
									},
									{
										"AirportLocationCode": [
											"KHI"
										],
										"CityLocationCode": [
											"khi"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PK"
										],
										"Description": [
											"Quaid-E-Azam - Karachi"
										]
									},
									{
										"AirportLocationCode": [
											"KHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Nanchang"
										]
									},
									{
										"AirportLocationCode": [
											"KHO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Khoka Moya"
										]
									},
									{
										"AirportLocationCode": [
											"KHV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Khabarovsk Novyy"
										]
									},
									{
										"AirportLocationCode": [
											"KHW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BW"
										],
										"Description": [
											"Khwai River Lodge"
										]
									},
									{
										"AirportLocationCode": [
											"KID"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Kristianstad"
										]
									},
									{
										"AirportLocationCode": [
											"KIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Koinghaas"
										]
									},
									{
										"AirportLocationCode": [
											"KIJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Niigata"
										]
									},
									{
										"AirportLocationCode": [
											"KIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Kimberley"
										]
									},
									{
										"AirportLocationCode": [
											"KIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JM"
										],
										"Description": [
											"Kingston Norman Manley"
										]
									},
									{
										"AirportLocationCode": [
											"KIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Kerry County"
										]
									},
									{
										"AirportLocationCode": [
											"KIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Kisumu"
										]
									},
									{
										"AirportLocationCode": [
											"KIT"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kithira"
										]
									},
									{
										"AirportLocationCode": [
											"KIV"
										],
										"CityLocationCode": [
											"KIV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MD"
										],
										"Description": [
											"Chisinau"
										]
									},
									{
										"AirportLocationCode": [
											"KIX"
										],
										"CityLocationCode": [
											"OSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Osaka Kansai"
										]
									},
									{
										"AirportLocationCode": [
											"KJA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Krasnoyarsk"
										]
									},
									{
										"AirportLocationCode": [
											"KKC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Khon Kaen"
										]
									},
									{
										"AirportLocationCode": [
											"KKE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Kerikeri"
										]
									},
									{
										"AirportLocationCode": [
											"KKN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Kirkenes Hoeybuktmoen"
										]
									},
									{
										"AirportLocationCode": [
											"KLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Kalibo"
										]
									},
									{
										"AirportLocationCode": [
											"KLR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Kalmar"
										]
									},
									{
										"AirportLocationCode": [
											"KLU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Klagenfurt"
										]
									},
									{
										"AirportLocationCode": [
											"KLX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kalamata"
										]
									},
									{
										"AirportLocationCode": [
											"KMG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Kunming"
										]
									},
									{
										"AirportLocationCode": [
											"KMJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Kumamoto"
										]
									},
									{
										"AirportLocationCode": [
											"KMQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Komatsu"
										]
									},
									{
										"AirportLocationCode": [
											"KMS"
										],
										"CityLocationCode": [
											"KMS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GH"
										],
										"Description": [
											"Kumasi"
										]
									},
									{
										"AirportLocationCode": [
											"KNO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Knokke Het Zoute"
										]
									},
									{
										"AirportLocationCode": [
											"KOA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kona Keahole"
										]
									},
									{
										"AirportLocationCode": [
											"KOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NC"
										],
										"Description": [
											"Koumac"
										]
									},
									{
										"AirportLocationCode": [
											"KOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Koolatah"
										]
									},
									{
										"AirportLocationCode": [
											"KOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Kirkwall"
										]
									},
									{
										"AirportLocationCode": [
											"KOJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Kagoshima"
										]
									},
									{
										"AirportLocationCode": [
											"KOK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kokkola Pietarsaari Kruunupyy"
										]
									},
									{
										"AirportLocationCode": [
											"KOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Kokoro"
										]
									},
									{
										"AirportLocationCode": [
											"KOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KH"
										],
										"Description": [
											"Sihanoukville"
										]
									},
									{
										"AirportLocationCode": [
											"KOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kotlik"
										]
									},
									{
										"AirportLocationCode": [
											"KOZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Ouzinkie"
										]
									},
									{
										"AirportLocationCode": [
											"KRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Kerang"
										]
									},
									{
										"AirportLocationCode": [
											"KRF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Kramfors"
										]
									},
									{
										"AirportLocationCode": [
											"KRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Kikori"
										]
									},
									{
										"AirportLocationCode": [
											"KRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Krakow Balice"
										]
									},
									{
										"AirportLocationCode": [
											"KRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Kiruna"
										]
									},
									{
										"AirportLocationCode": [
											"KRO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Kurgan"
										]
									},
									{
										"AirportLocationCode": [
											"KRP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Karup"
										]
									},
									{
										"AirportLocationCode": [
											"KRR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Krasnodar"
										]
									},
									{
										"AirportLocationCode": [
											"KRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Kristiansand Kjevik"
										]
									},
									{
										"AirportLocationCode": [
											"KRT"
										],
										"CityLocationCode": [
											"KRT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SD"
										],
										"Description": [
											"Khartoum"
										]
									},
									{
										"AirportLocationCode": [
											"KSC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SK"
										],
										"Description": [
											"Kosice"
										]
									},
									{
										"AirportLocationCode": [
											"KSD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Karlstad"
										]
									},
									{
										"AirportLocationCode": [
											"KSH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"SHAHID ASHRAFIESFAHANI, Kermanshah"
										]
									},
									{
										"AirportLocationCode": [
											"KSJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Isla Kasos"
										]
									},
									{
										"AirportLocationCode": [
											"KSO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kastoria Aristoteles"
										]
									},
									{
										"AirportLocationCode": [
											"KSU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Kristiansund Kvernberget"
										]
									},
									{
										"AirportLocationCode": [
											"KTM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NP"
										],
										"Description": [
											"Kathmandu Tribhuvan"
										]
									},
									{
										"AirportLocationCode": [
											"KTN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Ketchikan International"
										]
									},
									{
										"AirportLocationCode": [
											"KTP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JM"
										],
										"Description": [
											"Kingston Tinson"
										]
									},
									{
										"AirportLocationCode": [
											"KTT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kittila"
										]
									},
									{
										"AirportLocationCode": [
											"KTW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Katowice Pyrzowice"
										]
									},
									{
										"AirportLocationCode": [
											"KUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kuantan"
										]
									},
									{
										"AirportLocationCode": [
											"KUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Samara"
										]
									},
									{
										"AirportLocationCode": [
											"KUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kuala Lumpur Klia"
										]
									},
									{
										"AirportLocationCode": [
											"KUM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Yakushima"
										]
									},
									{
										"AirportLocationCode": [
											"KUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LT"
										],
										"Description": [
											"Kaunas"
										]
									},
									{
										"AirportLocationCode": [
											"KUO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Kuopio"
										]
									},
									{
										"AirportLocationCode": [
											"KUU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Kulu"
										]
									},
									{
										"AirportLocationCode": [
											"KUW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kugururok River"
										]
									},
									{
										"AirportLocationCode": [
											"KVA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kavala Megas Alexandros"
										]
									},
									{
										"AirportLocationCode": [
											"KWE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Guiyang"
										]
									},
									{
										"AirportLocationCode": [
											"KWI"
										],
										"CityLocationCode": [
											"KWI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KW"
										],
										"Description": [
											"Kuwait Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"KWL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Guilin"
										]
									},
									{
										"AirportLocationCode": [
											"KYA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Konya"
										]
									},
									{
										"AirportLocationCode": [
											"KZI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kozani Philippos"
										]
									},
									{
										"AirportLocationCode": [
											"KZN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Kazan"
										]
									},
									{
										"AirportLocationCode": [
											"KZS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Kastelorizo"
										]
									},
									{
										"AirportLocationCode": [
											"LAA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lamar"
										]
									},
									{
										"AirportLocationCode": [
											"LAD"
										],
										"CityLocationCode": [
											"LAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AO"
										],
										"Description": [
											"4 de Fevereiro"
										]
									},
									{
										"AirportLocationCode": [
											"LAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Lae Nadzab"
										]
									},
									{
										"AirportLocationCode": [
											"LAF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lafayette Purdue Universal"
										]
									},
									{
										"AirportLocationCode": [
											"LAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"La Guaira"
										]
									},
									{
										"AirportLocationCode": [
											"LAH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Labuha"
										]
									},
									{
										"AirportLocationCode": [
											"LAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lannion Servel"
										]
									},
									{
										"AirportLocationCode": [
											"LAJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Lages"
										]
									},
									{
										"AirportLocationCode": [
											"LAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Aklavik"
										]
									},
									{
										"AirportLocationCode": [
											"LAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lakeland"
										]
									},
									{
										"AirportLocationCode": [
											"LAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Los Alamos"
										]
									},
									{
										"AirportLocationCode": [
											"LAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lansing Capital City"
										]
									},
									{
										"AirportLocationCode": [
											"LAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"La Paz Leon"
										]
									},
									{
										"AirportLocationCode": [
											"LAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Laramie General Brees"
										]
									},
									{
										"AirportLocationCode": [
											"LAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Las Vegas McCarran"
										]
									},
									{
										"AirportLocationCode": [
											"LAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Lamu"
										]
									},
									{
										"AirportLocationCode": [
											"LAW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lawton"
										]
									},
									{
										"AirportLocationCode": [
											"LAX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Los Angeles"
										]
									},
									{
										"AirportLocationCode": [
											"LAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Bom Jesus Da Lapa"
										]
									},
									{
										"AirportLocationCode": [
											"LBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Leeds Bradford"
										]
									},
									{
										"AirportLocationCode": [
											"LBB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lubbock"
										]
									},
									{
										"AirportLocationCode": [
											"LBF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"North Platte Lee Bird"
										]
									},
									{
										"AirportLocationCode": [
											"LBS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Labasa"
										]
									},
									{
										"AirportLocationCode": [
											"LBU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Labuan"
										]
									},
									{
										"AirportLocationCode": [
											"LBV"
										],
										"CityLocationCode": [
											"LBV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GA"
										],
										"Description": [
											"Leon M´Ba Internacional Libreville"
										]
									},
									{
										"AirportLocationCode": [
											"LCA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CY"
										],
										"Description": [
											"Larnaca"
										]
									},
									{
										"AirportLocationCode": [
											"LCG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"La Coruna"
										]
									},
									{
										"AirportLocationCode": [
											"LCH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lake Charles"
										]
									},
									{
										"AirportLocationCode": [
											"LCJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Lodz Lublinek"
										]
									},
									{
										"AirportLocationCode": [
											"LCY"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London City"
										]
									},
									{
										"AirportLocationCode": [
											"LDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Londrina"
										]
									},
									{
										"AirportLocationCode": [
											"LDE"
										],
										"CityLocationCode": [
											"LDE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lourdes Tarbes"
										]
									},
									{
										"AirportLocationCode": [
											"LDH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Lord Howe Island"
										]
									},
									{
										"AirportLocationCode": [
											"LDU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Lahad Datu"
										]
									},
									{
										"AirportLocationCode": [
											"LDY"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Londonderry Eglinton"
										]
									},
									{
										"AirportLocationCode": [
											"LEB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lebanon Regional"
										]
									},
									{
										"AirportLocationCode": [
											"LED"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"St Petersburg Pulkovo"
										]
									},
									{
										"AirportLocationCode": [
											"LEE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Leesburg"
										]
									},
									{
										"AirportLocationCode": [
											"LEH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Le Havre Octeville"
										]
									},
									{
										"AirportLocationCode": [
											"LEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Almería"
										]
									},
									{
										"AirportLocationCode": [
											"LEJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Leipzig"
										]
									},
									{
										"AirportLocationCode": [
											"LEM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lemmon"
										]
									},
									{
										"AirportLocationCode": [
											"LEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"León"
										]
									},
									{
										"AirportLocationCode": [
											"LER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Leinster"
										]
									},
									{
										"AirportLocationCode": [
											"LET"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Leticia General A V Cobo"
										]
									},
									{
										"AirportLocationCode": [
											"LEW"
										],
										"CityLocationCode": [
											"LEW"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lewiston"
										]
									},
									{
										"AirportLocationCode": [
											"LEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lexington Blue Grass"
										]
									},
									{
										"AirportLocationCode": [
											"LFT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lafayette Regional"
										]
									},
									{
										"AirportLocationCode": [
											"LFW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TG"
										],
										"Description": [
											"Lome"
										]
									},
									{
										"AirportLocationCode": [
											"LGA"
										],
										"CityLocationCode": [
											"NYC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New York La Guardia"
										]
									},
									{
										"AirportLocationCode": [
											"LGB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Long Beach"
										]
									},
									{
										"AirportLocationCode": [
											"LGG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BE"
										],
										"Description": [
											"Liege"
										]
									},
									{
										"AirportLocationCode": [
											"LGK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Langkawi"
										]
									},
									{
										"AirportLocationCode": [
											"LGV"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Logroño, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"LGW"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London Gatwick"
										]
									},
									{
										"AirportLocationCode": [
											"LHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Lahr"
										]
									},
									{
										"AirportLocationCode": [
											"LHE"
										],
										"CityLocationCode": [
											"LHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PK"
										],
										"Description": [
											"Lahore"
										]
									},
									{
										"AirportLocationCode": [
											"LHR"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London Heathrow"
										]
									},
									{
										"AirportLocationCode": [
											"LHW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Lanzhou"
										]
									},
									{
										"AirportLocationCode": [
											"LIB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Limbunya"
										]
									},
									{
										"AirportLocationCode": [
											"LIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Limoges Bellegarde"
										]
									},
									{
										"AirportLocationCode": [
											"LIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kauai Island Lihue Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"LIJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Long Island"
										]
									},
									{
										"AirportLocationCode": [
											"LIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lille Lesquin"
										]
									},
									{
										"AirportLocationCode": [
											"LIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Lima J Chavez"
										]
									},
									{
										"AirportLocationCode": [
											"LIN"
										],
										"CityLocationCode": [
											"MIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Milán Linate"
										]
									},
									{
										"AirportLocationCode": [
											"LIO"
										],
										"CityLocationCode": [
											"LIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Limón"
										]
									},
									{
										"AirportLocationCode": [
											"LIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Liberia"
										]
									},
									{
										"AirportLocationCode": [
											"LIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Lisboa"
										]
									},
									{
										"AirportLocationCode": [
											"LIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Little Rock Regional"
										]
									},
									{
										"AirportLocationCode": [
											"LIV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Livengood"
										]
									},
									{
										"AirportLocationCode": [
											"LJG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Lijiang"
										]
									},
									{
										"AirportLocationCode": [
											"LJU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SI"
										],
										"Description": [
											"Ljubljana Brnik"
										]
									},
									{
										"AirportLocationCode": [
											"LKL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Lakselv Banak"
										]
									},
									{
										"AirportLocationCode": [
											"LKO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Lucknow Amausi"
										]
									},
									{
										"AirportLocationCode": [
											"LLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Lulea Kallax"
										]
									},
									{
										"AirportLocationCode": [
											"LLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MW"
										],
										"Description": [
											"Lilongwe"
										]
									},
									{
										"AirportLocationCode": [
											"LME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Le Mans Arnage"
										]
									},
									{
										"AirportLocationCode": [
											"LMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Los Mochis Federal"
										]
									},
									{
										"AirportLocationCode": [
											"LMP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Lampedusa"
										]
									},
									{
										"AirportLocationCode": [
											"LND"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lander"
										]
									},
									{
										"AirportLocationCode": [
											"LNK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lincoln"
										]
									},
									{
										"AirportLocationCode": [
											"LNY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lanai City"
										]
									},
									{
										"AirportLocationCode": [
											"LNZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Linz Hoersching"
										]
									},
									{
										"AirportLocationCode": [
											"LOG"
										],
										"CityLocationCode": [
											"LOG"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Longview"
										]
									},
									{
										"AirportLocationCode": [
											"LOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Loja"
										]
									},
									{
										"AirportLocationCode": [
											"LOK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Lodwar"
										]
									},
									{
										"AirportLocationCode": [
											"LOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Lagos De Moreno"
										]
									},
									{
										"AirportLocationCode": [
											"LON"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London"
										]
									},
									{
										"AirportLocationCode": [
											"LOP"
										],
										"CityLocationCode": [
											"AMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Lombok"
										]
									},
									{
										"AirportLocationCode": [
											"LOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Lagos"
										]
									},
									{
										"AirportLocationCode": [
											"LOV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Monclova"
										]
									},
									{
										"AirportLocationCode": [
											"LPA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Gran Canaria Las Palmas"
										]
									},
									{
										"AirportLocationCode": [
											"LPB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"La Paz El Alto"
										]
									},
									{
										"AirportLocationCode": [
											"LPI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Linkoping"
										]
									},
									{
										"AirportLocationCode": [
											"LPK"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Lipetsk"
										]
									},
									{
										"AirportLocationCode": [
											"LPL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Liverpool John Lennon"
										]
									},
									{
										"AirportLocationCode": [
											"LPP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Lappeenranta"
										]
									},
									{
										"AirportLocationCode": [
											"LPQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LA"
										],
										"Description": [
											"Luang Prabang"
										]
									},
									{
										"AirportLocationCode": [
											"LPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lopez Island"
										]
									},
									{
										"AirportLocationCode": [
											"LPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Lampang"
										]
									},
									{
										"AirportLocationCode": [
											"LPY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Le Puy Loudes"
										]
									},
									{
										"AirportLocationCode": [
											"LQN"
										],
										"CityLocationCode": [
											"LQN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AF"
										],
										"Description": [
											"Qala Nau"
										]
									},
									{
										"AirportLocationCode": [
											"LRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Laredo"
										]
									},
									{
										"AirportLocationCode": [
											"LRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Longreach"
										]
									},
									{
										"AirportLocationCode": [
											"LRH"
										],
										"CityLocationCode": [
											"EDM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"La Rochelle Laleu"
										]
									},
									{
										"AirportLocationCode": [
											"LRM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"La Romana"
										]
									},
									{
										"AirportLocationCode": [
											"LRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Leros"
										]
									},
									{
										"AirportLocationCode": [
											"LRT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lorient Lann Bihoue"
										]
									},
									{
										"AirportLocationCode": [
											"LRV"
										],
										"CityLocationCode": [
											"LRV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Los Roques"
										]
									},
									{
										"AirportLocationCode": [
											"LSC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"La Serena La Florida"
										]
									},
									{
										"AirportLocationCode": [
											"LSE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"La Crosse"
										]
									},
									{
										"AirportLocationCode": [
											"LSI"
										],
										"CityLocationCode": [
											"SDZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Shetland Islands Sumburgh"
										]
									},
									{
										"AirportLocationCode": [
											"LSP"
										],
										"CityLocationCode": [
											"LSP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Josefa Camejo, Las Piedras"
										]
									},
									{
										"AirportLocationCode": [
											"LST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Launceston"
										]
									},
									{
										"AirportLocationCode": [
											"LTN"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London Luton"
										]
									},
									{
										"AirportLocationCode": [
											"LTQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Le Touquet"
										]
									},
									{
										"AirportLocationCode": [
											"LTT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"St Tropez"
										]
									},
									{
										"AirportLocationCode": [
											"ltx"
										],
										"CityLocationCode": [
											"LTX"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Latacunga"
										]
									},
									{
										"AirportLocationCode": [
											"LUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NP"
										],
										"Description": [
											"Lukla"
										]
									},
									{
										"AirportLocationCode": [
											"LUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GY"
										],
										"Description": [
											"Lumid Pau"
										]
									},
									{
										"AirportLocationCode": [
											"LUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Laucala Island"
										]
									},
									{
										"AirportLocationCode": [
											"LUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Luderitz"
										]
									},
									{
										"AirportLocationCode": [
											"LUG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Lugano"
										]
									},
									{
										"AirportLocationCode": [
											"LUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Laurel Hesler Noble Field"
										]
									},
									{
										"AirportLocationCode": [
											"LUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZM"
										],
										"Description": [
											"Lusaka"
										]
									},
									{
										"AirportLocationCode": [
											"LUQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"San Luis"
										]
									},
									{
										"AirportLocationCode": [
											"LUT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Laura Station"
										]
									},
									{
										"AirportLocationCode": [
											"LUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LU"
										],
										"Description": [
											"Luxemburgo"
										]
									},
									{
										"AirportLocationCode": [
											"LUY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Lugo, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"LUZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Lushan"
										]
									},
									{
										"AirportLocationCode": [
											"LVI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZM"
										],
										"Description": [
											"Livingstone"
										]
									},
									{
										"AirportLocationCode": [
											"LWB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lewisburg Greenbrier Valley"
										]
									},
									{
										"AirportLocationCode": [
											"LWO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Lvov Snilow"
										]
									},
									{
										"AirportLocationCode": [
											"LWS"
										],
										"CityLocationCode": [
											"LEW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lewiston Nez Perce Cnt"
										]
									},
									{
										"AirportLocationCode": [
											"LXA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Lhasa"
										]
									},
									{
										"AirportLocationCode": [
											"LXR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Luxor"
										]
									},
									{
										"AirportLocationCode": [
											"LXS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Limnos"
										]
									},
									{
										"AirportLocationCode": [
											"LYA"
										],
										"CityLocationCode": [
											"LYA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Luoyang"
										]
									},
									{
										"AirportLocationCode": [
											"LYC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Lycksele"
										]
									},
									{
										"AirportLocationCode": [
											"LYH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lynchburg Preston Glenn"
										]
									},
									{
										"AirportLocationCode": [
											"LYO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lyons"
										]
									},
									{
										"AirportLocationCode": [
											"LYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Longyearbyen Svalbard"
										]
									},
									{
										"AirportLocationCode": [
											"LYS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Lyon Saint Exupery"
										]
									},
									{
										"AirportLocationCode": [
											"LZC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Lazaro Cardenas"
										]
									},
									{
										"AirportLocationCode": [
											"MAA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Chennai"
										]
									},
									{
										"AirportLocationCode": [
											"MAB"
										],
										"CityLocationCode": [
											"MAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Maraba"
										]
									},
									{
										"AirportLocationCode": [
											"MAD"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Madrid Barajas"
										]
									},
									{
										"AirportLocationCode": [
											"MAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Madera"
										]
									},
									{
										"AirportLocationCode": [
											"MAF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Midland/Odessa"
										]
									},
									{
										"AirportLocationCode": [
											"MAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Madang"
										]
									},
									{
										"AirportLocationCode": [
											"MAH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Menorca"
										]
									},
									{
										"AirportLocationCode": [
											"MAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Mangole"
										]
									},
									{
										"AirportLocationCode": [
											"MAM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Matamoros"
										]
									},
									{
										"AirportLocationCode": [
											"MAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Manchester"
										]
									},
									{
										"AirportLocationCode": [
											"MAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Manaus Eduardo Gomes"
										]
									},
									{
										"AirportLocationCode": [
											"MAQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Mae Sot"
										]
									},
									{
										"AirportLocationCode": [
											"MAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Maracaibo La Chinita"
										]
									},
									{
										"AirportLocationCode": [
											"MAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Manus Island Momote"
										]
									},
									{
										"AirportLocationCode": [
											"MAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Maupiti Island"
										]
									},
									{
										"AirportLocationCode": [
											"MAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PR"
										],
										"Description": [
											"Mayaguez De Hostos"
										]
									},
									{
										"AirportLocationCode": [
											"MBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Mombasa Moi"
										]
									},
									{
										"AirportLocationCode": [
											"MBJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JM"
										],
										"Description": [
											"Montego Bay Sangster"
										]
									},
									{
										"AirportLocationCode": [
											"MBS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Saginaw Tri City"
										]
									},
									{
										"AirportLocationCode": [
											"MBT"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Masbate"
										]
									},
									{
										"AirportLocationCode": [
											"MBX"
										],
										"CityLocationCode": [
											"MBX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SI"
										],
										"Description": [
											"Edvard Rusjan Maribor"
										]
									},
									{
										"AirportLocationCode": [
											"MCE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Merced Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"MCI"
										],
										"CityLocationCode": [
											"MCI"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kansas City"
										]
									},
									{
										"AirportLocationCode": [
											"MCM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Monte Carlo Monaco"
										]
									},
									{
										"AirportLocationCode": [
											"MCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Macon Lewis B Wilson"
										]
									},
									{
										"AirportLocationCode": [
											"MCO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Orlando International"
										]
									},
									{
										"AirportLocationCode": [
											"MCP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Macapa"
										]
									},
									{
										"AirportLocationCode": [
											"MCR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GT"
										],
										"Description": [
											"Melchor De Menco"
										]
									},
									{
										"AirportLocationCode": [
											"MCT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"OM"
										],
										"Description": [
											"Muscat Seeb"
										]
									},
									{
										"AirportLocationCode": [
											"MCY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Sunshine Coast Marroochydore"
										]
									},
									{
										"AirportLocationCode": [
											"MCZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Maceio Palmares"
										]
									},
									{
										"AirportLocationCode": [
											"MDC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Manado Samratulangi"
										]
									},
									{
										"AirportLocationCode": [
											"MDE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Medellin Jose Maria Cordova"
										]
									},
									{
										"AirportLocationCode": [
											"MDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MM"
										],
										"Description": [
											"Mandalay Annisaton"
										]
									},
									{
										"AirportLocationCode": [
											"MDQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Mar del Plata"
										]
									},
									{
										"AirportLocationCode": [
											"MDT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Harrisburg International"
										]
									},
									{
										"AirportLocationCode": [
											"MDW"
										],
										"CityLocationCode": [
											"CHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chicago Midway"
										]
									},
									{
										"AirportLocationCode": [
											"MDZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Mendoza El Plumerillo"
										]
									},
									{
										"AirportLocationCode": [
											"MEA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Macae"
										]
									},
									{
										"AirportLocationCode": [
											"MEC"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Manta"
										]
									},
									{
										"AirportLocationCode": [
											"MED"
										],
										"CityLocationCode": [
											"MED"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SA"
										],
										"Description": [
											"Madinah"
										]
									},
									{
										"AirportLocationCode": [
											"MEH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Mehamn"
										]
									},
									{
										"AirportLocationCode": [
											"MEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Meridian Key Field"
										]
									},
									{
										"AirportLocationCode": [
											"MEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Melbourne Tullamarine"
										]
									},
									{
										"AirportLocationCode": [
											"MEM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Memphis"
										]
									},
									{
										"AirportLocationCode": [
											"MEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Mende"
										]
									},
									{
										"AirportLocationCode": [
											"MES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Medan Polania"
										]
									},
									{
										"AirportLocationCode": [
											"MEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Mexico City Juarez"
										]
									},
									{
										"AirportLocationCode": [
											"MFE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"McAllen Miller"
										]
									},
									{
										"AirportLocationCode": [
											"MFM"
										],
										"CityLocationCode": [
											"MFM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MO"
										],
										"Description": [
											"Macau"
										]
									},
									{
										"AirportLocationCode": [
											"MFR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Medford Jackson County"
										]
									},
									{
										"AirportLocationCode": [
											"MGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NI"
										],
										"Description": [
											"Managua"
										]
									},
									{
										"AirportLocationCode": [
											"MGB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Mount Gambier"
										]
									},
									{
										"AirportLocationCode": [
											"MGF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Maringa"
										]
									},
									{
										"AirportLocationCode": [
											"MGH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Margate"
										]
									},
									{
										"AirportLocationCode": [
											"MGL"
										],
										"CityLocationCode": [
											"DUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Dusseldorf Monchengladbach"
										]
									},
									{
										"AirportLocationCode": [
											"MGM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Montgomery Dannelly Field"
										]
									},
									{
										"AirportLocationCode": [
											"MGW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Morgantown"
										]
									},
									{
										"AirportLocationCode": [
											"MHD"
										],
										"CityLocationCode": [
											"MHD"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Mashhad"
										]
									},
									{
										"AirportLocationCode": [
											"MHG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Mannheim"
										]
									},
									{
										"AirportLocationCode": [
											"MHH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Marsh Harbour"
										]
									},
									{
										"AirportLocationCode": [
											"MHK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Manhattan"
										]
									},
									{
										"AirportLocationCode": [
											"MHQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Mariehamn"
										]
									},
									{
										"AirportLocationCode": [
											"MHT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Manchester Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"MIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Miami"
										]
									},
									{
										"AirportLocationCode": [
											"MID"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Merida Rejon"
										]
									},
									{
										"AirportLocationCode": [
											"MIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Mian Yang"
										]
									},
									{
										"AirportLocationCode": [
											"MIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Mitchell Plateau"
										]
									},
									{
										"AirportLocationCode": [
											"MIK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Mikkeli"
										]
									},
									{
										"AirportLocationCode": [
											"MIL"
										],
										"CityLocationCode": [
											"MIL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Milán"
										]
									},
									{
										"AirportLocationCode": [
											"MIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Minnipa"
										]
									},
									{
										"AirportLocationCode": [
											"MIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Monastir Habib Bourguiba"
										]
									},
									{
										"AirportLocationCode": [
											"MIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Misima Island"
										]
									},
									{
										"AirportLocationCode": [
											"MJN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Majunga Amborovy"
										]
									},
									{
										"AirportLocationCode": [
											"MJT"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Mytilene"
										]
									},
									{
										"AirportLocationCode": [
											"MJV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Murcia San Javier"
										]
									},
									{
										"AirportLocationCode": [
											"MKC"
										],
										"CityLocationCode": [
											"MCI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kansas City Downtown"
										]
									},
									{
										"AirportLocationCode": [
											"MKE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Milwaukee General Mitchell"
										]
									},
									{
										"AirportLocationCode": [
											"MKK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hoolehua Molokai"
										]
									},
									{
										"AirportLocationCode": [
											"MKL"
										],
										"CityLocationCode": [
											"JAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jackson McKellar"
										]
									},
									{
										"AirportLocationCode": [
											"MKY"
										],
										"CityLocationCode": [
											"MKY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Mackay, Queensland"
										]
									},
									{
										"AirportLocationCode": [
											"MLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MT"
										],
										"Description": [
											"Malta Luqa"
										]
									},
									{
										"AirportLocationCode": [
											"MLB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Melbourne Intl"
										]
									},
									{
										"AirportLocationCode": [
											"MLE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MV"
										],
										"Description": [
											"Male"
										]
									},
									{
										"AirportLocationCode": [
											"MLG"
										],
										"CityLocationCode": [
											"MLG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Malang"
										]
									},
									{
										"AirportLocationCode": [
											"MLH"
										],
										"CityLocationCode": [
											"EAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Euroairport - Mulhouse"
										]
									},
									{
										"AirportLocationCode": [
											"MLI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Moline Quad City"
										]
									},
									{
										"AirportLocationCode": [
											"MLK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Malta"
										]
									},
									{
										"AirportLocationCode": [
											"MLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Morelia"
										]
									},
									{
										"AirportLocationCode": [
											"MLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Melilla"
										]
									},
									{
										"AirportLocationCode": [
											"MLO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Milo"
										]
									},
									{
										"AirportLocationCode": [
											"MLU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Monroe"
										]
									},
									{
										"AirportLocationCode": [
											"MLW"
										],
										"CityLocationCode": [
											"MLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LR"
										],
										"Description": [
											"Sprigg Payne"
										]
									},
									{
										"AirportLocationCode": [
											"MLX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Malatya"
										]
									},
									{
										"AirportLocationCode": [
											"MMA"
										],
										"CityLocationCode": [
											"MMA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Malmo"
										]
									},
									{
										"AirportLocationCode": [
											"MME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Teesside"
										]
									},
									{
										"AirportLocationCode": [
											"MMK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Murmansk"
										]
									},
									{
										"AirportLocationCode": [
											"MMX"
										],
										"CityLocationCode": [
											"MMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Malmo Sturup Apt"
										]
									},
									{
										"AirportLocationCode": [
											"MNL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Manila Ninoy Aquino"
										]
									},
									{
										"AirportLocationCode": [
											"MOB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Mobile Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"MOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Montes Claros"
										]
									},
									{
										"AirportLocationCode": [
											"MOD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Modesto"
										]
									},
									{
										"AirportLocationCode": [
											"MOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MM"
										],
										"Description": [
											"Mong Hsat"
										]
									},
									{
										"AirportLocationCode": [
											"MOH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Mohanbari"
										]
									},
									{
										"AirportLocationCode": [
											"MOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Molde Aro"
										]
									},
									{
										"AirportLocationCode": [
											"MON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Mount Cook"
										]
									},
									{
										"AirportLocationCode": [
											"MOO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Moomba"
										]
									},
									{
										"AirportLocationCode": [
											"MOQ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Morondava"
										]
									},
									{
										"AirportLocationCode": [
											"MOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Morristown"
										]
									},
									{
										"AirportLocationCode": [
											"MOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Moses Point"
										]
									},
									{
										"AirportLocationCode": [
											"MOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Minot"
										]
									},
									{
										"AirportLocationCode": [
											"MOW"
										],
										"CityLocationCode": [
											"MOW"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Moscú"
										]
									},
									{
										"AirportLocationCode": [
											"MOZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Moorea Temae"
										]
									},
									{
										"AirportLocationCode": [
											"MPH"
										],
										"CityLocationCode": [
											"MPH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Malay"
										]
									},
									{
										"AirportLocationCode": [
											"MPL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Montpellier Frejorgues"
										]
									},
									{
										"AirportLocationCode": [
											"MPM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Maputo"
										]
									},
									{
										"AirportLocationCode": [
											"MQL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Mildura"
										]
									},
									{
										"AirportLocationCode": [
											"MQM"
										],
										"CityLocationCode": [
											"MQM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Mardin"
										]
									},
									{
										"AirportLocationCode": [
											"MQP"
										],
										"CityLocationCode": [
											"MQP"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Nelspruit"
										]
									},
									{
										"AirportLocationCode": [
											"MQT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Marquette Sawyer"
										]
									},
									{
										"AirportLocationCode": [
											"MQX"
										],
										"CityLocationCode": [
											"MQX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ET"
										],
										"Description": [
											"Alula Aba Nega , Mekele"
										]
									},
									{
										"AirportLocationCode": [
											"MRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Merida A Carnevalli"
										]
									},
									{
										"AirportLocationCode": [
											"MRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Marseille"
										]
									},
									{
										"AirportLocationCode": [
											"MRU"
										],
										"CityLocationCode": [
											"MRU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MU"
										],
										"Description": [
											"Mauritius  Sir Seewoosagur"
										]
									},
									{
										"AirportLocationCode": [
											"MRV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Mineralnye Vody"
										]
									},
									{
										"AirportLocationCode": [
											"MRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Monterey Peninsula"
										]
									},
									{
										"AirportLocationCode": [
											"MRZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Moree"
										]
									},
									{
										"AirportLocationCode": [
											"MSJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Misawa"
										]
									},
									{
										"AirportLocationCode": [
											"MSL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Muscle Shoals"
										]
									},
									{
										"AirportLocationCode": [
											"MSN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Madison Dane County Regional"
										]
									},
									{
										"AirportLocationCode": [
											"MSO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Missoula Johnson Bell"
										]
									},
									{
										"AirportLocationCode": [
											"MSP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Minneapolis/St Paul"
										]
									},
									{
										"AirportLocationCode": [
											"MSQ"
										],
										"CityLocationCode": [
											"MSQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BY"
										],
										"Description": [
											"Minsk"
										]
									},
									{
										"AirportLocationCode": [
											"MSS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Massena Richards Field"
										]
									},
									{
										"AirportLocationCode": [
											"MST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Maastricht"
										]
									},
									{
										"AirportLocationCode": [
											"MSY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New Orleans Louis Armstrong"
										]
									},
									{
										"AirportLocationCode": [
											"MTH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Marathon Flight"
										]
									},
									{
										"AirportLocationCode": [
											"MTJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Montrose County"
										]
									},
									{
										"AirportLocationCode": [
											"MTR"
										],
										"CityLocationCode": [
											"MTR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Monteria (Los Garzones)"
										]
									},
									{
										"AirportLocationCode": [
											"MTT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Minatitlan"
										]
									},
									{
										"AirportLocationCode": [
											"MTY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Monterrey General Mariano Escobedo"
										]
									},
									{
										"AirportLocationCode": [
											"MUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BW"
										],
										"Description": [
											"Maun"
										]
									},
									{
										"AirportLocationCode": [
											"MUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Munich Franz Josef Strauss"
										]
									},
									{
										"AirportLocationCode": [
											"MUE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kamuela"
										]
									},
									{
										"AirportLocationCode": [
											"MUM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Mumias"
										]
									},
									{
										"AirportLocationCode": [
											"MUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Maturin Quiriquire"
										]
									},
									{
										"AirportLocationCode": [
											"MUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Marudi"
										]
									},
									{
										"AirportLocationCode": [
											"MUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Marcus Island"
										]
									},
									{
										"AirportLocationCode": [
											"MVD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UY"
										],
										"Description": [
											"Montevideo Carrasco"
										]
									},
									{
										"AirportLocationCode": [
											"MVO"
										],
										"CityLocationCode": [
											"MVO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CG"
										],
										"Description": [
											"Mongo"
										]
									},
									{
										"AirportLocationCode": [
											"MVR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CM"
										],
										"Description": [
											"Maroua Salam"
										]
									},
									{
										"AirportLocationCode": [
											"MWA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Marion Williamson County"
										]
									},
									{
										"AirportLocationCode": [
											"MWH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Moses Lake Grant County"
										]
									},
									{
										"AirportLocationCode": [
											"MWZ"
										],
										"CityLocationCode": [
											"MWZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Mwanza"
										]
									},
									{
										"AirportLocationCode": [
											"MXL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Mexicali"
										]
									},
									{
										"AirportLocationCode": [
											"MXP"
										],
										"CityLocationCode": [
											"MIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Milán Malpensa"
										]
									},
									{
										"AirportLocationCode": [
											"MXX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Mora"
										]
									},
									{
										"AirportLocationCode": [
											"MYD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Malindi"
										]
									},
									{
										"AirportLocationCode": [
											"MYJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Matsuyama"
										]
									},
									{
										"AirportLocationCode": [
											"MYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Myrtle Beach AFB"
										]
									},
									{
										"AirportLocationCode": [
											"MYT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MM"
										],
										"Description": [
											"Myitkyina"
										]
									},
									{
										"AirportLocationCode": [
											"MYY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Miri"
										]
									},
									{
										"AirportLocationCode": [
											"MZL"
										],
										"CityLocationCode": [
											"MZL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"La Nubia Manizales"
										]
									},
									{
										"AirportLocationCode": [
											"MZT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Mazatlan General Rafael Buelna"
										]
									},
									{
										"AirportLocationCode": [
											"NAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Macanal"
										]
									},
									{
										"AirportLocationCode": [
											"NAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Nagpur Sonegaon"
										]
									},
									{
										"AirportLocationCode": [
											"NAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Nakhon Ratchasima"
										]
									},
									{
										"AirportLocationCode": [
											"NAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Namlea"
										]
									},
									{
										"AirportLocationCode": [
											"NAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Nadi"
										]
									},
									{
										"AirportLocationCode": [
											"NAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Nápoles"
										]
									},
									{
										"AirportLocationCode": [
											"NAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Nare"
										]
									},
									{
										"AirportLocationCode": [
											"NAS"
										],
										"CityLocationCode": [
											"NAS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Nassau"
										]
									},
									{
										"AirportLocationCode": [
											"NAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Natal Augusto Severo"
										]
									},
									{
										"AirportLocationCode": [
											"NAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Napuka Island"
										]
									},
									{
										"AirportLocationCode": [
											"NAV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Nevsehir Airport"
										]
									},
									{
										"AirportLocationCode": [
											"NAX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Barbers Point"
										]
									},
									{
										"AirportLocationCode": [
											"NBO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Nairobi Jomo Kenyatta"
										]
									},
									{
										"AirportLocationCode": [
											"NCE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Nice Cote d Azur"
										]
									},
									{
										"AirportLocationCode": [
											"NCL"
										],
										"CityLocationCode": [
											"NCL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Newcastle"
										]
									},
									{
										"AirportLocationCode": [
											"NCU"
										],
										"CityLocationCode": [
											"NCU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UZ"
										],
										"Description": [
											"Nukus"
										]
									},
									{
										"AirportLocationCode": [
											"NCY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Annecy Meythe"
										]
									},
									{
										"AirportLocationCode": [
											"NDB"
										],
										"CityLocationCode": [
											"NDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MR"
										],
										"Description": [
											"Nouadhibou"
										]
									},
									{
										"AirportLocationCode": [
											"NDJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TD"
										],
										"Description": [
											"HASSAN DJAMOUS INTL"
										]
									},
									{
										"AirportLocationCode": [
											"NDR"
										],
										"CityLocationCode": [
											"NDR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Nador - El Arqui"
										]
									},
									{
										"AirportLocationCode": [
											"NEC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Necochea"
										]
									},
									{
										"AirportLocationCode": [
											"NEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lakehurst"
										]
									},
									{
										"AirportLocationCode": [
											"NEV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KN"
										],
										"Description": [
											"Nevis Newcastle"
										]
									},
									{
										"AirportLocationCode": [
											"NGB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Ningbo"
										]
									},
									{
										"AirportLocationCode": [
											"NGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Nagoya Komaki"
										]
									},
									{
										"AirportLocationCode": [
											"NGS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Nagasaki"
										]
									},
									{
										"AirportLocationCode": [
											"NGU"
										],
										"CityLocationCode": [
											"ORF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Norfolk NAS Chambers"
										]
									},
									{
										"AirportLocationCode": [
											"NHA"
										],
										"CityLocationCode": [
											"NHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Nha Trang"
										]
									},
									{
										"AirportLocationCode": [
											"NIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CY"
										],
										"Description": [
											"Nicosia"
										]
									},
									{
										"AirportLocationCode": [
											"NIM"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NE"
										],
										"Description": [
											"Niamey"
										]
									},
									{
										"AirportLocationCode": [
											"NIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Ninilchik"
										]
									},
									{
										"AirportLocationCode": [
											"NIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Niort"
										]
									},
									{
										"AirportLocationCode": [
											"NJC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Nizhnevartovsk"
										]
									},
									{
										"AirportLocationCode": [
											"NKC"
										],
										"CityLocationCode": [
											"NKC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MR"
										],
										"Description": [
											"Nouakchott"
										]
									},
									{
										"AirportLocationCode": [
											"NKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Nanking Nanjing"
										]
									},
									{
										"AirportLocationCode": [
											"NLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZM"
										],
										"Description": [
											"Ndola"
										]
									},
									{
										"AirportLocationCode": [
											"NLD"
										],
										"CityLocationCode": [
											"NLD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Nuevo Laredo Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"NNG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Nanning"
										]
									},
									{
										"AirportLocationCode": [
											"NNT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Nan"
										]
									},
									{
										"AirportLocationCode": [
											"NOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Knock"
										]
									},
									{
										"AirportLocationCode": [
											"NOI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Novorossijsk"
										]
									},
									{
										"AirportLocationCode": [
											"NOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Nordfjordur"
										]
									},
									{
										"AirportLocationCode": [
											"NOS"
										],
										"CityLocationCode": [
											"NOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Nosy Be"
										]
									},
									{
										"AirportLocationCode": [
											"NOT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Novato"
										]
									},
									{
										"AirportLocationCode": [
											"NOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NC"
										],
										"Description": [
											"Noumea Tontouta"
										]
									},
									{
										"AirportLocationCode": [
											"NOZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Novokuznetsk"
										]
									},
									{
										"AirportLocationCode": [
											"NPL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"New Plymouth"
										]
									},
									{
										"AirportLocationCode": [
											"NQN"
										],
										"CityLocationCode": [
											"NQN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Neuquen"
										]
									},
									{
										"AirportLocationCode": [
											"NQT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Nottingham"
										]
									},
									{
										"AirportLocationCode": [
											"NQY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Newquay Saint Morgan"
										]
									},
									{
										"AirportLocationCode": [
											"NRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Narrandera"
										]
									},
									{
										"AirportLocationCode": [
											"NRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Norrkoping Kungsangen"
										]
									},
									{
										"AirportLocationCode": [
											"NRN"
										],
										"CityLocationCode": [
											"DUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Weeze Airport,Duesseldorf"
										]
									},
									{
										"AirportLocationCode": [
											"NRT"
										],
										"CityLocationCode": [
											"TYO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tokyo Narita"
										]
									},
									{
										"AirportLocationCode": [
											"NRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Newry"
										]
									},
									{
										"AirportLocationCode": [
											"NSI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CM"
										],
										"Description": [
											"Yaounde Nsimalen"
										]
									},
									{
										"AirportLocationCode": [
											"NSN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Nelson"
										]
									},
									{
										"AirportLocationCode": [
											"NST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Nakhon Si Thammarat"
										]
									},
									{
										"AirportLocationCode": [
											"nsy"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Catania"
										]
									},
									{
										"AirportLocationCode": [
											"NTE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Nantes Atlantique"
										]
									},
									{
										"AirportLocationCode": [
											"NTG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Nantong"
										]
									},
									{
										"AirportLocationCode": [
											"NTL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Newcastle Williamtown"
										]
									},
									{
										"AirportLocationCode": [
											"NTY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Sun City Pilansberg"
										]
									},
									{
										"AirportLocationCode": [
											"NUE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Nuremberg"
										]
									},
									{
										"AirportLocationCode": [
											"NUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Nullarbor"
										]
									},
									{
										"AirportLocationCode": [
											"NUU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Nakuru"
										]
									},
									{
										"AirportLocationCode": [
											"NVA"
										],
										"CityLocationCode": [
											"NVA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"NEIVA - LA MARGUITA"
										]
									},
									{
										"AirportLocationCode": [
											"NVK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Narvik Framnes"
										]
									},
									{
										"AirportLocationCode": [
											"NVT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Navegantes"
										]
									},
									{
										"AirportLocationCode": [
											"NWI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Norwich"
										]
									},
									{
										"AirportLocationCode": [
											"NYC"
										],
										"CityLocationCode": [
											"NYC"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"New York"
										]
									},
									{
										"AirportLocationCode": [
											"OAJ"
										],
										"CityLocationCode": [
											"JAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Jacksonville A J Ellis"
										]
									},
									{
										"AirportLocationCode": [
											"OAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Oakland"
										]
									},
									{
										"AirportLocationCode": [
											"OAX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Oaxaca Xoxocotlan"
										]
									},
									{
										"AirportLocationCode": [
											"OCJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JM"
										],
										"Description": [
											"Ocho Rios Boscobel"
										]
									},
									{
										"AirportLocationCode": [
											"ODE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Odense Beldringe"
										]
									},
									{
										"AirportLocationCode": [
											"ODS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Odessa Central"
										]
									},
									{
										"AirportLocationCode": [
											"OER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Ornskoldsvik"
										]
									},
									{
										"AirportLocationCode": [
											"OFK"
										],
										"CityLocationCode": [
											"ORF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Norfolk Stefan Field"
										]
									},
									{
										"AirportLocationCode": [
											"OGG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Kahului"
										]
									},
									{
										"AirportLocationCode": [
											"OGZ"
										],
										"CityLocationCode": [
											"OGZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Beslan ,Vladikavkaz"
										]
									},
									{
										"AirportLocationCode": [
											"OIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Oita"
										]
									},
									{
										"AirportLocationCode": [
											"OKA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Okinawa Naha"
										]
									},
									{
										"AirportLocationCode": [
											"OKC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Oklahoma City Will Rogers"
										]
									},
									{
										"AirportLocationCode": [
											"OKE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Okino Erabu"
										]
									},
									{
										"AirportLocationCode": [
											"OKI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Oki Island"
										]
									},
									{
										"AirportLocationCode": [
											"OKJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Okayama"
										]
									},
									{
										"AirportLocationCode": [
											"OKL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Oksibil"
										]
									},
									{
										"AirportLocationCode": [
											"OLB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Olbia Costa Smeralda"
										]
									},
									{
										"AirportLocationCode": [
											"OMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Omaha Eppley Airfield"
										]
									},
									{
										"AirportLocationCode": [
											"OME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Nome"
										]
									},
									{
										"AirportLocationCode": [
											"OMO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BA"
										],
										"Description": [
											"Mostar"
										]
									},
									{
										"AirportLocationCode": [
											"OMR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Oradea"
										]
									},
									{
										"AirportLocationCode": [
											"OMS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Omsk"
										]
									},
									{
										"AirportLocationCode": [
											"ONT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Ontario"
										]
									},
									{
										"AirportLocationCode": [
											"OOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Gold Coast Coolangatta"
										]
									},
									{
										"AirportLocationCode": [
											"OOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Cooma"
										]
									},
									{
										"AirportLocationCode": [
											"OPO"
										],
										"CityLocationCode": [
											"OPO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Porto"
										]
									},
									{
										"AirportLocationCode": [
											"ORA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Oran"
										]
									},
									{
										"AirportLocationCode": [
											"ORB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Orebro Bofors"
										]
									},
									{
										"AirportLocationCode": [
											"ORD"
										],
										"CityLocationCode": [
											"CHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chicago - O'Hare Intl"
										]
									},
									{
										"AirportLocationCode": [
											"ORE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Orleans"
										]
									},
									{
										"AirportLocationCode": [
											"ORF"
										],
										"CityLocationCode": [
											"ORF"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Norfolk"
										]
									},
									{
										"AirportLocationCode": [
											"ORI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Port Lions"
										]
									},
									{
										"AirportLocationCode": [
											"ORK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Cork"
										]
									},
									{
										"AirportLocationCode": [
											"ORL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Orlando Herndon"
										]
									},
									{
										"AirportLocationCode": [
											"ORN"
										],
										"CityLocationCode": [
											"ORN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Oran - Es Senia"
										]
									},
									{
										"AirportLocationCode": [
											"ORS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Orpheus Island"
										]
									},
									{
										"AirportLocationCode": [
											"ORU"
										],
										"CityLocationCode": [
											"ORU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Oruro"
										]
									},
									{
										"AirportLocationCode": [
											"ORY"
										],
										"CityLocationCode": [
											"PAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Paris Orly"
										]
									},
									{
										"AirportLocationCode": [
											"OSA"
										],
										"CityLocationCode": [
											"OSA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Osaka"
										]
									},
									{
										"AirportLocationCode": [
											"OSD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Ostersund Froesoe"
										]
									},
									{
										"AirportLocationCode": [
											"OSL"
										],
										"CityLocationCode": [
											"OSL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Oslo"
										]
									},
									{
										"AirportLocationCode": [
											"OSR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CZ"
										],
										"Description": [
											"Ostrava Mosnov"
										]
									},
									{
										"AirportLocationCode": [
											"OST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BE"
										],
										"Description": [
											"Ostend"
										]
									},
									{
										"AirportLocationCode": [
											"OSY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Namsos"
										]
									},
									{
										"AirportLocationCode": [
											"OTH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"North Bend"
										]
									},
									{
										"AirportLocationCode": [
											"OTO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Otto"
										]
									},
									{
										"AirportLocationCode": [
											"OTP"
										],
										"CityLocationCode": [
											"BUH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Bucarest Otopeni"
										]
									},
									{
										"AirportLocationCode": [
											"OUA"
										],
										"CityLocationCode": [
											"OUA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BF"
										],
										"Description": [
											"Ouagadougou"
										]
									},
									{
										"AirportLocationCode": [
											"OUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Oujda Les Angades"
										]
									},
									{
										"AirportLocationCode": [
											"OUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Oulu"
										]
									},
									{
										"AirportLocationCode": [
											"OUQ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Orense, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"OUZ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MR"
										],
										"Description": [
											"Tazadit, Zouerate"
										]
									},
									{
										"AirportLocationCode": [
											"OVB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Novosibirsk Tolmachevo"
										]
									},
									{
										"AirportLocationCode": [
											"OVD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Asturias"
										]
									},
									{
										"AirportLocationCode": [
											"OVI"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Oviedo, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"OWB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Owensboro Daviess County"
										]
									},
									{
										"AirportLocationCode": [
											"OXB"
										],
										"CityLocationCode": [
											"OXB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GW"
										],
										"Description": [
											"O Vieira"
										]
									},
									{
										"AirportLocationCode": [
											"OXF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Oxford Kidlington"
										]
									},
									{
										"AirportLocationCode": [
											"OXR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Oxnard/Ventura"
										]
									},
									{
										"AirportLocationCode": [
											"OZC"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Labo"
										]
									},
									{
										"AirportLocationCode": [
											"OZZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Ouarzazate"
										]
									},
									{
										"AirportLocationCode": [
											"PAC"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PA"
										],
										"Description": [
											"Marcos a Gelabert"
										]
									},
									{
										"AirportLocationCode": [
											"PAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Paderborn"
										]
									},
									{
										"AirportLocationCode": [
											"PAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Pagadian"
										]
									},
									{
										"AirportLocationCode": [
											"PAH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Paducah Barkley Regional"
										]
									},
									{
										"AirportLocationCode": [
											"PAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Palanquero"
										]
									},
									{
										"AirportLocationCode": [
											"PAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Pattani"
										]
									},
									{
										"AirportLocationCode": [
											"PAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HT"
										],
										"Description": [
											"Port au Prince Mais Gate"
										]
									},
									{
										"AirportLocationCode": [
											"PAR"
										],
										"CityLocationCode": [
											"PAR"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Paris"
										]
									},
									{
										"AirportLocationCode": [
											"PAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Paros"
										]
									},
									{
										"AirportLocationCode": [
											"PAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Patna"
										]
									},
									{
										"AirportLocationCode": [
											"PAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Poza Rica Tajin"
										]
									},
									{
										"AirportLocationCode": [
											"PBC"
										],
										"CityLocationCode": [
											"PBC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Huejotsingo"
										]
									},
									{
										"AirportLocationCode": [
											"PBD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Porbandar"
										]
									},
									{
										"AirportLocationCode": [
											"PBH"
										],
										"CityLocationCode": [
											"PBH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BT"
										],
										"Description": [
											"PARO"
										]
									},
									{
										"AirportLocationCode": [
											"PBI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"West Palm Beach"
										]
									},
									{
										"AirportLocationCode": [
											"PBM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SR"
										],
										"Description": [
											"Paramaribo Zanderij"
										]
									},
									{
										"AirportLocationCode": [
											"PBZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Plettenberg Bay"
										]
									},
									{
										"AirportLocationCode": [
											"PCI"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Palencia, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"PCL"
										],
										"CityLocationCode": [
											"PCL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Capitán Rolden, Pucallpa"
										]
									},
									{
										"AirportLocationCode": [
											"PDG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Padang Tabing"
										]
									},
									{
										"AirportLocationCode": [
											"PDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Ponta Delgada Azores Nordela"
										]
									},
									{
										"AirportLocationCode": [
											"PDP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UY"
										],
										"Description": [
											"Punta Del Este"
										]
									},
									{
										"AirportLocationCode": [
											"PDV"
										],
										"CityLocationCode": [
											"PDV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Krumovo - Plovdiv"
										]
									},
									{
										"AirportLocationCode": [
											"PDX"
										],
										"CityLocationCode": [
											"PDX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Portland Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"PEA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Penneshaw"
										]
									},
									{
										"AirportLocationCode": [
											"PEE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Perm"
										]
									},
									{
										"AirportLocationCode": [
											"PEG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Perugia Sant Egidio"
										]
									},
									{
										"AirportLocationCode": [
											"PEI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Pereira Matecana"
										]
									},
									{
										"AirportLocationCode": [
											"PEK"
										],
										"CityLocationCode": [
											"BJS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Beijing Capital"
										]
									},
									{
										"AirportLocationCode": [
											"PEM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Puerto Maldonado"
										]
									},
									{
										"AirportLocationCode": [
											"PEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Penang"
										]
									},
									{
										"AirportLocationCode": [
											"PER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Perth"
										]
									},
									{
										"AirportLocationCode": [
											"PES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Petrozavodsk"
										]
									},
									{
										"AirportLocationCode": [
											"PET"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Pelotas Federal"
										]
									},
									{
										"AirportLocationCode": [
											"PFE"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ponferrada"
										]
									},
									{
										"AirportLocationCode": [
											"PFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Panama City Bay County"
										]
									},
									{
										"AirportLocationCode": [
											"PFO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CY"
										],
										"Description": [
											"Paphos"
										]
									},
									{
										"AirportLocationCode": [
											"PGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Page"
										]
									},
									{
										"AirportLocationCode": [
											"PGF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Perpignan Rivesaltes"
										]
									},
									{
										"AirportLocationCode": [
											"PGV"
										],
										"CityLocationCode": [
											"GRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NC"
										],
										"Description": [
											"Greenville Pitt Greenville"
										]
									},
									{
										"AirportLocationCode": [
											"PGX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Perigueux"
										]
									},
									{
										"AirportLocationCode": [
											"PHC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Port Harcourt"
										]
									},
									{
										"AirportLocationCode": [
											"PHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Port Hedland"
										]
									},
									{
										"AirportLocationCode": [
											"PHF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Newport News"
										]
									},
									{
										"AirportLocationCode": [
											"PHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Pinheiro"
										]
									},
									{
										"AirportLocationCode": [
											"PHL"
										],
										"CityLocationCode": [
											"PHL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Philadelphia"
										]
									},
									{
										"AirportLocationCode": [
											"PHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Port Huron"
										]
									},
									{
										"AirportLocationCode": [
											"PHR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Pacific Harbor"
										]
									},
									{
										"AirportLocationCode": [
											"PHS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Phitsanulok"
										]
									},
									{
										"AirportLocationCode": [
											"PHW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Phalaborwa H Van Eck"
										]
									},
									{
										"AirportLocationCode": [
											"PHX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Phoenix Sky Harbor"
										]
									},
									{
										"AirportLocationCode": [
											"PIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Greater Peoria"
										]
									},
									{
										"AirportLocationCode": [
											"PIB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Laurel Hattiesburg"
										]
									},
									{
										"AirportLocationCode": [
											"PID"
										],
										"CityLocationCode": [
											"NAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Nassau Paradise Island"
										]
									},
									{
										"AirportLocationCode": [
											"PIE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"St Petersburg"
										]
									},
									{
										"AirportLocationCode": [
											"PIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Pitinga"
										]
									},
									{
										"AirportLocationCode": [
											"PIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pocatello"
										]
									},
									{
										"AirportLocationCode": [
											"PIK"
										],
										"CityLocationCode": [
											"GLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Glasgow Prestwick"
										]
									},
									{
										"AirportLocationCode": [
											"PIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PY"
										],
										"Description": [
											"Pilar"
										]
									},
									{
										"AirportLocationCode": [
											"PIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Parintins"
										]
									},
									{
										"AirportLocationCode": [
											"PIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pierre"
										]
									},
									{
										"AirportLocationCode": [
											"PIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Poitiers Biard"
										]
									},
									{
										"AirportLocationCode": [
											"PIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pittsburgh"
										]
									},
									{
										"AirportLocationCode": [
											"PIU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Piura"
										]
									},
									{
										"AirportLocationCode": [
											"PIX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Pico Island"
										]
									},
									{
										"AirportLocationCode": [
											"PJM"
										],
										"CityLocationCode": [
											"PJM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Puerto Jimenez"
										]
									},
									{
										"AirportLocationCode": [
											"PKB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Parkersburg Wood County"
										]
									},
									{
										"AirportLocationCode": [
											"PKC"
										],
										"CityLocationCode": [
											"PKC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Yelizovo, Petropavlovsk"
										]
									},
									{
										"AirportLocationCode": [
											"PKE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Parkes"
										]
									},
									{
										"AirportLocationCode": [
											"PKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Pangkor Perka"
										]
									},
									{
										"AirportLocationCode": [
											"PKR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NP"
										],
										"Description": [
											"Pokhara"
										]
									},
									{
										"AirportLocationCode": [
											"PKZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LA"
										],
										"Description": [
											"Pakse"
										]
									},
									{
										"AirportLocationCode": [
											"PLB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Plattsburgh Clinton County"
										]
									},
									{
										"AirportLocationCode": [
											"PLH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Plymouth"
										]
									},
									{
										"AirportLocationCode": [
											"PLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Palembang M Badaruddin"
										]
									},
									{
										"AirportLocationCode": [
											"PLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pellston Emmet County"
										]
									},
									{
										"AirportLocationCode": [
											"PLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Port Lincoln"
										]
									},
									{
										"AirportLocationCode": [
											"PLQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LT"
										],
										"Description": [
											"Palanga"
										]
									},
									{
										"AirportLocationCode": [
											"PLS"
										],
										"CityLocationCode": [
											"PLS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TC"
										],
										"Description": [
											"Providenciales"
										]
									},
									{
										"AirportLocationCode": [
											"PLU"
										],
										"CityLocationCode": [
											"BHZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Belo Horizonte Pampulha"
										]
									},
									{
										"AirportLocationCode": [
											"PLZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Port Elizabeth"
										]
									},
									{
										"AirportLocationCode": [
											"PMC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Puerto Montt Tepual"
										]
									},
									{
										"AirportLocationCode": [
											"PMF"
										],
										"CityLocationCode": [
											"MIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Milán Parma"
										]
									},
									{
										"AirportLocationCode": [
											"PMI"
										],
										"CityLocationCode": [
											"PMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Palma Mallorca"
										]
									},
									{
										"AirportLocationCode": [
											"PMO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Palermo Punta Raisi"
										]
									},
									{
										"AirportLocationCode": [
											"PMR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Palmerston North"
										]
									},
									{
										"AirportLocationCode": [
											"PMV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Porlamar Delcaribe Gen S"
										]
									},
									{
										"AirportLocationCode": [
											"PMW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Palmas"
										]
									},
									{
										"AirportLocationCode": [
											"PMY"
										],
										"CityLocationCode": [
											"PMY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Tehuelche Puerto Madryn"
										]
									},
									{
										"AirportLocationCode": [
											"PNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Pamplona"
										]
									},
									{
										"AirportLocationCode": [
											"PNH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KH"
										],
										"Description": [
											"Phnom Penh Pochentong"
										]
									},
									{
										"AirportLocationCode": [
											"PNK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Pontianak Supadio"
										]
									},
									{
										"AirportLocationCode": [
											"PNL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Pantelleria"
										]
									},
									{
										"AirportLocationCode": [
											"PNQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Pune Lohegaon"
										]
									},
									{
										"AirportLocationCode": [
											"PNR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CG"
										],
										"Description": [
											"Pointe Noire"
										]
									},
									{
										"AirportLocationCode": [
											"PNS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pensacola Regional"
										]
									},
									{
										"AirportLocationCode": [
											"PNZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Petrolina"
										]
									},
									{
										"AirportLocationCode": [
											"POA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Porto Alegre Salgado Fil"
										]
									},
									{
										"AirportLocationCode": [
											"POC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"La Verne"
										]
									},
									{
										"AirportLocationCode": [
											"POI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Potosi"
										]
									},
									{
										"AirportLocationCode": [
											"POL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Pemba"
										]
									},
									{
										"AirportLocationCode": [
											"POM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Port Moresby Jackson Field"
										]
									},
									{
										"AirportLocationCode": [
											"PON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GT"
										],
										"Description": [
											"Poptun"
										]
									},
									{
										"AirportLocationCode": [
											"POO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Pocos De Caldas"
										]
									},
									{
										"AirportLocationCode": [
											"POP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Puerto Plata La Union"
										]
									},
									{
										"AirportLocationCode": [
											"POR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Pori"
										]
									},
									{
										"AirportLocationCode": [
											"POS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TT"
										],
										"Description": [
											"Port of Spain Piarco"
										]
									},
									{
										"AirportLocationCode": [
											"POU"
										],
										"CityLocationCode": [
											"POU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Poughkeepsie"
										]
									},
									{
										"AirportLocationCode": [
											"POW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SI"
										],
										"Description": [
											"Portoroz"
										]
									},
									{
										"AirportLocationCode": [
											"POZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Poznan Lawica"
										]
									},
									{
										"AirportLocationCode": [
											"PPB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"President Prudente Barros"
										]
									},
									{
										"AirportLocationCode": [
											"PPN"
										],
										"CityLocationCode": [
											"PPN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Guillermoleon Valencia Popayan"
										]
									},
									{
										"AirportLocationCode": [
											"PPP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Proserpine Whitsunday Coast"
										]
									},
									{
										"AirportLocationCode": [
											"PPS"
										],
										"CityLocationCode": [
											"PPS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Puerto Princesa"
										]
									},
									{
										"AirportLocationCode": [
											"PPT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Papeete Faaa"
										]
									},
									{
										"AirportLocationCode": [
											"PQC"
										],
										"CityLocationCode": [
											"PQC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Duong Dang, Phu Quoc"
										]
									},
									{
										"AirportLocationCode": [
											"PQI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Presque Isle Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"PQQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Port Macquarie"
										]
									},
									{
										"AirportLocationCode": [
											"PRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Parana"
										]
									},
									{
										"AirportLocationCode": [
											"PRC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Prescott"
										]
									},
									{
										"AirportLocationCode": [
											"PRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Pore"
										]
									},
									{
										"AirportLocationCode": [
											"PRG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CZ"
										],
										"Description": [
											"Praga Ruzyne"
										]
									},
									{
										"AirportLocationCode": [
											"PRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Praslin Island"
										]
									},
									{
										"AirportLocationCode": [
											"PRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"YU"
										],
										"Description": [
											"Pristina"
										]
									},
									{
										"AirportLocationCode": [
											"PRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Perry"
										]
									},
									{
										"AirportLocationCode": [
											"PRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Pretoria Wonderboom"
										]
									},
									{
										"AirportLocationCode": [
											"PSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Florencia Pisa"
										]
									},
									{
										"AirportLocationCode": [
											"PSC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pasco Tri Cities"
										]
									},
									{
										"AirportLocationCode": [
											"PSE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PR"
										],
										"Description": [
											"Ponce Mercedita"
										]
									},
									{
										"AirportLocationCode": [
											"PSO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Cano, Pasto"
										]
									},
									{
										"AirportLocationCode": [
											"PSP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Palm Springs"
										]
									},
									{
										"AirportLocationCode": [
											"PSR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Pescara Liberi"
										]
									},
									{
										"AirportLocationCode": [
											"PSS"
										],
										"CityLocationCode": [
											"PSS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Posadas"
										]
									},
									{
										"AirportLocationCode": [
											"PTE"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Pontevedra, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"ptg"
										],
										"CityLocationCode": [
											"ptg"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"POLOKWANE"
										]
									},
									{
										"AirportLocationCode": [
											"PTJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Portland"
										]
									},
									{
										"AirportLocationCode": [
											"PTP"
										],
										"CityLocationCode": [
											"PTP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GP"
										],
										"Description": [
											"Pole Caraibes"
										]
									},
									{
										"AirportLocationCode": [
											"PTY"
										],
										"CityLocationCode": [
											"PTY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PA"
										],
										"Description": [
											"Panamá - Tocumen Intl"
										]
									},
									{
										"AirportLocationCode": [
											"PUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pueblo Memorial"
										]
									},
									{
										"AirportLocationCode": [
											"PUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Price"
										]
									},
									{
										"AirportLocationCode": [
											"PUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Pau Uzein"
										]
									},
									{
										"AirportLocationCode": [
											"PUJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Punta Cana"
										]
									},
									{
										"AirportLocationCode": [
											"PUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Poulsbo"
										]
									},
									{
										"AirportLocationCode": [
											"PUQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Punta Arenas Pres Ibanez"
										]
									},
									{
										"AirportLocationCode": [
											"PUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Puerto Rico"
										]
									},
									{
										"AirportLocationCode": [
											"PUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Busan Gimhae"
										]
									},
									{
										"AirportLocationCode": [
											"PUT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Puttaprathi Airport"
										]
									},
									{
										"AirportLocationCode": [
											"PUW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Pullman Moscow Regional"
										]
									},
									{
										"AirportLocationCode": [
											"PUX"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Puerto Varas"
										]
									},
									{
										"AirportLocationCode": [
											"PUY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Pula"
										]
									},
									{
										"AirportLocationCode": [
											"PVC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Provincetown"
										]
									},
									{
										"AirportLocationCode": [
											"PVD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Providence T F Green"
										]
									},
									{
										"AirportLocationCode": [
											"PVG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shanghai Pu Dong"
										]
									},
									{
										"AirportLocationCode": [
											"PVH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Porto Velho Belmonte"
										]
									},
									{
										"AirportLocationCode": [
											"PVK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Preveza Lefkas Aktion"
										]
									},
									{
										"AirportLocationCode": [
											"PVR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Puerto Vallarta Ordaz"
										]
									},
									{
										"AirportLocationCode": [
											"PWM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Portland"
										]
									},
									{
										"AirportLocationCode": [
											"PWQ"
										],
										"CityLocationCode": [
											"PWQ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"KZ"
										],
										"Description": [
											"Pavlodar"
										]
									},
									{
										"AirportLocationCode": [
											"PXM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Puerto Escondido"
										]
									},
									{
										"AirportLocationCode": [
											"PXO"
										],
										"CityLocationCode": [
											"OPO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Porto Santo"
										]
									},
									{
										"AirportLocationCode": [
											"PYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Pyrgos"
										]
									},
									{
										"AirportLocationCode": [
											"PZB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Pietermaritzburg"
										]
									},
									{
										"AirportLocationCode": [
											"PZO"
										],
										"CityLocationCode": [
											"PZO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Puerto Ordaz"
										]
									},
									{
										"AirportLocationCode": [
											"QDU"
										],
										"CityLocationCode": [
											"DUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"DUSSELDORF HBF"
										]
									},
									{
										"AirportLocationCode": [
											"QGN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Tarragona, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"QIJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Gijón, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"QIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Mersin"
										]
									},
									{
										"AirportLocationCode": [
											"QJZ"
										],
										"CityLocationCode": [
											"NTE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"GARE DE NANTES ACCES NORD"
										]
									},
									{
										"AirportLocationCode": [
											"QKL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Colonia"
										]
									},
									{
										"AirportLocationCode": [
											"qlq"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Lleida, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"QOW"
										],
										"CityLocationCode": [
											"QOW"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Owerri"
										]
									},
									{
										"AirportLocationCode": [
											"QRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Queretaro"
										]
									},
									{
										"AirportLocationCode": [
											"QUA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Puttgarden"
										]
									},
									{
										"AirportLocationCode": [
											"QUI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Chuquicamata"
										]
									},
									{
										"AirportLocationCode": [
											"QUQ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Cáceres,RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"RAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Rabaul Tokua"
										]
									},
									{
										"AirportLocationCode": [
											"RAF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Ras An Naqb"
										]
									},
									{
										"AirportLocationCode": [
											"RAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Praia F Mendes"
										]
									},
									{
										"AirportLocationCode": [
											"RAJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Rajkot Civil"
										]
									},
									{
										"AirportLocationCode": [
											"RAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Marrakech Menara"
										]
									},
									{
										"AirportLocationCode": [
											"RAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Riverside"
										]
									},
									{
										"AirportLocationCode": [
											"RAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Ramingining"
										]
									},
									{
										"AirportLocationCode": [
											"RAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Ravenna"
										]
									},
									{
										"AirportLocationCode": [
											"RAO"
										],
										"CityLocationCode": [
											"RIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Ribeirao Preto Leite Lop"
										]
									},
									{
										"AirportLocationCode": [
											"RAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rapid City Regional"
										]
									},
									{
										"AirportLocationCode": [
											"RAR"
										],
										"CityLocationCode": [
											"RAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CK"
										],
										"Description": [
											"Rarotonga"
										]
									},
									{
										"AirportLocationCode": [
											"RBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Rabat Sale"
										]
									},
									{
										"AirportLocationCode": [
											"RBR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Rio Branco Pres Medici"
										]
									},
									{
										"AirportLocationCode": [
											"RCB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Richards Bay"
										]
									},
									{
										"AirportLocationCode": [
											"RCR"
										],
										"CityLocationCode": [
											"RCR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rochester Fulton County"
										]
									},
									{
										"AirportLocationCode": [
											"RCS"
										],
										"CityLocationCode": [
											"RCS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Rochester"
										]
									},
									{
										"AirportLocationCode": [
											"RDD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Redding"
										]
									},
									{
										"AirportLocationCode": [
											"RDG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Reading Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"RDM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Redmond Roberts Field"
										]
									},
									{
										"AirportLocationCode": [
											"RDN"
										],
										"CityLocationCode": [
											"RDN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"REDANG ISLAND"
										]
									},
									{
										"AirportLocationCode": [
											"RDU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Raleigh/Durham"
										]
									},
									{
										"AirportLocationCode": [
											"RDZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Rodez Marcillac"
										]
									},
									{
										"AirportLocationCode": [
											"REA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"Reao"
										]
									},
									{
										"AirportLocationCode": [
											"REB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Rechlin"
										]
									},
									{
										"AirportLocationCode": [
											"REC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Recife Guararapes"
										]
									},
									{
										"AirportLocationCode": [
											"RED"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Reedsville"
										]
									},
									{
										"AirportLocationCode": [
											"REG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Reggio Calabria T Menniti"
										]
									},
									{
										"AirportLocationCode": [
											"REI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GF"
										],
										"Description": [
											"Regina"
										]
									},
									{
										"AirportLocationCode": [
											"REK"
										],
										"CityLocationCode": [
											"REK"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Reykjavik"
										]
									},
									{
										"AirportLocationCode": [
											"REL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Trelew"
										]
									},
									{
										"AirportLocationCode": [
											"REN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Orenburg"
										]
									},
									{
										"AirportLocationCode": [
											"REP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KH"
										],
										"Description": [
											"Siem Reap"
										]
									},
									{
										"AirportLocationCode": [
											"RES"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Resistencia"
										]
									},
									{
										"AirportLocationCode": [
											"RET"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Rost Stolport"
										]
									},
									{
										"AirportLocationCode": [
											"REU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Reus"
										]
									},
									{
										"AirportLocationCode": [
											"REX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Reynosa General Lucio Blanco"
										]
									},
									{
										"AirportLocationCode": [
											"RFP"
										],
										"CityLocationCode": [
											"RFP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"RAIATEA"
										]
									},
									{
										"AirportLocationCode": [
											"RGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Rio Grande"
										]
									},
									{
										"AirportLocationCode": [
											"RGI"
										],
										"CityLocationCode": [
											"RGI"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PF"
										],
										"Description": [
											"RANGIROA"
										]
									},
									{
										"AirportLocationCode": [
											"RGL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Rio Gallegos"
										]
									},
									{
										"AirportLocationCode": [
											"RGN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MM"
										],
										"Description": [
											"Yangon Mingaladon"
										]
									},
									{
										"AirportLocationCode": [
											"RGS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Burgos"
										]
									},
									{
										"AirportLocationCode": [
											"RHD"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Termas de Rio Hondo"
										]
									},
									{
										"AirportLocationCode": [
											"RHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Reims"
										]
									},
									{
										"AirportLocationCode": [
											"RHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rhinelander Oneida County"
										]
									},
									{
										"AirportLocationCode": [
											"RHO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Rhodes Diagoras"
										]
									},
									{
										"AirportLocationCode": [
											"RIB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Riberalta General Buech"
										]
									},
									{
										"AirportLocationCode": [
											"RIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Richmond"
										]
									},
									{
										"AirportLocationCode": [
											"RII"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Soria, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"RIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Rodriguez De M"
										]
									},
									{
										"AirportLocationCode": [
											"RIO"
										],
										"CityLocationCode": [
											"RIO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Rio de Janeiro"
										]
									},
									{
										"AirportLocationCode": [
											"RIW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Riverton"
										]
									},
									{
										"AirportLocationCode": [
											"RIX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LV"
										],
										"Description": [
											"Riga"
										]
									},
									{
										"AirportLocationCode": [
											"RJK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Rijeka"
										]
									},
									{
										"AirportLocationCode": [
											"RJL"
										],
										"CityLocationCode": [
											"RJL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Agoncillo"
										]
									},
									{
										"AirportLocationCode": [
											"RKS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rock Springs Sweetwater"
										]
									},
									{
										"AirportLocationCode": [
											"RKV"
										],
										"CityLocationCode": [
											"REK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IS"
										],
										"Description": [
											"Reykjavik Domestic Airport"
										]
									},
									{
										"AirportLocationCode": [
											"RLG"
										],
										"CityLocationCode": [
											"RLG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Laage, Rostock"
										]
									},
									{
										"AirportLocationCode": [
											"RMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Roma"
										]
									},
									{
										"AirportLocationCode": [
											"RMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Rimini Miramare"
										]
									},
									{
										"AirportLocationCode": [
											"RMQ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TW"
										],
										"Description": [
											"Taichung"
										]
									},
									{
										"AirportLocationCode": [
											"RNB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Ronneby Kallinge"
										]
									},
									{
										"AirportLocationCode": [
											"RNN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Bornholm"
										]
									},
									{
										"AirportLocationCode": [
											"RNO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Reno Tahoe"
										]
									},
									{
										"AirportLocationCode": [
											"RNS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Rennes St Jacques"
										]
									},
									{
										"AirportLocationCode": [
											"ROA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Roanoke Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"ROB"
										],
										"CityLocationCode": [
											"MLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LR"
										],
										"Description": [
											"Roberts Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"ROC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rochester Monroe County"
										]
									},
									{
										"AirportLocationCode": [
											"ROD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Robertson"
										]
									},
									{
										"AirportLocationCode": [
											"ROI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Roi Et"
										]
									},
									{
										"AirportLocationCode": [
											"ROK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Rockhampton"
										]
									},
									{
										"AirportLocationCode": [
											"ROM"
										],
										"CityLocationCode": [
											"ROM"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Roma"
										]
									},
									{
										"AirportLocationCode": [
											"RON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Rondon"
										]
									},
									{
										"AirportLocationCode": [
											"ROR"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PW"
										],
										"Description": [
											"Airai"
										]
									},
									{
										"AirportLocationCode": [
											"ROS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Rosario Fisherton"
										]
									},
									{
										"AirportLocationCode": [
											"ROT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Rotorua"
										]
									},
									{
										"AirportLocationCode": [
											"ROU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Rousse"
										]
									},
									{
										"AirportLocationCode": [
											"ROV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Rostov"
										]
									},
									{
										"AirportLocationCode": [
											"ROW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Roswell Industrial"
										]
									},
									{
										"AirportLocationCode": [
											"RPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Raipur"
										]
									},
									{
										"AirportLocationCode": [
											"RRG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MU"
										],
										"Description": [
											"Rodrigues Is"
										]
									},
									{
										"AirportLocationCode": [
											"RSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Santa Rosa"
										]
									},
									{
										"AirportLocationCode": [
											"RSD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Rock Sound South Eleuthera"
										]
									},
									{
										"AirportLocationCode": [
											"RST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rochester Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"RSW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Myers SW Florida"
										]
									},
									{
										"AirportLocationCode": [
											"RTB"
										],
										"CityLocationCode": [
											"RTB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HN"
										],
										"Description": [
											"Roatan"
										]
									},
									{
										"AirportLocationCode": [
											"RTM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Rotterdam"
										]
									},
									{
										"AirportLocationCode": [
											"RTW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Saratov"
										]
									},
									{
										"AirportLocationCode": [
											"RUH"
										],
										"CityLocationCode": [
											"RUH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SA"
										],
										"Description": [
											"Riyadh King Khalid Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"RUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RE"
										],
										"Description": [
											"St Denis de la Reunion Gillot"
										]
									},
									{
										"AirportLocationCode": [
											"RUT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Rutland"
										]
									},
									{
										"AirportLocationCode": [
											"RVN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Rovaniemi"
										]
									},
									{
										"AirportLocationCode": [
											"RYG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Moss-Rygge"
										]
									},
									{
										"AirportLocationCode": [
											"RZE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Rzeszow Jasionka"
										]
									},
									{
										"AirportLocationCode": [
											"SAA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Saratoga"
										]
									},
									{
										"AirportLocationCode": [
											"SAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AN"
										],
										"Description": [
											"Saba"
										]
									},
									{
										"AirportLocationCode": [
											"SAC"
										],
										"CityLocationCode": [
											"SMF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sacramento Executive"
										]
									},
									{
										"AirportLocationCode": [
											"SAF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Santa Fe"
										]
									},
									{
										"AirportLocationCode": [
											"SAH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"YE"
										],
										"Description": [
											"Sanaa"
										]
									},
									{
										"AirportLocationCode": [
											"SAL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SV"
										],
										"Description": [
											"San Salvador- Comalapa Intl."
										]
									},
									{
										"AirportLocationCode": [
											"SAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Salamo"
										]
									},
									{
										"AirportLocationCode": [
											"SAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Diego Linberg Field"
										]
									},
									{
										"AirportLocationCode": [
											"SAO"
										],
										"CityLocationCode": [
											"SAO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Paulo"
										]
									},
									{
										"AirportLocationCode": [
											"SAP"
										],
										"CityLocationCode": [
											"SAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HN"
										],
										"Description": [
											"San Pedro Sula - Ramon Morales"
										]
									},
									{
										"AirportLocationCode": [
											"SAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sparta"
										]
									},
									{
										"AirportLocationCode": [
											"SAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Salton City"
										]
									},
									{
										"AirportLocationCode": [
											"SAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Antonio"
										]
									},
									{
										"AirportLocationCode": [
											"SAV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Savannah"
										]
									},
									{
										"AirportLocationCode": [
											"SAW"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Sabiha Gokcen"
										]
									},
									{
										"AirportLocationCode": [
											"SBA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Santa Barbara Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"SBH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GP"
										],
										"Description": [
											"St Barthelemy"
										]
									},
									{
										"AirportLocationCode": [
											"SBN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"South Bend Regional"
										]
									},
									{
										"AirportLocationCode": [
											"SBP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Luis Obispo County"
										]
									},
									{
										"AirportLocationCode": [
											"SBW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Sibu"
										]
									},
									{
										"AirportLocationCode": [
											"SBZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Sibiu"
										]
									},
									{
										"AirportLocationCode": [
											"SCE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"State College University Park"
										]
									},
									{
										"AirportLocationCode": [
											"SCH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Schenectady"
										]
									},
									{
										"AirportLocationCode": [
											"SCL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Santiago Arturo Merino Benitez"
										]
									},
									{
										"AirportLocationCode": [
											"SCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Saarbrucken Ensheim"
										]
									},
									{
										"AirportLocationCode": [
											"SCQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Santiago de Compostela"
										]
									},
									{
										"AirportLocationCode": [
											"SCU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Santiago Antoneo Maceo"
										]
									},
									{
										"AirportLocationCode": [
											"SCV"
										],
										"CityLocationCode": [
											"SCV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Suceava - Stefan Cel Mare"
										]
									},
									{
										"AirportLocationCode": [
											"SCY"
										],
										"CityLocationCode": [
											"SCY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"San Cristóbal Island"
										]
									},
									{
										"AirportLocationCode": [
											"SDD"
										],
										"CityLocationCode": [
											"SDD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AO"
										],
										"Description": [
											"Mukanka - Lubango"
										]
									},
									{
										"AirportLocationCode": [
											"SDE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Santiago del Estero"
										]
									},
									{
										"AirportLocationCode": [
											"SDF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Louisville"
										]
									},
									{
										"AirportLocationCode": [
											"SDJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Sendai"
										]
									},
									{
										"AirportLocationCode": [
											"SDK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Sandakan"
										]
									},
									{
										"AirportLocationCode": [
											"SDL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Sundsvall"
										]
									},
									{
										"AirportLocationCode": [
											"SDN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Sandane"
										]
									},
									{
										"AirportLocationCode": [
											"SDQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Santo Domingo Las Americas"
										]
									},
									{
										"AirportLocationCode": [
											"SDR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Santander"
										]
									},
									{
										"AirportLocationCode": [
											"SDU"
										],
										"CityLocationCode": [
											"RIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Rio de Janeiro Santos Dumont"
										]
									},
									{
										"AirportLocationCode": [
											"SDY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sidney Richland Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"SDZ"
										],
										"CityLocationCode": [
											"SDZ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Shetland Islands"
										]
									},
									{
										"AirportLocationCode": [
											"SEA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Seattle/Tacoma"
										]
									},
									{
										"AirportLocationCode": [
											"SEJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Salamanca, Renfe estación"
										]
									},
									{
										"AirportLocationCode": [
											"SEL"
										],
										"CityLocationCode": [
											"SEL"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Kimpo International Airport"
										]
									},
									{
										"AirportLocationCode": [
											"SEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Southend"
										]
									},
									{
										"AirportLocationCode": [
											"SEP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Stephenville"
										]
									},
									{
										"AirportLocationCode": [
											"SER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Seymour"
										]
									},
									{
										"AirportLocationCode": [
											"SEV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Severodoneck"
										]
									},
									{
										"AirportLocationCode": [
											"SEZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SC"
										],
										"Description": [
											"Mahe Island Seychelles"
										]
									},
									{
										"AirportLocationCode": [
											"SFA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Sfax El  Maou"
										]
									},
									{
										"AirportLocationCode": [
											"SFB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Orlando Sanford"
										]
									},
									{
										"AirportLocationCode": [
											"SFG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GP"
										],
										"Description": [
											"St Martin Esperance"
										]
									},
									{
										"AirportLocationCode": [
											"SFL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Sao Filipe"
										]
									},
									{
										"AirportLocationCode": [
											"SFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Santa Fe"
										]
									},
									{
										"AirportLocationCode": [
											"SFO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Francisco"
										]
									},
									{
										"AirportLocationCode": [
											"SFT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Skelleftea"
										]
									},
									{
										"AirportLocationCode": [
											"SGC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Surgut"
										]
									},
									{
										"AirportLocationCode": [
											"SGD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Sonderborg"
										]
									},
									{
										"AirportLocationCode": [
											"SGF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Springfield Branson RG"
										]
									},
									{
										"AirportLocationCode": [
											"SGN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Ho Chi Minh City"
										]
									},
									{
										"AirportLocationCode": [
											"SGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"St George"
										]
									},
									{
										"AirportLocationCode": [
											"SGU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"St George Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"SHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shanghai Hongqiao"
										]
									},
									{
										"AirportLocationCode": [
											"SHD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Staunton Shenandoah Val"
										]
									},
									{
										"AirportLocationCode": [
											"SHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shenyang"
										]
									},
									{
										"AirportLocationCode": [
											"SHI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Shimojishima"
										]
									},
									{
										"AirportLocationCode": [
											"SHJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Sharjah"
										]
									},
									{
										"AirportLocationCode": [
											"SHN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Shelton"
										]
									},
									{
										"AirportLocationCode": [
											"SHO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Sokcho Seolak"
										]
									},
									{
										"AirportLocationCode": [
											"SHR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sheridan"
										]
									},
									{
										"AirportLocationCode": [
											"SHV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Shreveport Regional"
										]
									},
									{
										"AirportLocationCode": [
											"SIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Xiguan"
										]
									},
									{
										"AirportLocationCode": [
											"SIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Sinop"
										]
									},
									{
										"AirportLocationCode": [
											"SID"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Sal Amilcar Cabral"
										]
									},
									{
										"AirportLocationCode": [
											"SIE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Sines"
										]
									},
									{
										"AirportLocationCode": [
											"SIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SG"
										],
										"Description": [
											"Singapore Changi"
										]
									},
									{
										"AirportLocationCode": [
											"SIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Smithton"
										]
									},
									{
										"AirportLocationCode": [
											"SIP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Simferopol"
										]
									},
									{
										"AirportLocationCode": [
											"SIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Sion"
										]
									},
									{
										"AirportLocationCode": [
											"SIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sitka"
										]
									},
									{
										"AirportLocationCode": [
											"SJC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Jose Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"SJD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"San Jose Cabo Los Cabos"
										]
									},
									{
										"AirportLocationCode": [
											"SJJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BA"
										],
										"Description": [
											"Sarajevo Butmir"
										]
									},
									{
										"AirportLocationCode": [
											"SJO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"San Jose Juan Santamaria"
										]
									},
									{
										"AirportLocationCode": [
											"SJP"
										],
										"CityLocationCode": [
											"SJP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Jose Do Rio Preto"
										]
									},
									{
										"AirportLocationCode": [
											"SJT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Angelo Mathis Field"
										]
									},
									{
										"AirportLocationCode": [
											"SJU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PR"
										],
										"Description": [
											"San Juan Luis Munoz Marin"
										]
									},
									{
										"AirportLocationCode": [
											"SJW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shijiazhuang Daguocun"
										]
									},
									{
										"AirportLocationCode": [
											"SJZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Sao Jorge Island"
										]
									},
									{
										"AirportLocationCode": [
											"SKB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KN"
										],
										"Description": [
											"St Kitts Robert L Bradshaw"
										]
									},
									{
										"AirportLocationCode": [
											"SKD"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UZ"
										],
										"Description": [
											"Samarkand"
										]
									},
									{
										"AirportLocationCode": [
											"SKE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Skien"
										]
									},
									{
										"AirportLocationCode": [
											"SKG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Thessaloniki Makedonia"
										]
									},
									{
										"AirportLocationCode": [
											"SKN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Stokmarknes Skagen"
										]
									},
									{
										"AirportLocationCode": [
											"SKO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Sokoto"
										]
									},
									{
										"AirportLocationCode": [
											"SKP"
										],
										"CityLocationCode": [
											"SKP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MK"
										],
										"Description": [
											"Alexander the Great Skopje"
										]
									},
									{
										"AirportLocationCode": [
											"SKT"
										],
										"CityLocationCode": [
											"SKT"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PK"
										],
										"Description": [
											"SIALKOT"
										]
									},
									{
										"AirportLocationCode": [
											"SKU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Skiros"
										]
									},
									{
										"AirportLocationCode": [
											"SKY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sandusky"
										]
									},
									{
										"AirportLocationCode": [
											"SLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Salta General Belgrano"
										]
									},
									{
										"AirportLocationCode": [
											"SLC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Salt Lake City"
										]
									},
									{
										"AirportLocationCode": [
											"SLD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SK"
										],
										"Description": [
											"Sliac"
										]
									},
									{
										"AirportLocationCode": [
											"SLI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZM"
										],
										"Description": [
											"Solwezi"
										]
									},
									{
										"AirportLocationCode": [
											"SLK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Saranac Lake Adirondack"
										]
									},
									{
										"AirportLocationCode": [
											"SLL"
										],
										"CityLocationCode": [
											"SLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"OM"
										],
										"Description": [
											"Salalah"
										]
									},
									{
										"AirportLocationCode": [
											"SLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Salamanca"
										]
									},
									{
										"AirportLocationCode": [
											"SLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Salem"
										]
									},
									{
										"AirportLocationCode": [
											"SLP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"San Luis Potosi"
										]
									},
									{
										"AirportLocationCode": [
											"SLU"
										],
										"CityLocationCode": [
											"SLU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"LC"
										],
										"Description": [
											"St Lucia"
										]
									},
									{
										"AirportLocationCode": [
											"SLW"
										],
										"CityLocationCode": [
											"SLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Saltillo"
										]
									},
									{
										"AirportLocationCode": [
											"SLZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Luiz Mal Cunha Macha"
										]
									},
									{
										"AirportLocationCode": [
											"SMA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Santa Maria Vila do Porto"
										]
									},
									{
										"AirportLocationCode": [
											"SMF"
										],
										"CityLocationCode": [
											"SMF"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sacramento"
										]
									},
									{
										"AirportLocationCode": [
											"SMI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Samos"
										]
									},
									{
										"AirportLocationCode": [
											"SMR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Santa Marta Simon Bolivar"
										]
									},
									{
										"AirportLocationCode": [
											"SMS"
										],
										"CityLocationCode": [
											"SMS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Sainte Marie"
										]
									},
									{
										"AirportLocationCode": [
											"SMX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Santa Maria Public"
										]
									},
									{
										"AirportLocationCode": [
											"SNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Santa Ana John Wayne"
										]
									},
									{
										"AirportLocationCode": [
											"SNE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Sao Nicolau"
										]
									},
									{
										"AirportLocationCode": [
											"SNN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Shannon"
										]
									},
									{
										"AirportLocationCode": [
											"SOB"
										],
										"CityLocationCode": [
											"SOB"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"HU"
										],
										"Description": [
											"Heviz"
										]
									},
									{
										"AirportLocationCode": [
											"SOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Solo City Adi Sumarmo"
										]
									},
									{
										"AirportLocationCode": [
											"SOE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CG"
										],
										"Description": [
											"Souanke"
										]
									},
									{
										"AirportLocationCode": [
											"SOF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Sofia"
										]
									},
									{
										"AirportLocationCode": [
											"SOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Sogndal Haukasen"
										]
									},
									{
										"AirportLocationCode": [
											"SOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Solomon"
										]
									},
									{
										"AirportLocationCode": [
											"SON"
										],
										"CityLocationCode": [
											"SON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VU"
										],
										"Description": [
											"Pekoa Espirito Santo"
										]
									},
									{
										"AirportLocationCode": [
											"SOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Southampton Eastleigh"
										]
									},
									{
										"AirportLocationCode": [
											"SPC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Santa Cruz de la Palma"
										]
									},
									{
										"AirportLocationCode": [
											"SPI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Springfield Capital"
										]
									},
									{
										"AirportLocationCode": [
											"SPK"
										],
										"CityLocationCode": [
											"SPK"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Sapporo"
										]
									},
									{
										"AirportLocationCode": [
											"SPL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Schiphol"
										]
									},
									{
										"AirportLocationCode": [
											"SPO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"San Pablo"
										]
									},
									{
										"AirportLocationCode": [
											"SPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BZ"
										],
										"Description": [
											"San Pedro"
										]
									},
									{
										"AirportLocationCode": [
											"SPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Wichita Falls Sheppard"
										]
									},
									{
										"AirportLocationCode": [
											"SPU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Split"
										]
									},
									{
										"AirportLocationCode": [
											"SQO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Storuman Gunnarn"
										]
									},
									{
										"AirportLocationCode": [
											"SRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Sucre"
										]
									},
									{
										"AirportLocationCode": [
											"SRG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Semarang Achmad Uani"
										]
									},
									{
										"AirportLocationCode": [
											"SRI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Samarinda"
										]
									},
									{
										"AirportLocationCode": [
											"SRP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Stord"
										]
									},
									{
										"AirportLocationCode": [
											"SRQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sarasota/Bradenton"
										]
									},
									{
										"AirportLocationCode": [
											"SRZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Santa Cruz El Trompillo"
										]
									},
									{
										"AirportLocationCode": [
											"SSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Salvador D.L.E. Magalhaes"
										]
									},
									{
										"AirportLocationCode": [
											"SSG"
										],
										"CityLocationCode": [
											"SSG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GQ"
										],
										"Description": [
											"Santa Isabel - Malabo"
										]
									},
									{
										"AirportLocationCode": [
											"SSH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EG"
										],
										"Description": [
											"Sharm El Sheikh Ophira"
										]
									},
									{
										"AirportLocationCode": [
											"SSJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Sandnessjoen Stokka"
										]
									},
									{
										"AirportLocationCode": [
											"SSM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sault Ste Marie"
										]
									},
									{
										"AirportLocationCode": [
											"SSX"
										],
										"CityLocationCode": [
											"SSX"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Samsun"
										]
									},
									{
										"AirportLocationCode": [
											"STA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DK"
										],
										"Description": [
											"Stauning"
										]
									},
									{
										"AirportLocationCode": [
											"STC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"St Cloud Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"STD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Santo Domingo Mayo Guerrero"
										]
									},
									{
										"AirportLocationCode": [
											"STE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Stevnspt Wiscr"
										]
									},
									{
										"AirportLocationCode": [
											"STG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"St George Island"
										]
									},
									{
										"AirportLocationCode": [
											"STI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DO"
										],
										"Description": [
											"Santiago"
										]
									},
									{
										"AirportLocationCode": [
											"STL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"St Louis Lambert"
										]
									},
									{
										"AirportLocationCode": [
											"STM"
										],
										"CityLocationCode": [
											"SAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Santarem Eduardo Gomes"
										]
									},
									{
										"AirportLocationCode": [
											"STN"
										],
										"CityLocationCode": [
											"LON"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"London Stansted"
										]
									},
									{
										"AirportLocationCode": [
											"STO"
										],
										"CityLocationCode": [
											"STO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Estocolmo"
										]
									},
									{
										"AirportLocationCode": [
											"STR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Stuttgart Echterdingen"
										]
									},
									{
										"AirportLocationCode": [
											"STT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VI"
										],
										"Description": [
											"St Thomas Island"
										]
									},
									{
										"AirportLocationCode": [
											"STU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BZ"
										],
										"Description": [
											"Santa Cruz"
										]
									},
									{
										"AirportLocationCode": [
											"STX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VI"
										],
										"Description": [
											"St Croix Henry E. Rohlsen"
										]
									},
									{
										"AirportLocationCode": [
											"SUB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Surabaya Juanda"
										]
									},
									{
										"AirportLocationCode": [
											"SUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sundance"
										]
									},
									{
										"AirportLocationCode": [
											"SUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Stroud"
										]
									},
									{
										"AirportLocationCode": [
											"SUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Lamezia Terme South Eufemia"
										]
									},
									{
										"AirportLocationCode": [
											"SUJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Satu Mare"
										]
									},
									{
										"AirportLocationCode": [
											"SUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Hailey Sun Valley"
										]
									},
									{
										"AirportLocationCode": [
											"SUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Summer Beaver"
										]
									},
									{
										"AirportLocationCode": [
											"SUV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FJ"
										],
										"Description": [
											"Suva Nausori"
										]
									},
									{
										"AirportLocationCode": [
											"SUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Sioux Gateway"
										]
									},
									{
										"AirportLocationCode": [
											"SVD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VC"
										],
										"Description": [
											"St Vincent"
										]
									},
									{
										"AirportLocationCode": [
											"SVG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Stavanger Sola"
										]
									},
									{
										"AirportLocationCode": [
											"SVJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Svolvaer Helle"
										]
									},
									{
										"AirportLocationCode": [
											"SVL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Savonlinna"
										]
									},
									{
										"AirportLocationCode": [
											"SVO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Moscú Sheremetyevo"
										]
									},
									{
										"AirportLocationCode": [
											"SVQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Sevilla"
										]
									},
									{
										"AirportLocationCode": [
											"SVR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KH"
										],
										"Description": [
											"Svay Rieng"
										]
									},
									{
										"AirportLocationCode": [
											"SVX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Ekaterinburg"
										]
									},
									{
										"AirportLocationCode": [
											"SVZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"San Antonio"
										]
									},
									{
										"AirportLocationCode": [
											"SWA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shantou"
										]
									},
									{
										"AirportLocationCode": [
											"SWD"
										],
										"CityLocationCode": [
											"SWD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Seward, Alaska"
										]
									},
									{
										"AirportLocationCode": [
											"SWF"
										],
										"CityLocationCode": [
											"POU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Poughkeepsie Newburgh"
										]
									},
									{
										"AirportLocationCode": [
											"SWI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Swindon"
										]
									},
									{
										"AirportLocationCode": [
											"SWS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Swansea Fairwood"
										]
									},
									{
										"AirportLocationCode": [
											"SXB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Strasbourg Entzheim"
										]
									},
									{
										"AirportLocationCode": [
											"SXF"
										],
										"CityLocationCode": [
											"BER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Berlín Schonefeld"
										]
									},
									{
										"AirportLocationCode": [
											"SXL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Sligo Collooney"
										]
									},
									{
										"AirportLocationCode": [
											"SXM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AN"
										],
										"Description": [
											"St Maarten Princ. Juliana"
										]
									},
									{
										"AirportLocationCode": [
											"SXR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Srinagar"
										]
									},
									{
										"AirportLocationCode": [
											"SYD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Sydney Kingsfordsmith"
										]
									},
									{
										"AirportLocationCode": [
											"SYL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"San Miguel"
										]
									},
									{
										"AirportLocationCode": [
											"SYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Syracuse Hancock"
										]
									},
									{
										"AirportLocationCode": [
											"SYT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Saint Yan"
										]
									},
									{
										"AirportLocationCode": [
											"SYX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Sanya"
										]
									},
									{
										"AirportLocationCode": [
											"SYY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Stornoway"
										]
									},
									{
										"AirportLocationCode": [
											"SYZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Shiraz"
										]
									},
									{
										"AirportLocationCode": [
											"SZB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kuala Lumpur Sultan Abdul Shah"
										]
									},
									{
										"AirportLocationCode": [
											"SZD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Sheffield"
										]
									},
									{
										"AirportLocationCode": [
											"SZF"
										],
										"CityLocationCode": [
											"SSX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Samsun Carsamba"
										]
									},
									{
										"AirportLocationCode": [
											"SZG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Salzburgo Wolfgang A Mozart"
										]
									},
									{
										"AirportLocationCode": [
											"SZV"
										],
										"CityLocationCode": [
											"SZV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Suzhou"
										]
									},
									{
										"AirportLocationCode": [
											"SZX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Shenzhen"
										]
									},
									{
										"AirportLocationCode": [
											"SZZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Szczecin Goleniow"
										]
									},
									{
										"AirportLocationCode": [
											"TAB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TT"
										],
										"Description": [
											"Tobago"
										]
									},
									{
										"AirportLocationCode": [
											"TAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Tacloban D Z Romualdez"
										]
									},
									{
										"AirportLocationCode": [
											"TAE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Daegu"
										]
									},
									{
										"AirportLocationCode": [
											"TAF"
										],
										"CityLocationCode": [
											"ORN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Oran - Tafaraqui"
										]
									},
									{
										"AirportLocationCode": [
											"TAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Tagbilaran"
										]
									},
									{
										"AirportLocationCode": [
											"TAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tanana Ralph Calhoun"
										]
									},
									{
										"AirportLocationCode": [
											"TAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Tampico General F Javier Mina"
										]
									},
									{
										"AirportLocationCode": [
											"TAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Tangalooma"
										]
									},
									{
										"AirportLocationCode": [
											"TAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Qingdao"
										]
									},
									{
										"AirportLocationCode": [
											"TAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Tapachula"
										]
									},
									{
										"AirportLocationCode": [
											"TAS"
										],
										"CityLocationCode": [
											"TAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UZ"
										],
										"Description": [
											"Vostohny"
										]
									},
									{
										"AirportLocationCode": [
											"TAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SK"
										],
										"Description": [
											"Tatry Poprad"
										]
									},
									{
										"AirportLocationCode": [
											"TAU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Tauramena"
										]
									},
									{
										"AirportLocationCode": [
											"TAW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UY"
										],
										"Description": [
											"Tacuarembo"
										]
									},
									{
										"AirportLocationCode": [
											"TAY"
										],
										"CityLocationCode": [
											"TAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EE"
										],
										"Description": [
											"Ulenurme, Tartu"
										]
									},
									{
										"AirportLocationCode": [
											"TBI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"The Bight"
										]
									},
									{
										"AirportLocationCode": [
											"TBJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Tabarka"
										]
									},
									{
										"AirportLocationCode": [
											"TBN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fort Leonard Wood Forney"
										]
									},
									{
										"AirportLocationCode": [
											"TBP"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Tumbes"
										]
									},
									{
										"AirportLocationCode": [
											"TBS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GE"
										],
										"Description": [
											"Tbilisi Novo Alexeyevka"
										]
									},
									{
										"AirportLocationCode": [
											"TBT"
										],
										"CityLocationCode": [
											"TBT"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Tabatinga"
										]
									},
									{
										"AirportLocationCode": [
											"TBU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TO"
										],
										"Description": [
											"Tonga"
										]
									},
									{
										"AirportLocationCode": [
											"TBZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Tabriz"
										]
									},
									{
										"AirportLocationCode": [
											"TCB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"Treasure Cay"
										]
									},
									{
										"AirportLocationCode": [
											"TCI"
										],
										"CityLocationCode": [
											"TCI"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Tenerife"
										]
									},
									{
										"AirportLocationCode": [
											"TCN"
										],
										"CityLocationCode": [
											"TCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"TEHUACAN"
										]
									},
									{
										"AirportLocationCode": [
											"TCQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Tacna"
										]
									},
									{
										"AirportLocationCode": [
											"TDD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Trinidad"
										]
									},
									{
										"AirportLocationCode": [
											"TDX"
										],
										"CityLocationCode": [
											"TDX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Trat Airport"
										]
									},
									{
										"AirportLocationCode": [
											"TEH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tetlin"
										]
									},
									{
										"AirportLocationCode": [
											"TEJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Teruel, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"TEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Telupid"
										]
									},
									{
										"AirportLocationCode": [
											"TEM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Temora"
										]
									},
									{
										"AirportLocationCode": [
											"TEN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Tongren"
										]
									},
									{
										"AirportLocationCode": [
											"TEP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Teptep"
										]
									},
									{
										"AirportLocationCode": [
											"TER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PT"
										],
										"Description": [
											"Terceira Island Lajes"
										]
									},
									{
										"AirportLocationCode": [
											"TEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Telluride"
										]
									},
									{
										"AirportLocationCode": [
											"TFN"
										],
										"CityLocationCode": [
											"TCI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Tenerife Norte Los Rodeos"
										]
									},
									{
										"AirportLocationCode": [
											"TFS"
										],
										"CityLocationCode": [
											"TCI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Tenerife Sur Reina Sofía"
										]
									},
									{
										"AirportLocationCode": [
											"TGD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"YU"
										],
										"Description": [
											"Podgorica Golubovci"
										]
									},
									{
										"AirportLocationCode": [
											"TGG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Kuala Terengganu Mahmood"
										]
									},
									{
										"AirportLocationCode": [
											"TGM"
										],
										"CityLocationCode": [
											"TGM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Tirgu Mures"
										]
									},
									{
										"AirportLocationCode": [
											"TGU"
										],
										"CityLocationCode": [
											"TGU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HN"
										],
										"Description": [
											"Tegucigalpa - Toncontin"
										]
									},
									{
										"AirportLocationCode": [
											"TGZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Tuxtla Gutierrez Llano San"
										]
									},
									{
										"AirportLocationCode": [
											"THA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tullahoma Northern"
										]
									},
									{
										"AirportLocationCode": [
											"THE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Teresina"
										]
									},
									{
										"AirportLocationCode": [
											"THF"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Berlín Tempelhof"
										]
									},
									{
										"AirportLocationCode": [
											"THN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Trollhattan"
										]
									},
									{
										"AirportLocationCode": [
											"THR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IR"
										],
										"Description": [
											"Tehran Mehrabad"
										]
									},
									{
										"AirportLocationCode": [
											"THS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Sukhothai"
										]
									},
									{
										"AirportLocationCode": [
											"TIA"
										],
										"CityLocationCode": [
											"TIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AL"
										],
										"Description": [
											"Nene Tereza Internacional"
										]
									},
									{
										"AirportLocationCode": [
											"TIJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Tijuana Rodriguez"
										]
									},
									{
										"AirportLocationCode": [
											"TIM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Tembagapura Timika"
										]
									},
									{
										"AirportLocationCode": [
											"TIN"
										],
										"CityLocationCode": [
											"TIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Tindouf"
										]
									},
									{
										"AirportLocationCode": [
											"TIP"
										],
										"CityLocationCode": [
											"TIP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LY"
										],
										"Description": [
											"Tripoli"
										]
									},
									{
										"AirportLocationCode": [
											"TIR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Tirupati"
										]
									},
									{
										"AirportLocationCode": [
											"TIU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Timaru"
										]
									},
									{
										"AirportLocationCode": [
											"TIV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"YU"
										],
										"Description": [
											"Tivat"
										]
									},
									{
										"AirportLocationCode": [
											"TJA"
										],
										"CityLocationCode": [
											"TJA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Tarija"
										]
									},
									{
										"AirportLocationCode": [
											"TJM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Tyumen"
										]
									},
									{
										"AirportLocationCode": [
											"TKN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tokunoshima"
										]
									},
									{
										"AirportLocationCode": [
											"TKS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tokushima"
										]
									},
									{
										"AirportLocationCode": [
											"TKU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Turku"
										]
									},
									{
										"AirportLocationCode": [
											"TLC"
										],
										"CityLocationCode": [
											"MEX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"TOLUCA"
										]
									},
									{
										"AirportLocationCode": [
											"TLH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tallahassee"
										]
									},
									{
										"AirportLocationCode": [
											"TLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EE"
										],
										"Description": [
											"Tallinn Ulemiste"
										]
									},
									{
										"AirportLocationCode": [
											"TLM"
										],
										"CityLocationCode": [
											"TLM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DZ"
										],
										"Description": [
											"Zenata Messali El Hadj, Tlemcen"
										]
									},
									{
										"AirportLocationCode": [
											"TLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Toulon Hyeres"
										]
									},
									{
										"AirportLocationCode": [
											"TLS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Toulouse Blagnac"
										]
									},
									{
										"AirportLocationCode": [
											"TLV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IL"
										],
										"Description": [
											"Tel Aviv"
										]
									},
									{
										"AirportLocationCode": [
											"TMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Tamatave"
										]
									},
									{
										"AirportLocationCode": [
											"TMP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Tampere"
										]
									},
									{
										"AirportLocationCode": [
											"TMS"
										],
										"CityLocationCode": [
											"TMS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ST"
										],
										"Description": [
											"Sao Tome Is"
										]
									},
									{
										"AirportLocationCode": [
											"TMW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Tamworth"
										]
									},
									{
										"AirportLocationCode": [
											"TNA"
										],
										"CityLocationCode": [
											"TNA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Jinan"
										]
									},
									{
										"AirportLocationCode": [
											"TNG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Tangier Boukhalef"
										]
									},
									{
										"AirportLocationCode": [
											"TNR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MG"
										],
										"Description": [
											"Antananarivo"
										]
									},
									{
										"AirportLocationCode": [
											"TOC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Toccoa"
										]
									},
									{
										"AirportLocationCode": [
											"TOD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Tioman"
										]
									},
									{
										"AirportLocationCode": [
											"TOE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Tozeur"
										]
									},
									{
										"AirportLocationCode": [
											"TOF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Tomsk"
										]
									},
									{
										"AirportLocationCode": [
											"TOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Toledo Express"
										]
									},
									{
										"AirportLocationCode": [
											"TOM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ML"
										],
										"Description": [
											"Tombouctou"
										]
									},
									{
										"AirportLocationCode": [
											"TOO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"San Vito"
										]
									},
									{
										"AirportLocationCode": [
											"TOR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Torrington"
										]
									},
									{
										"AirportLocationCode": [
											"TOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Tromso Langnes"
										]
									},
									{
										"AirportLocationCode": [
											"TOU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NC"
										],
										"Description": [
											"Touho"
										]
									},
									{
										"AirportLocationCode": [
											"TOW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Toledo"
										]
									},
									{
										"AirportLocationCode": [
											"TOY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Toyama"
										]
									},
									{
										"AirportLocationCode": [
											"TPA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tampa"
										]
									},
									{
										"AirportLocationCode": [
											"TPE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TW"
										],
										"Description": [
											"Taipei Chiang Kai Shek"
										]
									},
									{
										"AirportLocationCode": [
											"TPL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Temple"
										]
									},
									{
										"AirportLocationCode": [
											"TPP"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Tarapoto"
										]
									},
									{
										"AirportLocationCode": [
											"TPS"
										],
										"CityLocationCode": [
											"TPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Trapani"
										]
									},
									{
										"AirportLocationCode": [
											"TRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Taramajima Tarama"
										]
									},
									{
										"AirportLocationCode": [
											"TRC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Torreon"
										]
									},
									{
										"AirportLocationCode": [
											"TRD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Trondheim Vaernes"
										]
									},
									{
										"AirportLocationCode": [
											"TRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Tiree"
										]
									},
									{
										"AirportLocationCode": [
											"TRF"
										],
										"CityLocationCode": [
											"OSL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Oslo Torp Airport"
										]
									},
									{
										"AirportLocationCode": [
											"TRG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Tauranga"
										]
									},
									{
										"AirportLocationCode": [
											"TRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Turin Citta di Torino"
										]
									},
									{
										"AirportLocationCode": [
											"TRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Taree"
										]
									},
									{
										"AirportLocationCode": [
											"TRS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Trieste Dei Legionari"
										]
									},
									{
										"AirportLocationCode": [
											"TRU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Trujillo"
										]
									},
									{
										"AirportLocationCode": [
											"TRV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Thiruvananthapuram"
										]
									},
									{
										"AirportLocationCode": [
											"TRZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Tiruchirapally"
										]
									},
									{
										"AirportLocationCode": [
											"TSA"
										],
										"CityLocationCode": [
											"TPE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TW"
										],
										"Description": [
											"SUNG SHAN"
										]
									},
									{
										"AirportLocationCode": [
											"TSE"
										],
										"CityLocationCode": [
											"TSE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KZ"
										],
										"Description": [
											"Astana"
										]
									},
									{
										"AirportLocationCode": [
											"TSF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Venecia Treviso"
										]
									},
									{
										"AirportLocationCode": [
											"TSM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Taos"
										]
									},
									{
										"AirportLocationCode": [
											"TSN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Tianjin"
										]
									},
									{
										"AirportLocationCode": [
											"TSR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RO"
										],
										"Description": [
											"Timisoara"
										]
									},
									{
										"AirportLocationCode": [
											"TST"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Trang"
										]
									},
									{
										"AirportLocationCode": [
											"TSV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Townsville"
										]
									},
									{
										"AirportLocationCode": [
											"TTJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tottori"
										]
									},
									{
										"AirportLocationCode": [
											"TTN"
										],
										"CityLocationCode": [
											"PHL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Philadelphia Mercer County"
										]
									},
									{
										"AirportLocationCode": [
											"TTQ"
										],
										"CityLocationCode": [
											"TTQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CR"
										],
										"Description": [
											"Tortuquero"
										]
									},
									{
										"AirportLocationCode": [
											"TTU"
										],
										"CityLocationCode": [
											"TTU"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"TETOUAN"
										]
									},
									{
										"AirportLocationCode": [
											"TUC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Tucuman Benj Matienzo"
										]
									},
									{
										"AirportLocationCode": [
											"TUF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Tours Saint Symphorien"
										]
									},
									{
										"AirportLocationCode": [
											"TUL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tulsa"
										]
									},
									{
										"AirportLocationCode": [
											"TUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TN"
										],
										"Description": [
											"Túnez Carthage"
										]
									},
									{
										"AirportLocationCode": [
											"TUO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Taupo"
										]
									},
									{
										"AirportLocationCode": [
											"TUP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tupelo Lemons Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"TUR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Tucurui"
										]
									},
									{
										"AirportLocationCode": [
											"TUS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tucson"
										]
									},
									{
										"AirportLocationCode": [
											"TUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Tumbler Ridge"
										]
									},
									{
										"AirportLocationCode": [
											"TVC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Traverse City"
										]
									},
									{
										"AirportLocationCode": [
											"TWF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Twin Falls City County"
										]
									},
									{
										"AirportLocationCode": [
											"TWU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MY"
										],
										"Description": [
											"Tawau"
										]
									},
									{
										"AirportLocationCode": [
											"TXK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Texarkana Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"TXL"
										],
										"CityLocationCode": [
											"BER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Berlín Tegel"
										]
									},
									{
										"AirportLocationCode": [
											"TXN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Tunxi"
										]
									},
									{
										"AirportLocationCode": [
											"TYF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Torsby"
										]
									},
									{
										"AirportLocationCode": [
											"TYL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PE"
										],
										"Description": [
											"Talara"
										]
									},
									{
										"AirportLocationCode": [
											"TYN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Taiyuan"
										]
									},
									{
										"AirportLocationCode": [
											"TYO"
										],
										"CityLocationCode": [
											"TYO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Tokyo"
										]
									},
									{
										"AirportLocationCode": [
											"TYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Tyler Pounds Field"
										]
									},
									{
										"AirportLocationCode": [
											"TYS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Knoxville Mc Ghee Tyson"
										]
									},
									{
										"AirportLocationCode": [
											"TZX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Trabzon"
										]
									},
									{
										"AirportLocationCode": [
											"UAK"
										],
										"CityLocationCode": [
											"UAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GL"
										],
										"Description": [
											"Narsarsuaq"
										]
									},
									{
										"AirportLocationCode": [
											"UAQ"
										],
										"CityLocationCode": [
											"UAQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"SAN JUAN"
										]
									},
									{
										"AirportLocationCode": [
											"UBA"
										],
										"CityLocationCode": [
											"UBA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Uberaba"
										]
									},
									{
										"AirportLocationCode": [
											"UBP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Ubon Ratchathani Muang Ubon"
										]
									},
									{
										"AirportLocationCode": [
											"UDA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Undarra"
										]
									},
									{
										"AirportLocationCode": [
											"UDI"
										],
										"CityLocationCode": [
											"UDI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Eduardo Gomes - Uberlandia"
										]
									},
									{
										"AirportLocationCode": [
											"UDR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Udaipur Dabok"
										]
									},
									{
										"AirportLocationCode": [
											"UFA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Ufa"
										]
									},
									{
										"AirportLocationCode": [
											"UGR"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Burgos, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"UIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"EC"
										],
										"Description": [
											"Quito Mariscal"
										]
									},
									{
										"AirportLocationCode": [
											"UIP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Quimper Pluguffan"
										]
									},
									{
										"AirportLocationCode": [
											"UKS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Sevastopol"
										]
									},
									{
										"AirportLocationCode": [
											"UKY"
										],
										"CityLocationCode": [
											"UKY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Kyoto"
										]
									},
									{
										"AirportLocationCode": [
											"ULA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"San Julian"
										]
									},
									{
										"AirportLocationCode": [
											"ULE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Sule"
										]
									},
									{
										"AirportLocationCode": [
											"ULN"
										],
										"CityLocationCode": [
											"ULN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MN"
										],
										"Description": [
											"Buyant Uhaa,Ulaanbaatar"
										]
									},
									{
										"AirportLocationCode": [
											"ULS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Mulatos"
										]
									},
									{
										"AirportLocationCode": [
											"ULY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Ulyanosk"
										]
									},
									{
										"AirportLocationCode": [
											"UME"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Umea"
										]
									},
									{
										"AirportLocationCode": [
											"UMT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Umiat"
										]
									},
									{
										"AirportLocationCode": [
											"UNI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VC"
										],
										"Description": [
											"Union Island"
										]
									},
									{
										"AirportLocationCode": [
											"UPG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Ujung Pandang Hasanudin"
										]
									},
									{
										"AirportLocationCode": [
											"UPP"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"UPOLU POINT"
										]
									},
									{
										"AirportLocationCode": [
											"URA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KZ"
										],
										"Description": [
											"Uralsk"
										]
									},
									{
										"AirportLocationCode": [
											"URC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Urumqi"
										]
									},
									{
										"AirportLocationCode": [
											"URO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Rouen Boos"
										]
									},
									{
										"AirportLocationCode": [
											"URT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Surat Thani"
										]
									},
									{
										"AirportLocationCode": [
											"USH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"Ushuaia Islas Malvinas"
										]
									},
									{
										"AirportLocationCode": [
											"USM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Koh Samui"
										]
									},
									{
										"AirportLocationCode": [
											"USN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KR"
										],
										"Description": [
											"Ulsan"
										]
									},
									{
										"AirportLocationCode": [
											"UTC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NL"
										],
										"Description": [
											"Utrecht Soesterberg"
										]
									},
									{
										"AirportLocationCode": [
											"UTH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Udon Thani"
										]
									},
									{
										"AirportLocationCode": [
											"UTN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Upington Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"UTP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TH"
										],
										"Description": [
											"Utapao"
										]
									},
									{
										"AirportLocationCode": [
											"UTT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Umtata"
										]
									},
									{
										"AirportLocationCode": [
											"UVF"
										],
										"CityLocationCode": [
											"SLU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LC"
										],
										"Description": [
											"St Lucia Hewanorra"
										]
									},
									{
										"AirportLocationCode": [
											"VAA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Vaasa"
										]
									},
									{
										"AirportLocationCode": [
											"VAC"
										],
										"CityLocationCode": [
											"VAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Varrelbusch"
										]
									},
									{
										"AirportLocationCode": [
											"VAF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Valence"
										]
									},
									{
										"AirportLocationCode": [
											"VAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Varginha Maj Brig"
										]
									},
									{
										"AirportLocationCode": [
											"VAI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PG"
										],
										"Description": [
											"Vanimo"
										]
									},
									{
										"AirportLocationCode": [
											"VAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Valenca"
										]
									},
									{
										"AirportLocationCode": [
											"VAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Van"
										]
									},
									{
										"AirportLocationCode": [
											"VAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BG"
										],
										"Description": [
											"Varna"
										]
									},
									{
										"AirportLocationCode": [
											"VAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TR"
										],
										"Description": [
											"Sivas Airport"
										]
									},
									{
										"AirportLocationCode": [
											"VBS"
										],
										"CityLocationCode": [
											"VRN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Verona Montichiari"
										]
									},
									{
										"AirportLocationCode": [
											"VBY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Visby"
										]
									},
									{
										"AirportLocationCode": [
											"VCE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Venecia Marco Polo"
										]
									},
									{
										"AirportLocationCode": [
											"VCP"
										],
										"CityLocationCode": [
											"SAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Sao Paulo - Viracopos"
										]
									},
									{
										"AirportLocationCode": [
											"VCS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"Con Dao Island"
										]
									},
									{
										"AirportLocationCode": [
											"VDA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IL"
										],
										"Description": [
											"Ovda"
										]
									},
									{
										"AirportLocationCode": [
											"VDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NO"
										],
										"Description": [
											"Fagernes Valdres"
										]
									},
									{
										"AirportLocationCode": [
											"VDC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Vitoria Da Conquista"
										]
									},
									{
										"AirportLocationCode": [
											"VDE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valverde Hierro"
										]
									},
									{
										"AirportLocationCode": [
											"VDM"
										],
										"CityLocationCode": [
											"VDM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AR"
										],
										"Description": [
											"VIEDMA"
										]
									},
									{
										"AirportLocationCode": [
											"VEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Vernal"
										]
									},
									{
										"AirportLocationCode": [
											"VER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Veracruz Heriberto Jara"
										]
									},
									{
										"AirportLocationCode": [
											"VFA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZW"
										],
										"Description": [
											"Victoria Falls"
										]
									},
									{
										"AirportLocationCode": [
											"VGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Vigo"
										]
									},
									{
										"AirportLocationCode": [
											"VHM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Vilhelmina"
										]
									},
									{
										"AirportLocationCode": [
											"VIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Videira"
										]
									},
									{
										"AirportLocationCode": [
											"VIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Vicenza"
										]
									},
									{
										"AirportLocationCode": [
											"VIE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AT"
										],
										"Description": [
											"Viena"
										]
									},
									{
										"AirportLocationCode": [
											"VIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"El Vigia"
										]
									},
									{
										"AirportLocationCode": [
											"VII"
										],
										"CityLocationCode": [
											"VII"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VN"
										],
										"Description": [
											"VINH CITY"
										]
									},
									{
										"AirportLocationCode": [
											"VIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MA"
										],
										"Description": [
											"Dakhla"
										]
									},
									{
										"AirportLocationCode": [
											"VIO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Vitoria, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"VIS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Visalia"
										]
									},
									{
										"AirportLocationCode": [
											"VIT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Vitoria"
										]
									},
									{
										"AirportLocationCode": [
											"VIX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Vitoria Eurico Sales"
										]
									},
									{
										"AirportLocationCode": [
											"VJI"
										],
										"CityLocationCode": [
											"VJI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Virginia Highla,Abingdon"
										]
									},
									{
										"AirportLocationCode": [
											"VKO"
										],
										"CityLocationCode": [
											"MOW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Moscú - Vnukovo Intl"
										]
									},
									{
										"AirportLocationCode": [
											"VLA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Vandalia"
										]
									},
									{
										"AirportLocationCode": [
											"VLC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valencia"
										]
									},
									{
										"AirportLocationCode": [
											"VLD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Valdosta Regional"
										]
									},
									{
										"AirportLocationCode": [
											"VLF"
										],
										"CityLocationCode": [
											"NCL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Newcastle Unde"
										]
									},
									{
										"AirportLocationCode": [
											"VLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valladolid"
										]
									},
									{
										"AirportLocationCode": [
											"VLN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"VE"
										],
										"Description": [
											"Valencia"
										]
									},
									{
										"AirportLocationCode": [
											"VNO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LT"
										],
										"Description": [
											"Vilnius"
										]
									},
									{
										"AirportLocationCode": [
											"VNS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Varanasi"
										]
									},
									{
										"AirportLocationCode": [
											"VNX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MZ"
										],
										"Description": [
											"Vilanculos"
										]
									},
									{
										"AirportLocationCode": [
											"VOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Volgograd"
										]
									},
									{
										"AirportLocationCode": [
											"VOL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Almyros - Magnisia"
										]
									},
									{
										"AirportLocationCode": [
											"VOZ"
										],
										"CityLocationCode": [
											"VOZ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Voronezh"
										]
									},
									{
										"AirportLocationCode": [
											"VPS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Valparaiso Fort Walton Beach"
										]
									},
									{
										"AirportLocationCode": [
											"VRA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CU"
										],
										"Description": [
											"Varadero Juan Gualberto Gomez"
										]
									},
									{
										"AirportLocationCode": [
											"VRK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FI"
										],
										"Description": [
											"Varkaus"
										]
									},
									{
										"AirportLocationCode": [
											"VRN"
										],
										"CityLocationCode": [
											"VRN"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Verona"
										]
									},
									{
										"AirportLocationCode": [
											"VSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Villahermosa Cap C Perez"
										]
									},
									{
										"AirportLocationCode": [
											"VSG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UA"
										],
										"Description": [
											"Lugansk"
										]
									},
									{
										"AirportLocationCode": [
											"VST"
										],
										"CityLocationCode": [
											"STO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Estocolmo Vasteras Hasslo"
										]
									},
									{
										"AirportLocationCode": [
											"VTE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"LA"
										],
										"Description": [
											"Vientiane Wattay"
										]
									},
									{
										"AirportLocationCode": [
											"VTI"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Vitoria gasteiz"
										]
									},
									{
										"AirportLocationCode": [
											"VTZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IN"
										],
										"Description": [
											"Vishakhapatnam"
										]
									},
									{
										"AirportLocationCode": [
											"VUP"
										],
										"CityLocationCode": [
											"VUP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"Alfonso L Michelsen"
										]
									},
									{
										"AirportLocationCode": [
											"VVC"
										],
										"CityLocationCode": [
											"VVC"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CO"
										],
										"Description": [
											"VILLAVICENCIO"
										]
									},
									{
										"AirportLocationCode": [
											"VVI"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BO"
										],
										"Description": [
											"Santa Cruz Viru Viru"
										]
									},
									{
										"AirportLocationCode": [
											"VVO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Vladivostok"
										]
									},
									{
										"AirportLocationCode": [
											"VXE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CV"
										],
										"Description": [
											"Sao Vicente San Pedro"
										]
									},
									{
										"AirportLocationCode": [
											"VXO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SE"
										],
										"Description": [
											"Vaxjo"
										]
									},
									{
										"AirportLocationCode": [
											"WAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Wanganui"
										]
									},
									{
										"AirportLocationCode": [
											"WAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Chincoteague"
										]
									},
									{
										"AirportLocationCode": [
											"WAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Waverney"
										]
									},
									{
										"AirportLocationCode": [
											"WAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ID"
										],
										"Description": [
											"Waris"
										]
									},
									{
										"AirportLocationCode": [
											"WAS"
										],
										"CityLocationCode": [
											"WAS"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Washington"
										]
									},
									{
										"AirportLocationCode": [
											"WAT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IE"
										],
										"Description": [
											"Waterford"
										]
									},
									{
										"AirportLocationCode": [
											"WAW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Varsovia Okecie"
										]
									},
									{
										"AirportLocationCode": [
											"WAY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Waynesburg"
										]
									},
									{
										"AirportLocationCode": [
											"WDG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Enid Woodring Municipal"
										]
									},
									{
										"AirportLocationCode": [
											"WDH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Windhoek Hosea Kutako International"
										]
									},
									{
										"AirportLocationCode": [
											"WEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ZA"
										],
										"Description": [
											"Welkom"
										]
									},
									{
										"AirportLocationCode": [
											"WGA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Wagga Wagga Forest Hill"
										]
									},
									{
										"AirportLocationCode": [
											"WHK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Whakatane"
										]
									},
									{
										"AirportLocationCode": [
											"WIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"UK"
										],
										"Description": [
											"Wick"
										]
									},
									{
										"AirportLocationCode": [
											"WIL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"KE"
										],
										"Description": [
											"Nairobi Wilson"
										]
									},
									{
										"AirportLocationCode": [
											"WIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Winton"
										]
									},
									{
										"AirportLocationCode": [
											"WJF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Lancaster West J Fox"
										]
									},
									{
										"AirportLocationCode": [
											"WKJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Wakkanai Hokkaido"
										]
									},
									{
										"AirportLocationCode": [
											"WLG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Wellington"
										]
									},
									{
										"AirportLocationCode": [
											"WNZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Wenzhou"
										]
									},
									{
										"AirportLocationCode": [
											"WOL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AU"
										],
										"Description": [
											"Wollongong"
										]
									},
									{
										"AirportLocationCode": [
											"WOO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Woodchopper"
										]
									},
									{
										"AirportLocationCode": [
											"WRE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Whangarei"
										]
									},
									{
										"AirportLocationCode": [
											"WRO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PL"
										],
										"Description": [
											"Wroclaw Strachowice"
										]
									},
									{
										"AirportLocationCode": [
											"WUH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Wuhan"
										]
									},
									{
										"AirportLocationCode": [
											"WUX"
										],
										"CityLocationCode": [
											"WUX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Wuxi Airport"
										]
									},
									{
										"AirportLocationCode": [
											"WVB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NA"
										],
										"Description": [
											"Walvis Bay Rooikop"
										]
									},
									{
										"AirportLocationCode": [
											"WVN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"WILHELMSHAVEN"
										]
									},
									{
										"AirportLocationCode": [
											"WYS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"US"
										],
										"Description": [
											"WEST YELLOWSTONE"
										]
									},
									{
										"AirportLocationCode": [
											"XAP"
										],
										"CityLocationCode": [
											"XAP"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Chapeco"
										]
									},
									{
										"AirportLocationCode": [
											"XCR"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Vatry"
										]
									},
									{
										"AirportLocationCode": [
											"XDB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Rail Link"
										]
									},
									{
										"AirportLocationCode": [
											"XDS"
										],
										"CityLocationCode": [
											"YOW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"OTTAWA RAIL STATION"
										]
									},
									{
										"AirportLocationCode": [
											"XER"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"sxb"
										]
									},
									{
										"AirportLocationCode": [
											"XFN"
										],
										"CityLocationCode": [
											"XFN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"XIANGFAN"
										]
									},
									{
										"AirportLocationCode": [
											"XIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Guelph"
										]
									},
									{
										"AirportLocationCode": [
											"XIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BR"
										],
										"Description": [
											"Xinguera"
										]
									},
									{
										"AirportLocationCode": [
											"XIV"
										],
										"CityLocationCode": [
											"VLL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valladolid RENFE Campo Grande"
										]
									},
									{
										"AirportLocationCode": [
											"XIY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Xianyang"
										]
									},
									{
										"AirportLocationCode": [
											"XJI"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Ciudad Real, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"XJJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Albacete"
										]
									},
									{
										"AirportLocationCode": [
											"XJN"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valladolid"
										]
									},
									{
										"AirportLocationCode": [
											"XJO"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Soria"
										]
									},
									{
										"AirportLocationCode": [
											"XJR"
										],
										"CityLocationCode": [
											"XJR"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Lorca, Estación de Bus"
										]
									},
									{
										"AirportLocationCode": [
											"XJU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Burgos"
										]
									},
									{
										"AirportLocationCode": [
											"XLS"
										],
										"CityLocationCode": [
											"XLS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SN"
										],
										"Description": [
											"Saint Louis"
										]
									},
									{
										"AirportLocationCode": [
											"XLW"
										],
										"CityLocationCode": [
											"XLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Lemwerder"
										]
									},
									{
										"AirportLocationCode": [
											"XMN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Xiamen"
										]
									},
									{
										"AirportLocationCode": [
											"XNA"
										],
										"CityLocationCode": [
											"FYV"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Fayetteville Northwest Arkansas"
										]
									},
									{
										"AirportLocationCode": [
											"XNB"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Dubai"
										]
									},
									{
										"AirportLocationCode": [
											"XNN"
										],
										"CityLocationCode": [
											"XNN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Xining"
										]
									},
									{
										"AirportLocationCode": [
											"XOC"
										],
										"CityLocationCode": [
											"MAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Madrid RENFE Atocha"
										]
									},
									{
										"AirportLocationCode": [
											"XOJ"
										],
										"CityLocationCode": [
											"ODB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Córdoba RENFE Central"
										]
									},
									{
										"AirportLocationCode": [
											"XOU"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Segovia, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"XQA"
										],
										"CityLocationCode": [
											"SVQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Sevilla RENFE Santa Justa"
										]
									},
									{
										"AirportLocationCode": [
											"XRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Jerez de la Frontera La Parra"
										]
									},
									{
										"AirportLocationCode": [
											"XSP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SG"
										],
										"Description": [
											"Singapore Seletar"
										]
									},
									{
										"AirportLocationCode": [
											"XTI"
										],
										"CityLocationCode": [
											"MAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Madrid RENFE Chamartin"
										]
									},
									{
										"AirportLocationCode": [
											"XTJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Toledo, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"XUA"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Huesca, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"XUT"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Murcia, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"XVY"
										],
										"CityLocationCode": [
											"VCE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"VENEZIA MESTRE"
										]
									},
									{
										"AirportLocationCode": [
											"XWG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"Estrasburgo"
										]
									},
									{
										"AirportLocationCode": [
											"xxx"
										],
										"CityLocationCode": [
											"xxx"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"prueba"
										]
									},
									{
										"AirportLocationCode": [
											"XYD"
										],
										"CityLocationCode": [
											"LYS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"FR"
										],
										"Description": [
											"LYON PART DIEU"
										]
									},
									{
										"AirportLocationCode": [
											"XZZ"
										],
										"CityLocationCode": [
											"ZAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Zaragoza RENFE Delicias"
										]
									},
									{
										"AirportLocationCode": [
											"YAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Fort Frances"
										]
									},
									{
										"AirportLocationCode": [
											"YAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Yakutat"
										]
									},
									{
										"AirportLocationCode": [
											"YAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Sault Ste Marie"
										]
									},
									{
										"AirportLocationCode": [
											"YAO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CM"
										],
										"Description": [
											"Yaounde Airport"
										]
									},
									{
										"AirportLocationCode": [
											"YBC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Baie Comeau"
										]
									},
									{
										"AirportLocationCode": [
											"YBG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Bagotville"
										]
									},
									{
										"AirportLocationCode": [
											"YBL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Campbell River"
										]
									},
									{
										"AirportLocationCode": [
											"YCC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Cornwall"
										]
									},
									{
										"AirportLocationCode": [
											"YCD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Nanaimo"
										]
									},
									{
										"AirportLocationCode": [
											"YCG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Castlegar"
										]
									},
									{
										"AirportLocationCode": [
											"YDF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Deer Lake"
										]
									},
									{
										"AirportLocationCode": [
											"YDQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Dawson Creek"
										]
									},
									{
										"AirportLocationCode": [
											"YEA"
										],
										"CityLocationCode": [
											"YEA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Edmonton"
										]
									},
									{
										"AirportLocationCode": [
											"YEG"
										],
										"CityLocationCode": [
											"YEA"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Edmonton"
										]
									},
									{
										"AirportLocationCode": [
											"YEL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Elliot Lake"
										]
									},
									{
										"AirportLocationCode": [
											"YER"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Fort Severn"
										]
									},
									{
										"AirportLocationCode": [
											"YFB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Iqaluit"
										]
									},
									{
										"AirportLocationCode": [
											"YFC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Fredericton"
										]
									},
									{
										"AirportLocationCode": [
											"YGJ"
										],
										"CityLocationCode": [
											"YGJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"JP"
										],
										"Description": [
											"Miho, Yonago"
										]
									},
									{
										"AirportLocationCode": [
											"YGK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Kingston"
										]
									},
									{
										"AirportLocationCode": [
											"YGR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Iles de la Madeleine"
										]
									},
									{
										"AirportLocationCode": [
											"YHD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Dryden"
										]
									},
									{
										"AirportLocationCode": [
											"YHM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Hamilton"
										]
									},
									{
										"AirportLocationCode": [
											"YHZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Halifax"
										]
									},
									{
										"AirportLocationCode": [
											"YIC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Isachsen"
										]
									},
									{
										"AirportLocationCode": [
											"YIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yichang"
										]
									},
									{
										"AirportLocationCode": [
											"YIN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yining"
										]
									},
									{
										"AirportLocationCode": [
											"YIW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yiwu"
										]
									},
									{
										"AirportLocationCode": [
											"YJB"
										],
										"CityLocationCode": [
											"BCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Barcelona RENFE Sants"
										]
									},
									{
										"AirportLocationCode": [
											"YJC"
										],
										"CityLocationCode": [
											"LCG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"A Coruña RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJD"
										],
										"CityLocationCode": [
											"BCN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Barcelona RENFE Franca"
										]
									},
									{
										"AirportLocationCode": [
											"YJE"
										],
										"CityLocationCode": [
											"ALC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Alicante RENFE Terminal"
										]
									},
									{
										"AirportLocationCode": [
											"YJG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Granada, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJH"
										],
										"CityLocationCode": [
											"EAS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"San Sebastián RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJI"
										],
										"CityLocationCode": [
											"BIO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Bilbao RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJL"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"San Sebastián, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJM"
										],
										"CityLocationCode": [
											"AGP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Málaga RENFE Mario Zambrano"
										]
									},
									{
										"AirportLocationCode": [
											"YJR"
										],
										"CityLocationCode": [
											"VGO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Vigo RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJV"
										],
										"CityLocationCode": [
											"VLC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Valencia RENFE Estación"
										]
									},
									{
										"AirportLocationCode": [
											"YJW"
										],
										"CityLocationCode": [
											"XRY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Jerez RENFE EStación"
										]
									},
									{
										"AirportLocationCode": [
											"YKA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Kamloops"
										]
									},
									{
										"AirportLocationCode": [
											"YKM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Yakima Air Terminal"
										]
									},
									{
										"AirportLocationCode": [
											"YKS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RU"
										],
										"Description": [
											"Yakutsk"
										]
									},
									{
										"AirportLocationCode": [
											"YLW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Kelowna"
										]
									},
									{
										"AirportLocationCode": [
											"YMM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Fort McMurray"
										]
									},
									{
										"AirportLocationCode": [
											"YMQ"
										],
										"CityLocationCode": [
											"YMQ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Montreal"
										]
									},
									{
										"AirportLocationCode": [
											"YMX"
										],
										"CityLocationCode": [
											"YMQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Montreal Mirabel"
										]
									},
									{
										"AirportLocationCode": [
											"YNJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yanji"
										]
									},
									{
										"AirportLocationCode": [
											"YNT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Yantai Laishan"
										]
									},
									{
										"AirportLocationCode": [
											"YNZ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Nanyang"
										]
									},
									{
										"AirportLocationCode": [
											"YOG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Ogoki"
										]
									},
									{
										"AirportLocationCode": [
											"YOW"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Ottawa Rockcliffe"
										]
									},
									{
										"AirportLocationCode": [
											"YPR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Prince Rupert Digby Island"
										]
									},
									{
										"AirportLocationCode": [
											"YQB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Quebec"
										]
									},
									{
										"AirportLocationCode": [
											"YQD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"The Pas"
										]
									},
									{
										"AirportLocationCode": [
											"YQG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Windsor"
										]
									},
									{
										"AirportLocationCode": [
											"YQL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Lethbridge"
										]
									},
									{
										"AirportLocationCode": [
											"YQM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Moncton"
										]
									},
									{
										"AirportLocationCode": [
											"YQQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Comox"
										]
									},
									{
										"AirportLocationCode": [
											"YQR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Regina"
										]
									},
									{
										"AirportLocationCode": [
											"YQT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Thunder Bay"
										]
									},
									{
										"AirportLocationCode": [
											"YQU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Grande Prairie"
										]
									},
									{
										"AirportLocationCode": [
											"YQX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Gander"
										]
									},
									{
										"AirportLocationCode": [
											"YQY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Sydney"
										]
									},
									{
										"AirportLocationCode": [
											"YSB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Sudbury"
										]
									},
									{
										"AirportLocationCode": [
											"YSJ"
										],
										"CityLocationCode": [
											"YSJ"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"St John"
										]
									},
									{
										"AirportLocationCode": [
											"YTO"
										],
										"CityLocationCode": [
											"YTO"
										],
										"IsCity": [
											"true"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Toronto"
										]
									},
									{
										"AirportLocationCode": [
											"YTS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Timmins"
										]
									},
									{
										"AirportLocationCode": [
											"YTY"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"TAIZHOU, YANGZHOU"
										]
									},
									{
										"AirportLocationCode": [
											"YTZ"
										],
										"CityLocationCode": [
											"YTO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Toronto Island"
										]
									},
									{
										"AirportLocationCode": [
											"YUD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Umiujaq"
										]
									},
									{
										"AirportLocationCode": [
											"YUL"
										],
										"CityLocationCode": [
											"YMQ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Montreal Dorval"
										]
									},
									{
										"AirportLocationCode": [
											"YUM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Yuma"
										]
									},
									{
										"AirportLocationCode": [
											"YUN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Johnson Point"
										]
									},
									{
										"AirportLocationCode": [
											"YUY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Rouyn"
										]
									},
									{
										"AirportLocationCode": [
											"YVR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Vancouver"
										]
									},
									{
										"AirportLocationCode": [
											"YWG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Winnipeg"
										]
									},
									{
										"AirportLocationCode": [
											"YWK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Wabush"
										]
									},
									{
										"AirportLocationCode": [
											"YWL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Williams Lake"
										]
									},
									{
										"AirportLocationCode": [
											"YWS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Whistler"
										]
									},
									{
										"AirportLocationCode": [
											"YXC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Cranbrook"
										]
									},
									{
										"AirportLocationCode": [
											"YXE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Saskatoon"
										]
									},
									{
										"AirportLocationCode": [
											"YXH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Medicine Hat"
										]
									},
									{
										"AirportLocationCode": [
											"YXJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Fort St John"
										]
									},
									{
										"AirportLocationCode": [
											"YXL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Sioux Lookout"
										]
									},
									{
										"AirportLocationCode": [
											"YXS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Prince George"
										]
									},
									{
										"AirportLocationCode": [
											"YXT"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Terrace"
										]
									},
									{
										"AirportLocationCode": [
											"YXU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"London"
										]
									},
									{
										"AirportLocationCode": [
											"YXX"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Abbotsford"
										]
									},
									{
										"AirportLocationCode": [
											"YXY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Whitehorse"
										]
									},
									{
										"AirportLocationCode": [
											"YYB"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"North Bay"
										]
									},
									{
										"AirportLocationCode": [
											"YYC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Calgary"
										]
									},
									{
										"AirportLocationCode": [
											"YYD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Smithers"
										]
									},
									{
										"AirportLocationCode": [
											"YYF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Penticton"
										]
									},
									{
										"AirportLocationCode": [
											"YYG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Charlottetown"
										]
									},
									{
										"AirportLocationCode": [
											"YYJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Victoria"
										]
									},
									{
										"AirportLocationCode": [
											"YYR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Goose Bay"
										]
									},
									{
										"AirportLocationCode": [
											"YYT"
										],
										"CityLocationCode": [
											"YSJ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"St Johns"
										]
									},
									{
										"AirportLocationCode": [
											"YYY"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Mont Joli"
										]
									},
									{
										"AirportLocationCode": [
											"YYZ"
										],
										"CityLocationCode": [
											"YTO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Toronto Pearson"
										]
									},
									{
										"AirportLocationCode": [
											"YZF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Yellowknife"
										]
									},
									{
										"AirportLocationCode": [
											"YZR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Sarnia"
										]
									},
									{
										"AirportLocationCode": [
											"ZAC"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"York Landing"
										]
									},
									{
										"AirportLocationCode": [
											"ZAD"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Zadar"
										]
									},
									{
										"AirportLocationCode": [
											"ZAG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"HR"
										],
										"Description": [
											"Zagreb Pleso"
										]
									},
									{
										"AirportLocationCode": [
											"ZAK"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Chiusa Klausen"
										]
									},
									{
										"AirportLocationCode": [
											"ZAL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Valdivia Pichoy"
										]
									},
									{
										"AirportLocationCode": [
											"ZAM"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"PH"
										],
										"Description": [
											"Zamboanga"
										]
									},
									{
										"AirportLocationCode": [
											"ZAN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Aghios Nicolao"
										]
									},
									{
										"AirportLocationCode": [
											"ZAP"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Appenzell"
										]
									},
									{
										"AirportLocationCode": [
											"ZAR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NG"
										],
										"Description": [
											"Zaria"
										]
									},
									{
										"AirportLocationCode": [
											"ZAZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Zaragoza"
										]
									},
									{
										"AirportLocationCode": [
											"ZBF"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CA"
										],
										"Description": [
											"Bathurst"
										]
									},
									{
										"AirportLocationCode": [
											"ZBZ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Bad Salzungen"
										]
									},
									{
										"AirportLocationCode": [
											"ZCO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Temuco Maquehue"
										]
									},
									{
										"AirportLocationCode": [
											"ZFV"
										],
										"CityLocationCode": [
											"PHL"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"US"
										],
										"Description": [
											"Philadelphia Rail"
										]
									},
									{
										"AirportLocationCode": [
											"ZHA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CN"
										],
										"Description": [
											"Zhanjiang"
										]
									},
									{
										"AirportLocationCode": [
											"ZHE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Frauenfeld"
										]
									},
									{
										"AirportLocationCode": [
											"ZHU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Kreuzlingen"
										]
									},
									{
										"AirportLocationCode": [
											"ZIA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"Trento"
										]
									},
									{
										"AirportLocationCode": [
											"ZIG"
										],
										"CityLocationCode": [
											"ZIG"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"SN"
										],
										"Description": [
											"Ziguinchor"
										]
									},
									{
										"AirportLocationCode": [
											"ZIH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Ixtapa/Zihuatanejo"
										]
									},
									{
										"AirportLocationCode": [
											"ZLO"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MX"
										],
										"Description": [
											"Manzanillo"
										]
									},
									{
										"AirportLocationCode": [
											"ZMS"
										],
										"CityLocationCode": [
											"FLR"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"IT"
										],
										"Description": [
											"FLORENCE SANTA MARIA NOVELLA STATION"
										]
									},
									{
										"AirportLocationCode": [
											"ZNZ"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"TZ"
										],
										"Description": [
											"Zanzibar Kisauni"
										]
									},
									{
										"AirportLocationCode": [
											"ZOB"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Zamora, RENFE estación"
										]
									},
									{
										"AirportLocationCode": [
											"ZOS"
										],
										"CityLocationCode": [
											"ZOS"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CL"
										],
										"Description": [
											"Osorno"
										]
									},
									{
										"AirportLocationCode": [
											"ZQN"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"NZ"
										],
										"Description": [
											"Queenstown Frankton"
										]
									},
									{
										"AirportLocationCode": [
											"ZRG"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"ES"
										],
										"Description": [
											"Zaragoza"
										]
									},
									{
										"AirportLocationCode": [
											"ZRH"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"CH"
										],
										"Description": [
											"Zurich"
										]
									},
									{
										"AirportLocationCode": [
											"ZSA"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BS"
										],
										"Description": [
											"San Salvador"
										]
									},
									{
										"AirportLocationCode": [
											"ZSE"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"RE"
										],
										"Description": [
											"St Pierre de la Reunion"
										]
									},
									{
										"AirportLocationCode": [
											"ZTH"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"GR"
										],
										"Description": [
											"Zakinthos"
										]
									},
									{
										"AirportLocationCode": [
											"ZVJ"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"AE"
										],
										"Description": [
											"Abu Dhabi estación de autobuses"
										]
									},
									{
										"AirportLocationCode": [
											"ZWS"
										],
										"CityLocationCode": [
											""
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"DE"
										],
										"Description": [
											"Stuttgart"
										]
									},
									{
										"AirportLocationCode": [
											"ZYR"
										],
										"CityLocationCode": [
											"BRU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"BE"
										],
										"Description": [
											"BRUSSELS MIDI STN"
										]
									},
									{
										"AirportLocationCode": [
											"ZZU"
										],
										"IsCity": [
											"false"
										],
										"Country": [
											"MW"
										],
										"Description": [
											"Mzuzu"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}