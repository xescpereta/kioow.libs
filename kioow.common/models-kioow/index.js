﻿module.exports = function (core, dbname) {

    require('./affiliate')(core, dbname);
    require('./bookings')(core, dbname);
    require('./chat')(core, dbname);
    require('./destinationcountryzones')(core, dbname);
    require('./destinationcountry')(core, dbname);
    require('./destinationcity')(core, dbname);
    require('./dmcproduct')(core, dbname);
    require('./dmcproductremoved')(core, dbname);
    require('./dmc')(core, dbname);
    require('./mail')(core, dbname);
    require('./migrationproduct')(core, dbname);
    require('./omtrequests')(core, dbname);
    require('./permissions')(core, dbname);
    require('./quotes')(core, dbname);
    require('./roles')(core, dbname);
    require('./sequence')(core, dbname);
    require('./hevents')(core, dbname);
    require('./subscriptionmessages')(core, dbname);
    require('./traveler')(core, dbname);
    require('./userquery')(core, dbname);
    require('./admin')(core, dbname);
    require('./users')(core, dbname);

}
