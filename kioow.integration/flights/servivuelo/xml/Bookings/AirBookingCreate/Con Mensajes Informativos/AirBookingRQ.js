module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance"
		},
		"soap:Body": [
			{
				"AirBookingCreate": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingRQ": [
							{
								"AvailRequestID": [
									"2930464"
								],
								"AirTravelers": [
									{
										"AirTraveler": [
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"PAS"
												],
												"TravelerID": [
													"1"
												],
												"InfantID": [
													"0"
												],
												"FirstName": [
													"PrimerNombre"
												],
												"LastName": [
													"Apellidos"
												],
												"DocumentNumber": [
													"29216776Z"
												],
												"Email": [
													""
												],
												"Phone": [
													""
												],
												"BirthDate": [
													"07/09/1986"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"ResidentDocumentType": [
													{
														"$": {
															"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance",
															"p4:nil": "true"
														}
													}
												],
												"ResidentCityCode": [
													""
												],
												"ResidentCertificateNumber": [
													""
												],
												"TSAData": [
													{
														"BirthDate": [
															{
																"$": {
																	"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance",
																	"p5:nil": "true"
																}
															}
														],
														"DocumentExpirationDate": [
															{
																"$": {
																	"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance",
																	"p5:nil": "true"
																}
															}
														]
													}
												]
											},
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mrs"
												],
												"DocumentType": [
													"PAS"
												],
												"TravelerID": [
													"2"
												],
												"InfantID": [
													"0"
												],
												"FirstName": [
													"Nombre"
												],
												"LastName": [
													"Apellido"
												],
												"DocumentNumber": [
													"00000000H"
												],
												"Email": [
													""
												],
												"Phone": [
													""
												],
												"BirthDate": [
													"09/09/1986"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"ResidentDocumentType": [
													{
														"$": {
															"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance",
															"p4:nil": "true"
														}
													}
												],
												"ResidentCityCode": [
													""
												],
												"ResidentCertificateNumber": [
													""
												],
												"TSAData": [
													{
														"BirthDate": [
															{
																"$": {
																	"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance",
																	"p5:nil": "true"
																}
															}
														],
														"DocumentExpirationDate": [
															{
																"$": {
																	"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance",
																	"p5:nil": "true"
																}
															}
														]
													}
												]
											}
										]
									}
								],
								"IssuerNotes": [
									""
								],
								"CustomerNotes": [
									""
								],
								"AgentEmail": [
									"email@avanze.net"
								],
								"DestinationContactPhone": [
									""
								],
								"PricingGroupID": [
									"1"
								],
								"CancellationInsurance": [
									"false"
								],
								"ItinerariesID": [
									{
										"int": [
											"1000",
											"2000"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}