﻿
var _ = require('underscore');
var inflect = require('i')();
// Diacritics support
var diacritics = require('./diacritics');
// Cyrillic transliteration
var transliteration = require('./transliteration.cyr');


function _escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
function _replaceAll(string, find, replace) {
    return string.replace(new RegExp(_escapeRegExp(find), 'g'), replace);
}
function _buildTOKEN() {
    var d = new Date().getTime();
    var token = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    
    return token;
}

// HTML Entities for Text <> HTML conversion
var htmlEntities = require('./htmlEntities'),
    htmlEntitiesMap = {},
    htmlEntitiesRegExp = '';

(function () {
    for (var i in htmlEntities) {
        var ent = String.fromCharCode(htmlEntities[i]);
        htmlEntitiesMap[ent] = i;
        htmlEntitiesRegExp += '|' + ent;
    }
    htmlEntitiesRegExp = new RegExp(htmlEntitiesRegExp.substr(1), 'g');
})();

var isValidObjectId = exports.isValidObjectId = function (arg) {
    var len = arg.length;
    if (len == 12 || len == 24) {
        return /^[0-9a-fA-F]+$/.test(arg);
    } else {
        return false;
    }
}

var getHermesSubscriptionsByCollection = exports.getHermesSubscriptionByCollection = function (collectionname) {
    var scs = require('./staticdata').hermessuscriptions;

    var rs = _.filter(scs, function (subscription) { 
        return (subscription.relatedcollections.indexOf(collectionname) >= 0);
    });

    return rs;
}

var isArray = exports.isArray = function (arg) {
    return Array.isArray(arg);
}

var isDate = exports.isDate = function (arg) {
    return '[object Date]' == Object.prototype.toString.call(arg);
}

var isString = exports.isString = function (arg) {
    return 'string' == typeof arg;
}

var options = exports.options = function (defaults, options) {
    
    options = options || {};
    
    if (!defaults)
        return options;
    
    var keys = Object.keys(defaults), i = keys.length, k;
    
    while (i--) {
        k = keys[i];
        if (!(k in options)) {
            options[k] = defaults[k];
        }
    }
    
    return options;

}

var randomize = exports.randomize = function (min, max) { 
    return Math.floor(Math.random() * (max - min + 1) + min);
}

var randomString = exports.randomString = function (len, chars) {
    
    var str = '';
    
    if (!len) {
        len = 10;
    } else if (isArray(len)) {
        var min = number(len[0]);
        var max = number(len[1]);
        len = Math.round(Math.random() * (max - min)) + min;
    } else {
        len = number(len);
    }
    
    chars = chars || '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    
    for (var i = 0; i < len; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return str;

}

var guidGenerate = exports.guidGenerate = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

var optionsMap = exports.optionsMap = function (arr, property, clone) {
    if (arguments.length == 2 && 'boolean' == typeof property) {
        clone = property;
        property = undefined;
    }
    var obj = {};
    for (var i = 0; i < arr.length; i++) {
        var prop = (property) ? arr[i][property] : arr[i];
        if (clone) {
            prop = _.clone(prop);
        }
        obj[arr[i].value] = prop;
    }
    return obj;
}

var pad = exports.pad = function(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

var escapeRegExp = exports.escapeRegExp = function (str) {
    if (!isString(str) || !str.length) return '';
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

var slug = exports.slug = function (str, sep) {
    if (!isString(str) || !str.length) return '';
    sep = sep || '-';
    var esc = escapeRegExp(sep);
    str = stripDiacritics(str);
    str = transliterate(str);
    return str.replace(/['"()\.]/g, '').replace(/[^a-z0-9_\-]+/gi, sep).replace(new RegExp(esc + '+', 'g'), sep).replace(new RegExp('^' + esc + '+|' + esc + '+$'), '').toLowerCase();
}

var singular = exports.singular = function (str) {
    return inflect.singularize(str);
}

var upcase = exports.upcase = function (str) {
    if (!isString(str) || !str.length) return '';
    return (str.substr(0, 1).toUpperCase() + str.substr(1));
}

var downcase = exports.downcase = function (str) {
    if (!isString(str) || !str.length) return '';
    return (str.substr(0, 1).toLowerCase() + str.substr(1));
}

var titlecase = exports.titlecase = function (str) {
    if (!isString(str) || !str.length) return '';
    str = str.replace(/([a-z])([A-Z])/g, '$1 $2');
    var parts = str.split(/\s|_|\-/);
    for (var i = 0; i < parts.length; i++) {
        if (parts[i] && !/^[A-Z0-9]+$/.test(parts[i])) {
            parts[i] = upcase(parts[i]);
        }
    }
    return _.compact(parts).join(' ');
}

var camelcase = exports.camelcase = function (str, lc) {
    return inflect.camelize(str, !(lc));
}

var decodeHTMLEntities = exports.decodeHTMLEntities = function (str) {
    if (!isString(str) || !str.length) return '';
    return str.replace(/&[^;]+;/g, function (match, ent) {
        return String.fromCharCode(ent[0] !== '#' ? htmlEntities[ent] : ent[1] === 'x' ? parseInt(ent.substr(2), 16) : parseInt(ent.substr(1), 10));
    });
};

var cropHTMLString = exports.cropHTMLString = function (str, length, append, preserveWords) {
    return textToHTML(cropString(htmlToText(str), length, append, preserveWords));
}

var keyToLabel = exports.keyToLabel = function (str) {
    
    if (!isString(str) || !str.length) return '';
    
    str = str.replace(/([a-z])([A-Z])/g, '$1 $2');
    str = str.replace(/([0-9])([a-zA-Z])/g, '$1 $2');
    str = str.replace(/([a-zA-Z])([0-9])/g, '$1 $2');
    
    var parts = str.split(/\s|\.|_|-|:|;|([A-z\u00C0-\u00ff]+)/);
    
    for (var i = 0; i < parts.length; i++) {
        if (parts[i] && !/^[A-Z0-9]+$/.test(parts[i])) {
            parts[i] = upcase(parts[i]);
        }
    }
    
    return _.compact(parts).join(' ');

}

var keyToPath = exports.keyToPath = function (str, plural) {
    if (!isString(str) || !str.length) return '';
    parts = slug(keyToLabel(str)).split('-');
    if (parts.length && plural) {
        parts[parts.length - 1] = inflect.pluralize(parts[parts.length - 1])
    }
    return parts.join('-');
}

var keyToProperty = exports.keyToProperty = function (str, plural) {
    if (!isString(str) || !str.length) return '';
    parts = slug(keyToLabel(str)).split('-');
    if (parts.length && plural) {
        parts[parts.length - 1] = inflect.pluralize(parts[parts.length - 1])
    }
    for (var i = 1; i < parts.length; i++) {
        parts[i] = upcase(parts[i]);
    }
    return parts.join('');
}

var calculateDistance = exports.calculateDistance = function (point1, point2) {
    
    var dLng = (point2[0] - point1[0]) * Math.PI / 180;
    var dLat = (point2[1] - point1[1]) * Math.PI / 180;
    var lat1 = (point1[1]) * Math.PI / 180;
    var lat2 = (point2[1]) * Math.PI / 180;
    
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLng / 2) * Math.sin(dLng / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    
    return c;

}

var kmBetween = exports.kmBetween = function (point1, point2) {
    return calculateDistance(point1, point2) * RADIUS_KM;
}

var milesBetween = exports.milesBetween = function (point1, point2) {
    return calculateDistance(point1, point2) * RADIUS_MILES;
}

var _addNumSeps = exports._addNumSeps = function (nStr) {
    var sep = ',';
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + sep + '$2');
    }
    return x1 + x2;
}

var cropString = exports.cropString = function (str, length, append, preserveWords) {
    
    if (!isString(str) || !str.length) return '';
    
    if ('boolean' == typeof append) {
        preserveWords = append;
        append = null;
    }
    
    str = String(str);
    
    if (str.length <= length) return str;
    
    var cropTo = length;
    
    if (preserveWords) {
        var r = str.substr(cropTo);
        var word = r.match(/^\w+/);
        if (word && word.length) {
            cropTo += word[0].length;
        }
    }
    
    var rtn = str.substr(0, cropTo);
    
    return (rtn.length < str.length && append) ? rtn + append : rtn;

}


var htmlToText = exports.htmlToText = function (str) {
    if (!isString(str) || !str.length) return '';
    // remove all source-code line-breaks first
    str = str.replace(/\n/g, '');
    // turn non-breaking spaces into normal spaces
    str = str.replace(/&nbsp;/g, ' ');
    // <br> tags become single line-breaks
    str = str.replace(/<br>/gi, '\n');
    // <p>, <li>, <td> and <th> tags become double line-breaks
    str = str.replace(/<(?:p|li|td|th)[^>]*>/gi, '\n');
    // strip all other tags (including closing tags)
    str = str.replace(/<[^>]*>/g, '');
    // compress white space
    str = str.replace(/(\s)\s+/g, '$1');
    // remove leading or trailing spaces
    str = str.replace(/^\s+|\s+$/g, '');
    
    return decodeHTMLEntities(str);
}

var encodeHTMLEntities = exports.encodeHTMLEntities = function (str) {
    if (!isString(str) || !str.length) return '';
    return str.replace(htmlEntitiesRegExp, function (match) {
        return '&' + htmlEntitiesMap[match] + ';';
    });
};

var textToHTML = exports.textToHTML = function (str) {
    if (!isString(str) || !str.length) return '';
    return encodeHTMLEntities(str).replace(/\n/g, '<br>');
}

var plural = exports.plural = function (count, sn, pl) {
    
    if (arguments.length == 1) {
        return inflect.pluralize(count);
    }
    
    if ('string' != typeof sn) sn = '';
    
    if (!pl) {
        pl = inflect.pluralize(sn);
    }
    
    if ('string' == typeof count) {
        count = Number(count);
    } else if ('number' != typeof count) {
        count = _.size(count);
    }
    
    return (count == 1 ? sn : pl).replace('*', count);

}

var escapeString = exports.escapeString = function (str) {
    if (!isString(str) || !str.length) return '';
    return str.replace(/[\\'"]/g, "\\$&");
}

var stripDiacritics = exports.stripDiacritics = function (str) {
    if (!isString(str) || !str.length) return '';
    var rtn = [];
    for (var i = 0; i < str.length; i++) {
        var c = str.charAt(i);
        rtn.push(diacritics[c] || c);
    }
    return rtn.join('');
}

var transliterate = exports.transliterate = function (str) {
    if (!isString(str) || !str.length) return '';
    var rtn = [];
    // applying зг-zgh rule
    str = str.replace(transliteration.regexp.Zgh, 'Zgh');
    str = str.replace(transliteration.regexp.zgh, 'zgh');
    // replace characters with equivalent maps
    for (var i = 0; i < str.length; i++) {
        var character = str[i],
            latinCharacter = transliteration.characterMap[character];
        rtn.push((latinCharacter || '' === latinCharacter) ? latinCharacter : character);
    }
    return rtn.join('');
};

var isObject = exports.isObject = function (arg) {
    return '[object Object]' == Object.prototype.toString.call(arg);
}

var number = exports.number = function (str) {
    return parseFloat(String(str).replace(/[^\-0-9\.]/g, ''));
}


var isNumber = exports.isNumber = function (arg) {
    return 'number' == typeof arg;
}

var isEmail = exports.isEmail = function (str) {
    return /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.['a-z0-9!#$%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i.test(str);
}

var hasProperty = exports.hasProperty = function (object, key) {
    return _.has(object, key);
}


var isFunction = exports.isFunction = function (arg) {
    return ('function' == typeof arg);
}

var bindMethods = exports.bindMethods = function (obj, scope) {
    
    var bound = {};
    
    for (var prop in obj) {
        if ('function' == typeof obj[prop]) {
            bound[prop] = obj[prop].bind(scope);
        } else if (isObject(obj[prop])) {
            bound[prop] = bindMethods(obj[prop], scope);
        }
    }
    
    return bound;

}

var cloneObj = exports.cloneObj = function (obj) {
    return JSON.parse(JSON.stringify(obj));
}

var replaceAll = exports.replaceAll = function (input, find, replace) { 
    return _replaceAll(input, find, replace);
}

var getToken = exports.getToken = function () { 
    return _buildTOKEN();
}

var getfileextension = exports.getfileextension = function (filename) {
    var a = filename.split(".");
    if (a.length === 1 || (a[0] === "" && a.length === 2)) {
        return "";
    }
    return a.pop();
}

var copyfile = exports.copyfile = function (source, target, callback) {
    var cbCalled = false;
    var fs = require('fs');
    function done(err) {
        if (!cbCalled) {
            callback(err);
            cbCalled = true;
        }
    }

    var rd = fs.createReadStream(source);
    rd.on("error", function (err) {
        done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function (err) {
        done(err);
    });
    wr.on("close", function (ex) {
        done();
    });
    rd.pipe(wr);
    
    
}
//base 0
var getMonthNameEnglish = module.exports.getMonthNameEnglish = function(monthindex) {
    switch (monthindex) {
        case 0: return 'January'; break;
        case 1: return 'February'; break;
        case 2: return 'March'; break;
        case 3: return 'April'; break;
        case 4: return 'May'; break;
        case 5: return 'June'; break;
        case 6: return 'July'; break;
        case 7: return 'August'; break;
        case 8: return 'September'; break;
        case 9: return 'October'; break;
        case 10: return 'November'; break;
        case 11: return 'December'; break;
    }
}
//base 0
var getMonthNameSpanish = module.exports.getMonthNameSpanish = function (monthindex) {
    switch (monthindex) {
        case 0: return 'Enero'; break;
        case 1: return 'Febrero'; break;
        case 2: return 'Marzo'; break;
        case 3: return 'Abril'; break;
        case 4: return 'Mayo'; break;
        case 5: return 'Junio'; break;
        case 6: return 'Julio'; break;
        case 7: return 'Agosto'; break;
        case 8: return 'Septiembre'; break;
        case 9: return 'Octubre'; break;
        case 10: return 'Noviembre'; break;
        case 11: return 'Diciembre'; break;
    }
}

var convertValueToCurrency = module.exports.convertValueToCurrency = function(value, currencyOrig, currencyDest, exchanges) {
    
    var fin = false;
    var finded = null;
    for (var itExc = 0, len = exchanges.length; itExc < len; itExc++) {
        
        // 1) si encuentro la tasa de cambio
        if (exchanges[itExc].origin == currencyOrig && exchanges[itExc].destination == currencyDest) {
            finded = Math.round(value * exchanges[itExc].change);
            break;
        }
    		
    			// 2) si encuentro la tasa de cambio inversa
        else if (exchanges[itExc].destination == currencyOrig && exchanges[itExc].origin == currencyDest) {
            finded = Math.round(value / exchanges[itExc].change);
            break;
        }
    }
    return finded;
}

var synchronyzeProperties = module.exports.synchronyzeProperties = function (source, target, options) {
    
    function eachRecursive(Rsource, Rtarget) {
        for (var prop in Rsource) {
           
            if (typeof Rsource[prop] == "object" && Rsource[prop] != null) {
                if (Object.prototype.toString.call(Rsource[prop]) == '[object Array]') {
                    function emptyArray(propertyArray) {
                        if (typeof (propertyArray.remove) === 'function') {
                            var ids = _.pluck(propertyArray, '_id');
                            for (var i = 0, len = ids.length; i < len; i++) {
                                propertyArray.remove(ids[i]);
                            }
                        }
                        else {
                            propertyArray = propertyArray.splice(0, propertyArray.length);
                        }
                        return propertyArray;
                    }
                    Rtarget[prop] = (Rtarget[prop] != null && Rtarget[prop].length > 0) ? emptyArray(Rtarget[prop]) : [];
                    //push elements
                    for (var i = 0, len = Rsource[prop].length; i < len; i++) {
                        Rtarget[prop].push(Rsource[prop][i]);
                    }
                } else {
                    if (Rtarget[prop] == null) {
                        Rtarget[prop] = JSON.parse(JSON.stringify(Rsource[prop]));
                    }
                    eachRecursive(Rsource[prop], Rtarget[prop]);
                }
            }
            else {
                if (!Rsource.hasOwnProperty(prop)) {
                    continue;
                } else {
                    Rtarget = (Rtarget != null) ? Rtarget : JSON.parse(JSON.stringify(Rsource));
                    Rtarget[prop] = (prop != '__v') ? Rsource[prop] : Rtarget[prop];
                }
            }
        }
    }
    
    eachRecursive(source, target);
    
    return target;
}

var sortingHelpers = module.exports.sortingHelpers = {
    orderProductByPriceAsc : function (a, b) { 
        return a.minprice.value - b.minprice.value;
    },
    orderProductByPriceDesc : function (a, b) {
        return  b.minprice.value - a.minprice.value;
    }
}


var removeObsoleteAvailability = exports.removeObsoleteAvailability = function (product) {
    var saveyears = [];
    if (product != null && product.availability != null) {
        var currentyear = new Date().getFullYear();
        for (var i = 0, len = product.availability.length; i < len; i++) {
            //remove past years...
            
            if (product.availability[i].year < currentyear) { 
                //not to save...
            }
            else {
                saveyears.push(product.availability[i]);
            }
        }

    }
    return saveyears;
}

var getMinimumPrices = exports.getMinimumPrices = function (product) {
    var months = require('./staticdata').months_en;
    var actualdate = new Date();
    actualdate.setHours(0);
    actualdate.setMinutes(0);
    actualdate.setSeconds(0);
    actualdate.setMilliseconds(0);

    function getMinimum(monthname, monthdays) {
        var price = {
            year: 0,
            month: '',
            minprice: 0,
            currency: {
                label: '',
                symbol: '',
                value: ''
            }
        };
        
        if (monthdays != null && monthdays.length > 0) {
            
            for (var i = 0, len = monthdays.length; i < len; i++) {
                var day = monthdays[i];
                var actualmonth = months[actualdate.getMonth()];
                var actualday = actualdate.getDate();
                //check the same month and date...
                if (actualmonth == monthname) {
                    if (day.day >= actualday) {
                        
                        if (price.minprice == 0) {
                            price.minprice = day.rooms.double.price;
                            price.currency.label = day.rooms.currency.label;
                            price.currency.symbol = day.rooms.currency.symbol;
                            price.currency.value = day.rooms.currency.value;
                        } else {
                            if (price.minprice > day.rooms.double.price && price.minprice > 0) {
                                price.minprice = day.rooms.double.price;
                                price.currency.label = day.rooms.currency.label;
                                price.currency.symbol = day.rooms.currency.symbol;
                                price.currency.value = day.rooms.currency.value;
                            }
                        }


                    }
                }
                else {
                    
                    if (price.minprice == 0) {
                        price.minprice = day.rooms.double.price;
                        price.currency.label = day.rooms.currency.label;
                        price.currency.symbol = day.rooms.currency.symbol;
                        price.currency.value = day.rooms.currency.value;
                    } else {
                        if (price.minprice > day.rooms.double.price && price.minprice > 0) {
                            price.minprice = day.rooms.double.price;
                            price.currency.label = day.rooms.currency.label;
                            price.currency.symbol = day.rooms.currency.symbol;
                            price.currency.value = day.rooms.currency.value;
                        }
                    }

                }

                
            }
        }
        return price;
    }
    var minimums = [];
    //triple for...
    
    if (product != null && product.availability != null) {
        //Every year...
        for (var i = 0, len = product.availability.length; i < len; i++) {
            var avail = product.availability[i];
            //Every month
            for (var m = 0, lenn = months.length; m < lenn; m++) {
                
                var mdays = avail[months[m]].availability;
                var price = {
                    year: avail.year,
                    month: months[m],
                    minprice: 0,
                    currency: {
                        label: '',
                        symbol: '',
                        value: ''
                    }
                };
                var std = 30;
                if (m == 1) {
                    std = 27;
                }
                var evaldate = new Date(price.year, m, std, 23, 59, 59);
                if (evaldate >= actualdate) {
                    
                    if (mdays != null && mdays.length > 0) {
                        price = getMinimum(months[m], mdays);
                        price.year = avail.year;
                        price.month = months[m];
                    }
                    minimums.push(price);
                }
                
            }

        }
    }
    return minimums;
}

var getMinimumPriceFromPricesBy = exports.getMinimumPriceFromPricesBy = function (year, month, prices) {
    var minprice = {
        value : 0,
        currency : ""
    }
    var currentyear = new Date().getFullYear();
    
    if (prices != null && prices.length > 0) {
        var filteredprices = _.filter(prices, function (price) {
            return (price.year == year) && (price.month.toLowerCase() == month.toLowerCase());
        });
        if (filteredprices != null && filteredprices.length > 0) {
            minprice = filteredprices[0];
        }
    }
    return minprice;
}

var getMinimumPriceFromPrices = exports.getMinimumPriceFromPrices = function (prices) {
    var minprice = {
        value : 0,
        currency : ""
    }
    
    if (prices != null && prices.length > 0) {
        var pricevalues = _.pluck(prices, 'minprice');
        var pricecurrency = _.find(prices, function (price) {
            return (price.currency != null && price.currency.label != null && price.currency.label != '');
        });
        if (pricecurrency != null) {
            minprice.currency = pricecurrency.currency;
        }
        if (pricevalues != null && pricevalues.length > 0) {
            pricevalues.sort(function (a, b) { return a - b; });
            minprice = pricevalues[0];
        }
    }
    return minprice;
}

var getMinimumPrice = exports.getMinimumPrice = function (from, to, availability) {
    var prices = [];
    var currency = null;
    
    var pricemin = {
        value : 0,
        currency : ""
    };
    
    if (availability == null || availability.length == 0) {
        return pricemin;
    }
    
    var iterate = new Date(from.getFullYear(), from.getMonth(), from.getDate());
    while (iterate <= to) {
        var avail = null;
        var month = _getMonthNameEnglish(iterate.getMonth())
        for (var i = 0, len = availability.length; i < len; i++) {
            if (availability[i].year == iterate.getFullYear()) {
                avail = availability[i];
                break;
            }
        }
        if (avail != null) {
            var days = avail[month].availability;
            if (days != null && days.length > 0) {
                for (var t = 0, len = days.length; t < len; t++) {
                    var day = days[t];
                    if (day != null && day.rooms.double != null && day.rooms.double.price > 0) {
                        prices.push(day.rooms.double.price);
                        currency = day.rooms.currency;
                        if (pricemin.value == 0) {
                            pricemin.value = day.rooms.double.price;
                            pricemin.currency = day.rooms.currency;
                        } else {
                            if (pricemin.value > day.rooms.double.price && pricemin.value > 0) {
                                pricemin.value = day.rooms.double.price;
                                pricemin.currency = day.rooms.currency;
                            }
                        }
                        
                    }
                }
            }
        }
        iterate.setMonth(iterate.getMonth() + 1);
    }
    return pricemin;
    
}


var updateproductcities = exports.updateproductcities = function (product, cmscities) {
    if (product.itinerary != null && product.itinerary.length > 0) {
        for (var i = 0, len = product.itinerary.length; i < len; i++) {
            //DEPARTURE CITIES
            if (product.itinerary[i].departurecity != null && 
                    product.itinerary[i].departurecity.city != '' && 
                    product.itinerary[i].departurecity.location != null) {
                var dslug = slug(
                    product.itinerary[i].departurecity.city + ' ' + 
                        product.itinerary[i].departurecity.location.countrycode);
                var cmscity = cmscities.get(cslug);
                if (cmscity != null && cmscity.label_es != null && cmscity.label_es != '') {
                    product.itinerary[i].departurecity.city_es = cmscity.label_es;
                }
                product.itinerary[i].departurecity.slug = cslug;
            }
            //SLEEP CITIES
            if (product.itinerary[i].sleepcity != null && 
                    product.itinerary[i].sleepcity.city != '' && 
                    product.itinerary[i].sleepcity.location != null) {
                var sslug = slug(
                    product.itinerary[i].sleepcity.city + ' ' + 
                        product.itinerary[i].sleepcity.location.countrycode);
                var cmscity = cmscities.get(sslug);
                if (cmscity != null && cmscity.label_es != null && cmscity.label_es != '') {
                    product.itinerary[i].sleepcity.city_es = cmscity.label_es;
                }
                product.itinerary[i].sleepcity.slug = sslug;
            }
            //STOP CITIES
            if (product.itinerary[i].stopcities != null && product.itinerary[i].stopcities.length > 0) {
                for (var st = 0, len = product.itinerary[i].stopcities.length; st < len; st++) {
                    if (product.itinerary[i].stopcities[st] != null && 
                            product.itinerary[i].stopcities[st].city != '' && 
                            product.itinerary[i].stopcities[st].location != null) {
                        var tslug = slug(
                            product.itinerary[i].stopcities[st].city + ' ' + 
                            product.itinerary[i].stopcities[st].location.countrycode);
                        var cmscity = cmscities.get(tslug);
                        if (cmscity != null && cmscity.label_es != null && cmscity.label_es != '') {
                            product.itinerary[i].stopcities[st].city_es = cmscity.label_es;
                        }
                        product.itinerary[i].stopcities[st].slug = tslug;
                    }
                }
            }
        }
    }
    return product;
}

var getfieldvalue = exports.getfieldvalue = function (fieldname, modelobject, options) {
    var fields = fieldname.split('.');
    var currentfield = (fields != null && fields.length >0) ? modelobject[fields[0]] : null;
    for (var i = 1, len = fields.length; i < len; i++) { 
        currentfield = (currentfield != null) ? currentfield[fields[i]] : null;
    }
    return currentfield;
}

var accentsTidy = exports.accentsTidy = function (s) {
    var r = s || '';
    r = r.replace(new RegExp("[àáâãäå]", 'g'), "a");
    r = r.replace(new RegExp("æ", 'g'), "ae");
    r = r.replace(new RegExp("ç", 'g'), "c");
    r = r.replace(new RegExp("[èéêë]", 'g'), "e");
    r = r.replace(new RegExp("[ìíîï]", 'g'), "i");
    r = r.replace(new RegExp("ñ", 'g'), "n");
    r = r.replace(new RegExp("[òóôõö]", 'g'), "o");
    r = r.replace(new RegExp("œ", 'g'), "oe");
    r = r.replace(new RegExp("[ùúûü]", 'g'), "u");
    r = r.replace(new RegExp("[ýÿ]", 'g'), "y");
    return r;
};