﻿module.exports = function (kioowcore, dbname) {
    var Types = kioowcore.Field.Types;
    
    var KioowAdminRequests = new kioowcore.List('KioowAdminRequests', {
        map: { name: 'title' },
        autokey: { path: 'slug', from: 'title', unique: true }
    });
    
    KioowAdminRequests.add({
        title: { type: String, required: true },
        slug: { type: String, index: true },
        from: { type: String, index: true },
        to: { type: String, index: true },
        subject: { type: String },
        text: { type: String },
        html: { type: Types.Html, wysiwyg: true, height: 250 },
        date: { type: Types.Date },
        type: { type: String },
        key: { type: String }
    });
    
    
    KioowAdminRequests.schema.add({
        tags: {
            type: [{
                    title: { type: String },
                    publishedDate: { type: Types.Date },
                    description: { type: String }
                }]
        }
    });
    
    
    KioowAdminRequests.schema.virtual('to.full').get(function () {
        return this.to.split(';');
    });
    
    
    KioowAdminRequests.addPattern('standard meta');
    KioowAdminRequests.defaultColumns = 'title, slug, type, publishedDate|20%, subject|20%, date';
    KioowAdminRequests.register(dbname);
}