module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AgencyCardsListResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AgencyCardsListResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AgencyCards": [
									{
										"AgencyCard": [
											{
												"CardID": [
													"37"
												],
												"CardNumber": [
													"******7890"
												],
												"CardOwner": [
													"Alessandro Re"
												],
												"CardType": [
													"VISA CREDITO"
												],
												"ExpirationDate": [
													"0101"
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}