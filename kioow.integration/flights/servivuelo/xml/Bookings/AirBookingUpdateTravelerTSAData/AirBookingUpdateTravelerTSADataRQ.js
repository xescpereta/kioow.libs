module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingUpdateTravelerTSAData": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingUpdateTsaTravelerRQ": [
							{
								"BookingID": [
									"2957"
								],
								"TravelerID": [
									"4183"
								],
								"TravelerTSAData": [
									{
										"FirstName": [
											"ALESSANDRO"
										],
										"LastName": [
											"RE"
										],
										"Gender": [
											"M"
										],
										"BirthDate": [
											"27/09/1973"
										],
										"DocumentExpirationDate": [
											"12/12/2015"
										],
										"DocumentIssueCountry": [
											"ESP"
										],
										"DocumentType": [
											"IP"
										],
										"DocumentNumber": [
											"x3493743c"
										],
										"BirthCountry": [
											"ITA"
										],
										"NationalityCountry": [
											"ITA"
										],
										"VisaIssueCity": [
											"MADRID"
										],
										"VisaNumber": [
											"VISADO"
										],
										"VisaIssueCountry": [
											"ESP"
										],
										"VisaIssueDate": [
											"01/01/2013"
										],
										"IsResidentUSA": [
											"true"
										],
										"USA_City": [
											"NW"
										],
										"USA_State": [
											"NE"
										],
										"USA_ResidenceType": [
											"D"
										],
										"USA_Address": [
											"DIRECCION"
										],
										"USA_ZipCode": [
											"777766"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}