﻿module.exports = function (core, dbname) {
    require('./city')(core, dbname);
    require('./country')(core, dbname);
    require('./countryzones')(core, dbname);
    require('./exchange')(core, dbname);
    require('./exchangearchive')(core, dbname);
    require('./gallery')(core, dbname);
    require('./page')(core, dbname);
    require('./pagecategories')(core, dbname);
    require('./profiles')(core, dbname);
    require('./promotion')(core, dbname);
    require('./kioowtags')(core, dbname);
    require('./kioowtagscategory')(core, dbname);
    require('./faq')(core, dbname);
    require('./faqcategory')(core, dbname);
    require('./files')(core, dbname);
    require('./pressroom')(core, dbname);
    require('./pressroomcategory')(core, dbname);
}