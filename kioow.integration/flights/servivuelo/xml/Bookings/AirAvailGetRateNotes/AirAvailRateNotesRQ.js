module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirAvailGetRateNotes": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airAvailRateNotesRQ": [
							{
								"AvailRequestID": [
									"2212122"
								],
								"PricingGroupID": [
									"18"
								],
								"ItinerariesID": [
									{
										"int": [
											"1011",
											"2001"
										]
									}
								],
								"AdultNumber": [
									"1"
								],
								"ChildrenNumber": [
									"1"
								],
								"InfantNumber": [
									"1"
								]
							}
						]
					}
				]
			}
		]
	}
}