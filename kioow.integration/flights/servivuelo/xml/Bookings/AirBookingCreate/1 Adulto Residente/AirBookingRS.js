module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "b8460da0-67a6-45f3-89fa-1e9b0745d475",
						"$": {
							"CorrelationId": "b9f8c48e-d094-44dd-8a5c-161d4c6df69c",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirBookingCreateResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCreateResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"2956"
										],
										"AvailRequestID": [
											"2212122"
										],
										"BookingRequestID": [
											"2212163"
										],
										"BookingReference": [
											"6VUS7O"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"NIE"
														],
														"TravelerID": [
															"1"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"ALESSANDRO"
														],
														"LastName": [
															"RE"
														],
														"DocumentNumber": [
															"X3493743C"
														],
														"Email": [
															"alessandro.re@avanze.net"
														],
														"Phone": [
															"687464925"
														],
														"BirthDate": [
															""
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"true"
														],
														"ResidentDocumentType": [
															"DN"
														],
														"ResidentCityCode": [
															"070051"
														],
														"ResidentCertificateNumber": [
															""
														],
														"TSAData": [
															{
																"FirstName": [
																	"ALESSANDRO"
																],
																"LastName": [
																	"RE"
																],
																"Gender": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthDate": [
																	""
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"NationalityCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCity": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_State": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ResidenceType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_Address": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ZipCode": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"AirBookingNote": [
													{
														"NoteCode": [
															"PEN"
														],
														"Description": [
															"TICKETS ARE NON-REFUNDABLE"
														]
													},
													{
														"NoteCode": [
															"LTD"
														],
														"Description": [
															"LAST TKT DTE01MAR13 - SEE ADV PURCHASE"
														]
													},
													{
														"NoteCode": [
															"APM"
														],
														"Description": [
															"PRIVATE RATES USED *F*"
														]
													},
													{
														"NoteCode": [
															"SUR"
														],
														"Description": [
															"FARE VALID FOR E TICKET ONLY"
														]
													}
												]
											}
										],
										"BookingDateTime": [
											"16/05/2013 15:14"
										],
										"TicketIssueDateLimit": [
											"16/05/2013"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0"
										],
										"AgentEmail": [
											"alessandro.re@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"0.00"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"21:00"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureDateTime": [
															"23/05/2013 15:50"
														],
														"ArrivalDateTime": [
															"24/05/2013 12:50"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"ALC"
																		],
																		"DepartureDateTime": [
																			"23/05/2013 15:50"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 17:00"
																		],
																		"FlightNumber": [
																			"3886"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"32S"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"O"
																		],
																		"FareCode": [
																			"ODSLRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"ALC"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureDateTime": [
																			"24/05/2013 12:05"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 12:50"
																		],
																		"FlightNumber": [
																			"8906"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AircraftType": [
																			"AT7"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:10"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"25/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"25/05/2013 14:45"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"25/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 14:45"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AircraftType": [
																			"CRK"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"130.00"
														],
														"TaxAmount": [
															"43.34"
														],
														"AramixFeeAmount": [
															"5"
														],
														"AgencyFeeAmount": [
															"0"
														]
													}
												]
											}
										],
										"FlightTickets": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"Warnings": [
											""
										],
										"Messages": [
											""
										]
									}
								],
								"Error": [
									{
										"$": {
											"i:nil": "true"
										}
									}
								],
								"RequestID": [
									"2212163"
								]
							}
						]
					}
				]
			}
		]
	}
}