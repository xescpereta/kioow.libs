module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirAvailSearch": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"availRQ": [
							{
								"DirectFlightsOnly": [
									"false"
								],
								"IncludeLowCost": [
									"false"
								],
								"Carriers": [
									{
										"string": [
											"IB"
										]
									}
								],
								"ClassPref": [
									"NotSet"
								],
								"Travelers": [
									{
										"AirTravelerInfo": [
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"false"
												]
											},
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"false"
												]
											},
											{
												"TravelerType": [
													"Infant"
												],
												"IsResident": [
													"false"
												]
											}
										]
									}
								],
								"FlightSegments": [
									{
										"$": {
											"xmlns": "http://webservices.aramix.es"
										},
										"AirFlightSegmentRQ": [
											{
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"IBZ"
												],
												"DepartureDate": [
													"23/07/2015"
												]
											},
											{
												"DepartureAirportLocationCode": [
													"IBZ"
												],
												"ArrivalAirportLocationCode": [
													"MAD"
												],
												"DepartureDate": [
													"25/07/2015"
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}