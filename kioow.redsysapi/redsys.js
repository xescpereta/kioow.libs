﻿
var redsysAPI = function (options) {
    var common = require('yourttoo.common');

    var currentpath = require('path').dirname(module.filename);
    currentpath = common.utils.replaceAll(currentpath, '\\', '/') + '/';

    this.dependencies = [ 
        currentpath + '/java/org.json.jar', 
        currentpath + '/java/bcprov-jdk15on-1.4.7.jar', 
        currentpath + '/java/commons-codec-1.3.jar', 
        currentpath + '/java/apiSha256.jar'];
    this.java = require('java');
    if (options != null && options.dependencies != null) { 
        this.dependencies = this.dependencies.concat(options.dependencies);
    }
    for (var i = 0, len = this.dependencies.length; i < len; i++) { 
        this.java.classpath.push(this.dependencies[i]);
    }
}

//REDSYS REQUEST HANDLERS
redsysAPI.prototype.createMerchantParameters = function (merchantparameters) {
    var self = this;
    if (merchantparameters == null) { throw new Error('the input parameter can not be null or undefined'); }
    
    for (var prop in merchantparameters) {
        self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "setParameter", prop, merchantparameters[prop]);
    }

    return self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "createMerchantParameters");
}

redsysAPI.prototype.getParameter = function (parametername) {
    var self = this;
    if (parametername == null) { throw new Error('the input parametername can not be null or undefined'); }
    
    return self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "getParameter", parametername);
}

redsysAPI.prototype.setParameter = function (options) {
    var self = this;
    if (options == null) { throw new Error('the input options or value can not be null or undefined'); }
    var parametername = options.key;
    var value = options.value;
    self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "setParameter", parametername, value);
}

redsysAPI.prototype.createMerchantSignature = function (privatekey) {
    var self = this;
    if (privatekey == null) { throw new Error('the input privatekey can not be null or undefined'); }

    return self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "createMerchantSignature", privatekey);
}


// REDSYS RESPONSE HANDLERS
redsysAPI.prototype.decodeMerchantParameters = function (merchantparameters) {
    var self = this;
    if (merchantparameters == null) { throw new Error('the input parameter can not be null or undefined'); }
    
    return self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "decodeMerchantParameters", merchantparameters);;
}

redsysAPI.prototype.createMerchantSignatureNotif = function (privatekey, merchantparameters) {
    var self = this;
    if (privatekey == null) { throw new Error('the input privatekey can not be null or undefined'); }
    
    return self.java.callStaticMethodSync("sis.redsys.api.ApiMacSha256", "createMerchantSignatureNotif", privatekey, merchantparameters);
}


module.exports.redsysAPI = redsysAPI;
