module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingMarkToTicketingResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingMarkToTicketingResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"43840"
										],
										"AvailRequestID": [
											"0"
										],
										"BookingRequestID": [
											"2843924"
										],
										"BookingReference": [
											"6V3PW4"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"PAS"
														],
														"TravelerID": [
															"45633"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"ADRI"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"102030"
														],
														"Email": [
															""
														],
														"Phone": [
															"913329781"
														],
														"BirthDate": [
															"31/07/1975"
														],
														"FrequentFlyerProgram": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"FrequentFlyerCardNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ADRI"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	""
																],
																"BirthDate": [
																	"31/07/1975"
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	""
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"102030"
																],
																"BirthCountry": [
																	""
																],
																"NationalityCountry": [
																	""
																],
																"VisaIssueCity": [
																	""
																],
																"VisaNumber": [
																	""
																],
																"VisaIssueCountry": [
																	""
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	" "
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														],
														"TicketAmount": [
															"27.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"OBFeeAmount": [
															"0.00"
														],
														"Markup": [
															"0.00"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"47461"
																		],
																		"CabinClass": [
																			""
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			""
																		],
																		"FareBasis": [
																			"PDSYRX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"47462"
																		],
																		"CabinClass": [
																			""
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			""
																		],
																		"FareBasis": [
																			"PDSYRX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"BookingDateTime": [
											"02/07/2014 16:39"
										],
										"TicketIssueDateLimit": [
											"15/05/2014"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0.00"
										],
										"AgentEmail": [
											""
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RLE"
										],
										"AmountDifference": [
											"0"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"01:20"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"BCN"
														],
														"DepartureDateTime": [
															"10/07/2014 07:45"
														],
														"ArrivalDateTime": [
															"10/07/2014 09:05"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"BCN"
																		],
																		"DepartureDateTime": [
																			"10/07/2014 07:45"
																		],
																		"ArrivalDateTime": [
																			"10/07/2014 09:05"
																		],
																		"FlightNumber": [
																			"2730  "
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDSYRX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"47461"
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:25"
														],
														"DepartureAirportLocationCode": [
															"BCN"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"15/08/2014 17:45"
														],
														"ArrivalDateTime": [
															"15/08/2014 19:10"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"BCN"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"15/08/2014 17:45"
																		],
																		"ArrivalDateTime": [
																			"15/08/2014 19:10"
																		],
																		"FlightNumber": [
																			"2735  "
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			"1"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDSYRX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"47462"
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"27.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"AgencyFeeAmount": [
															"1.50"
														]
													}
												]
											}
										],
										"FlightTickets": [
											""
										],
										"Warnings": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"TotalAgencyAmount": [
											"76.51"
										],
										"TotalAmount": [
											"78.01"
										]
									}
								],
								"RequestID": [
									"2843945"
								]
							}
						]
					}
				]
			}
		]
	}
}