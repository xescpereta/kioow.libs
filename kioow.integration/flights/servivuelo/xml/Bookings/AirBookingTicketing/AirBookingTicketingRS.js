module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingTicketingResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingTicketingResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"BookingID": [
									"2923"
								],
								"AvailRequestID": [
									"225485"
								],
								"UniversalBookingReference": [
									""
								],
								"BookingReference": [
									"6QMXJA"
								],
								"MarketingCarrierCode": [
									"IB"
								],
								"BookingAmount": [
									"184.95"
								],
								"ManagementFeeAmount": [
									"0"
								],
								"AramixFeeAmount": [
									{
										"_": "5.00",
										"$": {
											"xmlns": "http://webservices.aramix.es"
										}
									}
								],
								"GdsFeeAmount": [
									{
										"_": "5.00",
										"$": {
											"xmlns": "http://webservices.aramix.es"
										}
									}
								],
								"BookingDateTime": [
									{
										"_": "22/03/2013 12:10",
										"$": {
											"xmlns": "http://webservices.aramix.es"
										}
									}
								],
								"AmountDifference": [
									{
										"_": "0.00",
										"$": {
											"xmlns": "http://webservices.aramix.es"
										}
									}
								],
								"AirBookingFlightSegments": [
									{
										"$": {
											"xmlns": "http://webservices.aramix.es"
										},
										"AirBookingFlightSegment": [
											{
												"SegmentNumber": [
													"1"
												],
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"IBZ"
												],
												"DepartureDateTime": [
													"23/03/2013 07:30"
												],
												"ArrivalDateTime": [
													"23/03/2013 21:50"
												],
												"FlightSegmentLegs": [
													{
														"AirBookingFlightSegmentLeg": [
															{
																"DepartureAirportLocationCode": [
																	"MAD-Madrid Barajas"
																],
																"ArrivalAirportLocationCode": [
																	"ALC-Alicante"
																],
																"DepartureDateTime": [
																	"23/03/2013 07:30"
																],
																"ArrivalDateTime": [
																	"23/03/2013 08:40"
																],
																"FlightNumber": [
																	"3890  "
																],
																"ArrivalAirportTerminal": [
																	"3"
																],
																"DepartureAirportTerminal": [
																	"4"
																],
																"AircraftType": [
																	"32S"
																],
																"FareCode": [
																	"PDSXRTNY"
																],
																"ETicket": [
																	"false"
																]
															},
															{
																"DepartureAirportLocationCode": [
																	"ALC"
																],
																"ArrivalAirportLocationCode": [
																	"IBZ"
																],
																"DepartureDateTime": [
																	"23/03/2013 21:10"
																],
																"ArrivalDateTime": [
																	"23/03/2013 21:50"
																],
																"FlightNumber": [
																	"8904  "
																],
																"ArrivalAirportTerminal": [
																	""
																],
																"DepartureAirportTerminal": [
																	""
																],
																"AircraftType": [
																	"CR9"
																],
																"FareCode": [
																	"PDSXRTNY"
																],
																"ETicket": [
																	"false"
																]
															}
														]
													}
												]
											},
											{
												"SegmentNumber": [
													"2"
												],
												"DepartureAirportLocationCode": [
													"IBZ"
												],
												"ArrivalAirportLocationCode": [
													"MAD"
												],
												"DepartureDateTime": [
													"25/03/2013 23:30"
												],
												"ArrivalDateTime": [
													"26/03/2013 11:15"
												],
												"FlightSegmentLegs": [
													{
														"AirBookingFlightSegmentLeg": [
															{
																"DepartureAirportLocationCode": [
																	"IBZ"
																],
																"ArrivalAirportLocationCode": [
																	"VLC"
																],
																"DepartureDateTime": [
																	"25/03/2013 23:30"
																],
																"ArrivalDateTime": [
																	"26/03/2013 00:15"
																],
																"FlightNumber": [
																	"8439  "
																],
																"ArrivalAirportTerminal": [
																	""
																],
																"DepartureAirportTerminal": [
																	""
																],
																"AircraftType": [
																	"AT7"
																],
																"FareCode": [
																	"PD2LRT"
																],
																"ETicket": [
																	"false"
																]
															},
															{
																"DepartureAirportLocationCode": [
																	"VLC"
																],
																"ArrivalAirportLocationCode": [
																	"MAD"
																],
																"DepartureDateTime": [
																	"26/03/2013 10:20"
																],
																"ArrivalDateTime": [
																	"26/03/2013 11:15"
																],
																"FlightNumber": [
																	"8993  "
																],
																"ArrivalAirportTerminal": [
																	"4"
																],
																"DepartureAirportTerminal": [
																	""
																],
																"AircraftType": [
																	"CR9"
																],
																"FareCode": [
																	"PD2LRT"
																],
																"ETicket": [
																	"false"
																]
															}
														]
													}
												]
											}
										]
									}
								],
								"AirTravelers": [
									{
										"$": {
											"xmlns": "http://webservices.aramix.es"
										},
										"AirTraveler": [
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"PAS"
												],
												"TravelerID": [
													"4135"
												],
												"InfantID": [
													"0"
												],
												"FirstName": [
													"PRUEBAS"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"102050"
												],
												"Email": [
													""
												],
												"Phone": [
													"913329781"
												],
												"BirthDate": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"ResidentDocumentType": [
													{
														"$": {
															"p4:nil": "true",
															"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance"
														}
													}
												],
												"TSAData": [
													{
														"FirstName": [
															"PRUEBAS"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															""
														],
														"BirthDate": [
															""
														],
														"DocumentExpirationDate": [
															""
														],
														"DocumentIssueCountry": [
															""
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"102050"
														],
														"BirthCountry": [
															""
														],
														"NationalityCountry": [
															""
														],
														"VisaIssueCity": [
															""
														],
														"VisaNumber": [
															""
														],
														"VisaIssueCountry": [
															""
														],
														"VisaIssueDate": [
															""
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															""
														],
														"USA_State": [
															""
														],
														"USA_ResidenceType": [
															""
														],
														"USA_Address": [
															""
														],
														"USA_ZipCode": [
															""
														]
													}
												]
											}
										]
									}
								],
								"FlightTickets": [
									{
										"AirFlightTicket": [
											{
												"TicketID": [
													"0"
												],
												"TicketNumber": [
													"PAX 075-3220921290/ETIB/EUR184.95/15FEB13/MADI13032/78282466"
												],
												"TravelerID": [
													"4135"
												],
												"SegmentID": [
													"4192"
												]
											}
										]
									}
								],
								"TravelersInfoFares": [
									{
										"$": {
											"xmlns": "http://webservices.aramix.es"
										},
										"AirBookingFare": [
											{
												"TravelerInfo": [
													{
														"TravelerType": [
															"Adult"
														],
														"IsResident": [
															"false"
														]
													}
												],
												"TotalTravelers": [
													"1"
												],
												"FareAmount": [
													"134.00"
												],
												"TaxAmount": [
													"50.95"
												],
												"AramixFeeAmount": [
													"5.00"
												],
												"AgencyFeeAmount": [
													"0.00"
												]
											}
										]
									}
								],
								"RequestID": [
									"0"
								]
							}
						]
					}
				]
			}
		]
	}
}