﻿module.exports.getjson = function (fn, op) {
    var cnt = {
        Availability : require('./Availability/soap.index'),
        Bookings : require('./Bookings/soap.index')
    };

    return require(cnt[fn][op]);

}

module.exports.getxml = function (fn, op) {
    var cnt = {
        Availability : require('./Availability/soap.index'),
        Bookings : require('./Bookings/soap.index')
    };
    var fs = require('fs');
    var file = cnt[fn][op];
    file = file.replace('.js', '.xml');
    var xml = fs.readFileSync(file, 'utf8');

    return xml;

}