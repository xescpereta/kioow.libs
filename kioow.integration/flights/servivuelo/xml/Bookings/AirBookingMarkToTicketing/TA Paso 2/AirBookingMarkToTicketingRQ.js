module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingMarkToTicketing": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingMarkToTicketingRQ": [
							{
								"BookingID": [
									"43825"
								],
								"PaymentForm": [
									"UYU"
								],
								"CardID": [
									"1077"
								],
								"InsuranceAmount": [
									"0"
								],
								"Observations": [
									"0"
								],
								"AmountDifference": [
									"5.00"
								]
							}
						]
					}
				]
			}
		]
	}
}