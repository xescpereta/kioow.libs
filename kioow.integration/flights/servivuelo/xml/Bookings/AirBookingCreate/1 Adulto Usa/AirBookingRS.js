module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "db6e5a9b-2bce-43e2-b53c-af594b6caccc",
						"$": {
							"CorrelationId": "18916548-39d3-4bd5-9ce7-25a5837b3e1b",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirBookingCreateResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCreateResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"2957"
										],
										"AvailRequestID": [
											"2212122"
										],
										"BookingRequestID": [
											"2212171"
										],
										"BookingReference": [
											"6VUWJF"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"NIE"
														],
														"TravelerID": [
															"1"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"ALESSANDRO"
														],
														"LastName": [
															"RE"
														],
														"DocumentNumber": [
															"X3493743C"
														],
														"Email": [
															"alessandro.re@avanze.net"
														],
														"Phone": [
															"687464925"
														],
														"BirthDate": [
															"31/07/1975"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ALESSANDRO"
																],
																"LastName": [
																	"RE"
																],
																"Gender": [
																	"M"
																],
																"BirthDate": [
																	"31/07/1975"
																],
																"DocumentExpirationDate": [
																	"12/12/2013"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"AAAAAAAA0"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00001"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2012"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	""
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"AirBookingNote": [
													{
														"NoteCode": [
															"PEN"
														],
														"Description": [
															"TICKETS ARE NON-REFUNDABLE"
														]
													},
													{
														"NoteCode": [
															"LTD"
														],
														"Description": [
															"LAST TKT DTE01MAR13 - SEE ADV PURCHASE"
														]
													},
													{
														"NoteCode": [
															"APM"
														],
														"Description": [
															"PRIVATE RATES USED *F*"
														]
													},
													{
														"NoteCode": [
															"SUR"
														],
														"Description": [
															"FARE VALID FOR E TICKET ONLY"
														]
													}
												]
											}
										],
										"BookingDateTime": [
											"16/05/2013 15:40"
										],
										"TicketIssueDateLimit": [
											"16/05/2013"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0"
										],
										"AgentEmail": [
											"alessandro.re@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"0.00"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"21:00"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureDateTime": [
															"23/05/2013 15:50"
														],
														"ArrivalDateTime": [
															"24/05/2013 12:50"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"ALC"
																		],
																		"DepartureDateTime": [
																			"23/05/2013 15:50"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 17:00"
																		],
																		"FlightNumber": [
																			"3886"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"32S"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"O"
																		],
																		"FareCode": [
																			"ODSLRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"ALC"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureDateTime": [
																			"24/05/2013 12:05"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 12:50"
																		],
																		"FlightNumber": [
																			"8906"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AircraftType": [
																			"AT7"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:10"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"25/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"25/05/2013 14:45"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"25/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 14:45"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AircraftType": [
																			"CRK"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"130.00"
														],
														"TaxAmount": [
															"43.34"
														],
														"AramixFeeAmount": [
															"5"
														],
														"AgencyFeeAmount": [
															"0"
														]
													}
												]
											}
										],
										"FlightTickets": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"Warnings": [
											""
										],
										"Messages": [
											""
										]
									}
								],
								"Error": [
									{
										"$": {
											"i:nil": "true"
										}
									}
								],
								"RequestID": [
									"2212171"
								]
							}
						]
					}
				]
			}
		]
	}
}