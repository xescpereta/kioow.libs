﻿
var _redsys = require('./redsys');
var redsys = new _redsys.redsysAPI();

process.send({ message: 'redsys.ready' });

process.on('message', function (command) { 
    
    var rpc = command.id;
    var response = null;
    response = redsys[command.method](command.params);

    process.send({ message: 'redsys.response', id: rpc, result: response });

});
