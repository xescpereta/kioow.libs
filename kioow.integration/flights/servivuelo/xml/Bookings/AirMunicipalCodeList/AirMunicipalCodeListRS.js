module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirMunicipalCodeListResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirMunicipalCodeListResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirMunicipalCode": [
									{
										"Code": [
											"380012"
										],
										"Name": [
											"Adeje"
										]
									},
									{
										"Code": [
											"350017"
										],
										"Name": [
											"Agaete"
										]
									},
									{
										"Code": [
											"350022"
										],
										"Name": [
											"Agüimes"
										]
									},
									{
										"Code": [
											"380027"
										],
										"Name": [
											"Agulo"
										]
									},
									{
										"Code": [
											"380033"
										],
										"Name": [
											"Alajeró"
										]
									},
									{
										"Code": [
											"350038"
										],
										"Name": [
											"Antigua"
										]
									},
									{
										"Code": [
											"380048"
										],
										"Name": [
											"Arafo"
										]
									},
									{
										"Code": [
											"380051"
										],
										"Name": [
											"Arico"
										]
									},
									{
										"Code": [
											"380064"
										],
										"Name": [
											"Arona"
										]
									},
									{
										"Code": [
											"350043"
										],
										"Name": [
											"Arrecife"
										]
									},
									{
										"Code": [
											"350056"
										],
										"Name": [
											"Artenara"
										]
									},
									{
										"Code": [
											"350069"
										],
										"Name": [
											"Arucas"
										]
									},
									{
										"Code": [
											"380070"
										],
										"Name": [
											"Barlovento"
										]
									},
									{
										"Code": [
											"350075"
										],
										"Name": [
											"Betancuria"
										]
									},
									{
										"Code": [
											"380086"
										],
										"Name": [
											"Breña Alta"
										]
									},
									{
										"Code": [
											"380099"
										],
										"Name": [
											"Breña Baja"
										]
									},
									{
										"Code": [
											"380103"
										],
										"Name": [
											"Buenavista del Norte"
										]
									},
									{
										"Code": [
											"380110"
										],
										"Name": [
											"Candelaria"
										]
									},
									{
										"Code": [
											"380125"
										],
										"Name": [
											"Fasnia"
										]
									},
									{
										"Code": [
											"350081"
										],
										"Name": [
											"Firgas"
										]
									},
									{
										"Code": [
											"380131"
										],
										"Name": [
											"Frontera"
										]
									},
									{
										"Code": [
											"380146"
										],
										"Name": [
											"Fuencaliente de la Palma"
										]
									},
									{
										"Code": [
											"350094"
										],
										"Name": [
											"Gáldar"
										]
									},
									{
										"Code": [
											"380159"
										],
										"Name": [
											"Garachico"
										]
									},
									{
										"Code": [
											"380162"
										],
										"Name": [
											"Garafía"
										]
									},
									{
										"Code": [
											"380178"
										],
										"Name": [
											"Granadilla de Abona"
										]
									},
									{
										"Code": [
											"380184"
										],
										"Name": [
											"Guancha (La)"
										]
									},
									{
										"Code": [
											"380197"
										],
										"Name": [
											"Guía de Isora"
										]
									},
									{
										"Code": [
											"380201"
										],
										"Name": [
											"Güimar"
										]
									},
									{
										"Code": [
											"350108"
										],
										"Name": [
											"Haría"
										]
									},
									{
										"Code": [
											"380218"
										],
										"Name": [
											"Hermigua"
										]
									},
									{
										"Code": [
											"380223"
										],
										"Name": [
											"Icod de los Vinos"
										]
									},
									{
										"Code": [
											"350115"
										],
										"Name": [
											"Ingenio"
										]
									},
									{
										"Code": [
											"380244"
										],
										"Name": [
											"Llanos de Aridane (Los)"
										]
									},
									{
										"Code": [
											"380257"
										],
										"Name": [
											"Matanza de Acentejo (La)"
										]
									},
									{
										"Code": [
											"350120"
										],
										"Name": [
											"Mogán"
										]
									},
									{
										"Code": [
											"350136"
										],
										"Name": [
											"Moya"
										]
									},
									{
										"Code": [
											"350141"
										],
										"Name": [
											"Oliva (La)"
										]
									},
									{
										"Code": [
											"380260"
										],
										"Name": [
											"Orotava (La)"
										]
									},
									{
										"Code": [
											"350154"
										],
										"Name": [
											"Pájara"
										]
									},
									{
										"Code": [
											"350167"
										],
										"Name": [
											"Palmas de Gran Canaria (Las)"
										]
									},
									{
										"Code": [
											"380276"
										],
										"Name": [
											"Paso (El)"
										]
									},
									{
										"Code": [
											"380282"
										],
										"Name": [
											"Puerto de la Cruz"
										]
									},
									{
										"Code": [
											"350173"
										],
										"Name": [
											"Puerto del Rosario"
										]
									},
									{
										"Code": [
											"380295"
										],
										"Name": [
											"Puntagorda"
										]
									},
									{
										"Code": [
											"380309"
										],
										"Name": [
											"Puntallana"
										]
									},
									{
										"Code": [
											"380316"
										],
										"Name": [
											"Realejos (Los)"
										]
									},
									{
										"Code": [
											"380321"
										],
										"Name": [
											"Rosario (El)"
										]
									},
									{
										"Code": [
											"380337"
										],
										"Name": [
											"San Andrés y Sauces"
										]
									},
									{
										"Code": [
											"350189"
										],
										"Name": [
											"San Bartolomé"
										]
									},
									{
										"Code": [
											"350192"
										],
										"Name": [
											"San Bartolomé de Tirajana"
										]
									},
									{
										"Code": [
											"380239"
										],
										"Name": [
											"San Cristobal de La Laguna"
										]
									},
									{
										"Code": [
											"380342"
										],
										"Name": [
											"San Juan de la Rambla"
										]
									},
									{
										"Code": [
											"380355"
										],
										"Name": [
											"San Miguel de Abona"
										]
									},
									{
										"Code": [
											"350206"
										],
										"Name": [
											"San Nicolás de Tolentino"
										]
									},
									{
										"Code": [
											"380368"
										],
										"Name": [
											"San Sebastián de la Gomera"
										]
									},
									{
										"Code": [
											"350213"
										],
										"Name": [
											"Santa Brígida"
										]
									},
									{
										"Code": [
											"380374"
										],
										"Name": [
											"Santa Cruz de la Palma"
										]
									},
									{
										"Code": [
											"380380"
										],
										"Name": [
											"Santa Cruz de Tenerife"
										]
									},
									{
										"Code": [
											"350228"
										],
										"Name": [
											"Santa Lucía de Tirajana"
										]
									},
									{
										"Code": [
											"350234"
										],
										"Name": [
											"Santa María de Guía de Gran Canaria"
										]
									},
									{
										"Code": [
											"380393"
										],
										"Name": [
											"Santa Ursula"
										]
									},
									{
										"Code": [
											"380407"
										],
										"Name": [
											"Santiago del Teide"
										]
									},
									{
										"Code": [
											"380414"
										],
										"Name": [
											"Sauzal (El)"
										]
									},
									{
										"Code": [
											"380429"
										],
										"Name": [
											"Silos (Los)"
										]
									},
									{
										"Code": [
											"380435"
										],
										"Name": [
											"Tacoronte"
										]
									},
									{
										"Code": [
											"380440"
										],
										"Name": [
											"Tanque (El)"
										]
									},
									{
										"Code": [
											"380453"
										],
										"Name": [
											"Tazacorte"
										]
									},
									{
										"Code": [
											"380466"
										],
										"Name": [
											"Tegueste"
										]
									},
									{
										"Code": [
											"350249"
										],
										"Name": [
											"Teguise"
										]
									},
									{
										"Code": [
											"350252"
										],
										"Name": [
											"Tejeda"
										]
									},
									{
										"Code": [
											"350265"
										],
										"Name": [
											"Telde"
										]
									},
									{
										"Code": [
											"350271"
										],
										"Name": [
											"Teror"
										]
									},
									{
										"Code": [
											"350287"
										],
										"Name": [
											"Tías"
										]
									},
									{
										"Code": [
											"380472"
										],
										"Name": [
											"Tijarafe"
										]
									},
									{
										"Code": [
											"350290"
										],
										"Name": [
											"Tinajo"
										]
									},
									{
										"Code": [
											"350304"
										],
										"Name": [
											"Tuineje"
										]
									},
									{
										"Code": [
											"380491"
										],
										"Name": [
											"Valle Gran Rey"
										]
									},
									{
										"Code": [
											"380504"
										],
										"Name": [
											"Vallehermoso"
										]
									},
									{
										"Code": [
											"350326"
										],
										"Name": [
											"Valleseco"
										]
									},
									{
										"Code": [
											"350311"
										],
										"Name": [
											"Valsequillo de Gran Canaria"
										]
									},
									{
										"Code": [
											"380488"
										],
										"Name": [
											"Valverde"
										]
									},
									{
										"Code": [
											"350332"
										],
										"Name": [
											"Vega de San Mateo"
										]
									},
									{
										"Code": [
											"380511"
										],
										"Name": [
											"Victoria de Acentejo (La)"
										]
									},
									{
										"Code": [
											"380526"
										],
										"Name": [
											"Vilaflor"
										]
									},
									{
										"Code": [
											"380532"
										],
										"Name": [
											"Villa de Mazo"
										]
									},
									{
										"Code": [
											"350347"
										],
										"Name": [
											"Yaiza"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}