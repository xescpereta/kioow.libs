
module.exports.templates = function (mailspath) { 
    var templates = {
        travelerthanks_es: mailspath + '/user/user-01-welcome.html',
        travelerthanks_en: mailspath + '/user/user-01-welcome.html',
        estravelermessage: mailspath + '/user/es-user-26-message.html.swig',
        entravelermessage: mailspath + '/user/en-user-26-message.html.swig',   
        
        dmcthanks_en: mailspath + '/dmc/en-dmc-01-thanks.html',
        dmcwelcome_en: mailspath + '/dmc/en-dmc-02-welcome.html',
        dmcemailconfirmed_en: mailspath + '/dmc/en-dmc-03-email-confirmed.html.swig',
        dmcemailautopublish_en: mailspath + '/dmc/en-dmc-03.1-you-can-publish.html.swig',
        dmcroomrqbooking_en: mailspath + '/dmc/en-dmc-11-new-booking-triple-single-room.html.swig',
        dmcthanks_es: mailspath + '/dmc/es-dmc-01-thanks.html',
        dmcwelcome_es: mailspath + '/dmc/es-dmc-02-welcome.html',
        dmcemailconfirmed_es: mailspath + '/dmc/es-dmc-03-email-confirmed.html',
        dmcforgotpassword: {
            path: mailspath +'/dmc/en-dmc-000-forgot.html.swig',
            subject: 'Password Recovery Request',
            sender: 'userinfo@openmarket.travel',
            replaces: null
        },
        dmcbooking: {
            path: mailspath + '/dmc/en-dmc-12-new-booking.html.swig', 
            subject: '[kow] New Booking %idbooking%',
            sender: 'sender@openmarket.travel',
            replaces: ['idbooking']
        },
        dmcbookingcancelled: {
            path: mailspath + '/dmc/en-dmc-18-booking-cancell.html.swig', 
            subject: '[kow] Booking %idbooking% has been CANCELLED',
            sender: 'sender@openmarket.travel',
            replaces: ['idbooking']
        },        
        dmcquotelost_en: {
            path: mailspath + '/dmc/en-dmc-14-quote-lost.html.swig', 
            subject: '[kow] Your quote %quotecode% has been not selected.',
            sender: 'sender@openmarket.travel',
            replaces: ['quotecode']
        },        
        dmcqueryclosed_en: {
            path: mailspath + '/dmc/en-dmc-13-query-closed.html.swig', 
            subject:'[kow] The request %querycode% is canceled.',
            sender: 'sender@openmarket.travel',
            replaces: ['querycode']
        },                
        
        dmcnewrequest: mailspath + '/dmc/dmc-07-new-request.html.swig',
        dmcnewrequest_en: mailspath + '/dmc/en-dmc-07-new-request.html.swig',        
        dmcquotewin_en: mailspath + '/dmc/en-dmc-15-quote-won.html.swig',        
        dmcquotediscard_en: mailspath + '/dmc/en-dmc-16-quote-discarded.html.swig',
        
        dmcquerycanceled_en: mailspath + '/dmc/en-dmc-17-query-canceled.html.swig',
        dmcbookingchange: {
            path: mailspath + '/dmc/en-dmc-19-booking-change.html.swig', 
            subject: '[kow] Booking %idbooking% has been MODIFIED',
            sender: 'sender@openmarket.travel',
            replaces: ['idbooking']
        },
        
        dmcnewmessagetailormade: {
            path: mailspath + '/dmc/en-dmc-08-new-message.html.swig', 
            subject: '[kow] New message Tailor Made ID:  %querycode%',
            sender: 'sender@openmarket.travel',
            replaces: ['querycode']
        },
               
        kownewmessagetailormade: {
            path: mailspath + '/kow/kow-16-query-msg.html.swig', 
            subject: 'Nuevo mensaje Tailor Made ID: - %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['querycode']
        },
        kowquotediscard: {
        	path: mailspath + '/kow/kow-13-quote-discarded.html.swig',        	  
            subject: 'Descartada una respuesta petición a medida:  %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['querycode']
        },        
        kowquotecanceled: mailspath + '/kow/kow-14-query-canceled.html.swig',
        kowquotediscard: mailspath + '/kow/kow-13-quote-discarded.html.swig',
        kownewdmc: mailspath + '/kow/kow-01-new-dmc.html',
        kowdmcchanged: mailspath + '/kow/kow-02-dmc-changed-profile.html.swig',
        kowgunoca: mailspath + '/kow/kow-03-gu-noca.html.swig',
        kowmessage: mailspath + '/kow/kow-10-message.html.swig',
        kowadminmessage: {
            path: mailspath + '/kow/38-admin-message.html.swig',
            subject: 'Nuevo mensaje de %name%',
            sender: 'sender@kioow.com',
            replaces: ['name']
        },
        kowroomrqbooking: mailspath + '/kow/kow-04-new-booking-triple-single-room.html.swig',
        kowroomrqcancelled: mailspath + '/kow/kow-11-triple-single-cancelled.html.swig',
        kowroomrqconfirmed: mailspath + '/kow/kow-12-triple-single-confirmed.html.swig',
        kownewrequest: mailspath + '/kow/kow-07-new-request.html.swig',
        kowprebooking: mailspath + '/kow/kow-05-new-prebooking.html.swig',
        
        kowbooking:{
            path: mailspath + '/kow/36-admin-new-booking.html.swig', 
            subject: 'Nueva reserva de afiliado - %idbooking%',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        kowbooking:{
            path: mailspath + '/kow/kow-06-new-booking.html.swig', 
            subject: 'Nueva reserva de afiliado - %idbooking%',
            sender: 'sender@openmarket.travel',
            replaces: ['idbooking']
        },
        	
        kownewaffiliate: {
            path: mailspath + '/kow/31-admin-new-agency.html.swig', 
            subject: 'Nuevo registro de afiliado - %company.name%',
            sender: 'sender@kioow.com',
            replaces: ['company.name']
        },
        kownewaffiliaterequest: {
            path: mailspath + '/kow/kow-07-new-request.html.swig', 
            subject: 'Nueva solicitud de %companyname% ID: %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['companyname', 'querycode']
        },
        userroomrqbooking: mailspath + '/user/user-22-request-booking-triple-single.html.swig',
        userprebooking: mailspath + '/user/user-19-prebooking.html.swig',
        userbookingtransfer: mailspath + '/user/user-24-booking-transfer.html.swig',
        userbooking: mailspath + '/user/user-21-booking-100.html.swig',
        userbookingcancelled: {
            path: mailspath + '/user/user-29-booking-cancelled.html.swig', 
            subject: 'Reserva %idbooking% cancelada',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        userforgotpassword: {
            path: mailspath + '/user/user-000-forgot.html.swig',
            subject: 'Password Recovery Request',
            sender: 'userinfo@openmarket.travel',
            replaces: null
        },
        userremember60: mailspath + '/user/user-20-booking-remember-60.html.swig',
        userroomrqcancelled: mailspath + '/user/user-22.1-request-triple-single-cancelled.html.swig',
        userroomrqconfirmed: mailspath + '/user/user-22.2-request-triple-single-confirmed.html.swig',
        userremembervoucher: mailspath + '/user/es-user-30-remember-voucher.html.swig',
        usernewrequest: mailspath + '/user/user-09-quote-sent.html.swig',
        usernewquote: mailspath + '/user/user-12-new-quote.html.swig',    
        usernewmessage: mailspath + '/user/user-17-new-chat-msg.html.swig',
        
        
        
        kowaffiliatecontact: {
            path: mailspath + '/kow/00-message.html.swig', 
            subject: 'Gracias por tu mensaje',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliatethanks: {
            path: mailspath + '/kow/01-account-welcome.html.swig', 
            subject: 'Gracias por registrarte',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliateconfirmed: {
            path: mailspath + '/kow/02-account-email-confirmed.html.swig', 
            subject: 'Cuenta habilitada',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliateforgotpassword: {
            path: mailspath + '/kow/03-account-password-forgotten.html.swig', 
            subject: 'Recuperar contraseña',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliatechangeemail: {
            path: mailspath + '/kow/04-account-email-changed.html.swig', 
            subject: 'Correo electrónico confirmado.',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliatechangepassword: {
            path: mailspath + '/kow/05-account-password-changed.html.swig', 
            subject: 'Contraseña restablecida.',
            sender: 'sender@kioow.com',
            replaces: null
        },
        kowaffiliatebooking: {
            path: mailspath + '/kow/11-booking-new.html.swig', 
            subject: 'Reserva - %idbooking%',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        kowaffiliatenewrequest: {
            path: mailspath + '/kow/22-tailormade-new.html.swig', 
            subject: 'Tu petición a medida ID: %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['querycode']
        },
        kowaffiliatenewquote: {
            path: mailspath + '/kow/23-tailormade-quote.html.swig', 
            subject: 'Nueva cotizacion para tu viaje a medida ID: %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['querycode']
        },
        kowaffiliatenewmessage: {
            path: mailspath + '/kow/23-tailormade-quote.html.swig', 
            subject: 'Nuevo mensaje de %dmccompanyname%',
            sender: 'sender@kioow.com',
            replaces: ['dmccompanyname']
        },
        kowaffiliaterememberpayment: {
            path: mailspath + '/kow/12-booking-payment-second-pendending.html.swig', 
            subject: 'Completa el pago de la reserva - %idbooking%',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        kowaffiliatecancellbooking: {
            path: mailspath + '/kow/13-booking-cancelled.html.swig', 
            subject: 'Reserva - %idbooking% cancelada',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        kowaffiliateupdatedbooking: {
            path: mailspath + '/kow/15-booking-updated-by-affiliate.html.swig', 
            subject: 'Reserva - %idbooking% actualizada',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },        
        kowadmincancellquery: {        
            path: mailspath + '/kow/kow-14-query-canceled.html.swig', 
            subject: 'Cancelación de solicitud - %querycode%',
            sender: 'sender@kioow.com',
            replaces: ['querycode']
        },
        kowadmincancellbooking: {
        	 path: mailspath + '/kow/34-admin-booking-cancelled.html.swig', 
             subject: 'Reserva - %idbooking% cancelada',
             sender: 'sender@kioow.com',
             replaces: ['idbooking']
        },
        kowadminbookingchange: {
            path: mailspath + '/kow/35-admin-booking-updated.html.swig', 
            subject: '[kow]La Reseva %idbooking% ha sido MODIFICADA',
            sender: 'sender@openmarket.travel',
            replaces: ['idbooking']
        },
        kowadminbookingchat: {
            path: mailspath + '/kow/37-admin-booking-chat-msg.html.swig', 
            subject: 'Nuevo Mensaje para la Reserva %idbooking%',
            sender: 'sender@kioow.com',
            replaces: ['idbooking']
        },
        
        
       
        //Debes realizar el pago de la reserva - %idbooking%
        generic: mailspath + '/kow/skeletonmail.html'
    };
    return templates;
};