module.exports = {
	"AirBookingRS": {
		"$": {
			"xmlns": "http://webservices.aramix.es"
		},
		"AirBooking": [
			{
				"BookingID": [
					"2925"
				],
				"AvailRequestID": [
					"225498"
				],
				"BookingRequestID": [
					"225499"
				],
				"BookingReference": [
					"6QM8N6"
				],
				"AirTravelers": [
					{
						"AirTraveler": [
							{
								"TravelerType": [
									"Adult"
								],
								"TravelerTitle": [
									"Mr"
								],
								"DocumentType": [
									"PAS"
								],
								"TravelerID": [
									"1"
								],
								"InfantID": [
									"0"
								],
								"FirstName": [
									"DAVID"
								],
								"LastName": [
									"JIMENEZ"
								],
								"DocumentNumber": [
									"102030"
								],
								"Email": [
									""
								],
								"Phone": [
									"913329781"
								],
								"BirthDate": [
									"31/07/1975"
								],
								"FrequentFlyerProgram": [
									"IB"
								],
								"FrequentFlyerCardNumber": [
									""
								],
								"AdditionalBaggages": [
									"0"
								],
								"IsResident": [
									"false"
								],
								"ResidentDocumentType": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"ResidentCityCode": [
									""
								],
								"ResidentCertificateNumber": [
									""
								],
								"TSAData": [
									{
										"FirstName": [
											"DAVID"
										],
										"LastName": [
											"JIMENEZ"
										],
										"BirthDate": [
											"31/07/1975"
										],
										"DocumentExpirationDate": [
											""
										],
										"VisaIssueDate": [
											""
										],
										"IsResidentUSA": [
											"false"
										]
									}
								]
							},
							{
								"TravelerType": [
									"Adult"
								],
								"TravelerTitle": [
									"Mr"
								],
								"DocumentType": [
									"PAS"
								],
								"TravelerID": [
									"2"
								],
								"InfantID": [
									"0"
								],
								"FirstName": [
									"LUIS"
								],
								"LastName": [
									"JIMENEZ"
								],
								"DocumentNumber": [
									"102050"
								],
								"Email": [
									""
								],
								"Phone": [
									"913329781"
								],
								"BirthDate": [
									"22/05/1977"
								],
								"FrequentFlyerProgram": [
									"IB"
								],
								"FrequentFlyerCardNumber": [
									""
								],
								"AdditionalBaggages": [
									"0"
								],
								"IsResident": [
									"false"
								],
								"ResidentDocumentType": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"ResidentCityCode": [
									""
								],
								"ResidentCertificateNumber": [
									""
								],
								"TSAData": [
									{
										"FirstName": [
											"LUIS"
										],
										"LastName": [
											"JIMENEZ"
										],
										"BirthDate": [
											"22/05/1977"
										],
										"DocumentExpirationDate": [
											""
										],
										"VisaIssueDate": [
											""
										],
										"IsResidentUSA": [
											"false"
										]
									}
								]
							},
							{
								"TravelerType": [
									"Child"
								],
								"TravelerTitle": [
									"Mr"
								],
								"DocumentType": [
									"PAS"
								],
								"TravelerID": [
									"3"
								],
								"InfantID": [
									"0"
								],
								"FirstName": [
									"CARMEN"
								],
								"LastName": [
									"JIMENEZ"
								],
								"DocumentNumber": [
									"102040"
								],
								"Email": [
									""
								],
								"Phone": [
									"913329781"
								],
								"BirthDate": [
									"22/05/2006"
								],
								"FrequentFlyerProgram": [
									"IB"
								],
								"FrequentFlyerCardNumber": [
									""
								],
								"AdditionalBaggages": [
									"0"
								],
								"IsResident": [
									"false"
								],
								"ResidentDocumentType": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"ResidentCityCode": [
									""
								],
								"ResidentCertificateNumber": [
									""
								],
								"TSAData": [
									{
										"FirstName": [
											"CARMEN"
										],
										"LastName": [
											"JIMENEZ"
										],
										"BirthDate": [
											"22/05/2006"
										],
										"DocumentExpirationDate": [
											""
										],
										"VisaIssueDate": [
											""
										],
										"IsResidentUSA": [
											"false"
										]
									}
								]
							}
						]
					}
				],
				"AirBookingNotes": [
					{
						"AirBookingNote": [
							{
								"NoteCode": [
									"PEN"
								],
								"Description": [
									"TICKETS ARE NON-REFUNDABLE"
								]
							},
							{
								"NoteCode": [
									"LTD"
								],
								"Description": [
									"LAST TKT DTE16FEB13 - SEE ADV PURCHASE"
								]
							},
							{
								"NoteCode": [
									"APM"
								],
								"Description": [
									"PRIVATE RATES USED *F*"
								]
							},
							{
								"NoteCode": [
									"SUR"
								],
								"Description": [
									"FARE VALID FOR E TICKET ONLY"
								]
							},
							{
								"NoteCode": [
									"MSJ_EMISOR"
								],
								"Description": [
									"Notas del emisor"
								]
							},
							{
								"NoteCode": [
									"MSJ_CLIENTE"
								],
								"Description": [
									"Notas del cliente"
								]
							}
						]
					}
				],
				"BookingDateTime": [
					"22/03/2013 12:46"
				],
				"TicketIssueDateLimit": [
					"16/02/2013"
				],
				"MarketingCarrierCode": [
					"IB"
				],
				"CancellationInsurance": [
					"false"
				],
				"CancellationInsuranceAmount": [
					"0"
				],
				"AgentEmail": [
					"alessandro.re@avanze.net"
				],
				"DestinationContactPhone": [
					"Telefono en destino"
				],
				"BookingStatus": [
					"RES"
				],
				"AmountDifference": [
					"0.00"
				],
				"AirBookingFlightSegments": [
					{
						"AirBookingFlightSegment": [
							{
								"SegmentNumber": [
									"1"
								],
								"TotalDuration": [
									"14:20"
								],
								"DepartureAirportLocationCode": [
									"MAD"
								],
								"ArrivalAirportLocationCode": [
									"IBZ"
								],
								"DepartureDateTime": [
									"23/03/2013 07:30"
								],
								"ArrivalDateTime": [
									"23/03/2013 21:50"
								],
								"FlightSegmentLegs": [
									{
										"AirBookingFlightSegmentLeg": [
											{
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"ALC"
												],
												"DepartureDateTime": [
													"23/03/2013 07:30"
												],
												"ArrivalDateTime": [
													"23/03/2013 08:40"
												],
												"FlightNumber": [
													"3890"
												],
												"OperatingCarrierCode": [
													"I2"
												],
												"ArrivalAirportTerminal": [
													"3"
												],
												"DepartureAirportTerminal": [
													"4"
												],
												"AircraftType": [
													"32S"
												],
												"CabinType": [
													"M"
												],
												"CabinClass": [
													"P"
												],
												"FareCode": [
													"PDSXRTNY"
												],
												"FareType": [
													"Privada"
												],
												"ETicket": [
													"true"
												],
												"AllowedBaggage": [
													{
														"BaggageWeight": [
															"0"
														],
														"BaggageQuantity": [
															"1"
														],
														"BaggageType": [
															"N"
														]
													}
												],
												"TechnicalStops": [
													""
												]
											},
											{
												"DepartureAirportLocationCode": [
													"ALC"
												],
												"ArrivalAirportLocationCode": [
													"IBZ"
												],
												"DepartureDateTime": [
													"23/03/2013 21:10"
												],
												"ArrivalDateTime": [
													"23/03/2013 21:50"
												],
												"FlightNumber": [
													"8904"
												],
												"OperatingCarrierCode": [
													"YW"
												],
												"AircraftType": [
													"CR9"
												],
												"CabinType": [
													"M"
												],
												"CabinClass": [
													"P"
												],
												"FareCode": [
													"PD"
												],
												"FareType": [
													"Publicada"
												],
												"ETicket": [
													"true"
												],
												"AllowedBaggage": [
													{
														"BaggageWeight": [
															"0"
														],
														"BaggageQuantity": [
															"1"
														],
														"BaggageType": [
															"N"
														]
													}
												],
												"TechnicalStops": [
													""
												]
											}
										]
									}
								]
							},
							{
								"SegmentNumber": [
									"2"
								],
								"TotalDuration": [
									"11:45"
								],
								"DepartureAirportLocationCode": [
									"IBZ"
								],
								"ArrivalAirportLocationCode": [
									"MAD"
								],
								"DepartureDateTime": [
									"25/03/2013 23:30"
								],
								"ArrivalDateTime": [
									"26/03/2013 11:15"
								],
								"FlightSegmentLegs": [
									{
										"AirBookingFlightSegmentLeg": [
											{
												"DepartureAirportLocationCode": [
													"IBZ"
												],
												"ArrivalAirportLocationCode": [
													"VLC"
												],
												"DepartureDateTime": [
													"25/03/2013 23:30"
												],
												"ArrivalDateTime": [
													"26/03/2013 00:15"
												],
												"FlightNumber": [
													"8439"
												],
												"OperatingCarrierCode": [
													"YW"
												],
												"AircraftType": [
													"AT7"
												],
												"CabinType": [
													"M"
												],
												"CabinClass": [
													"P"
												],
												"FareCode": [
													"PD2LRT"
												],
												"FareType": [
													"Privada"
												],
												"ETicket": [
													"true"
												],
												"AllowedBaggage": [
													{
														"BaggageWeight": [
															"0"
														],
														"BaggageQuantity": [
															"1"
														],
														"BaggageType": [
															"N"
														]
													}
												],
												"TechnicalStops": [
													""
												]
											},
											{
												"DepartureAirportLocationCode": [
													"VLC"
												],
												"ArrivalAirportLocationCode": [
													"MAD"
												],
												"DepartureDateTime": [
													"26/03/2013 10:20"
												],
												"ArrivalDateTime": [
													"26/03/2013 11:15"
												],
												"FlightNumber": [
													"8993"
												],
												"OperatingCarrierCode": [
													"YW"
												],
												"ArrivalAirportTerminal": [
													"4"
												],
												"AircraftType": [
													"CR9"
												],
												"CabinType": [
													"M"
												],
												"CabinClass": [
													"P"
												],
												"FareCode": [
													"PD2RTNEY"
												],
												"FareType": [
													"Privada"
												],
												"ETicket": [
													"true"
												],
												"AllowedBaggage": [
													{
														"BaggageWeight": [
															"0"
														],
														"BaggageQuantity": [
															"1"
														],
														"BaggageType": [
															"N"
														]
													}
												],
												"TechnicalStops": [
													""
												]
											}
										]
									}
								]
							}
						]
					}
				],
				"TravelersInfoFares": [
					{
						"AirBookingFare": [
							{
								"TravelerInfo": [
									{
										"TravelerType": [
											"Adult"
										],
										"IsResident": [
											"false"
										]
									}
								],
								"TotalTravelers": [
									"2"
								],
								"FareAmount": [
									"134.00"
								],
								"TaxAmount": [
									"50.95"
								],
								"AramixFeeAmount": [
									"5"
								],
								"AgencyFeeAmount": [
									"0"
								]
							},
							{
								"TravelerInfo": [
									{
										"TravelerType": [
											"Child"
										],
										"IsResident": [
											"false"
										]
									}
								],
								"TotalTravelers": [
									"1"
								],
								"FareAmount": [
									"121.00"
								],
								"TaxAmount": [
									"50.95"
								],
								"AramixFeeAmount": [
									"5"
								],
								"AgencyFeeAmount": [
									"0"
								]
							}
						]
					}
				],
				"Warnings": [
					""
				],
				"Messages": [
					""
				]
			}
		],
		"RequestID": [
			"225499"
		]
	}
}