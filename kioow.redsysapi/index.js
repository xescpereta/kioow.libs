﻿var common = require('yourttoo.common');
var wp = require('child_process');

var Redsys = function (options) {
  
    this.id = common.utils.getToken();
    this.configuration = options || { readyevent: 'ready.' + this.id, responseevent: 'response.' + this.id };
    this.readyevent = this.configuration.readyevent || 'ready.' + this.id;
    this.responseevent = this.configuration.responseevent || 'response.' + this.id;
    this.worker = wp.fork('./redsysproxy');
    this.requests = new common.hashtable.HashTable();
    this.worker.on('message', function (message) {

        if (message.message == 'redsys.ready') { 
            self.emit(self.readyevent, self);
        }

        if (message.message == 'redsys.response') {
            var result = message.result;
            var response = self.requests.get(message.id);
            if (response != null) {
                response.emit(self.responseevent, result);
                response.emit(message.id, result);
                self.requests.remove(message.id);
            }
        }

    });
    var self = this;
    return self;
}

var eventThis = common.eventtrigger;
eventThis.eventtrigger(Redsys);

Redsys.prototype.send = function (command) {
    var self = this;
    console.log('command for ' + self.id);
    self.worker.send(command);
}

Redsys.prototype.createMerchantParameters = function (merchantparameters, callback) {
    var self = this;
    if (merchantparameters == null) { throw new Error('the input parameter can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'createMerchantParameters';
        this.params = merchantparameters;
    };
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}

Redsys.prototype.getParameter = function (options, callback) {
    var self = this;
    if (options == null) { throw new Error('the input parametername can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'getParameter';
        this.params = options;
    }
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}

Redsys.prototype.setParameter = function (parametername, value, callback) {
    var self = this;
    if (parametername == null || value == null) { throw new Error('the input parametername or value can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'setParameter';
        this.params = { key: parametername, value: value };
    }
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}

Redsys.prototype.createMerchantSignature = function (privatekey, callback) {
    var self = this;
    if (privatekey == null) { throw new Error('the input privatekey can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'createMerchantSignature';
        this.params = privatekey;
    }
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}


// REDSYS RESPONSE HANDLERS
Redsys.prototype.decodeMerchantParameters = function (merchantparameters, callback) {
    var self = this;
    if (merchantparameters == null) { throw new Error('the input parameter can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'decodeMerchantParameters';
        this.params = merchantparameters;
    }
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}

Redsys.prototype.createMerchantSignatureNotif = function (privatekey, merchantparameters, callback) {
    var self = this;
    if (privatekey == null) { throw new Error('the input privatekey can not be null or undefined'); }
    var prresponse = function () {
        this.id = common.utils.getToken();
        this.method = 'createMerchantSignatureNotif';
        this.params = [privatekey, merchantparameters];
    }
    common.eventtrigger.eventtrigger(prresponse);
    var response = new prresponse();
    response.on(prresponse.id, function (result) {
        if (callback) {
            callback(result);
        }
    });
    self.requests.set(response.id, response);
    self.send(response);
    return response;
}


module.exports.Redsys = Redsys;
module.exports.RedsysAPI = require('./redsys');

