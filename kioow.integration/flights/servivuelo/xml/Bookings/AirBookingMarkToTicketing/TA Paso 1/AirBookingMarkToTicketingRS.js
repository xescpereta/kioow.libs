module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingMarkToTicketingResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingMarkToTicketingResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"43825"
										],
										"AvailRequestID": [
											"0"
										],
										"BookingRequestID": [
											"2843724"
										],
										"BookingReference": [
											"6VRTCE"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"PAS"
														],
														"TravelerID": [
															"45610"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"ADRIAN"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"102030"
														],
														"Email": [
															""
														],
														"Phone": [
															"913329781"
														],
														"BirthDate": [
															"31/07/1975"
														],
														"FrequentFlyerProgram": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"FrequentFlyerCardNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ADRIAN"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	""
																],
																"BirthDate": [
																	"31/07/1975"
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	""
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"102030"
																],
																"BirthCountry": [
																	""
																],
																"NationalityCountry": [
																	""
																],
																"VisaIssueCity": [
																	""
																],
																"VisaNumber": [
																	""
																],
																"VisaIssueCountry": [
																	""
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	""
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														],
														"TicketAmount": [
															"50.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"OBFeeAmount": [
															"0.00"
														],
														"Markup": [
															"0.00"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"47423"
																		],
																		"CabinClass": [
																			""
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			""
																		],
																		"FareBasis": [
																			"PDSYRX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"47424"
																		],
																		"CabinClass": [
																			""
																		],
																		"CabinType": [
																			"Q"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			""
																		],
																		"FareBasis": [
																			"QDS1RX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"BookingDateTime": [
											"27/06/2014 08:16"
										],
										"TicketIssueDateLimit": [
											"10/05/2014"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0.00"
										],
										"AgentEmail": [
											""
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"5.00"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"01:15"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"BCN"
														],
														"DepartureDateTime": [
															"10/07/2014 17:20"
														],
														"ArrivalDateTime": [
															"10/07/2014 18:35"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"BCN"
																		],
																		"DepartureDateTime": [
																			"10/07/2014 17:20"
																		],
																		"ArrivalDateTime": [
																			"10/07/2014 18:35"
																		],
																		"FlightNumber": [
																			"2732  "
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"321"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDSYRX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"47423"
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:30"
														],
														"DepartureAirportLocationCode": [
															"BCN"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"13/07/2014 13:20"
														],
														"ArrivalDateTime": [
															"13/07/2014 14:50"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"BCN"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"13/07/2014 13:20"
																		],
																		"ArrivalDateTime": [
																			"13/07/2014 14:50"
																		],
																		"FlightNumber": [
																			"2731  "
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			"1"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"Q"
																		],
																		"FareCode": [
																			"QDS1RX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"47424"
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"50.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"AgencyFeeAmount": [
															"1.50"
														]
													}
												]
											}
										],
										"FlightTickets": [
											""
										],
										"Warnings": [
											{
												"Warning": [
													{
														"WarningCode": [
															"W001_DIFFEMB"
														],
														"Description": [
															"Existe una diferencia entre el importe original y la retarificación de: 5,00"
														]
													}
												]
											}
										],
										"TotalAgencyAmount": [
											"99.51"
										],
										"TotalAmount": [
											"101.01"
										]
									}
								],
								"RequestID": [
									"2843727"
								]
							}
						]
					}
				]
			}
		]
	}
}