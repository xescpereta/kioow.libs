module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "52205729-64e6-4e29-833c-c8d23ade7d5a",
						"$": {
							"CorrelationId": "4e304dd8-5d79-4947-89f1-9e4ad18034a4",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirBookingCancelResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCancelResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"2957"
										],
										"AvailRequestID": [
											"0"
										],
										"BookingRequestID": [
											"2212171"
										],
										"BookingReference": [
											"6VUWJF"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"NIE"
														],
														"TravelerID": [
															"4183"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"ALESSANDRO"
														],
														"LastName": [
															"RE"
														],
														"DocumentNumber": [
															"X3493743C"
														],
														"Email": [
															"alessandro.re@avanze.net"
														],
														"Phone": [
															"687464925"
														],
														"BirthDate": [
															"31/07/1975"
														],
														"FrequentFlyerProgram": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"FrequentFlyerCardNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ALESSANDRO"
																],
																"LastName": [
																	"RE"
																],
																"Gender": [
																	""
																],
																"BirthDate": [
																	"31/07/1975"
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	""
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"X3493743C"
																],
																"BirthCountry": [
																	""
																],
																"NationalityCountry": [
																	""
																],
																"VisaIssueCity": [
																	""
																],
																"VisaNumber": [
																	""
																],
																"VisaIssueCountry": [
																	""
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	""
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											""
										],
										"BookingDateTime": [
											"16/05/2013 15:44"
										],
										"TicketIssueDateLimit": [
											"16/05/2013"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0.00"
										],
										"AgentEmail": [
											"alessandro.re@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"CAN"
										],
										"AmountDifference": [
											"0"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"21:00"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureDateTime": [
															"23/05/2013 15:50"
														],
														"ArrivalDateTime": [
															"24/05/2013 12:50"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"ALC"
																		],
																		"DepartureDateTime": [
																			"23/05/2013 15:50"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 17:00"
																		],
																		"FlightNumber": [
																			"3886  "
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"32S"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"O"
																		],
																		"FareCode": [
																			"ODSLRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"ALC"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureDateTime": [
																			"24/05/2013 12:05"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 12:50"
																		],
																		"FlightNumber": [
																			"8906  "
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			""
																		],
																		"DepartureAirportTerminal": [
																			""
																		],
																		"AircraftType": [
																			"AT7"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"ODSLRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:10"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"25/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"25/05/2013 14:45"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"25/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 14:45"
																		],
																		"FlightNumber": [
																			"8961  "
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			""
																		],
																		"AircraftType": [
																			"CRK"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"130.00"
														],
														"TaxAmount": [
															"43.34"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"AgencyFeeAmount": [
															"0.00"
														]
													}
												]
											}
										],
										"FlightTickets": [
											""
										],
										"Warnings": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										]
									}
								],
								"Error": [
									{
										"$": {
											"i:nil": "true"
										}
									}
								],
								"RequestID": [
									"2212173"
								]
							}
						]
					}
				]
			}
		]
	}
}