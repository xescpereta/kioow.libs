module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingImportResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingImportResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"3041"
										],
										"AvailRequestID": [
											"0"
										],
										"BookingRequestID": [
											"2750533"
										],
										"BookingReference": [
											"ZV2OUB"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"OTR"
														],
														"TravelerID": [
															"4303"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"DAVID"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															""
														],
														"Email": [
															""
														],
														"Phone": [
															"913329781"
														],
														"BirthDate": [
															""
														],
														"FrequentFlyerProgram": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"FrequentFlyerCardNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"DAVID"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	""
																],
																"BirthDate": [
																	""
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	""
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	""
																],
																"BirthCountry": [
																	""
																],
																"NationalityCountry": [
																	""
																],
																"VisaIssueCity": [
																	""
																],
																"VisaNumber": [
																	""
																],
																"VisaIssueCountry": [
																	""
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	""
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"BookingDateTime": [
											"08/10/2013 13:22"
										],
										"TicketIssueDateLimit": [
											"10/07/2013"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0"
										],
										"AgentEmail": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"DestinationContactPhone": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"0"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"01:05"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureDateTime": [
															"11/10/2013 12:00"
														],
														"ArrivalDateTime": [
															"11/10/2013 13:05"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureDateTime": [
																			"11/10/2013 12:00"
																		],
																		"ArrivalDateTime": [
																			"11/10/2013 13:05"
																		],
																		"FlightNumber": [
																			"8960  "
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			""
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"CRK"
																		],
																		"CabinType": [
																			" "
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			""
																		],
																		"FareType": [
																			""
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					"K"
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"50.00"
														],
														"TaxAmount": [
															"21.33"
														],
														"AramixFeeAmount": [
															"1.1"
														],
														"AgencyFeeAmount": [
															"6.6"
														]
													}
												]
											}
										],
										"FlightTickets": [
											""
										],
										"Warnings": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										]
									}
								],
								"RequestID": [
									"2750533"
								]
							}
						]
					}
				]
			}
		]
	}
}