﻿

module.exports = function (kioowcore, dbname) {
    var Types = kioowcore.Field.Types;

    var Kioow = new kioowcore.List('Kioows', {
        map: { name: 'code' },
        autokey: { path: 'slug', from: 'code', unique: true },
    });
    
    Kioow.add({
        code: { type: String, required: true, index: true },
        name: { type: String, required: true, index: true },
        slug: { type: String, index: true },
        info: {
            name: { type: String, index: true },
            phone: { type: String },
            celphone: { type: String },
            emergencyphone: { type: String },
            website: { type: String },
            location: {
                fulladdress: { type: String, index: true },
                city: { type: String, index: true },
                stateorprovince: { type: String, index: true },
                cp: { type: String, index: true },
                country: { type: String, index: true },
                countrycode: { type: String, index: true },
                continent: { type: String },
                latitude: { type: Types.Number },
                longitude: { type: Types.Number },
                route: { type: String },
            }
        },
        contact: {
            firstname: { type: String },
            lastname: { type: String },
            email: { type: Types.Email, index: true },
            fax: { type: String },
            skype: { type: String }
        },
        membership: {
            acceptterms: { type: Types.Boolean, index: true },
            membershipDate: { type: Types.Date, index: true },
            registervalid: { type: Types.Boolean, index: true },
            requestdelete: { type: Types.Boolean },
            colaborationagree: { type: Types.Boolean }
        },
        images: 
        {
            photo: { type: Types.CloudinaryImage, collapse: true },
            logo: { type: Types.CloudinaryImage, collapse: true },
            splash: { type: Types.CloudinaryImage, collapse: true }
        },
        currency: {
            label: String,
            symbol: String,
            value: String
        },
        timeZone: {
            gmtAdjustment: String,
            label: String,
            timeZoneId: String,
            useDaylightTime: String,
            value: String
        },
        comment : { type: String },
        user: { type: Types.Relationship, initial: true, ref: 'Users', index: true }
    });
    
    Kioow.schema.add({
        services: {
            type: [{
                    key: { type: String, index: true },
                    name: { type: String, index: true },
                    description: { type: String },
                    price: { type: Types.Number, index: true }
                }]
        }
    })
    
    Kioow.schema.add({
        pets: {
            type: [{
                    id: { type: String , index: true }
                }]
        }
    });

    Kioow.schema.add({
        availability: {
            type: [{
                    name: { type: String },
                    publishedDate: { type: Types.Date },
                    year: { type: Types.Number },
                    January: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    February: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    March: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    April: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    May: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    June: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    July: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    August: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    September: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    October: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    November: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    },
                    December: {
                        availability: {
                            type: [{
                                    date: { type: String },
                                    day: { type: Types.Number },
                                    available: { type: Types.Boolean },
                                    services: {
                                        type: [{
                                                key: { type: String, index: true },
                                                sameprice: { type: Types.Boolean },
                                                price: { type: Types.Number }
                                            }]
                                    }
                                }]
                        }
                    }
                }]
        }
    });
    
    
    
    Affiliate.schema.virtual('content.full').get(function () {
        return this.code || '';
    });
    
    /** 
        Relationships
        =============
    */

    Affiliate.addPattern('standard meta');
    Affiliate.defaultColumns = 'code, slug, name, phone|20%, weburl|20%, email';
    Affiliate.register(dbname);
}