module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingCreate": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingRQ": [
							{
								"AvailRequestID": [
									"2882775"
								],
								"AirTravelers": [
									{
										"AirTraveler": [
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"1"
												],
												"InfantID": [
													"5"
												],
												"FirstName": [
													"ADTA"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"50543958D"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"31/07/1988"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"ADTA"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"M"
														],
														"BirthDate": [
															"31/07/1988"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"50543958D"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00001"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											},
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"2"
												],
												"InfantID": [
													"6"
												],
												"FirstName": [
													"ADTB"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"50543955Y"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"31/07/1988"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"ADTA"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"F"
														],
														"BirthDate": [
															"31/07/1988"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"50543955Y"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00001"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											},
											{
												"TravelerType": [
													"Child"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"3"
												],
												"FirstName": [
													"CHDA"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"50543949T"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"31/07/2010"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"CHDA"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"M"
														],
														"BirthDate": [
															"31/07/2010"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"50543949T"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00001"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											},
											{
												"TravelerType": [
													"Child"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"4"
												],
												"FirstName": [
													"CHDB"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"30543949H"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"31/07/2010"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"CHDB"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"F"
														],
														"BirthDate": [
															"31/07/2010"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"30543949H"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00005"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											},
											{
												"TravelerType": [
													"Infant"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"5"
												],
												"FirstName": [
													"INFA"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"70543949M"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"02/02/2014"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"CHDB"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"MI"
														],
														"BirthDate": [
															"02/02/2014"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"70543949M"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00005"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											},
											{
												"TravelerType": [
													"Infant"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"DNI"
												],
												"TravelerID": [
													"6"
												],
												"FirstName": [
													"INFB"
												],
												"LastName": [
													"AVANZE"
												],
												"DocumentNumber": [
													"10543949J"
												],
												"Email": [
													"adrian.cejudo@avanze.net"
												],
												"Phone": [
													"918096859"
												],
												"BirthDate": [
													"02/02/2014"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"CHDB"
														],
														"LastName": [
															"AVANZE"
														],
														"Gender": [
															"FI"
														],
														"BirthDate": [
															"02/02/2014"
														],
														"DocumentExpirationDate": [
															"12/12/2020"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"10543949J"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00009"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2018"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															"NW"
														],
														"USA_State": [
															"NE"
														],
														"USA_ResidenceType": [
															"D"
														],
														"USA_Address": [
															"DIRECCION"
														],
														"USA_ZipCode": [
															"777766"
														]
													}
												]
											}
										]
									}
								],
								"IssuerNotes": [
									""
								],
								"CustomerNotes": [
									""
								],
								"AgentEmail": [
									"adrian.cejudo@avanze.net"
								],
								"DestinationContactPhone": [
									""
								],
								"PricingGroupID": [
									"1"
								],
								"ItinerariesID": [
									{
										"int": [
											"1000",
											"2000"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}