module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirAvailSearchResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirAvailSearchResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"RequestID": [
									"2930481"
								],
								"AirAvail": [
									{
										"AirTravelersInfo": [
											{
												"AirTravelerInfo": [
													{
														"TravelerType": [
															"Adult"
														],
														"IsResident": [
															"false"
														]
													},
													{
														"TravelerType": [
															"Adult"
														],
														"IsResident": [
															"true"
														]
													}
												]
											}
										],
										"AirItineraries": [
											{
												"AirItinerary": [
													{
														"ItineraryID": [
															"1000"
														],
														"DepartureDateTime": [
															"11/12/2015 08:45"
														],
														"ArrivalDateTime": [
															"11/12/2015 10:40"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 08:45"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 10:40"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1000"
																		],
																		"FlightNumber": [
																			"3942"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1001"
														],
														"DepartureDateTime": [
															"11/12/2015 07:05"
														],
														"ArrivalDateTime": [
															"11/12/2015 09:00"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 07:05"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 09:00"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1001"
																		],
																		"FlightNumber": [
																			"9059"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"332"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1002"
														],
														"DepartureDateTime": [
															"11/12/2015 08:50"
														],
														"ArrivalDateTime": [
															"11/12/2015 10:45"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 08:50"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 10:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1002"
																		],
																		"FlightNumber": [
																			"9066"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1003"
														],
														"DepartureDateTime": [
															"11/12/2015 12:00"
														],
														"ArrivalDateTime": [
															"11/12/2015 13:55"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 12:00"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 13:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1003"
																		],
																		"FlightNumber": [
																			"9118"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1004"
														],
														"DepartureDateTime": [
															"11/12/2015 15:00"
														],
														"ArrivalDateTime": [
															"11/12/2015 16:55"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 15:00"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 16:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1004"
																		],
																		"FlightNumber": [
																			"9048"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"332"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1005"
														],
														"DepartureDateTime": [
															"11/12/2015 18:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 20:20"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 18:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 20:20"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1005"
																		],
																		"FlightNumber": [
																			"9057"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1006"
														],
														"DepartureDateTime": [
															"11/12/2015 13:05"
														],
														"ArrivalDateTime": [
															"11/12/2015 15:00"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 13:05"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 15:00"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1006"
																		],
																		"FlightNumber": [
																			"3946"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1007"
														],
														"DepartureDateTime": [
															"11/12/2015 16:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 18:20"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 16:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 18:20"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1007"
																		],
																		"FlightNumber": [
																			"3944"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1008"
														],
														"DepartureDateTime": [
															"11/12/2015 19:50"
														],
														"ArrivalDateTime": [
															"11/12/2015 21:45"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 19:50"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 21:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1008"
																		],
																		"FlightNumber": [
																			"3938"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1009"
														],
														"DepartureDateTime": [
															"11/12/2015 11:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 13:30"
														],
														"ArrivalAirportLocationCode": [
															"TFS"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 11:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFS"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1009"
																		],
																		"FlightNumber": [
																			"3910"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"03:05"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1010"
														],
														"DepartureDateTime": [
															"11/12/2015 07:05"
														],
														"ArrivalDateTime": [
															"11/12/2015 09:00"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 07:05"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 09:00"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1010"
																		],
																		"FlightNumber": [
																			"9059"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"332"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1011"
														],
														"DepartureDateTime": [
															"11/12/2015 08:50"
														],
														"ArrivalDateTime": [
															"11/12/2015 10:45"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 08:50"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 10:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1011"
																		],
																		"FlightNumber": [
																			"9066"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1012"
														],
														"DepartureDateTime": [
															"11/12/2015 12:00"
														],
														"ArrivalDateTime": [
															"11/12/2015 13:55"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 12:00"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 13:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1012"
																		],
																		"FlightNumber": [
																			"9118"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1013"
														],
														"DepartureDateTime": [
															"11/12/2015 15:00"
														],
														"ArrivalDateTime": [
															"11/12/2015 16:55"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 15:00"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 16:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1013"
																		],
																		"FlightNumber": [
																			"9048"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"332"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1014"
														],
														"DepartureDateTime": [
															"11/12/2015 08:45"
														],
														"ArrivalDateTime": [
															"11/12/2015 10:40"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 08:45"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 10:40"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1014"
																		],
																		"FlightNumber": [
																			"3942"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1015"
														],
														"DepartureDateTime": [
															"11/12/2015 13:05"
														],
														"ArrivalDateTime": [
															"11/12/2015 15:00"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 13:05"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 15:00"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1015"
																		],
																		"FlightNumber": [
																			"3946"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1016"
														],
														"DepartureDateTime": [
															"11/12/2015 16:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 18:20"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 16:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 18:20"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1016"
																		],
																		"FlightNumber": [
																			"3944"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1017"
														],
														"DepartureDateTime": [
															"11/12/2015 19:50"
														],
														"ArrivalDateTime": [
															"11/12/2015 21:45"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 19:50"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 21:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1017"
																		],
																		"FlightNumber": [
																			"3938"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1018"
														],
														"DepartureDateTime": [
															"11/12/2015 11:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 13:30"
														],
														"ArrivalAirportLocationCode": [
															"TFS"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 11:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFS"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1018"
																		],
																		"FlightNumber": [
																			"3910"
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"32S"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"03:05"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1019"
														],
														"DepartureDateTime": [
															"11/12/2015 18:25"
														],
														"ArrivalDateTime": [
															"11/12/2015 20:20"
														],
														"ArrivalAirportLocationCode": [
															"TFN"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"11/12/2015 18:25"
																		],
																		"ArrivalDateTime": [
																			"11/12/2015 20:20"
																		],
																		"ArrivalAirportLocationCode": [
																			"TFN"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1019"
																		],
																		"FlightNumber": [
																			"9057"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"02:55"
														],
														"SegmentNumber": [
															"1"
														]
													}
												]
											}
										],
										"AirPricingGroups": [
											{
												"AirPricingGroup": [
													{
														"PricingGroupID": [
															"7"
														],
														"AdultTicketAmount": [
															"104.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"04/12/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"71"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE AFTER DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE04DEC15 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"101"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1010"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PLTOWMP"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"102"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1011"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PLTOWMP"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"103"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1012"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PLTOWMP"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"104"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1013"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PLTOWMP"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"1"
														],
														"IsTourOperator": [
															"true"
														]
													},
													{
														"PricingGroupID": [
															"8"
														],
														"AdultTicketAmount": [
															"117.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"02/05/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"71"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE AFTER DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE02MAY15 - SEE ADV PURCHASE"
																		]
																	},
																	{
																		"NoteCode": [
																			"SUR"
																		],
																		"Category": [
																			"79"
																		],
																		"Description": [
																			"FARE VALID FOR E TICKET ONLY"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"201"
																		],
																		"PricingGroupID": [
																			"4"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1014"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"QDTTO5NE"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"Q"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"2"
														],
														"IsTourOperator": [
															"true"
														]
													},
													{
														"PricingGroupID": [
															"9"
														],
														"AdultTicketAmount": [
															"135.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"03/12/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"71"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE AFTER DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE03DEC15 - SEE ADV PURCHASE"
																		]
																	},
																	{
																		"NoteCode": [
																			"SUR"
																		],
																		"Category": [
																			"79"
																		],
																		"Description": [
																			"FARE VALID FOR E TICKET ONLY"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"301"
																		],
																		"PricingGroupID": [
																			"5"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1015"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"NDTTO5NE"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"N"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"302"
																		],
																		"PricingGroupID": [
																			"5"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1016"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"NDTTO5NE"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"N"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"303"
																		],
																		"PricingGroupID": [
																			"5"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1017"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"NDTTO5NE"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"N"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"304"
																		],
																		"PricingGroupID": [
																			"5"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1018"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"NDTTO5NE"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"N"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"3"
														],
														"IsTourOperator": [
															"true"
														]
													},
													{
														"PricingGroupID": [
															"10"
														],
														"AdultTicketAmount": [
															"147.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"04/12/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"71"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE AFTER DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE04DEC15 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"401"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1019"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"RLTOWMP"
																										],
																										"FareType": [
																											"TourOperacion"
																										],
																										"CabinClass": [
																											"R"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"4"
														],
														"IsTourOperator": [
															"true"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"AdultTicketAmount": [
															"198.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"23/04/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"70"
																		],
																		"Description": [
																			"TICKETS ARE NON-REFUNDABLE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE23APR15 - SEE ADV PURCHASE"
																		]
																	},
																	{
																		"NoteCode": [
																			"SUR"
																		],
																		"Category": [
																			"79"
																		],
																		"Description": [
																			"FARE VALID FOR E TICKET ONLY"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"101"
																		],
																		"PricingGroupID": [
																			"1"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1000"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ZRNOWY"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"Z"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"1"
														],
														"IsTourOperator": [
															"false"
														]
													},
													{
														"PricingGroupID": [
															"2"
														],
														"AdultTicketAmount": [
															"204.00"
														],
														"ChildrenTicketAmount": [
															"0"
														],
														"InfantTicketAmount": [
															"0"
														],
														"AdultTaxAmount": [
															"20.78"
														],
														"ChildrenTaxAmount": [
															"0"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"1.50"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"DiscountAmount": [
															"0.5"
														],
														"LastTicketDate": [
															"23/04/2015"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"71"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE AFTER DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE23APR15 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"201"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"HDOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"H"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"202"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"HDOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"H"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"203"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"HDOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"H"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"204"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"HDOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"H"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"205"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"HDOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"H"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"301"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ZRSNOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"Z"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"302"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ZRSNOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"Z"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"303"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1008"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ZRSNOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"Z"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"304"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1009"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ZRSNOW"
																										],
																										"FareType": [
																											"Publicada"
																										],
																										"CabinClass": [
																											"Z"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"9"
																										],
																										"QuantityBaggageADT": [
																											"1"
																										],
																										"QuantityWeightADT": [
																											"0"
																										],
																										"QuantityTypeADT": [
																											"N"
																										],
																										"MeasureUnitADT": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"2"
														],
														"IsTourOperator": [
															"false"
														]
													}
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}