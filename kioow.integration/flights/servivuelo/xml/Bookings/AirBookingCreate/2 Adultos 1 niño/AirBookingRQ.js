module.exports = {
	"AirBookingRQ": {
		"AvailRequestID": [
			{
				"_": "225498",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"AirTravelers": [
			{
				"$": {
					"xmlns": "http://webservices.aramix.es"
				},
				"AirTraveler": [
					{
						"TravelerType": [
							"Adult"
						],
						"TravelerTitle": [
							"Mr"
						],
						"DocumentType": [
							"PAS"
						],
						"TravelerID": [
							"1"
						],
						"InfantID": [
							"0"
						],
						"FirstName": [
							"DAVID"
						],
						"LastName": [
							"JIMENEZ"
						],
						"DocumentNumber": [
							"102030"
						],
						"Email": [
							""
						],
						"Phone": [
							"913329781"
						],
						"BirthDate": [
							"31/07/1975"
						],
						"FrequentFlyerProgram": [
							"IB"
						],
						"FrequentFlyerCardNumber": [
							""
						],
						"AdditionalBaggages": [
							"0"
						],
						"IsResident": [
							"false"
						],
						"ResidentDocumentType": [
							{
								"$": {
									"p4:nil": "true",
									"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance"
								}
							}
						],
						"ResidentCityCode": [
							""
						],
						"ResidentCertificateNumber": [
							""
						],
						"TSAData": [
							{
								"BirthDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"DocumentExpirationDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								]
							}
						]
					},
					{
						"TravelerType": [
							"Adult"
						],
						"TravelerTitle": [
							"Mr"
						],
						"DocumentType": [
							"PAS"
						],
						"TravelerID": [
							"2"
						],
						"InfantID": [
							"0"
						],
						"FirstName": [
							"LUIS"
						],
						"LastName": [
							"JIMENEZ"
						],
						"DocumentNumber": [
							"102050"
						],
						"Email": [
							""
						],
						"Phone": [
							"913329781"
						],
						"BirthDate": [
							"22/05/1977"
						],
						"FrequentFlyerProgram": [
							"IB"
						],
						"FrequentFlyerCardNumber": [
							""
						],
						"AdditionalBaggages": [
							"0"
						],
						"IsResident": [
							"false"
						],
						"ResidentDocumentType": [
							{
								"$": {
									"p4:nil": "true",
									"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance"
								}
							}
						],
						"ResidentCityCode": [
							""
						],
						"ResidentCertificateNumber": [
							""
						],
						"TSAData": [
							{
								"BirthDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"DocumentExpirationDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								]
							}
						]
					},
					{
						"TravelerType": [
							"Child"
						],
						"TravelerTitle": [
							"Mr"
						],
						"DocumentType": [
							"PAS"
						],
						"TravelerID": [
							"3"
						],
						"InfantID": [
							"0"
						],
						"FirstName": [
							"CARMEN"
						],
						"LastName": [
							"JIMENEZ"
						],
						"DocumentNumber": [
							"102040"
						],
						"Email": [
							""
						],
						"Phone": [
							"913329781"
						],
						"BirthDate": [
							"22/05/2006"
						],
						"FrequentFlyerProgram": [
							"IB"
						],
						"FrequentFlyerCardNumber": [
							""
						],
						"AdditionalBaggages": [
							"0"
						],
						"IsResident": [
							"false"
						],
						"ResidentDocumentType": [
							{
								"$": {
									"p4:nil": "true",
									"xmlns:p4": "http://www.w3.org/2001/XMLSchema-instance"
								}
							}
						],
						"ResidentCityCode": [
							""
						],
						"ResidentCertificateNumber": [
							""
						],
						"TSAData": [
							{
								"BirthDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								],
								"DocumentExpirationDate": [
									{
										"$": {
											"p5:nil": "true",
											"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
										}
									}
								]
							}
						]
					}
				]
			}
		],
		"IssuerNotes": [
			{
				"_": "Notas del emisor",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"CustomerNotes": [
			{
				"_": "Notas del cliente",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"AgentEmail": [
			{
				"_": "alessandro.re@avanze.net",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"DestinationContactPhone": [
			{
				"_": "Telefono en destino",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"CancellationInsurance": [
			{
				"_": "false",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"PricingGroupID": [
			{
				"_": "3",
				"$": {
					"xmlns": "http://webservices.aramix.es"
				}
			}
		],
		"ItinerariesID": [
			{
				"$": {
					"xmlns": "http://webservices.aramix.es"
				},
				"int": [
					"1001",
					"2000"
				]
			}
		]
	}
}