module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "58cac1ff-d2ed-4dd2-b63f-2a499fc9997d",
						"$": {
							"CorrelationId": "5f213b3a-f038-4a88-a4e3-47c1222c158f",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirBookingUpdateTravelerTSADataResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingUpdateTravelerTSADataResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"RequestID": [
									"2212182"
								]
							}
						]
					}
				]
			}
		]
	}
}