module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirAvailSearch": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"availRQ": [
							{
								"DirectFlightsOnly": [
									"false"
								],
								"IncludeLowCost": [
									"false"
								],
								"ClassPref": [
									"NotSet"
								],
								"Travelers": [
									{
										"AirTravelerInfo": [
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"false"
												]
											}
										]
									}
								],
								"FlightSegments": [
									{
										"AirFlightSegmentRQ": [
											{
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"BCN"
												],
												"DepartureDate": [
													"26/08/2015"
												],
												"DepartureTime": [
													""
												]
											},
											{
												"DepartureAirportLocationCode": [
													"BCN"
												],
												"ArrivalAirportLocationCode": [
													"MAD"
												],
												"DepartureDate": [
													"29/08/2015"
												],
												"DepartureTime": [
													""
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}