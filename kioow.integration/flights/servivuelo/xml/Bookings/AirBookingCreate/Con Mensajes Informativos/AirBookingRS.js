module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingCreateResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCreateResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"86457"
										],
										"AvailRequestID": [
											"2930464"
										],
										"BookingRequestID": [
											"2930466"
										],
										"BookingReference": [
											"YOIOV9"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"PAS"
														],
														"TravelerID": [
															"1"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"PRIMERNOMBRE"
														],
														"LastName": [
															"APELLIDOS"
														],
														"DocumentNumber": [
															"29216776Z"
														],
														"Email": [
															""
														],
														"Phone": [
															""
														],
														"BirthDate": [
															"07/09/1986"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"IsResidentValidate": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															""
														],
														"ResidentCertificateNumber": [
															""
														],
														"TSAData": [
															{
																"FirstName": [
																	"PRIMERNOMBRE"
																],
																"LastName": [
																	"APELLIDOS"
																],
																"Gender": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthDate": [
																	"07/09/1986"
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"NationalityCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCity": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_State": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ResidenceType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_Address": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ZipCode": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																]
															}
														],
														"TicketAmount": [
															"26.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AgencyFeeAmount": [
															"1.5"
														],
														"AramixFeeAmount": [
															"5"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			"M"
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"PDS1RX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			"M"
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"PDS1RX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mrs"
														],
														"DocumentType": [
															"PAS"
														],
														"TravelerID": [
															"2"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"FULANA"
														],
														"LastName": [
															"TAL"
														],
														"DocumentNumber": [
															"89745874Z"
														],
														"Email": [
															""
														],
														"Phone": [
															""
														],
														"BirthDate": [
															"09/09/1986"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"IsResidentValidate": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															""
														],
														"ResidentCertificateNumber": [
															""
														],
														"TSAData": [
															{
																"FirstName": [
																	"FULANA"
																],
																"LastName": [
																	"TAL"
																],
																"Gender": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthDate": [
																	"09/09/1986"
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"DocumentNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"BirthCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"NationalityCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCity": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaNumber": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueCountry": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_State": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ResidenceType": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_Address": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																],
																"USA_ZipCode": [
																	{
																		"$": {
																			"i:nil": "true"
																		}
																	}
																]
															}
														],
														"TicketAmount": [
															"26.00"
														],
														"TaxAmount": [
															"44.51"
														],
														"AgencyFeeAmount": [
															"1.5"
														],
														"AramixFeeAmount": [
															"5"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			"M"
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"PDS1RX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			"M"
																		],
																		"CabinType": [
																			"P"
																		],
																		"BaggageQuantity": [
																			"0"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"PDS1RX4K"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"AirBookingNote": [
													{
														"NoteCode": [
															"PEN"
														],
														"Description": [
															"TICKETS ARE NON-REFUNDABLE"
														]
													},
													{
														"NoteCode": [
															"LTD"
														],
														"Description": [
															"LAST TKT DTE23APR15 - SEE ADV PURCHASE"
														]
													},
													{
														"NoteCode": [
															"APM"
														],
														"Description": [
															"PRIVATE RATES USED *F*"
														]
													}
												]
											}
										],
										"BookingDateTime": [
											"22/04/2015 12:22"
										],
										"TicketIssueDateLimit": [
											"23/04/2015"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0"
										],
										"AgentEmail": [
											"adrian.cejudo@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"0.00"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"01:15"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"BCN"
														],
														"DepartureDateTime": [
															"26/05/2015 09:30"
														],
														"ArrivalDateTime": [
															"26/05/2015 10:45"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"BCN"
																		],
																		"DepartureDateTime": [
																			"26/05/2015 09:30"
																		],
																		"ArrivalDateTime": [
																			"26/05/2015 10:45"
																		],
																		"FlightNumber": [
																			"2704"
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"321"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDS1RX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"1"
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"01:25"
														],
														"DepartureAirportLocationCode": [
															"BCN"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"28/05/2015 11:30"
														],
														"ArrivalDateTime": [
															"28/05/2015 12:55"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"BCN"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"28/05/2015 11:30"
																		],
																		"ArrivalDateTime": [
																			"28/05/2015 12:55"
																		],
																		"FlightNumber": [
																			"2737"
																		],
																		"OperatingCarrierCode": [
																			"IB"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			"1"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDS1RX4K"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"0"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"2"
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"FlightTickets": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"Warnings": [
											""
										],
										"TotalAgencyAmount": [
											"151.02"
										],
										"TotalAmount": [
											"154.02"
										],
										"Messages": [
											{
												"$": {
													"xmlns:a": "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
												},
												"a:string": [
													"Mensaje para la tarifa PDS1RX4K",
													"La tarifa de dicha reserva es de tipo Tour Operacion"
												]
											}
										]
									}
								],
								"RequestID": [
									"2930466"
								]
							}
						]
					}
				]
			}
		]
	}
}