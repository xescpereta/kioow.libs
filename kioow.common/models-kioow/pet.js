﻿module.exports = function (kioowcore, dbname) {
    var Types = kioowcore.Field.Types;
    
    var Pet = new kioowcore.List('Pets', {
        map: { name: 'code' },
        autokey: { path: 'slug', from: 'code', unique: true },
    });
    
    Pet.add({
        code: { type: String, required: true, index: true },
        name: { type: String, required: true, index: true },
        slug: { type: String, index: true },
        breed: { type: String },
        membershipDate: { type: Types.Date, index: true },
        membership: {
            acceptterms: { type: Types.Boolean },
            membershipDate: { type: Types.Date },
            registervalid: { type: Types.Boolean },
            publicprofilecomplete: { type: Types.Boolean }
        },
        images: 
        {
            photo: { type: Types.CloudinaryImage, collapse: true },
            logo: { type: Types.CloudinaryImage, collapse: true },
            splash: { type: Types.CloudinaryImage, collapse: true }
        },
        omtcomment : { type: String },
        user: { type: Types.Relationship, initial: true, ref: 'Users', index: true },
        client: { type: Types.Relationship, initial: true, ref: 'Clients', index: true },
        kioow: { type: Types.Relationship, initial: true, ref: 'Kioow', index: true }
    });
    
    
    Pet.schema.virtual('content.full').get(function () {
        return this.code || '';
    });
    
    /** 
        Relationships
        =============
    */
    Pet.relationship({ ref: 'Clients', path: 'pets' });

    DMC.addPattern('standard meta');
    DMC.defaultColumns = 'code, slug, name, phone|20%, weburl|20%, email';
    DMC.register(dbname);
}