module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "c9611f41-c468-4eb8-89b4-73ab5914b7ec",
						"$": {
							"CorrelationId": "a74e27b9-2a0c-4221-9838-421740841959",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirBookingSearchResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingSearchResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"RecordCount": [
									"3"
								],
								"AirBookings": [
									{
										"AirBookingSearchItem": [
											{
												"BookingID": [
													"2954"
												],
												"IATA": [
													"IB"
												],
												"BookingReference": [
													"6VUR74"
												],
												"BookingStatus": [
													"RES"
												],
												"BookingDateTime": [
													"16/05/2013 15:08"
												],
												"DeadLineDate": [
													"16/05/2013"
												],
												"TotalAmount": [
													"178.34"
												],
												"CancellationInsuranceAmount": [
													"7.29"
												],
												"CancellationInsurance": [
													"false"
												],
												"PaymentForm": [
													"CYC"
												],
												"FirstPassenger": [
													"ALESSANDRO RE"
												]
											},
											{
												"BookingID": [
													"2955"
												],
												"IATA": [
													"IB"
												],
												"BookingReference": [
													"6VUSNS"
												],
												"BookingStatus": [
													"RES"
												],
												"BookingDateTime": [
													"16/05/2013 15:12"
												],
												"DeadLineDate": [
													"16/05/2013"
												],
												"TotalAmount": [
													"178.34"
												],
												"CancellationInsuranceAmount": [
													"7.29"
												],
												"CancellationInsurance": [
													"false"
												],
												"PaymentForm": [
													"CYC"
												],
												"FirstPassenger": [
													"ALESSANDRO RE"
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}