module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingTicketing": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingTicketingRQ": [
							{
								"BookingID": [
									"2958"
								],
								"CardID": [
									"1111"
								],
								"InsuranceAmount": [
									"0"
								],
								"AmountDifference": [
									"50"
								]
							}
						]
					}
				]
			}
		]
	}
}