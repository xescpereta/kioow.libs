﻿function test1() {
    var _redsys = require('./redsys');
    var redsys = new _redsys.redsysAPI();
    
    redsys.setParameter({ key: 'test', value: 'memekkd'});
    console.log(redsys.getParameter('test'));
    
    var DS_MerchantParameters = {
        DS_MERCHANT_AMOUNT: '145', 
        DS_MERCHANT_ORDER: '1442772645', 
        DS_MERCHANT_MERCHANTCODE: '999008881', 
        DS_MERCHANT_CURRENCY : '978',
        DS_MERCHANT_TRANSACTIONTYPE : '0' , 
        DS_MERCHANT_TERMINAL: '871' ,  
        DS_MERCHANT_MERCHANTURL : 'https:\/\/ejemplo\/ejemplo_URL_Notif.php',
        DS_MERCHANT_URLOK: 'https:\/\/ejemplo\/ejemplo_URL_OK_KO.php',
        DS_MERCHANT_URLKO: 'https:\/\/ejemplo\/ejemplo_URL_OK_KO.php'
    };
    
    var encodedMParams = redsys.createMerchantParameters(DS_MerchantParameters);
    console.log(encodedMParams);
    
    rawMparams = redsys.decodeMerchantParameters(encodedMParams);
    console.log(rawMparams);
}

function test2() {
    var _redsys = require('./index');
    
    var redsys = new _redsys.Redsys();
    //console.log(redsys);
    var redsys2 = new _redsys.Redsys();
    //console.log(redsys2);

    redsys.on(redsys.configuration.readyevent, function () {
        var rs = redsys.setParameter('test', 'memekkd');
        rs.on(redsys.configuration.responseevent, function (res) {
            console.log('from 1: ' + res);
            var rs2 = redsys.getParameter('test');
            rs2.on(redsys.configuration.responseevent, function (res) {
                console.log('from 1: ' + res);
            });
        });
    });

    redsys2.on(redsys2.configuration.readyevent, function () {
        var rs = redsys2.setParameter('test2', '2222memekkd');
        rs.on(redsys2.configuration.responseevent, function (res) {
            console.log('from 2: ' + res);
            var rs2 = redsys2.getParameter('test2');
            rs2.on(redsys2.configuration.responseevent, function (res) {
                console.log('from 2: ' + res);
            });
        });
    });
}


test1();


