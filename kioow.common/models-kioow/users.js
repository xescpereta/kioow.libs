﻿var _ = require('underscore');
module.exports = function (kioowcore, dbname) {
    var Types = kioowcore.Field.Types;
    
    /**
     * Users
     * =====
     */

    var User = new kioowcore.List('Users', {
        // use nodelete to prevent people from deleting the demo admin user
        nodelete: true
    });
    
    User.add({
        username: { type: String, required: true, index: true },
        email: { type: Types.Email, initial: true, required: true, index: true },
        code: { type: String, index: true },
        phone: { type: String, width: 'short' },
        photo: { type: Types.CloudinaryImage, collapse: true },
        password: { type: Types.Password, initial: true, required: false },
        isKioow: { type: Types.Boolean, required: true },
        isClient: { type: Types.Boolean, required: true },
        isAdmin: { type: Types.Boolean },
        isPet: { type: Types.Boolean },
        roles: { type: Types.Relationship, ref: 'Roles', many: true },
        active: { type: Types.Boolean, index: true },
        isLocal: { type: Types.Boolean, index: true },
        isFacebookLinked : { type: Types.Boolean, index: true },
        isTwitterLinked: { type: Types.Boolean, index: true },
        isGoogleLinked: { type: Types.Boolean, index: true },
        apikey: { type: String, index: true },
        facebook         : 
        {
            id           : String,
            token        : String,
            email        : String,
            name         : String,
            link         : String
        },
        twitter:
        {
            id           : String,
            token        : String,
            displayName  : String,
            username     : String,
            link         : String
        },
        google           : 
        {
            id           : String,
            token        : String,
            email        : String,
            name         : String,
            link         : String
        },
        timeZone: {
            ObjectgmtAdjustment: String ,
            label: String,
            timeZoneId: String,
            useDaylightTime: String,
            value: String
        },
        currency: {
            label: String,
            symbol: String,
            value: String
        }
    });
    
    
    
    
    /**
     * Relationships
     */

    User.relationship({ ref: 'Kioows', path: 'user' });
    User.relationship({ ref: 'Clients', path: 'user' });
    User.relationship({ ref: 'Pets', path: 'user' });
    User.relationship({ ref: 'Admins', path: 'user' });

    
    User.schema.methods.wasActive = function () {
        this.lastActiveOn = new Date();
        return this;
    }
    
    
    var protect = function (path) {
        User.schema.path(path).set(function (value) {
            return (this.isProtected) ? this.get(path) : value;
        });
    }
    
    _.each(['username', 'email'], protect);
    
    User.schema.path('password').set(function (value) {
        return (this.isProtected) ? '$2a$10$b4vkksMQaQwKKlSQSfxRwO/9JI7Fclw6SKMv92qfaNJB9PlclaONK' : value;
    });
    
    
    
    
    User.addPattern('standard meta');
    User.defaultColumns = 'username, email';
    User.register(dbname);
}