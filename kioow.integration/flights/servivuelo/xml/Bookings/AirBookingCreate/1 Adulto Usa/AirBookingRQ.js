module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingCreate": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingRQ": [
							{
								"AvailRequestID": [
									"2212122"
								],
								"AirTravelers": [
									{
										"AirTraveler": [
											{
												"TravelerType": [
													"Adult"
												],
												"TravelerTitle": [
													"Mr"
												],
												"DocumentType": [
													"NIE"
												],
												"TravelerID": [
													"1"
												],
												"InfantID": [
													"0"
												],
												"FirstName": [
													"ALESSANDRO"
												],
												"LastName": [
													"RE"
												],
												"DocumentNumber": [
													"X3493743C"
												],
												"Email": [
													"alessandro.re@avanze.net"
												],
												"Phone": [
													"687464925"
												],
												"BirthDate": [
													"31/07/1975"
												],
												"FrequentFlyerProgram": [
													""
												],
												"FrequentFlyerCardNumber": [
													""
												],
												"AdditionalBaggages": [
													"0"
												],
												"IsResident": [
													"false"
												],
												"TSAData": [
													{
														"FirstName": [
															"ALESSANDRO"
														],
														"LastName": [
															"RE"
														],
														"Gender": [
															"M"
														],
														"BirthDate": [
															"31/07/1975"
														],
														"DocumentExpirationDate": [
															"12/12/2013"
														],
														"DocumentIssueCountry": [
															"ESP"
														],
														"DocumentType": [
															"P"
														],
														"DocumentNumber": [
															"AAAAAAAA0"
														],
														"BirthCountry": [
															"ESP"
														],
														"NationalityCountry": [
															"ESP"
														],
														"VisaIssueCity": [
															"MADRID"
														],
														"VisaNumber": [
															"VS00001"
														],
														"VisaIssueCountry": [
															"ESP"
														],
														"VisaIssueDate": [
															"12/10/2012"
														],
														"IsResidentUSA": [
															"false"
														],
														"USA_City": [
															""
														],
														"USA_State": [
															""
														],
														"USA_ResidenceType": [
															""
														],
														"USA_Address": [
															""
														],
														"USA_ZipCode": [
															""
														]
													}
												]
											}
										]
									}
								],
								"IssuerNotes": [
									""
								],
								"CustomerNotes": [
									""
								],
								"AgentEmail": [
									"alessandro.re@avanze.net"
								],
								"DestinationContactPhone": [
									""
								],
								"PricingGroupID": [
									"18"
								],
								"ItinerariesID": [
									{
										"int": [
											"1011",
											"2001"
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}