module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingCancelTicketsResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCancelTicketsResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"$": {
											"xmlns": "http://webservices.aramix.es"
										},
										"BookingID": [
											"2956"
										],
										"AvailRequestID": [
											"0"
										],
										"BookingRequestID": [
											"225480"
										],
										"BookingReference": [
											"6QMXJA"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"PAS"
														],
														"TravelerID": [
															"4135"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"PRUEBAS"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"102050"
														],
														"Email": [
															""
														],
														"Phone": [
															"913329781"
														],
														"BirthDate": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"p5:nil": "true",
																	"xmlns:p5": "http://www.w3.org/2001/XMLSchema-instance"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"PRUEBAS"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	""
																],
																"BirthDate": [
																	""
																],
																"DocumentExpirationDate": [
																	""
																],
																"DocumentIssueCountry": [
																	""
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"102050"
																],
																"BirthCountry": [
																	""
																],
																"NationalityCountry": [
																	""
																],
																"VisaIssueCity": [
																	""
																],
																"VisaNumber": [
																	""
																],
																"VisaIssueCountry": [
																	""
																],
																"VisaIssueDate": [
																	""
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	""
																],
																"USA_State": [
																	""
																],
																"USA_ResidenceType": [
																	""
																],
																"USA_Address": [
																	""
																],
																"USA_ZipCode": [
																	""
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											""
										],
										"BookingDateTime": [
											"22/03/2013 12:24"
										],
										"TicketIssueDateLimit": [
											"16/02/2013"
										],
										"MarketingCarrierCode": [
											"IB"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0.00"
										],
										"AgentEmail": [
											"alessandro.re@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"BCN"
										],
										"AmountDifference": [
											"0"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"14:20"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureDateTime": [
															"23/03/2013 07:30"
														],
														"ArrivalDateTime": [
															"23/03/2013 21:50"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"ALC"
																		],
																		"DepartureDateTime": [
																			"23/03/2013 07:30"
																		],
																		"ArrivalDateTime": [
																			"23/03/2013 08:40"
																		],
																		"FlightNumber": [
																			"3890  "
																		],
																		"OperatingCarrierCode": [
																			"I2"
																		],
																		"ArrivalAirportTerminal": [
																			"3"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AircraftType": [
																			"32S"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDSXRTNY"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"ALC"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureDateTime": [
																			"23/03/2013 21:10"
																		],
																		"ArrivalDateTime": [
																			"23/03/2013 21:50"
																		],
																		"FlightNumber": [
																			"8904  "
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			""
																		],
																		"DepartureAirportTerminal": [
																			""
																		],
																		"AircraftType": [
																			"CR9"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PDSXRTNY"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"11:45"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"25/03/2013 23:30"
														],
														"ArrivalDateTime": [
															"26/03/2013 11:15"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportLocationCode": [
																			"VLC"
																		],
																		"DepartureDateTime": [
																			"25/03/2013 23:30"
																		],
																		"ArrivalDateTime": [
																			"26/03/2013 00:15"
																		],
																		"FlightNumber": [
																			"8439  "
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			""
																		],
																		"DepartureAirportTerminal": [
																			""
																		],
																		"AircraftType": [
																			"AT7"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD2LRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"VLC"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"26/03/2013 10:20"
																		],
																		"ArrivalDateTime": [
																			"26/03/2013 11:15"
																		],
																		"FlightNumber": [
																			"8993  "
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportTerminal": [
																			""
																		],
																		"AircraftType": [
																			"CR9"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"P"
																		],
																		"FareCode": [
																			"PD2LRT"
																		],
																		"FareType": [
																			"Privada"
																		],
																		"ETicket": [
																			"false"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0.00"
																				],
																				"MeasureUnit": [
																					""
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"AirBookingFare": [
													{
														"TravelerInfo": [
															{
																"TravelerType": [
																	"Adult"
																],
																"IsResident": [
																	"false"
																]
															}
														],
														"TotalTravelers": [
															"1"
														],
														"FareAmount": [
															"134.00"
														],
														"TaxAmount": [
															"50.95"
														],
														"AramixFeeAmount": [
															"5.00"
														],
														"AgencyFeeAmount": [
															"0.00"
														]
													}
												]
											}
										],
										"FlightTickets": [
											""
										]
									}
								],
								"RequestID": [
									{
										"_": "225488",
										"$": {
											"xmlns": "http://webservices.aramix.es"
										}
									}
								]
							}
						]
					}
				]
			}
		]
	}
}