﻿
var xmls = function (requestname, callback, errorcallback) {
    
    var _ = require('underscore');
    var fs = require('fs');
    var path = require('path');
    var walk = function (dir, done) {
        var results = [];
        fs.readdir(dir, function (err, list) {
            if (err) return done(err);
            var pending = list.length;
            if (!pending) return done(null, results);
            list.forEach(function (file) {
                file = path.resolve(dir, file);
                fs.stat(file, function (err, stat) {
                    if (stat && stat.isDirectory()) {
                        walk(file, function (err, res) {
                            results = results.concat(res);
                            if (!--pending) done(null, results);
                        });
                    } else {
                        results.push(file);
                        if (!--pending) done(null, results);
                    }
                });
            });
        });
    };

    walk('./Bookings', function (err, results) {
        if (err) throw err;
        console.log(results);
        var fn = {
            _name : 'soap reference [servivuelos]'
        };
        _.each(results, function (xmlfile) { 
            if (xmlfile.indexOf('.xml') >= 0) {
                fs.readFile(xmlfile, 'utf8', function (err, xmltext) {
                    if (err) throw err;
                    var parseString = require('xml2js').parseString;
                    //xmltext = xmltext.replace("\ufeff", "");
                    
                    parseString(xmltext, function (err, result) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log(result);
                            var json = JSON.stringify(result, null, '\t');
                            
                            var fs = require('fs');
                            var filename = xmlfile.replace('.xml', '.js');
                            var name = path.basename(filename, '.js');

                            fn[name] = filename;
                            var index = JSON.stringify(fn, null, '\t');
                            var jsindex = 'module.exports = ' + index;
                            //var f = function (name, data) {
                            //    for (prop in json) {
                            //        if (typeof (json[prop]) == 'object') { 
                    
                            //        }
                            //    }
                            //}
                            var js = 'module.exports = ' + json;
                            fs.writeFile('./Bookings/' + 'soap.index.js', jsindex, function (err) {
                                if (err) throw err;
                                console.log('INDEX It\'s saved!');
                            });
                            fs.writeFile(filename, js, function (err) {
                                if (err) throw err;
                                console.log('It\'s saved!');
                            });
                        }
                    });

                });
            }
        });
    });

    walk('./Availability', function (err, results) {
        if (err) throw err;
        console.log(results);
        var fn = {
            _name : 'soap reference [servivuelos]'
        };
        _.each(results, function (xmlfile) {
            if (xmlfile.indexOf('.xml') >= 0) {
                fs.readFile(xmlfile, 'utf8', function (err, xmltext) {
                    if (err) throw err;
                    var parseString = require('xml2js').parseString;
                    //xmltext = xmltext.replace("\ufeff", "");
                    
                    parseString(xmltext, function (err, result) {
                        if (err) {
                            console.error(err);
                        } else {
                            console.log(result);
                            var json = JSON.stringify(result, null, '\t');
                            
                            var fs = require('fs');
                            var filename = xmlfile.replace('.xml', '.js');
                            var name = path.basename(filename, '.js');
                            
                            fn[name] = filename;
                            var index = JSON.stringify(fn, null, '\t');
                            var jsindex = 'module.exports = ' + index;
                            //var f = function (name, data) {
                            //    for (prop in json) {
                            //        if (typeof (json[prop]) == 'object') { 
                            
                            //        }
                            //    }
                            //}
                            var js = 'module.exports = ' + json;
                            fs.writeFile('./Availability/' + 'soap.index.js', jsindex, function (err) {
                                if (err) throw err;
                                console.log('INDEX It\'s saved!');
                            });
                            fs.writeFile(filename, js, function (err) {
                                if (err) throw err;
                                console.log('It\'s saved!');
                            });
                        }
                    });

                });
            }
        });
    });
}

xmls();
