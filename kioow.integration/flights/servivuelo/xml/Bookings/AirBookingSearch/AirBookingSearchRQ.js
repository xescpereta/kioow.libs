module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirBookingSearch": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"airBookingSearchRQ": [
							{
								"BookingReference": [
									""
								],
								"LastName": [
									"Re"
								],
								"BookingStatus": [
									"RES"
								],
								"BookingDate": [
									""
								],
								"BookingDateFrom": [
									""
								],
								"BookingDateTo": [
									""
								],
								"DeadLineDate": [
									""
								],
								"DeadLineDateFrom": [
									""
								],
								"DeadLineDateTo": [
									""
								],
								"NumberPage": [
									"0"
								],
								"NumberRecordsPage": [
									"10"
								],
								"Sort": [
									"None"
								],
								"SortOrder": [
									"None"
								]
							}
						]
					}
				]
			}
		]
	}
}