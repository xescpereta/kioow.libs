module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Header": [
			{
				"ActivityId": [
					{
						"_": "6593b34e-487e-45ca-931d-40eb888b9578",
						"$": {
							"CorrelationId": "a29683a6-3fd2-4566-8ccd-84ee7b8c69e6",
							"xmlns": "http://schemas.microsoft.com/2004/09/ServiceModel/Diagnostics"
						}
					}
				]
			}
		],
		"s:Body": [
			{
				"AirAvailCalendarSearchResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirAvailCalendarSearchResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"RequestID": [
									"2212144"
								],
								"AirAvail": [
									{
										"AirTravelersInfo": [
											{
												"AirTravelerInfo": [
													{
														"TravelerType": [
															"Adult"
														],
														"IsResident": [
															"false"
														]
													},
													{
														"TravelerType": [
															"Child"
														],
														"IsResident": [
															"false"
														]
													},
													{
														"TravelerType": [
															"Infant"
														],
														"IsResident": [
															"false"
														]
													}
												]
											}
										],
										"Warnings": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"AirItineraries": [
											{
												"AirItinerary": [
													{
														"ItineraryID": [
															"1000"
														],
														"DepartureDateTime": [
															"20/05/2013 12:05"
														],
														"ArrivalDateTime": [
															"20/05/2013 13:15"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"20/05/2013 12:05"
																		],
																		"ArrivalDateTime": [
																			"20/05/2013 13:15"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1000"
																		],
																		"FlightNumber": [
																			"6025"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"E90"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1001"
														],
														"DepartureDateTime": [
															"20/05/2013 10:55"
														],
														"ArrivalDateTime": [
															"20/05/2013 11:55"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"20/05/2013 10:55"
																		],
																		"ArrivalDateTime": [
																			"20/05/2013 11:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1001"
																		],
																		"FlightNumber": [
																			"3415"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:00"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1002"
														],
														"DepartureDateTime": [
															"22/05/2013 10:55"
														],
														"ArrivalDateTime": [
															"22/05/2013 11:55"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"22/05/2013 10:55"
																		],
																		"ArrivalDateTime": [
																			"22/05/2013 11:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1002"
																		],
																		"FlightNumber": [
																			"3415"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:00"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1003"
														],
														"DepartureDateTime": [
															"21/05/2013 17:35"
														],
														"ArrivalDateTime": [
															"21/05/2013 18:45"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"21/05/2013 17:35"
																		],
																		"ArrivalDateTime": [
																			"21/05/2013 18:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1003"
																		],
																		"FlightNumber": [
																			"3417"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1004"
														],
														"DepartureDateTime": [
															"26/05/2013 08:00"
														],
														"ArrivalDateTime": [
															"26/05/2013 09:10"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"26/05/2013 08:00"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 09:10"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1004"
																		],
																		"FlightNumber": [
																			"3415"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1005"
														],
														"DepartureDateTime": [
															"23/05/2013 10:55"
														],
														"ArrivalDateTime": [
															"23/05/2013 11:55"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"23/05/2013 10:55"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 11:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1005"
																		],
																		"FlightNumber": [
																			"3415"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:00"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1006"
														],
														"DepartureDateTime": [
															"24/05/2013 12:00"
														],
														"ArrivalDateTime": [
															"24/05/2013 13:05"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"24/05/2013 12:00"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 13:05"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1006"
																		],
																		"FlightNumber": [
																			"8960"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1007"
														],
														"DepartureDateTime": [
															"25/05/2013 12:00"
														],
														"ArrivalDateTime": [
															"25/05/2013 13:05"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"25/05/2013 12:00"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 13:05"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1007"
																		],
																		"FlightNumber": [
																			"8960"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"1008"
														],
														"DepartureDateTime": [
															"24/05/2013 10:55"
														],
														"ArrivalDateTime": [
															"24/05/2013 11:55"
														],
														"ArrivalAirportLocationCode": [
															"IBZ"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"24/05/2013 10:55"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 11:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"IBZ"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureAirportTerminal": [
																			"4"
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"1008"
																		],
																		"FlightNumber": [
																			"3415"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:00"
														],
														"SegmentNumber": [
															"1"
														]
													},
													{
														"ItineraryID": [
															"2000"
														],
														"DepartureDateTime": [
															"23/05/2013 14:00"
														],
														"ArrivalDateTime": [
															"23/05/2013 15:15"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"23/05/2013 14:00"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 15:15"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"2"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2000"
																		],
																		"FlightNumber": [
																			"6024"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"E90"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2001"
														],
														"DepartureDateTime": [
															"24/05/2013 21:35"
														],
														"ArrivalDateTime": [
															"24/05/2013 22:50"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"24/05/2013 21:35"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 22:50"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"2"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2001"
																		],
																		"FlightNumber": [
																			"6028"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2002"
														],
														"DepartureDateTime": [
															"25/05/2013 21:35"
														],
														"ArrivalDateTime": [
															"25/05/2013 22:50"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"25/05/2013 21:35"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 22:50"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"2"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2002"
																		],
																		"FlightNumber": [
																			"6028"
																		],
																		"OperatingCarrierCode": [
																			"UX"
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AircraftType": [
																			"738"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2003"
														],
														"DepartureDateTime": [
															"22/05/2013 12:25"
														],
														"ArrivalDateTime": [
															"22/05/2013 13:30"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"22/05/2013 12:25"
																		],
																		"ArrivalDateTime": [
																			"22/05/2013 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2003"
																		],
																		"FlightNumber": [
																			"3416"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2004"
														],
														"DepartureDateTime": [
															"27/05/2013 12:25"
														],
														"ArrivalDateTime": [
															"27/05/2013 13:30"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"27/05/2013 12:25"
																		],
																		"ArrivalDateTime": [
																			"27/05/2013 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2004"
																		],
																		"FlightNumber": [
																			"3416"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2005"
														],
														"DepartureDateTime": [
															"23/05/2013 12:25"
														],
														"ArrivalDateTime": [
															"23/05/2013 13:30"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"23/05/2013 12:25"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2005"
																		],
																		"FlightNumber": [
																			"3416"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2006"
														],
														"DepartureDateTime": [
															"24/05/2013 12:25"
														],
														"ArrivalDateTime": [
															"24/05/2013 13:30"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"24/05/2013 12:25"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 13:30"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2006"
																		],
																		"FlightNumber": [
																			"3416"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:05"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2007"
														],
														"DepartureDateTime": [
															"28/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"28/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"28/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"28/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2007"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2008"
														],
														"DepartureDateTime": [
															"22/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"22/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"22/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"22/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2008"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2009"
														],
														"DepartureDateTime": [
															"25/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"25/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"25/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2009"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2010"
														],
														"DepartureDateTime": [
															"23/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"23/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"23/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"23/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2010"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2011"
														],
														"DepartureDateTime": [
															"26/05/2013 09:40"
														],
														"ArrivalDateTime": [
															"26/05/2013 10:55"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"26/05/2013 09:40"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 10:55"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2011"
																		],
																		"FlightNumber": [
																			"3416"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2012"
														],
														"DepartureDateTime": [
															"25/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"25/05/2013 14:45"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"25/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"25/05/2013 14:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2012"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2013"
														],
														"DepartureDateTime": [
															"26/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"26/05/2013 14:45"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"26/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 14:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2013"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2014"
														],
														"DepartureDateTime": [
															"27/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"27/05/2013 14:45"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"27/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"27/05/2013 14:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2014"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2015"
														],
														"DepartureDateTime": [
															"28/05/2013 13:35"
														],
														"ArrivalDateTime": [
															"28/05/2013 14:45"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"28/05/2013 13:35"
																		],
																		"ArrivalDateTime": [
																			"28/05/2013 14:45"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2015"
																		],
																		"FlightNumber": [
																			"8961"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CRK"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:10"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2016"
														],
														"DepartureDateTime": [
															"24/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"24/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"24/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"24/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2016"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2017"
														],
														"DepartureDateTime": [
															"26/05/2013 19:20"
														],
														"ArrivalDateTime": [
															"26/05/2013 20:35"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"26/05/2013 19:20"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 20:35"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2017"
																		],
																		"FlightNumber": [
																			"3418"
																		],
																		"OperatingCarrierCode": [
																			"VY"
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AircraftType": [
																			"320"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"01:15"
														],
														"SegmentNumber": [
															"2"
														]
													},
													{
														"ItineraryID": [
															"2018"
														],
														"DepartureDateTime": [
															"25/05/2013 23:30"
														],
														"ArrivalDateTime": [
															"26/05/2013 15:05"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureAirportLocationCode": [
															"IBZ"
														],
														"AirItineraryLegs": [
															{
																"AirItineraryLeg": [
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"25/05/2013 23:30"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 00:15"
																		],
																		"ArrivalAirportLocationCode": [
																			"VLC"
																		],
																		"ArrivalAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureAirportLocationCode": [
																			"IBZ"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"1"
																		],
																		"ItineraryID": [
																			"2018"
																		],
																		"FlightNumber": [
																			"8439"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"AT7"
																		]
																	},
																	{
																		"TechnicalStops": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"DepartureDateTime": [
																			"26/05/2013 14:10"
																		],
																		"ArrivalDateTime": [
																			"26/05/2013 15:05"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportTerminal": [
																			"4"
																		],
																		"DepartureAirportLocationCode": [
																			"VLC"
																		],
																		"DepartureAirportTerminal": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"AirItineraryLegID": [
																			"2"
																		],
																		"ItineraryID": [
																			"2018"
																		],
																		"FlightNumber": [
																			"8981"
																		],
																		"OperatingCarrierCode": [
																			"YW"
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AircraftType": [
																			"CR9"
																		]
																	}
																]
															}
														],
														"TotalDuration": [
															"15:35"
														],
														"SegmentNumber": [
															"2"
														]
													}
												]
											}
										],
										"AirPricingGroups": [
											{
												"AirPricingGroup": [
													{
														"PricingGroupID": [
															"1"
														],
														"AdultTicketAmount": [
															"63.00"
														],
														"ChildrenTicketAmount": [
															"57.00"
														],
														"InfantTicketAmount": [
															"30.46"
														],
														"AdultTaxAmount": [
															"30.46"
														],
														"ChildrenTaxAmount": [
															"30.46"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"01/01/0001"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"70"
																		],
																		"Description": [
																			"TICKETS ARE NON-REFUNDABLE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE01MAR13 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"101"
																		],
																		"PricingGroupID": [
																			"1"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1000"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2000"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"102"
																		],
																		"PricingGroupID": [
																			"1"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1000"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"103"
																		],
																		"PricingGroupID": [
																			"1"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"UX"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1000"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PDRT"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"201"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"202"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"203"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"204"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"205"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"206"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"207"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"208"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"209"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"210"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"211"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2008"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"212"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2009"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"213"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"214"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"215"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2009"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"216"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"217"
																		],
																		"PricingGroupID": [
																			"2"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"1"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"AdultTicketAmount": [
															"75.00"
														],
														"ChildrenTicketAmount": [
															"75.00"
														],
														"InfantTicketAmount": [
															"30.46"
														],
														"AdultTaxAmount": [
															"30.46"
														],
														"ChildrenTaxAmount": [
															"30.46"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"28/02/2013"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"72"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE BEFORE DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE28FEB13 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"301"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"302"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"303"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2010"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"304"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2009"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"305"
																		],
																		"PricingGroupID": [
																			"3"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"401"
																		],
																		"PricingGroupID": [
																			"4"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1001"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2011"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"402"
																		],
																		"PricingGroupID": [
																			"4"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1002"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2011"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"403"
																		],
																		"PricingGroupID": [
																			"4"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1003"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2011"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"3"
														]
													},
													{
														"PricingGroupID": [
															"5"
														],
														"AdultTicketAmount": [
															"87.00"
														],
														"ChildrenTicketAmount": [
															"87.00"
														],
														"InfantTicketAmount": [
															"30.46"
														],
														"AdultTaxAmount": [
															"30.46"
														],
														"ChildrenTaxAmount": [
															"30.46"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"28/02/2013"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"72"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE BEFORE DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE28FEB13 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"501"
																		],
																		"PricingGroupID": [
																			"5"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1005"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2011"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"ORTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"O"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"5"
														]
													},
													{
														"PricingGroupID": [
															"7"
														],
														"AdultTicketAmount": [
															"97.00"
														],
														"ChildrenTicketAmount": [
															"97.00"
														],
														"InfantTicketAmount": [
															"30.46"
														],
														"AdultTaxAmount": [
															"30.46"
														],
														"ChildrenTaxAmount": [
															"30.46"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"28/02/2013"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"72"
																		],
																		"Description": [
																			"TICKETS ARE NON REFUNDABLE BEFORE DEPARTURE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE28FEB13 - SEE ADV PURCHASE"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"701"
																		],
																		"PricingGroupID": [
																			"7"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1008"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"WRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"W"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2016"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"801"
																		],
																		"PricingGroupID": [
																			"8"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"VY"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1004"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2017"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"WRTVY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"W"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"7"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"AdultTicketAmount": [
															"100.00"
														],
														"ChildrenTicketAmount": [
															"90.00"
														],
														"InfantTicketAmount": [
															"30.46"
														],
														"AdultTaxAmount": [
															"30.46"
														],
														"ChildrenTaxAmount": [
															"30.46"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"01/01/0001"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"70"
																		],
																		"Description": [
																			"TICKETS ARE NON-REFUNDABLE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE01MAR13 - SEE ADV PURCHASE"
																		]
																	},
																	{
																		"NoteCode": [
																			"SUR"
																		],
																		"Category": [
																			"79"
																		],
																		"Description": [
																			"FARE VALID FOR E TICKET ONLY"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"601"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2012"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"602"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2013"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"603"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2014"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"604"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1006"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2015"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"605"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2013"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"606"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2014"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"PricingGroupOptionID": [
																			"607"
																		],
																		"PricingGroupID": [
																			"6"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2015"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"6"
														]
													},
													{
														"PricingGroupID": [
															"10"
														],
														"AdultTicketAmount": [
															"2089.000000"
														],
														"ChildrenTicketAmount": [
															"2081.000000"
														],
														"InfantTicketAmount": [
															"41.77"
														],
														"AdultTaxAmount": [
															"41.77"
														],
														"ChildrenTaxAmount": [
															"41.77"
														],
														"InfantTaxAmount": [
															"0"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"5"
														],
														"DiscountAmount": [
															"0"
														],
														"LastTicketDate": [
															"01/01/0001"
														],
														"IsLowCost": [
															"false"
														],
														"GdsProposalIndex": [
															"0"
														],
														"FareNotes": [
															{
																"AirFareNote": [
																	{
																		"NoteCode": [
																			"PEN"
																		],
																		"Category": [
																			"70"
																		],
																		"Description": [
																			"TICKETS ARE NON-REFUNDABLE"
																		]
																	},
																	{
																		"NoteCode": [
																			"LTD"
																		],
																		"Category": [
																			"40"
																		],
																		"Description": [
																			"LAST TKT DTE01MAR13 - SEE ADV PURCHASE"
																		]
																	},
																	{
																		"NoteCode": [
																			"APM"
																		],
																		"Category": [
																			"F"
																		],
																		"Description": [
																			"PRIVATE RATES USED *F*"
																		]
																	},
																	{
																		"NoteCode": [
																			"SUR"
																		],
																		"Category": [
																			"79"
																		],
																		"Description": [
																			"FARE VALID FOR E TICKET ONLY"
																		]
																	}
																]
															}
														],
														"AirPricingGroupOptions": [
															{
																"AirPricingGroupOption": [
																	{
																		"PricingGroupOptionID": [
																			"901"
																		],
																		"PricingGroupID": [
																			"10"
																		],
																		"ExternalReference": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"MarketingCarrierCode": [
																			"IB"
																		],
																		"AirPricedItineraries": [
																			{
																				"AirPricedItinerary": [
																					{
																						"ItineraryID": [
																							"1007"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					},
																					{
																						"ItineraryID": [
																							"2018"
																						],
																						"AirPricedItineraryLegs": [
																							{
																								"AirPricedItineraryLeg": [
																									{
																										"AirItineraryLegID": [
																											"1"
																										],
																										"FareCode": [
																											"PD2YSABA"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									},
																									{
																										"AirItineraryLegID": [
																											"2"
																										],
																										"FareCode": [
																											"PD2ARTLY"
																										],
																										"FareType": [
																											{
																												"$": {
																													"i:nil": "true"
																												}
																											}
																										],
																										"CabinClass": [
																											"P"
																										],
																										"CabinType": [
																											"M"
																										],
																										"AvailableSeats": [
																											"0"
																										]
																									}
																								]
																							}
																						]
																					}
																				]
																			}
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														],
														"GroupID": [
															"9"
														]
													}
												]
											}
										],
										"AirCalendarCombinations": [
											{
												"AirCalendarCombination": [
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"23/05/2013"
														],
														"GoingItineraryID": [
															"1000"
														],
														"ReturnItineraryID": [
															"2000"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"24/05/2013"
														],
														"GoingItineraryID": [
															"1000"
														],
														"ReturnItineraryID": [
															"2001"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1000"
														],
														"ReturnItineraryID": [
															"2002"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"22/05/2013"
														],
														"GoingItineraryID": [
															"1001"
														],
														"ReturnItineraryID": [
															"2003"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1001"
														],
														"ReturnItineraryID": [
															"2004"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"23/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2005"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"24/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2006"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2004"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1001"
														],
														"ReturnItineraryID": [
															"2007"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"22/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2003"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"23/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2005"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"24/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2006"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2004"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"22/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2008"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2009"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2007"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"26/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1004"
														],
														"ReturnItineraryID": [
															"2004"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2009"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2007"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"1"
														],
														"GoingDate": [
															"26/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1004"
														],
														"ReturnItineraryID": [
															"2007"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"24/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2006"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2004"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"23/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2010"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2009"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2007"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"20/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1001"
														],
														"ReturnItineraryID": [
															"2011"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"22/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1002"
														],
														"ReturnItineraryID": [
															"2011"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"3"
														],
														"GoingDate": [
															"21/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1003"
														],
														"ReturnItineraryID": [
															"2011"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"5"
														],
														"GoingDate": [
															"23/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1005"
														],
														"ReturnItineraryID": [
															"2011"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"24/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1006"
														],
														"ReturnItineraryID": [
															"2012"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"24/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1006"
														],
														"ReturnItineraryID": [
															"2013"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"24/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1006"
														],
														"ReturnItineraryID": [
															"2014"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"24/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1006"
														],
														"ReturnItineraryID": [
															"2015"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"25/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1007"
														],
														"ReturnItineraryID": [
															"2013"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"25/05/2013"
														],
														"ReturnDate": [
															"27/05/2013"
														],
														"GoingItineraryID": [
															"1007"
														],
														"ReturnItineraryID": [
															"2014"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"6"
														],
														"GoingDate": [
															"25/05/2013"
														],
														"ReturnDate": [
															"28/05/2013"
														],
														"GoingItineraryID": [
															"1007"
														],
														"ReturnItineraryID": [
															"2015"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"7"
														],
														"GoingDate": [
															"24/05/2013"
														],
														"ReturnDate": [
															"24/05/2013"
														],
														"GoingItineraryID": [
															"1008"
														],
														"ReturnItineraryID": [
															"2016"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"7"
														],
														"GoingDate": [
															"26/05/2013"
														],
														"ReturnDate": [
															"26/05/2013"
														],
														"GoingItineraryID": [
															"1004"
														],
														"ReturnItineraryID": [
															"2017"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													},
													{
														"PricingGroupID": [
															"10"
														],
														"GoingDate": [
															"25/05/2013"
														],
														"ReturnDate": [
															"25/05/2013"
														],
														"GoingItineraryID": [
															"1007"
														],
														"ReturnItineraryID": [
															"2018"
														],
														"AgencyFeeAmount": [
															"0"
														],
														"AramixFeeAmount": [
															"0"
														],
														"TicketAmount": [
															"0"
														],
														"TaxAmount": [
															"0"
														],
														"TotalAmount": [
															"0"
														]
													}
												]
											}
										]
									}
								],
								"Error": [
									{
										"$": {
											"i:nil": "true"
										}
									}
								]
							}
						]
					}
				]
			}
		]
	}
}