module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirAvailMultiSegmentSearch": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"availRQ": [
							{
								"DirectFlightsOnly": [
									"false"
								],
								"IncludeLowCost": [
									"false"
								],
								"ClassPref": [
									"NotSet"
								],
								"Travelers": [
									{
										"AirTravelerInfo": [
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"false"
												]
											},
											{
												"TravelerType": [
													"Child"
												],
												"IsResident": [
													"false"
												]
											},
											{
												"TravelerType": [
													"Infant"
												],
												"IsResident": [
													"false"
												]
											}
										]
									}
								],
								"FlightSegments": [
									{
										"AirFlightSegmentRQ": [
											{
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"ROM"
												],
												"DepartureDate": [
													"25/05/2013"
												]
											},
											{
												"DepartureAirportLocationCode": [
													"ROM"
												],
												"ArrivalAirportLocationCode": [
													"BCN"
												],
												"DepartureDate": [
													"28/05/2013"
												]
											},
											{
												"DepartureAirportLocationCode": [
													"BCN"
												],
												"ArrivalAirportLocationCode": [
													"MAD"
												],
												"DepartureDate": [
													"31/05/2013"
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}