module.exports = {
	"s:Envelope": {
		"$": {
			"xmlns:s": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"s:Body": [
			{
				"AirBookingCreateResponse": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"AirBookingCreateResult": [
							{
								"$": {
									"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
								},
								"AirBooking": [
									{
										"BookingID": [
											"64932"
										],
										"AvailRequestID": [
											"2882775"
										],
										"BookingRequestID": [
											"2882776"
										],
										"BookingReference": [
											"6INYRR"
										],
										"AirTravelers": [
											{
												"AirTraveler": [
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"1"
														],
														"InfantID": [
															"5"
														],
														"FirstName": [
															"ADTA"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"50543958D"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"31/07/1988"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ADTA"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"M"
																],
																"BirthDate": [
																	"31/07/1988"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"50543958D"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00001"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"1007.00"
														],
														"TaxAmount": [
															"102.08"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Adult"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"2"
														],
														"InfantID": [
															"6"
														],
														"FirstName": [
															"ADTB"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"50543955Y"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"31/07/1988"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"ADTA"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"F"
																],
																"BirthDate": [
																	"31/07/1988"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"50543955Y"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00001"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"1007.00"
														],
														"TaxAmount": [
															"102.08"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Child"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"3"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"CHDA"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"50543949T"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"31/07/2010"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"CHDA"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"M"
																],
																"BirthDate": [
																	"31/07/2010"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"50543949T"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00001"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"806.00"
														],
														"TaxAmount": [
															"102.08"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Child"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"4"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"CHDB"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"30543949H"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"31/07/2010"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"CHDB"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"F"
																],
																"BirthDate": [
																	"31/07/2010"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"30543949H"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00005"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"806.00"
														],
														"TaxAmount": [
															"102.08"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"2"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41C"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Infant"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"5"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"INFA"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"70543949M"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"02/02/2014"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"CHDB"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"MI"
																],
																"BirthDate": [
																	"02/02/2014"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"70543949M"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00005"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"101.00"
														],
														"TaxAmount": [
															"63.46"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													},
													{
														"TravelerType": [
															"Infant"
														],
														"TravelerTitle": [
															"Mr"
														],
														"DocumentType": [
															"DNI"
														],
														"TravelerID": [
															"6"
														],
														"InfantID": [
															"0"
														],
														"FirstName": [
															"INFB"
														],
														"LastName": [
															"AVANZE"
														],
														"DocumentNumber": [
															"10543949J"
														],
														"Email": [
															"ADRIAN.CEJUDO@AVANZE.NET"
														],
														"Phone": [
															"918096859"
														],
														"BirthDate": [
															"02/02/2014"
														],
														"FrequentFlyerProgram": [
															""
														],
														"FrequentFlyerCardNumber": [
															""
														],
														"AdditionalBaggages": [
															"0"
														],
														"IsResident": [
															"false"
														],
														"ResidentDocumentType": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCityCode": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"ResidentCertificateNumber": [
															{
																"$": {
																	"i:nil": "true"
																}
															}
														],
														"TSAData": [
															{
																"FirstName": [
																	"CHDB"
																],
																"LastName": [
																	"AVANZE"
																],
																"Gender": [
																	"FI"
																],
																"BirthDate": [
																	"02/02/2014"
																],
																"DocumentExpirationDate": [
																	"12/12/2020"
																],
																"DocumentIssueCountry": [
																	"ESP"
																],
																"DocumentType": [
																	"P"
																],
																"DocumentNumber": [
																	"10543949J"
																],
																"BirthCountry": [
																	"ESP"
																],
																"NationalityCountry": [
																	"ESP"
																],
																"VisaIssueCity": [
																	"MADRID"
																],
																"VisaNumber": [
																	"VS00009"
																],
																"VisaIssueCountry": [
																	"ESP"
																],
																"VisaIssueDate": [
																	"12/10/2018"
																],
																"IsResidentUSA": [
																	"false"
																],
																"USA_City": [
																	"NW"
																],
																"USA_State": [
																	"NE"
																],
																"USA_ResidenceType": [
																	"D"
																],
																"USA_Address": [
																	"DIRECCION"
																],
																"USA_ZipCode": [
																	"777766"
																]
															}
														],
														"TicketAmount": [
															"101.00"
														],
														"TaxAmount": [
															"63.46"
														],
														"AgencyFeeAmount": [
															"3.5"
														],
														"AramixFeeAmount": [
															"9"
														],
														"OBFeeAmount": [
															"0"
														],
														"LstAirTravelerFlightSegmentLeg": [
															{
																"AirTravelerFlightSegmentLeg": [
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"1"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"2"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"3"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	},
																	{
																		"IDAirbookingFlightSegmentLeg": [
																			"4"
																		],
																		"CabinClass": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"CabinType": [
																			"K"
																		],
																		"BaggageQuantity": [
																			"1"
																		],
																		"BaggageType": [
																			"N"
																		],
																		"BaggageWeight": [
																			"0"
																		],
																		"MeasureUnit": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		],
																		"FareBasis": [
																			"KRTVAS41I"
																		],
																		"Notes": [
																			{
																				"$": {
																					"i:nil": "true"
																				}
																			}
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"AirBookingNotes": [
											{
												"AirBookingNote": [
													{
														"NoteCode": [
															"PEN"
														],
														"Description": [
															"PENALTY APPLIES"
														]
													},
													{
														"NoteCode": [
															"LTD"
														],
														"Description": [
															"LAST TKT DTE04AUG14 - SEE ADV PURCHASE"
														]
													}
												]
											}
										],
										"BookingDateTime": [
											"08/09/2014 16:20"
										],
										"TicketIssueDateLimit": [
											"04/08/2014"
										],
										"MarketingCarrierCode": [
											"S4"
										],
										"CancellationInsurance": [
											"false"
										],
										"CancellationInsuranceAmount": [
											"0"
										],
										"AgentEmail": [
											"adrian.cejudo@avanze.net"
										],
										"DestinationContactPhone": [
											""
										],
										"BookingStatus": [
											"RES"
										],
										"AmountDifference": [
											"0.00"
										],
										"AirBookingFlightSegments": [
											{
												"AirBookingFlightSegment": [
													{
														"SegmentNumber": [
															"1"
														],
														"TotalDuration": [
															"11:20"
														],
														"DepartureAirportLocationCode": [
															"MAD"
														],
														"ArrivalAirportLocationCode": [
															"BOS"
														],
														"DepartureDateTime": [
															"12/09/2014 12:10"
														],
														"ArrivalDateTime": [
															"12/09/2014 17:30"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"MAD"
																		],
																		"ArrivalAirportLocationCode": [
																			"LIS"
																		],
																		"DepartureDateTime": [
																			"12/09/2014 12:10"
																		],
																		"ArrivalDateTime": [
																			"12/09/2014 12:25"
																		],
																		"FlightNumber": [
																			"8703"
																		],
																		"OperatingCarrierCode": [
																			"TP"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"2"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"K"
																		],
																		"FareCode": [
																			"KRTVAS41"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"1"
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"LIS"
																		],
																		"ArrivalAirportLocationCode": [
																			"BOS"
																		],
																		"DepartureDateTime": [
																			"12/09/2014 15:25"
																		],
																		"ArrivalDateTime": [
																			"12/09/2014 17:30"
																		],
																		"FlightNumber": [
																			"201"
																		],
																		"OperatingCarrierCode": [
																			"S4"
																		],
																		"ArrivalAirportTerminal": [
																			"E"
																		],
																		"DepartureAirportTerminal": [
																			"1"
																		],
																		"AircraftType": [
																			"313"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"K"
																		],
																		"FareCode": [
																			"KRTVAS41"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"2"
																		]
																	}
																]
															}
														]
													},
													{
														"SegmentNumber": [
															"2"
														],
														"TotalDuration": [
															"13:55"
														],
														"DepartureAirportLocationCode": [
															"BOS"
														],
														"ArrivalAirportLocationCode": [
															"MAD"
														],
														"DepartureDateTime": [
															"12/09/2014 22:15"
														],
														"ArrivalDateTime": [
															"13/09/2014 18:10"
														],
														"FlightSegmentLegs": [
															{
																"AirBookingFlightSegmentLeg": [
																	{
																		"DepartureAirportLocationCode": [
																			"BOS"
																		],
																		"ArrivalAirportLocationCode": [
																			"LIS"
																		],
																		"DepartureDateTime": [
																			"12/09/2014 22:15"
																		],
																		"ArrivalDateTime": [
																			"13/09/2014 11:30"
																		],
																		"FlightNumber": [
																			"220"
																		],
																		"OperatingCarrierCode": [
																			"S4"
																		],
																		"ArrivalAirportTerminal": [
																			"1"
																		],
																		"DepartureAirportTerminal": [
																			"E"
																		],
																		"AircraftType": [
																			"313"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"K"
																		],
																		"FareCode": [
																			"KRTVAS41"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			{
																				"AirTechnicalStop": [
																					{
																						"AirportLocationCode": [
																							"PDL"
																						],
																						"StartDateTime": [
																							"13/09/2014 07:00"
																						],
																						"EndDateTime": [
																							"13/09/2014 08:25"
																						]
																					}
																				]
																			}
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"3"
																		]
																	},
																	{
																		"DepartureAirportLocationCode": [
																			"LIS"
																		],
																		"ArrivalAirportLocationCode": [
																			"MAD"
																		],
																		"DepartureDateTime": [
																			"13/09/2014 15:55"
																		],
																		"ArrivalDateTime": [
																			"13/09/2014 18:10"
																		],
																		"FlightNumber": [
																			"8702"
																		],
																		"OperatingCarrierCode": [
																			"TP"
																		],
																		"ArrivalAirportTerminal": [
																			"2"
																		],
																		"DepartureAirportTerminal": [
																			"1"
																		],
																		"AircraftType": [
																			"320"
																		],
																		"CabinType": [
																			"M"
																		],
																		"CabinClass": [
																			"K"
																		],
																		"FareCode": [
																			"KRTVAS41"
																		],
																		"FareType": [
																			"Publicada"
																		],
																		"ETicket": [
																			"true"
																		],
																		"AllowedBaggage": [
																			{
																				"BaggageWeight": [
																					"0"
																				],
																				"MeasureUnit": [
																					{
																						"$": {
																							"i:nil": "true"
																						}
																					}
																				],
																				"BaggageQuantity": [
																					"1"
																				],
																				"BaggageType": [
																					"N"
																				]
																			}
																		],
																		"TechnicalStops": [
																			""
																		],
																		"IDAirBookingFlightSegmentLeg": [
																			"4"
																		]
																	}
																]
															}
														]
													}
												]
											}
										],
										"TravelersInfoFares": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"FlightTickets": [
											{
												"$": {
													"i:nil": "true"
												}
											}
										],
										"TotalAgencyAmount": [
											"4417.24"
										],
										"TotalAmount": [
											"4438.24"
										],
										"Warnings": [
											""
										],
										"Messages": [
											""
										]
									}
								],
								"RequestID": [
									"2882776"
								]
							}
						]
					}
				]
			}
		]
	}
}