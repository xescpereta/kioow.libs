﻿module.exports = function (kioowcore, dbname) {
    var Types = kioowcore.Field.Types;
    
    var Chat = new kioowcore.List('Chats', {
    	map: { name: 'title' },
    	autokey: { path: 'slug', from: 'title', unique: true }
	});

	Chat.add({
	    code: { type: String, required: true, index: true  },
	    title: { type: String },
	    slug: { type: String, index: true },    
	    date: { type: Types.Date, index: true },
	    admin: { type: Types.Relationship, index: true, initial: true, ref: 'Admins' },
	    kioow: { type: Types.Relationship, index: true, initial: true, ref: 'Kioows' },	    
	    client: { type: Types.Relationship, index:true, initial: true, ref: 'Clients' },
	    status: { type: String, index: true  }
	});
	
	Chat.schema.add(
	    {
	        messages: {
	            type: [{
	                title: { type: String },
	                message: { type: String },
	                slug: { type: String, index: true   },
	                date: { type: Types.Date, index: true },
	                to : {
	                    status: { type: String },
	                    type : {type: String} ,
                        id : { type: String } ,
                        email : { type: String } ,
                        name : { type: String } ,
                        avatarimg : { type: String }
	                },
	                from : {
	                    status: { type: String },
	                    type :  {type: String} ,
                        id : { type: String } ,
                        email : { type: String } ,
                        name : { type: String } ,
                        avatarimg : { type: String }
	                }
	            }]
	        }
	    });
	
	Chat.schema.virtual('chat.detail').get(function () {
	    return this.title;
	});
	
	
	Chat.addPattern('standard meta');
    Chat.defaultColumns = 'title, slug, type, date|20%, state|20%';
    Chat.register(dbname);
}