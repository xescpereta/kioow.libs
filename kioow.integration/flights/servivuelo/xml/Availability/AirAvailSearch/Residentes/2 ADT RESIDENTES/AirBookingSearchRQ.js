module.exports = {
	"soap:Envelope": {
		"$": {
			"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
			"xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
			"xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
		},
		"soap:Body": [
			{
				"AirAvailSearch": [
					{
						"$": {
							"xmlns": "http://webservices.aramix.es"
						},
						"credentials": [
							{
								"AgencyID": [
									"101080"
								],
								"UserID": [
									"1141"
								],
								"Password": [
									"RR-34fJ"
								]
							}
						],
						"availRQ": [
							{
								"DirectFlightsOnly": [
									"true"
								],
								"IncludeLowCost": [
									"false"
								],
								"ClassPref": [
									"NotSet"
								],
								"Travelers": [
									{
										"AirTravelerInfo": [
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"true"
												]
											},
											{
												"TravelerType": [
													"Adult"
												],
												"IsResident": [
													"true"
												]
											}
										]
									}
								],
								"FlightSegments": [
									{
										"AirFlightSegmentRQ": [
											{
												"DepartureAirportLocationCode": [
													"MAD"
												],
												"ArrivalAirportLocationCode": [
													"TCI"
												],
												"DepartureDate": [
													"11/12/2015"
												],
												"DepartureTime": [
													""
												]
											}
										]
									}
								]
							}
						]
					}
				]
			}
		]
	}
}