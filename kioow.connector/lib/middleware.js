﻿module.exports.expressconnector = function (url, endopointinterface) { 

    var connection = null;

    var _expressconnector = function (req, res, next) {
        var _kwcnx = require('kioow.connector');

        connection = (connection != null) ? connection : _kwcnx.createAPIConnector({
            url: url,   //url to the endpoint (could be any url...)
            endpointinterface: endopointinterface      //endpointinterface: 'http' or 'socket'
        });
        req.kioowconnector = connection;
        req.app.set('kioowconnector', connection);
        
        next();
    }

    return _expressconnector;
}
